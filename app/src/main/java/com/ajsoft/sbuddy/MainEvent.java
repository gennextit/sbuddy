package com.ajsoft.sbuddy;

import java.util.ArrayList;

import com.ajsoft.sbuddy.fragments.MainRSSFeed;
import com.ajsoft.sbuddy.fragments.event.MainCreateEvent;
import com.ajsoft.sbuddy.fragments.event.MainCreateEvent.OnCreateEventListener;
import com.ajsoft.sbuddy.fragments.event.MainEventContactDetails;
import com.ajsoft.sbuddy.fragments.event.MainEventDetails;
import com.ajsoft.sbuddy.fragments.event.MainEventDetails.OnEventDetailsListener;
import com.ajsoft.sbuddy.fragments.profile.ProfileMyProfile;
import com.ajsoft.sbuddy.model.EventImageModel;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.L;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainEvent extends BaseActivity
		implements  OnCreateEventListener, OnEventDetailsListener {
	FragmentManager mannager;
	TextView ActionBarHeading;
	TextView tvCreateEvent, tvEventDetails, tvContactDetails;
	static String EventName, SportId, age;
	static ArrayList<EventImageModel> list;
	static String location, address, startDate, endDate, startTime, endTime, addMoreDetail;
	int counter = 1;
	LinearLayout llTab1, llTab2, llTab3;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_event);
		hideActionBar();
		mannager = getFragmentManager();

		setActionBarOpt();
		initUi();

	}

	private void initUi() {
		tvCreateEvent = (TextView) findViewById(R.id.tv_event_createEvent);
		tvEventDetails = (TextView) findViewById(R.id.tv_event_eventDetails);
		tvContactDetails = (TextView) findViewById(R.id.tv_event_contactDetails);
//		llTab1 = (LinearLayout) findViewById(R.id.ll_event_tab1);
//		llTab2 = (LinearLayout) findViewById(R.id.ll_event_tab2);
//		llTab3 = (LinearLayout) findViewById(R.id.ll_event_tab3);
//		llTab1.setOnClickListener(this);
//		llTab2.setOnClickListener(this);
//		llTab3.setOnClickListener(this);
		setScreenDynamically(1);
	}

	public void setActionBarOpt() {
		ImageView ActionBack = (ImageView) findViewById(R.id.actionbar_back);
		ImageView ActionHome = (ImageView) findViewById(R.id.actionbar_home);
		ActionBarHeading = (TextView) findViewById(R.id.actionbar_title);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (mannager.getBackStackEntryCount() > 1) {
					int count = mannager.getBackStackEntryCount();
					counter--;
					L.m(String.valueOf(count));
					mannager.popBackStack();
					setTabColor(count - 1);

				} else {
					L.m("MainEvent" + "nothing on backstack, calling super");
					finish();
				}
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// mannager.popBackStack("mainRSSFeed", 0);
				finish();
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		startActivityToSetImage();

	}


	private void startActivityToSetImage() {


		MainCreateEvent mainCreateEvent = (MainCreateEvent) getFragmentManager().findFragmentByTag("mainCreateEvent");
		if (mainCreateEvent != null) {

			if(MainCreateEvent.mTempCropImageUri!=null){
				mainCreateEvent.setCropedImageAndUri(null,MainCreateEvent.mTempCropImageUri);
			}
		}
	}

	private void setScreenDynamically(int position) {
		FragmentTransaction transaction;
		switch (position) {
		case 1:
			setTabColor(position);
			ActionBarHeading.setText(setActionBarTitle("CREATE EVENT"));
			MainCreateEvent mainCreateEvent=(MainCreateEvent) mannager.findFragmentByTag("mainCreateEvent");
			if(mainCreateEvent==null){
				mainCreateEvent = new MainCreateEvent();
				mainCreateEvent.setCommunicator(MainEvent.this);
				transaction = mannager.beginTransaction();
				transaction.add(R.id.group_event, mainCreateEvent, "mainCreateEvent");
				transaction.addToBackStack("mainCreateEvent");
				transaction.commit();
			}else{
				mainCreateEvent.setCommunicator(MainEvent.this);
			}
			

			break;
		case 2:
			setTabColor(position);
			ActionBarHeading.setText(setActionBarTitle("EVENT DETAILS"));
			MainEventDetails mainEventDetails = new MainEventDetails();
			mainEventDetails.setCommunicator(MainEvent.this);
			// mainEventDetails.setData(JsonData, coachId, sportId);
			transaction = mannager.beginTransaction();
			transaction.add(R.id.group_event, mainEventDetails, "mainEventDetails");
			transaction.addToBackStack("mainEventDetails");
			transaction.commit();

			break;
		case 3:
			setTabColor(position);
			ActionBarHeading.setText(setActionBarTitle("CONTACT DETAILS"));
			MainEventContactDetails mainEventContactDetails = new MainEventContactDetails();
			mainEventContactDetails.setCreateEvent(EventName, SportId, age, list);
			mainEventContactDetails.setEventDetails(location, address, startDate, endDate, startTime, endTime,
					addMoreDetail);
			transaction = mannager.beginTransaction();
			transaction.add(R.id.group_event, mainEventContactDetails, "mainEventContactDetails");
			transaction.addToBackStack("mainEventContactDetails");
			transaction.commit();

			break;
		}
	}

	private void setTabColor(int position) {
		switch (position) {
		case 1:
			tvCreateEvent.setTextAppearance(MainEvent.this, R.style.orangeHUGEText);
			tvEventDetails.setTextAppearance(MainEvent.this, R.style.lightHUGEText);
			tvContactDetails.setTextAppearance(MainEvent.this, R.style.lightHUGEText);
			break;
		case 2:
			tvCreateEvent.setTextAppearance(MainEvent.this, R.style.normalHUGEText);
			tvEventDetails.setTextAppearance(MainEvent.this, R.style.orangeHUGEText);
			tvContactDetails.setTextAppearance(MainEvent.this, R.style.lightHUGEText);
			break;
		case 3:
			tvCreateEvent.setTextAppearance(MainEvent.this, R.style.normalHUGEText);
			tvEventDetails.setTextAppearance(MainEvent.this, R.style.normalHUGEText);
			tvContactDetails.setTextAppearance(MainEvent.this, R.style.orangeHUGEText);
			break;
		}
	}

	@Override
	public void onBackPressed() {
		if (mannager.getBackStackEntryCount() > 1) {
			int count = mannager.getBackStackEntryCount();
			counter--;
			L.m(String.valueOf(count));
			mannager.popBackStack();
			setTabColor(count - 1);

		} else {
			L.m("MainEvent" + "nothing on backstack, calling super");
			super.onBackPressed();
		}
	}

	@Override
	public void onCreateEventListener(String EventName, String sportId, String age, ArrayList<EventImageModel> list) {
		this.EventName = EventName;
		this.SportId = sportId;
		this.age = age;
		this.list = list;
		setScreenDynamically(2);
	}

	@Override
	public void onEventDetailsListener(String location, String address, String startDate, String endDate,
			String startTime, String endTime, String addMoreDetail) {
		// TODO Auto-generated method stub
		this.location = location;
		this.address = address;
		this.startDate = startDate;
		this.endDate = endDate;
		this.startTime = startTime;
		this.endTime = endTime;
		this.addMoreDetail = addMoreDetail;
		setScreenDynamically(3);
	}

	

}
