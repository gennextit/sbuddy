package com.ajsoft.sbuddy.util;

import android.content.Context;
import android.content.Intent;

import com.ajsoft.sbuddy.model.ChatSbuddyModel;
import com.ajsoft.sbuddy.model.MessageModel;
import com.ajsoft.sbuddy.model.NotificationModel;

import java.util.ArrayList;

/**
 * Created by Abhijit on 3/5/2016.
 */
public class AppTokens {

	public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

	public static String EDIT_PROFILE_FROM="";

	public static final String COMMON = "sBuddy";
	


	public static final String APP_USER = "appuser" + COMMON;

	// cannnot place location
	public static final String lat = "28.6324065";
	public static final String lng = "77.2183613";
	// public static final String latitudeLongitude = "28.6324065,77.2183613";

	public static final String ENTRY = "entry" + COMMON;
	public static final String GooglePlaceId = "GooglePlaceId" + COMMON;
	public static final String GpsUpdateStatue = "GpsUpdateStatue" + COMMON;
	public static final String GpsLatitude = "GpsLatitude" + COMMON;
	public static final String GpsLongitude = "GpsLongitude" + COMMON;

	public static final String LoginStatus = "LoginStatus" + COMMON;

	public static final String FolderDirectory = COMMON;
	public static final String ClearCache = "ClearCache"+COMMON;

	public static final String SportList = "SportList" + COMMON;
	public static final String selectSportAndSportList = "selectSportAndSportList" + COMMON;

	// google api tokens
	public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
	public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
	public static final String TYPE_DETAIL = "/details";
	public static final String OUT_JSON = "/json";
	// ------------ make your specific key ------------
	public static final String API_KEY = "AIzaSyDrCDcmxA5hLBu3U4rVsQxUy_2wSRNAR_s";

	// google api tokens gor geoLocation
	// ?placeid=PLACE_ID&key=SERVER_KEY
	public static final String GEOLOCATION_BASE = "https://maps.googleapis.com/maps/api/place/details/json?placeid=";
	// pass PlaceID
	public static final String GEOLOCATION_KEY = "&key=" + API_KEY;

	// SMS provider identification
	// It should match with your SMS gateway origin
	// You can use MSGIND, TESTER and ALERTS as sender ID
	// If you want custom sender Id, approve MSG91 to get one
	public static final String SMS_ORIGIN = "-SBUDDY";

	// special character to prefix the otp. Make sure this character appears
	// only once in the sms
	public static final String SessionIntro = "SessionIntro" + COMMON;
	public static final String SessionSignup = "SessionSignup" + COMMON;
	public static final String SessionProfile = "SessionProfile" + COMMON;

	// public static final String SearchBuddyData = "SearchBuddyData" + COMMON;
	// public static final String SearchCoachesData = "SearchCoachesData" +
	// COMMON;
	// public static final String SearchVenueData = "SearchVenueData" + COMMON;

	public static final String PROFILE_IMAGE_DIRECTORY_NAME = "sBuddy";

	// google place api for lattitude and longit
	// https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJLbZ-NFv9DDkRzk0gTkm3wlI&key=AIzaSyDrCDcmxA5hLBu3U4rVsQxUy_2wSRNAR_s
	// Google User Register id
	public static final String GCM_ID = "gcmid" + COMMON;

	// Google project id
	public static final String SENDER_ID = "850381322511";

	public static final String DISPLAY_MESSAGE_ACTION = "com.ajsoft.sbuddy.util.DISPLAY_MESSAGE";

	public static final String EXTRA_MESSAGE = "message", EXTRA_RESULT = "result";
	public static final String EXTRA_MESSAGE_SID = "senderId";
	public static final String EXTRA_MESSAGE_SNAME = "senderName";
	public static final int CHAT = 1, MESSAGE = 2, NOTIFICATION = 3;


	public static ArrayList<MessageModel> newMessage = new ArrayList<MessageModel>();
	public static ArrayList<NotificationModel> newNotificationPush = new ArrayList<NotificationModel>();// reject aproved block notificatin list
	public static ArrayList<NotificationModel> newNotificationPull = new ArrayList<NotificationModel>();// request notificatin list
	public static ArrayList<ChatSbuddyModel> newChat = new ArrayList<ChatSbuddyModel>();
	public static String tempPlayerId = "";


	// public static String visitFromChat;

	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 *
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	public static void displayMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}

	public static void displayChatMessage(Context context,String senderId,String senderName, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_RESULT, CHAT);
		intent.putExtra(EXTRA_MESSAGE, message);
		intent.putExtra(EXTRA_MESSAGE_SID, senderId);
		intent.putExtra(EXTRA_MESSAGE_SNAME, senderName);
		context.sendBroadcast(intent);
	}

	public static void displayNotificationMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_RESULT, NOTIFICATION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}

	public static void displayMsgMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_RESULT, MESSAGE);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}

	public static String[] executeQuery(String message) {
		// chat__msg,id,senderName,msg;
		String temp[];
		String[] chatData = new String[3];
		if (message.contains(",")) {
			temp = message.split(",");
			if (temp.length >= 4 && temp[0].equalsIgnoreCase("chat__msg")) {
				if (temp.length == 4) {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[2] = temp[3];

				} else {
					chatData[0] = temp[1];
					for (int i = 3; i < temp.length; i++) {
						if (chatData[2] != null) {
							chatData[2] += ", " + temp[i];
						} else {
							chatData[2] = temp[i];
						}
					}
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
		return chatData;
	}

	public static String[] executeMsgQuery(String message) {
		// chat__msg,id,senderName,msg;
		String temp[];
		String[] chatData = new String[4];
		if (message.contains(",")) {
			temp = message.split(",");
			if (temp.length >= 4 && temp[0].equalsIgnoreCase("coach__msg")) {
				if (temp.length == 4) {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[2] = temp[3];
					chatData[3] = "coach";

				} else {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[3] = "coach";
					for (int i = 3; i < temp.length; i++) {
						if (chatData[2] != null) {
							chatData[2] += ", " + temp[i];
						} else {
							chatData[2] = temp[i];
						}
					}
				}
			} else if (temp.length >= 4 && temp[0].equalsIgnoreCase("venue__msg")) {
				if (temp.length == 4) {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[2] = temp[3];
					chatData[3] = "venue";

				} else {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[3] = "venue";
					for (int i = 3; i < temp.length; i++) {
						if (chatData[2] != null) {
							chatData[2] += ", " + temp[i];
						} else {
							chatData[2] = temp[i];
						}
					}
				} 
			}else if (temp.length >= 4 && temp[0].equalsIgnoreCase("buddy__msg")) {
				if (temp.length == 4) {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[2] = temp[3];
					chatData[3] = "buddy";

				} else {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[3] = "buddy";
					for (int i = 3; i < temp.length; i++) {
						if (chatData[2] != null) {
							chatData[2] += ", " + temp[i];
						} else {
							chatData[2] = temp[i];
						}
					}
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
		return chatData;
	}

	public static String[] executeNotiQuery(String message) {
		// chat__msg,id,senderName,msg;
		String temp[];
		String[] chatData = new String[4];
		if (message.contains(",")) {
			temp = message.split(",");
			if (temp.length >= 4 && temp[0].equalsIgnoreCase("request__msg")) {
				if (temp.length == 4) {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[2] = temp[3];
					chatData[3] = "request";

				} else {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[3] = "request";
					for (int i = 3; i < temp.length; i++) {
						if (chatData[2] != null) {
							chatData[2] += ", " + temp[i];
						} else {
							chatData[2] = temp[i];
						}
					}
				}
			} else if (temp.length >= 4 && temp[0].equalsIgnoreCase("approve__msg")) {
				if (temp.length == 4) {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[2] = temp[3];
					chatData[3] = "approve";

				} else {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[3] = "approve";
					for (int i = 3; i < temp.length; i++) {
						if (chatData[2] != null) {
							chatData[2] += ", " + temp[i];
						} else {
							chatData[2] = temp[i];
						}
					}
				}
			}else if (temp.length >= 4 && temp[0].equalsIgnoreCase("reject__msg")) {
				if (temp.length == 4) {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[2] = temp[3];
					chatData[3] = "reject";

				} else {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[3] = "reject";
					for (int i = 3; i < temp.length; i++) {
						if (chatData[2] != null) {
							chatData[2] += ", " + temp[i];
						} else {
							chatData[2] = temp[i];
						}
					}
				}
			}else if (temp.length >= 4 && temp[0].equalsIgnoreCase("block__msg")) {
				if (temp.length == 4) {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[2] = temp[3];
					chatData[3] = "block";

				} else {
					chatData[0] = temp[1];
					chatData[1] = temp[2];
					chatData[3] = "block";
					for (int i = 3; i < temp.length; i++) {
						if (chatData[2] != null) {
							chatData[2] += ", " + temp[i];
						} else {
							chatData[2] = temp[i];
						}
					}
				}
			} else {
				return null;
			}
		} else {
			return null;
		}
		return chatData;
	}

}
