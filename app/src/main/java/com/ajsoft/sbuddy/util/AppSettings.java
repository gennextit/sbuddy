package com.ajsoft.sbuddy.util;

public class AppSettings {

//	public static final String my_IP = "http://192.168.1.8/";
//	public static final String _test_IP = "http://192.168.1.20/";
//	public static final String _ServerHost = "http://kushwaha.freevar.com/";
//public static final String IP_ADDRESS = "http://www.gennextit.com/";


	public static final String WEB_ADDRESS = "http://trip.esy.es/";   // Test
//	public static final String WEB_ADDRESS = "http://www.sbuddy.co.in/"; // Production

	public static final String RSS_Feed = "http://www.thesportsmirror.com/feed/";
	public static final String RSS_Feed_ZEE = "http://zeenews.india.com/rss/sports-news.xml";
	public static final String RSS_Feed_SBUDDY = "http://sbuddy.webflow.io/post/rss.xml";
	public static final String RSS_Feed_SBUDDY1 = "https://sbuddyblog.wordpress.com/feed/";
	public static final String USER_GUIDE = WEB_ADDRESS+"sbuddy/userguide.pdf";
	public static final String USER_GUIDE_TC = "http://www.sbuddy.in/application-terms-conditions.html";
	public static final String USER_GUIDE_POLICY = "http://www.sbuddy.in/application-privacy-policy.html";
	
	// Change IP_Address with test ip or production
//	public static final String WebServiceAPI = WEB_ADDRESS + "sbuddy/";
	public static final String WebServiceAPI = WEB_ADDRESS + "sbuddy/index.php/";

	/******************************* BUDDY PANEL *****************************/
	public static final String SET_PLAYER_REGISTRATION_BY_MOBILE = WebServiceAPI
			+ "playerRegistration/setPlayerRegistrationByMobile";
	public static final String verifyOtp = WebServiceAPI + "playerRegistration/verifyOtp";
	public static final String validateOTP = WebServiceAPI + "playerRegistration/validateOTP";
	public static final String getSportList = WebServiceAPI + "sportMaster/getSportList";
	public static final String getSportListAndPlayerSelectedSport = WebServiceAPI
			+ "playerSportSelect/getSportListAndPlayerSelectedSport";
	public static final String setPlayerSport = WebServiceAPI + "playerSportSelect/setPlayerSport";
	public static final String getMyBuddyDetails = WebServiceAPI + "Connect/getMyBuddyDetails"; //get playerId

	// Buddy API's
	public static final String findSbuddy = WebServiceAPI + "Search/findSbuddy";
	public static final String updatePlayerProfileByM = WebServiceAPI + "playerRegistration/updatePlayerProfileByM";
	public static final String setPlayerRegistrationByFacebook = WebServiceAPI
			+ "playerRegistration/setPlayerRegistrationByFacebook";
	public static final String setPlayerRegistrationByGoogle = WebServiceAPI
			+ "playerRegistration/setPlayerRegistrationByGoogle";
	public static final String feedback = WebServiceAPI + "HomeMenu/feedback";
	
	// Coach API's
	// post sportId,location,radius,latitude,longitude
	public static final String findCoach = WebServiceAPI + "Search/findCoach";
	public static final String setCoachRegistrationRequest = WebServiceAPI + "RegistrationRequest/setCoachRegistrationRequest";
	// parameters-  coachName, coachGenderAge,coachGenderAge,coachEmailTel,sportCoaching,coachingFee,achievements,locations,ageGroup,weeklyOff,otherInfo
	
	public static final String getCoachProfile = WebServiceAPI + "Search/getCoachProfile";// playerId=2&coachId=10
	// setCoachMessage=WebServiceAPI+"CoachMessaging/setCoachMessage";//post
	// coachId,playerId,sportId,message
	// public static final String
	// getCoachMessage=WebServiceAPI+"CoachMessaging/getCoachMessage";//get
	// coachId,playerId
	public static final String setCoachRating = WebServiceAPI + "CoachRating/setCoachRating";
	// post coachId,playerId,sportId,rating,review
	public static final String getCoachRating = WebServiceAPI + "CoachRating/getCoachRating";
	// post ?coachId=1&playerId=1&sportId=1
	public static final String setFavouriteCoach = WebServiceAPI + "favouriteCoaches/setFavouriteCoach";
	// post coachId playerId
	public static final String getFavouriteCoach = WebServiceAPI + "HomeMenu/getFavouriteCoach";
	// get playerId
	public static final String informCoach = WebServiceAPI + "HomeMenu/informCoach";
	// post playerId,coachName, coachNumber, sportName, location and remarks

	public static final String sendP2CMessage = WebServiceAPI + "messaging/sendP2CMessage";
	// post playerId,coachId,message
	public static final String receivePlayerCoachMessages = WebServiceAPI + "messaging/receivePlayerCoachMessages";
	// get playerId,coachId

	// Venu API's
	public static final String findVenue = WebServiceAPI + "Search/findVenue";
	// post sportId,location,radius,latitude,longitude
	public static final String setVenueRegistrationRequest = WebServiceAPI + "RegistrationRequest/setVenueRegistrationRequest";
	//parameters-- $venueName,venueEmailTel,venueMemberFlag,venuePnPFlag,facilities,workingDays,timings,address,contactName,contactPhone,contactEmail,otherInfo

	
	public static final String getVenueProfile = WebServiceAPI + "Search/getVenueProfile";
	// playerId=2&venueId=10
	// public static final String
	// setVenueMessage=WebServiceAPI+"venueMessaging/setVenueMessage";//post
	// coachId,playerId,sportId,message
	// public static final String
	// getVenueMessage=WebServiceAPI+"venueMessaging/getVenueMessage";//get
	// ?venueId=1&playerId=1
	
	// post coachId,playerId,sportId,rating,review
	public static final String setVenueRating = WebServiceAPI + "venueRating/setVenueRating";
	public static final String getVenueRating = WebServiceAPI + "venueRating/getVenueRating";// get
																								// ?coachId=1&playerId=1&sportId=1
	public static final String setJoinMessage = WebServiceAPI + "Interest/setJoinMessage";// post
																							// sportId,choosePlan,
																							// chooseDate,
																							// chooseSlot,
																							// playerId,
																							// venueId
	public static final String setIamIntrested = WebServiceAPI + "Interest/setIamInterested";// post
																								// sportId,playerId,venueId
	public static final String setFavouriteVenue = WebServiceAPI + "favouriteVenues/setFavouriteVenue";// post
																										// venueId
																										// playerId
	public static final String getFavouriteVenue = WebServiceAPI + "HomeMenu/getFavouriteVenue";// get
																								// playerId
	public static final String informVenue = WebServiceAPI + "HomeMenu/informVenue";// post
																					// playerId,venueName,
																					// venueNumber,
																					// sportName,
																					// location
																					// and
																					// remarks

	public static final String sendP2VMessage = WebServiceAPI + "messaging/sendP2VMessage";// post
																							// playerId,
																							// venueId,
																							// message
	public static final String receivePlayerVenueMessages = WebServiceAPI + "messaging/receivePlayerVenueMessages";// get
																													// playerId,
																													// venueId

	// Chat API's
	public static final String findMySbuddy = WebServiceAPI + "HomeMenu/findMySbuddy";
	// post playerId
	public static final String setChatMessage = WebServiceAPI + "chatMessaging/setChatMessage";
	// post senderId, receiverId, message
	public static final String getChatMessage = WebServiceAPI + "chatMessaging/getChatMessage";
	// get  playerId

	// connect API's
	public static final String setConnectRequest = WebServiceAPI + "connect/setConnectRequest";// post
																								// senderId
																								// receiverId
	public static final String setConnectApprove = WebServiceAPI + "connect/setConnectApprove";// post
																								// senderId
																								// receiverId
	public static final String blockPlayer = WebServiceAPI + "Chat/blockPlayer";// post
																							// senderId (who send block repuest)
																							// receiverId

	public static final String unblockPlayer = WebServiceAPI + "Chat/unblockPlayer";// post
	// senderId (who send block repuest)
	// receiverId


	public static final String setConnectBlock = WebServiceAPI + "connect/setConnectBlock";

	public static final String setConnectReject = WebServiceAPI + "connect/setConnectReject";// post
																								// senderId
																								// receiverId

	public static final String getConnectRequest = WebServiceAPI + "connect/getConnectRequest";// get
																								// playerId
	public static final String getConnectApproved = WebServiceAPI + "connect/getConnectApproved";// get
																									// playerId
	public static final String getConnectBlocked = WebServiceAPI + "connect/getConnectBlocked";// get
																								// playerId
	public static final String getConnectRejected = WebServiceAPI + "connect/getConnectRejected";// get
																									// playerId

	// Create Event API's
	public static final String setEventRegistrationByMobile = WebServiceAPI
			+ "eventRegistration/setEventRegistrationByMobile";// post
																// $eventId,$eventName,$sportId,$ageGroup,$location,$address,$startDate,$endDate,$startTime,$endTime,$pFullName,$pMobile,$pEmail,$sFullName,$sMobile,$sEmail,$webUrl,$facebookUrl,$eventType,$registrationFee,$imagePath,$playerId
	// public static final String
	// setEventRegistration="http://chemistryquiz.esy.es/chemistryQuiz/index.php/Equation_list/photo";//get
	// playerId
	public static final String getMyEvents = WebServiceAPI + "HomeMenu/getMyEvents";// get playerId
	public static final String getCoachEvents = WebServiceAPI + "HomeMenu/getCoachEvents";// get coachId
	public static final String getVenueEvents = WebServiceAPI + "HomeMenu/getVenueEvents";// get venueId
																					// 
	public static final String getAllEvents = WebServiceAPI + "HomeMenu/getAllEvents";// get
																						// playerId
	// http://www.gennextit.com/sbuddy/HomeMenu/getMyEvents

	/******************************* COACH PANEL *****************************/
	public static final String coachLogin = WebServiceAPI + "coachRegistration/coachLogin";// post loginId,password,gcmId
	public static final String forgetCoachPassword = WebServiceAPI + "coachRegistration/forgetPassword";// post userId
	public static final String forgetVenuePassword = WebServiceAPI + "venueRegistration/forgetPassword";// post userId

	public static final String sendC2PMessage = WebServiceAPI + "messaging/sendC2PMessage";// post
																							// playerId,coachId,message
	public static final String receiveCoachMessages = WebServiceAPI + "messaging/receiveCoachMessages";// get
																										// coachId
	public static final String getAvailableCoins = WebServiceAPI + "CoachRegistration/getAvailableCoins";// post
																											// coachId

	/******************************* VENUE PANEL *****************************/
	public static final String venueLogin = WebServiceAPI + "VenueRegistration/venueLogin";// post
																							// loginId
																							// password
																							// gcmId
	public static final String getVenueAvailableCoins = WebServiceAPI + "VenueRegistration/getAvailableCoins";// post

	public static final String sendV2PMessage = WebServiceAPI + "messaging/sendV2PMessage";// post
																							// venueId,playerId,
																							// message
	public static final String receiveVenueMessages = WebServiceAPI + "messaging/receiveVenueMessages";// get
																										// venueId

	public static final String requestCoachCoins = WebServiceAPI + "CoachRegistration/requestCoins";//get coachId noOfCoins,
	public static final String requestVenueCoins = WebServiceAPI + "VenueRegistration/requestCoins";//get coachId noOfCoins,
	
	// Messaging API
	public static final String deleteCoachPlayerMessages = WebServiceAPI + "messaging/deleteCoachPlayerMessages";//post parameters coachId, PlayerId
	public static final String deleteCoachPlayerMessage= WebServiceAPI + "messaging/deleteCoachPlayerMessage";//post parameters coachId, PlayerId messageId 
	
	public static final String deleteVenuePlayerMessages = WebServiceAPI + "messaging/deleteVenuePlayerMessages";//post parameters venueId, PlayerId 
	public static final String deleteVenuePlayerMessage = WebServiceAPI + "messaging/deleteVenuePlayerMessage";//post parameters venueId, PlayerId, messageId 
	
	
}
