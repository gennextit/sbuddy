package com.ajsoft.sbuddy.util;

import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import com.ajsoft.sbuddy.model.CoachDashboardAdapter.DashboardTask;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

// call method 
/*
 *   NonUITaskFragment task=new NonUITaskFragment();
 *   getSupportFragmentManager().beginTransaction().add(task,"taskFragment").commit();
 *   					or find fragment
 *   task=(NonUITaskFragment)getSupportFragmentManager().findFragmentByTag("taskFragment");
 *   
 */


public class CoachVenueAdapterTask extends Fragment{
	
	public final static int GET = 1;
	public final static int PUT = 2;
	public final static int POST = 3;
	private List<BasicNameValuePair> params;
	private int method; 
	ProgressBar pBar;
	Button btn;
	Activity activity;
	DashboardTask myTask;
	
	public CoachVenueAdapterTask() {
		// TODO Auto-generated constructor stub
	}
//	public CoachVenueAdapterTask(Activity a, int method) {
//		this.activity = a;
//		this.method = method;
//	}
//
//	public CoachVenueAdapterTask(Activity a,ProgressBar pBar,Button btn, int method, List<BasicNameValuePair> params) {
//		this.activity = a;
//		this.method = method;
//		this.params = params;
//		this.pBar= pBar;
//		this.btn=btn;
//	}
  

	public void beginTask() {

		//myTask =new DashboardTask(getActivity(), pBar, btn);
		
	}
	
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity=activity;
		if(myTask!=null){
			myTask.onAttach(activity);
		}
	}
	
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		this.activity=null;
		if(myTask!=null){
			myTask.onDetach();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
	}

	
	
	 
}
