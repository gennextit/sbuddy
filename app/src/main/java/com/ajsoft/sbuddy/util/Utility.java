package com.ajsoft.sbuddy.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.util.DisplayMetrics;
import android.widget.EditText;
import android.widget.TextView;

public class Utility {

	public Utility() {
		// TODO Auto-generated constructor stub
	}
	
	public static SpannableString setBoldFont(Context context,int colorId,int typeface, String title) {
		SpannableString s = new SpannableString(title);
		Typeface externalFont=Typeface.createFromAsset(context.getAssets(), "fonts/segoeui.ttf");
		s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(context.getResources().getColor(colorId)), 0, s.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new StyleSpan(typeface), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		return s;
	}

	public static void setTypsFace(Context context,TextView et) {
		Typeface externalFont=Typeface.createFromAsset(context.getAssets(), "fonts/segoeui.ttf");
		et.setTypeface(externalFont);
	}
	

	
	public static String LoadPref(Context context,String key) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String data = sharedPreferences.getString(key, "");
		return data;
	}
	
	public static void SavePref(Context context,String key, String value) {
		
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();

	}
	
	
	
	
	public static String[] JsonSerise1(String resource){
		String[] output={null,null};
		if (resource.contains("[")) {
			try {
				JSONArray json=new JSONArray(resource);
				for(int i=0;i<json.length();i++){
					JSONObject obj=json.getJSONObject(i);
					if(!obj.optString("status").equals("")){
						output[0]=obj.optString("status");
						output[1]=obj.optString("message");
					}
				}
				
			} catch (JSONException e) {
				L.m("Json Error :"+e.toString());
				output[0]="json";
				output[1]=e.toString();
				return output;
			}
		} else {
			L.m("Invalid JSON found : " + resource);
			output[0]="json";
			output[1]=resource;
			return output;
		}
		return output;
	}
	
	private static float dpToPx(float dp,Context context) {
		Resources resources=context.getResources();
		DisplayMetrics displayMetrics=resources.getDisplayMetrics();
		float pixels= dp*(displayMetrics.densityDpi)/160.0f;
		String data = null;
		try{
			float d=Float.parseFloat(data);
		}catch(NumberFormatException e){
			L.m("Invalid input");
		}
		
		return pixels;
	}
	private static float PxToDp(float px,Context context) {
		Resources resources=context.getResources();
		DisplayMetrics displayMetrics=resources.getDisplayMetrics();
		float dp= px/(displayMetrics.densityDpi/160.0f);
		return dp;
	}
	 
}
