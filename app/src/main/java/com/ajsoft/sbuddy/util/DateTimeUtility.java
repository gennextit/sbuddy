package com.ajsoft.sbuddy.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateTimeUtility {

	public static String convertDate(int day, int month, int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.MONTH, month);
		String format = new SimpleDateFormat("d MMMM yyyy, EEEE").format(cal.getTime());

		return format;
	}

	public static String convertDateStamp(int year, int month, int day) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.MONTH, month);
		String format = new SimpleDateFormat("yyyyMMdd").format(cal.getTime());

		return format;
	}
	public static String convertDateTimeStamp(int year, int month, int day,int hour,int min) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE,min); 
		
		String format = new SimpleDateFormat("yyyyMMddHHmm").format(cal.getTime());

		return format;
	}

	// enter fix Date eg.type 1- dd-MM-yyyy ,type 2- yyyy-MM-dd
	// enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
	public static String convertFormateDate(String Date, int type, String dateFormat) {
		String Day, middle, Month, Year;
		String finalDate = Date;
		if (type == 1) {
			Day = Date.substring(0, 2);
			middle = Date.substring(2, 3);
			Month = Date.substring(3, 5);
			Year = Date.substring(6, 10);

		} else {
			Day = Date.substring(0, 4);
			middle = Date.substring(4, 5);
			Month = Date.substring(5, 7);
			Year = Date.substring(8, 10);
		}

		switch (dateFormat) {
		case "dd-MM-yyyy":
			finalDate = Day + middle + Month + middle + Year;
			break;
		case "yyyy-MM-dd":
			finalDate = Year + middle + Month + middle + Day;
			break;
		case "MM-dd-yyyy":
			finalDate = Month + middle + Day + middle + Year;
			break;
		default:
			finalDate = "Date Format Incorrest";
		}
		return finalDate;
	}

	public static String convertTime(String time) {
		// String s = "12:18:00";
		String[] res = time.split(":");
		int hr = Integer.parseInt(res[0]);
		String min = res[1];

		if (hr == 12) {
			return (12 + ":" + min + " " + ((hr >= 12) ? "PM" : "AM"));
		}

		return (hr % 12 + ":" + min + " " + ((hr >= 12) ? "PM" : "AM"));
	}
}
