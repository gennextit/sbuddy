package com.ajsoft.sbuddy.util;

import com.ajsoft.sbuddy.model.ChatSbuddyModel;
import com.ajsoft.sbuddy.model.MessageModel;
import com.ajsoft.sbuddy.model.NotificationModel;

import java.util.ArrayList;

/**
 * Created by Abhijit on 03-Aug-16.
 */
public class MyNotification {

    public static ArrayList<MessageModel> newMessage = new ArrayList<MessageModel>();
    public static ArrayList<NotificationModel> newNotification = new ArrayList<NotificationModel>();
    public static ArrayList<ChatSbuddyModel> newChat = new ArrayList<ChatSbuddyModel>();


    public static void removeChatCounter(String playerId){

        for (int j = 0; j <  AppTokens.newChat.size(); j++) {

            if (AppTokens.newChat.get(j).getSenderId().equalsIgnoreCase(playerId)) {
                AppTokens.newChat.remove(j);
            }
        }
    }


    public static void removeNotiReqCounter(String playerId){

        for (int j = 0; j <  AppTokens.newNotificationPull.size(); j++) {
            if (AppTokens.newNotificationPull.get(j).getRequestId().equalsIgnoreCase(playerId)) {
                if(AppTokens.newNotificationPull.get(j).getRequestedStatue().equals("request")){
                    AppTokens.newNotificationPull.remove(j);
                }
            }
        }
    }

    public static void removeNotiRejAprBlockCounter(String playerId){

        for (int j = 0; j <  AppTokens.newNotificationPush.size(); j++) {
            if (AppTokens.newNotificationPush.get(j).getRequestId().equalsIgnoreCase(playerId)) {
                AppTokens.newNotificationPush.remove(j);

            }
        }
    }





}
