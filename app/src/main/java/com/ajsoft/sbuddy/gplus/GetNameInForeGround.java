package com.ajsoft.sbuddy.gplus;

import java.io.IOException;

import com.ajsoft.sbuddy.SignUpGoogle;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;

public class GetNameInForeGround extends AbstractGetNameNameTask{

	public GetNameInForeGround(SignUpGoogle mActivity, String mEmail, String mScope) {
		super(mActivity, mEmail, mScope);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String fetchToken() throws IOException {
		// TODO Auto-generated method stub
		try{
			return GoogleAuthUtil.getToken(mActivity, mEmail, mScope);
		}catch(GooglePlayServicesAvailabilityException playEx){
			
		}catch(UserRecoverableAuthException ur){
			mActivity.startActivityForResult(ur.getIntent(), mRequest);
		}catch(GoogleAuthException fatalEx){
			fatalEx.printStackTrace();
		}
		
		return null;
	}

}
