package com.ajsoft.sbuddy.gps;

import android.location.Location;

public interface GPSCallback
{
   public abstract void onGPSUpdate(Location location);
}

