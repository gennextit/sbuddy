package com.ajsoft.sbuddy;

import com.ajsoft.sbuddy.coach.CoachRegistration;
import com.ajsoft.sbuddy.fragments.ImageCroper;
import com.ajsoft.sbuddy.fragments.MainRSSFeed;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Base;
import com.ajsoft.sbuddy.util.Utility;
import com.google.android.gcm.GCMRegistrar;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;

public class SplashScreen extends Base {
	final Context context = this;
	static String TAG = "splAshAcreen";
	// Splash screen timer
	private static int SPLASH_TIME_OUT = 2000;

	// ImageView iv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove the Title Bar
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash_screen);
		MainRSSFeed.oneTimeLoadSports = false;
//		AppTokens.EDIT_PROFILE_FROM = "";
		// iv=(ImageView)findViewById(R.id.imageView1);
		// iv.setColorFilter(getResources().getColor(R.color.mySport_green));

		// registerReceiver(mHandleMessageReceiver, new IntentFilter(
		// AppTokens.DISPLAY_MESSAGE_ACTION));
		//
		// L.m(getGCMRegId());
		// Utility.SavePref(context, AppTokens.GCM_ID, getGCMRegId());

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(dialog!=null){
			dialog.dismiss();
		}
		if (isOnline()) {
			appClearCache();
		} else {
			showInternetAlertBox(SplashScreen.this);
		}
	}

	/*public void CropImageTesting(){
		ImageCroper imageCroper = new ImageCroper();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.add(android.R.id.content, imageCroper, "imageCroper");
		transaction.addToBackStack("imageCroper");
		transaction.commit();
	}*/


	private void appClearCache() {
		if (Utility.LoadPref(context, AppTokens.ClearCache).equals("")) {
			ImageLoader imageloader = new ImageLoader(SplashScreen.this);
			imageloader.clearCache();
			Utility.SavePref(context, AppTokens.ClearCache, "success");
			startTimerForMainActivity();
		} else {
			startTimerForMainActivity();
		}
	}

	public String getGCMRegId() {
		String regId;
		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		GCMRegistrar.checkManifest(this);

		// Get GCM registration id
		regId = GCMRegistrar.getRegistrationId(this);
		Log.e("@-regId", regId);
		// Check if regid already presents
		if (regId.equals("")) {
			// Registration is not present, register now with GCM
			GCMRegistrar.register(this, AppTokens.SENDER_ID);
			regId = GCMRegistrar.getRegistrationId(this);
			return regId;
		}
		return regId;
	}

	private void startTimerForMainActivity() {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (!LoadPref(AppTokens.SessionIntro).equals("")) {
					if (!LoadPref(AppTokens.SessionSignup).equals("")) {
						if (LoadPref(AppTokens.APP_USER).equals("sbuddy")) {
							if (!LoadPref(AppTokens.SessionProfile).equals("")) {
								Intent i = new Intent(SplashScreen.this, MainActivity.class);
								startActivity(i);
								finish();
							} else {
								Intent i = new Intent(SplashScreen.this, ProfileActivity.class);
								i.putExtra("from", "splash");
								startActivity(i);
								finish();
							}
						} else {
							if (LoadPref(AppTokens.APP_USER).equals("coach")) {
								Intent i = new Intent(SplashScreen.this, CoachMainActivity.class);
								startActivity(i);
								finish();
							} else if (LoadPref(AppTokens.APP_USER).equals("venue")) {
								Intent i = new Intent(SplashScreen.this, VenueMainActivity.class);
								startActivity(i);
								finish();
							}
						}
					} else {
						Intent i = new Intent(SplashScreen.this, SignUp.class);
						startActivity(i);
						finish();
					}

				} else {
					Intent i = new Intent(SplashScreen.this, AppIntroActivity.class);
					startActivity(i);
					finish();
				}

			}
		}, SPLASH_TIME_OUT);
	}

	// /**
	// * Receiving push messages
	// * */
	// private final BroadcastReceiver mHandleMessageReceiver = new
	// BroadcastReceiver() {
	// @Override
	// public void onReceive(Context context, Intent intent) {
	// String newMessage =
	// intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
	// // Waking up mobile if it is sleeping
	// WakeLocker.acquire(getApplicationContext());
	//
	// /**
	// * Take appropriate action on this message
	// * depending upon your app requirement
	// * For now i am just displaying it on the screen
	// * */
	//
	// // Showing received message
	// Toast.makeText(getApplicationContext(), "New Message: " + newMessage,
	// Toast.LENGTH_LONG).show();
	//
	// // Releasing wake lock
	// WakeLocker.release();
	// }
	// };
	// @Override
	// protected void onDestroy() {
	// try {
	// unregisterReceiver(mHandleMessageReceiver);
	// GCMRegistrar.onDestroy(this);
	// } catch (Exception e) {
	// Log.e("UnRegister Receiver Error", "> " + e.getMessage());
	// }
	// super.onDestroy();
	// }
}
