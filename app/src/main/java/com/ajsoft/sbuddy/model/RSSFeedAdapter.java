package com.ajsoft.sbuddy.model;
 

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.image_cache.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RSSFeedAdapter extends ArrayAdapter<RSSFeedModel> {
	private final ArrayList<RSSFeedModel> list;
	
	private Context context;
	public ImageLoader imageLoader; 
	
	public RSSFeedAdapter(Context context, int textViewResourceId, ArrayList<RSSFeedModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());
	       
	}

//	@Override
//	public int getCount() {
//		if(list.size()>=8){
//			return 8;
//		}else{
//			return list.size();
//		}
//	}


//	@Override
//	public int getCount() {
//		return super.getCount();
//	}

	class ViewHolder{
	TextView tvBlog,tvTitle,tvCreatedBy,tvPubDate;
	ImageView ivProfile;
	
		public ViewHolder(View v) {
			tvBlog = (TextView) v.findViewById(R.id. tv_custom_rss_feed_slot_blog);
			tvTitle = (TextView) v.findViewById(R.id. tv_custom_rss_feed_slot_title);
			tvCreatedBy = (TextView) v.findViewById(R.id. tv_custom_rss_feed_slot_createdBy);
			tvPubDate = (TextView) v.findViewById(R.id. tv_custom_rss_feed_slot_blogDandT);
			ivProfile = (ImageView) v.findViewById(R.id. iv_custom_rss_feed_slot_img);
			
		}
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_rss_feed_slot, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		
		if(list.get(position).getCreator()!=null){
			holder.tvCreatedBy.setText(list.get(position).getCreator());
		}
		if(list.get(position).getTitel()!=null){
			StringBuilder res=list.get(position).getTitel();
//			res=res.substring(0,25);
//			res=res+"...";
			holder.tvTitle.setText(res);
		}
		if(list.get(position).getBlog()!=null){
			holder.tvBlog.setText(list.get(position).getBlog());
		}else{
			holder.tvBlog.setText("BLOG");
		}
		if(list.get(position).getPubDate()!=null){
			String pubDate=list.get(position).getPubDate(); 

			holder.tvPubDate.setText(pubDate); 
		}
		
		if(list.get(position).getImgSrc()!=null){
			imageLoader.DisplayImage(list.get(position).getImgSrc(), holder.ivProfile,"base_small");
		}else{
			holder.ivProfile.setImageResource(R.drawable.sport_bg_medium);
		}
		
		
		
		
		return v;
	}
//
//	public static String toTitleCase(String givenString) {//
//	    String[] arr = givenString.split(" ");//
//	    StringBuffer sb = new StringBuffer();//
////
//	    for (int i = 0; i < arr.length; i++) {//
//	        sb.append(Character.toUpperCase(arr[i].charAt(0)))//
//	            .append(arr[i].substring(1)).append(" ");//
//	    }          //
//	    return sb.toString().trim();//
//	}
	
	public String convertTime(String time){
		//String s = "12:18:00";
		
	  String  finaltime = "" ;

	     SimpleDateFormat f1 = new SimpleDateFormat("kk:mm");
	     Date d = null;
	        try {
	             d = f1.parse(time);
	             SimpleDateFormat f2 = new SimpleDateFormat("h:mm a");
	             finaltime = f2.format(d).toUpperCase(); // "12:18am"

	    } catch (ParseException e) {

	        // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	    
	    
	    return finaltime;
	}
}