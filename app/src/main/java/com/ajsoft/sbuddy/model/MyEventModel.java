package com.ajsoft.sbuddy.model;

import java.util.ArrayList;

public class MyEventModel {

	private String eventID;
	private String eventName;
	private String sport;
	private String sportImage;
	
	private String ageGroup;
	private String location;
	private String address;
	private String startDate;
	private String endDate;
	private String startTime;
	private String endTime;
	private String moreDetails;
	private String pFullName;
	private String pMobile;
	private String pEmail;
	private String sFullName;
	private String sMobile;
	private String sEmail;
	private String webUrl;
	private String facebookUrl;
	private String eventType;
	private String createdByRole;
	private String registrationFee;
	private String lastDateOfRegistration;
	private String registrationLink;
	private String eventImages;
	private String ownerId;
	private String dateCreate;
	private String timeCreate;
	private int SliderStatus;
	public final int ADVERTISMENT=1;
	public final int EVENT=2;
	private ArrayList<MyEventModel> imgList;
	
	
	public int getSliderStatus() {
		return SliderStatus;
	}

	public void setSliderStatus(int sliderStatus) {
		SliderStatus = sliderStatus;
	}

	public ArrayList<MyEventModel> getImgList() {
		return imgList;
	}

	public void setImgList(ArrayList<MyEventModel> imgList) {
		this.imgList = imgList;
	}

	public String getSportImage() {
		return sportImage;
	}

	public void setSportImage(String sportImage) {
		this.sportImage = sportImage;
	}

	public String getEventID() {
		return eventID;
	}

	public void setEventID(String eventID) {
		this.eventID = eventID;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getSport() {
		return sport;
	}

	public void setSport(String sport) {
		this.sport = sport;
	}

	public String getAgeGroup() {
		return ageGroup;
	}

	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public String getpFullName() {
		return pFullName;
	}

	public void setpFullName(String pFullName) {
		this.pFullName = pFullName;
	}

	public String getpMobile() {
		return pMobile;
	}

	public void setpMobile(String pMobile) {
		this.pMobile = pMobile;
	}

	public String getpEmail() {
		return pEmail;
	}

	public void setpEmail(String pEmail) {
		this.pEmail = pEmail;
	}

	public String getsFullName() {
		return sFullName;
	}

	public void setsFullName(String sFullName) {
		this.sFullName = sFullName;
	}

	public String getsMobile() {
		return sMobile;
	}

	public void setsMobile(String sMobile) {
		this.sMobile = sMobile;
	}

	public String getsEmail() {
		return sEmail;
	}

	public void setsEmail(String sEmail) {
		this.sEmail = sEmail;
	}

	public String getWebUrl() {
		return webUrl;
	}

	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}

	public String getFacebookUrl() {
		return facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getCreatedByRole() {
		return createdByRole;
	}

	public void setCreatedByRole(String createdByRole) {
		this.createdByRole = createdByRole;
	}

	public String getRegistrationFee() {
		return registrationFee;
	}

	public void setRegistrationFee(String registrationFee) {
		this.registrationFee = registrationFee;
	}

	public String getLastDateOfRegistration() {
		return lastDateOfRegistration;
	}

	public void setLastDateOfRegistration(String lastDateOfRegistration) {
		this.lastDateOfRegistration = lastDateOfRegistration;
	}

	public String getRegistrationLink() {
		return registrationLink;
	}

	public void setRegistrationLink(String registrationLink) {
		this.registrationLink = registrationLink;
	}

	public String getEventImages() {
		return eventImages;
	}

	public void setEventImages(String eventImages) {
		this.eventImages = eventImages;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}

	public String getTimeCreate() {
		return timeCreate;
	}

	public void setTimeCreate(String timeCreate) {
		this.timeCreate = timeCreate;
	}

}
