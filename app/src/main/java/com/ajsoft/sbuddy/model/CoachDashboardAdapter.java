package com.ajsoft.sbuddy.model;
 

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Coach;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;
import com.ajsoft.sbuddy.util.Venue;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CoachDashboardAdapter extends ArrayAdapter<CoachDashboardModel> {
	private final ArrayList<CoachDashboardModel> list;
	
	private Activity context;
	public ImageLoader imageLoader; 
	public static int STATE=0,COACH=1,VENUE=2;
//	CoachVenueAdapterTask taskFragment;
//	LinearLayout llDelete;
//	ProgressBar progressBar;
	ProgressDialog progressDialog;
	public AlertDialog dialog = null;
	
	
	public CoachDashboardAdapter(Activity context, int textViewResourceId, ArrayList<CoachDashboardModel> list,int State) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		if(State==COACH){
			STATE=COACH;
		}else{
			STATE=VENUE;
		}
		imageLoader = new ImageLoader(context.getApplicationContext());
		   
	}

	private class ViewHolder{
		TextView tvName,tvGender,tvAge,tvMsgCounter,tvLastAccess;
		ImageView ivProfile;
		LinearLayout llDelete;
		public ViewHolder(View v) {
			tvName = (TextView) v.findViewById(R.id.tv_custom_coach_dashboard_name);
			tvGender = (TextView) v.findViewById(R.id.tv_custom_coach_dashboard_gender);
			tvAge = (TextView) v.findViewById(R.id.tv_custom_coach_dashboard_age);
			tvLastAccess = (TextView) v.findViewById(R.id.tv_custom_coach_dashboard_timeLastAccess);
			ivProfile = (ImageView) v.findViewById(R.id.iv_custom_coach_dashboard_profile);
			tvMsgCounter = (TextView) v.findViewById(R.id.chat_message_counter); 
			llDelete = (LinearLayout) v.findViewById(R.id.ll_custom_coach_dashboard_delete);
			
		}
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.coach_dashboard_custom_lead_slot, parent, false);
//			progressBar = (ProgressBar) v.findViewById(R.id.progressBar1); 
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
//			llDelete = (LinearLayout) v.findViewById(R.id.ll_custom_coach_dashboard_delete);
//			progressBar = (ProgressBar) v.findViewById(R.id.progressBar1); 
			holder=(ViewHolder) v.getTag();
		}
		
		if(list.get(position).getFullName()!=null){
			holder.tvName.setText(list.get(position).getFullName()); 
		}
		if(list.get(position).getGender()!=null){
			holder.tvGender.setText(list.get(position).getGender()); 
		}
		if(list.get(position).getAge()!=null){
			holder.tvAge.setText(list.get(position).getAge()); 
		}
		if(list.get(position).getLastAccessDateTime()!=null){
			holder.tvLastAccess.setText(list.get(position).getLastAccessDateTime()); 
		}
		if(list.get(position).getCounter()!=0){
			holder.tvMsgCounter.setVisibility(View.VISIBLE);
			holder.tvMsgCounter.setText(String.valueOf(list.get(position).getCounter())); 
		}else{
			holder.tvMsgCounter.setVisibility(View.GONE);
		}
		if(list.get(position).getImageUrl()!=null){
			imageLoader.DisplayImage(list.get(position).getImageUrl(), holder.ivProfile,"profile");
			//ivProfile.setBackground(new ColorDrawable(context.getResources().getColor(R.color.mySport_green)));
		}
		
		
		holder.llDelete.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
//				taskFragment=(CoachVenueAdapterTask)context.getFragmentManager().findFragmentByTag("taskFragment");
//				if(taskFragment!=null){
//					taskFragment=new CoachVenueAdapterTask(context, progressBar, llDelete, list.get(position).getPlayerId(), position);
//					taskFragment.beginTask();
//				}else{
//					taskFragment=new CoachVenueAdapterTask(context, progressBar, llDelete, list.get(position).getPlayerId(),position);
//					context.getFragmentManager().beginTransaction().add(taskFragment,"taskFragment").commit();
//					taskFragment.beginTask();
//				}
				showBaseAlertBox(position,"Alert!!!", "Do you really want to delete this player from your leads", 2,null);
				
			}
		});
		
		
		return v;
	}

	public static String toTitleCase(String givenString) {
	    String[] arr = givenString.split(" ");
	    StringBuffer sb = new StringBuffer();

	    for (int i = 0; i < arr.length; i++) {
	        sb.append(Character.toUpperCase(arr[i].charAt(0)))
	            .append(arr[i].substring(1)).append(" ");
	    }          
	    return sb.toString().trim();
	}
	
	public void replaceItemAt(int position) { 
		 
		this.list.remove(position); 
		// Let the custom adapter know it needs to refresh the view
		this.notifyDataSetChanged();
	}
	
	
	public class DashboardTask extends AsyncTask<Void, Void, String> {

		private Activity activity; 
		String playerId;
		int position;
		public DashboardTask(Activity activity,String playerId,int position) {
			onAttach(activity); 
			this.playerId=playerId;
			this.position=position;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			//((MainActivity)activity).showProgressBar();
			showPDialog(context, "Processing... please wait");
//			if(pBar!=null && btn!=null){
//				pBar.setVisibility(View.VISIBLE);
//				btn.setVisibility(View.GONE);
//			}
//			if(pBar!=null){
//				pBar.setVisibility(View.VISIBLE);
//			}
		}

		@Override
		protected String doInBackground(Void... urls) {
			String url = "",response;
			HttpReq ob = new HttpReq();
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				if(STATE==COACH){
					url=AppSettings.deleteCoachPlayerMessages;
					params.add(new BasicNameValuePair("coachId", Utility.LoadPref(context,Coach.CoachId)));
					params.add(new BasicNameValuePair("playerId",playerId));
				}else{
					url=AppSettings.deleteVenuePlayerMessages;
					params.add(new BasicNameValuePair("venueId", Utility.LoadPref(context,Venue.VenueId)));
					params.add(new BasicNameValuePair("playerId",playerId));
				}
			}
			return response=ob.makeConnection(url, HttpReq.POST, params, HttpReq.EXECUTE_TASK);

		}

		@Override
		protected void onPostExecute(String result) {
			//((MainActivity)activity).hideProgressBar();
			//((MainActivity)activity).updateDetailInUi();
			
			if(activity!=null){
//				if(pBar!=null && btn!=null){
//					pBar.setVisibility(View.GONE);
//					btn.setVisibility(View.VISIBLE);
//				}
//				if(pBar!=null){
//					pBar.setVisibility(View.GONE);
//				} 
				dismissPDialog();
				
				if(result!=null){
					if(result.equals("success")){
						replaceItemAt(position);
					}else{
						Toast.makeText(context, result, Toast.LENGTH_LONG).show();
						L.m(result);
					}
				}else{
					Toast.makeText(context, CompactFragment.ErrorMessage, Toast.LENGTH_LONG).show();
					L.m(CompactFragment.ErrorMessage);
				}
			}
		}
	}
	

	public void showPDialog(Context context, String msg) {
		progressDialog = new ProgressDialog(context);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(msg);
		progressDialog.setIndeterminate(false);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	public void dismissPDialog() {
		if (progressDialog != null)
			progressDialog.dismiss();
	}

	
//	public class CoachVenueAdapterTask extends Fragment{
//		 
//		ProgressBar pBar;
//		LinearLayout btn;
//		Activity activity;
//		DashboardTask myTask;
//		String playerId; 
//		int position;
//		public CoachVenueAdapterTask(Activity a) {
//			this.activity = a; 
//		}
//
//		public CoachVenueAdapterTask(Activity a,ProgressBar pBar,LinearLayout btn,String playerId,int position) {
//			this.activity = a; 
//			this.pBar= pBar;
//			this.btn=btn; 
//			this.playerId=playerId;
//			this.position=position;
//		}
//	  
//
//		public void beginTask() {
//			
//			myTask =new DashboardTask(getActivity(), pBar, btn,playerId,position);
//			myTask.execute();
//			
//		}
//		
//		
//		@Override
//		public void onAttach(Activity activity) {
//			// TODO Auto-generated method stub
//			super.onAttach(activity);
//			this.activity=activity;
//			if(myTask!=null){
//				myTask.onAttach(activity);
//			}
//		}
//		
//		@Override
//		public void onDetach() {
//			// TODO Auto-generated method stub
//			super.onDetach();
//			this.activity=null;
//			if(myTask!=null){
//				myTask.onDetach();
//			}
//		}
//		
//		@Override
//		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//		
//		@Override
//		public void onActivityCreated(Bundle savedInstanceState) {
//			// TODO Auto-generated method stub
//			super.onActivityCreated(savedInstanceState);
//			setRetainInstance(true);
//		}
//
//		
//		
//		 
//	}
	
	 

	public Button showBaseAlertBox(final int position,String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = context.getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("OK");
		button1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				dialog.dismiss();
				DashboardTask task=new DashboardTask(context, list.get(position).getPlayerId(), position);
				task.execute();
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();
		return button1;
	}

}