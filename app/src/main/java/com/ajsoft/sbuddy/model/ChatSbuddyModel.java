package com.ajsoft.sbuddy.model;

import android.graphics.Bitmap;

public class ChatSbuddyModel {
	
	private String senderId;
	private String receiverId;
	private String message;
	private String dateOfMsg;
	private String timeOfMsg;
	private String status;
	private String name;
	private String Image ;
	
	
	
	public String getImage() {
		return Image;
	}
	public void setImage(String image) {
		Image = image;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSenderId() {
		return senderId;
	}
	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}
	public String getReceiverId() {
		return receiverId;
	}
	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDateOfMsg() {
		return dateOfMsg;
	}
	public void setDateOfMsg(String dateOfMsg) {
		this.dateOfMsg = dateOfMsg;
	}
	public String getTimeOfMsg() {
		return timeOfMsg;
	}
	public void setTimeOfMsg(String timeOfMsg) {
		this.timeOfMsg = timeOfMsg;
	}
	

	
	
}
