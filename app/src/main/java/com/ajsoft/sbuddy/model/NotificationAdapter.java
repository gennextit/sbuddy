package com.ajsoft.sbuddy.model;
 

import java.util.ArrayList;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.image_cache.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NotificationAdapter extends ArrayAdapter<NotificationModel> {
	private final ArrayList<NotificationModel> list;
	
	private Context context;
	public ImageLoader imageLoader; 
	
	public NotificationAdapter(Context context, int textViewResourceId, ArrayList<NotificationModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());
	       
	}

class ViewHolder{
	TextView tvNotification; 
	
		public ViewHolder(View v) {
			tvNotification = (TextView) v.findViewById(R.id. lbl_name);
			
		}
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_notification_slot, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		 
		
		if(list.get(position).getNotification()!=null&&list.get(position).getRequestedName()!=null){
			holder.tvNotification.setText(list.get(position).getNotification()); 
		}
		 
		
		return v;
	}

	 
	
	
}