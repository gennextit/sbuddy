package com.ajsoft.sbuddy.model;

import java.util.Comparator;

import android.graphics.Bitmap;

public class FindVenueModel {

	private String favouriteVenue;
	private String venueId;
	private String venueName;
	private String imageUrl;
	private String about;
	private String highlightOrDetails;
	private String openHours;
	private String membershipDetails;
	private String locality;
	private String localityAvailable;
	private Float km;
	private String review;
	private String rating;

	private String sportId;
	private String sportName;
	private String logo;
	private String memFee;
	private String pnpFee;
	private String latitude;
	private String longitude;
	private String address;
	private int sltStatus;
	private Bitmap imgBitmap;

	public Bitmap getImgBitmap() {
		return imgBitmap;
	}

	public void setImgBitmap(Bitmap imgBitmap) {
		this.imgBitmap = imgBitmap;
	}

	public int getSltStatus() {
		return sltStatus;
	}

	public void setSltStatus(int sltStatus) {
		this.sltStatus = sltStatus;
	}

	public static Comparator<FindVenueModel> COMPARE_BY_KM = new Comparator<FindVenueModel>() {
		public int compare(FindVenueModel one, FindVenueModel other) {
			return one.km.compareTo(other.km);
		}
	};

	public String getFavouriteVenue() {
		return favouriteVenue;
	}

	public void setFavouriteVenue(String favouriteVenue) {
		this.favouriteVenue = favouriteVenue;
	}

	public String getSportId() {
		return sportId;
	}

	public void setSportId(String sportId) {
		this.sportId = sportId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getSportName() {
		return sportName;
	}

	public void setSportName(String sportName) {
		this.sportName = sportName;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getmemFee() {
		return memFee;
	}

	public void setmemFee(String memFee) {
		this.memFee = memFee;
	}

	public String getPnpFee() {
		return pnpFee;
	}

	public void setPnpFee(String pnpFee) {
		this.pnpFee = pnpFee;
	}

	public Float getKm() {
		return km;
	}

	public void setKm(Float km) {
		this.km = km;
	}

	public String getVenueId() {
		return venueId;
	}

	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getHighlightOrDetails() {
		return highlightOrDetails;
	}

	public void setHighlightOrDetails(String highlightOrDetails) {
		this.highlightOrDetails = highlightOrDetails;
	}

	public String getOpenHours() {
		return openHours;
	}

	public void setOpenHours(String openHours) {
		this.openHours = openHours;
	}

	public String getMembershipDetails() {
		return membershipDetails;
	}

	public void setMembershipDetails(String membershipDetails) {
		this.membershipDetails = membershipDetails;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getLocalityAvailable() {
		return localityAvailable;
	}

	public void setLocalityAvailable(String localityAvailable) {
		this.localityAvailable = localityAvailable;
	}

}
