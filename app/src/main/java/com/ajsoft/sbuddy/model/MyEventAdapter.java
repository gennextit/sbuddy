package com.ajsoft.sbuddy.model;
 

import java.util.ArrayList;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.image_cache.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyEventAdapter extends ArrayAdapter<MyEventModel> {
	private final ArrayList<MyEventModel> list;
	
	private Context context;
	public ImageLoader imageLoader; 
	
	public MyEventAdapter(Context context, int textViewResourceId, ArrayList<MyEventModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());
	       
	}

class ViewHolder{
	TextView tvEventName,tvLocation,tvDate; 
	ImageView ivEventImage,ivSportImage;

		public ViewHolder(View v) {
			tvEventName = (TextView) v.findViewById(R.id.tv_custom_slot_my_events_name);
			tvLocation = (TextView) v.findViewById(R.id.tv_custom_slot_my_events_location);
			tvDate = (TextView) v.findViewById(R.id.tv_custom_slot_my_events_created_date);
			ivEventImage = (ImageView) v.findViewById(R.id.iv_custom_slot_my_events_base);
			ivSportImage = (ImageView) v.findViewById(R.id.iv_custom_slot_my_events_sltSport);
		}
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_slot_my_events, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		
		
		
		if(list.get(position).getSportImage()!=null){
			imageLoader.DisplayImage(list.get(position).getSportImage(), holder.ivSportImage,"blank");
		} 
		if(list.get(position).getEventImages()!=null){
			imageLoader.DisplayImage(list.get(position).getEventImages(), holder.ivEventImage,"base_medium",false);
		} 
		
		if(list.get(position).getEventName()!=null){
			holder.tvEventName.setText(list.get(position).getEventName()); 
		} 
		
		if(list.get(position).getLocation()!=null){
			holder.tvLocation.setText(list.get(position).getLocation()); 
		} 
		if(list.get(position).getStartDate()!=null){
			holder.tvDate.setText(list.get(position).getStartDate()+", "+list.get(position).getStartTime()+", Onwards"); 
		} 
		 
		
		
		return v;
	}

	 
	
	
}