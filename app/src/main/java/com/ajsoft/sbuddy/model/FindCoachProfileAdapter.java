package com.ajsoft.sbuddy.model;
 

import java.util.ArrayList;

import com.ajsoft.sbuddy.R;import com.ajsoft.sbuddy.image_cache.ImageLoader;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FindCoachProfileAdapter extends ArrayAdapter<FindCoachModel> {
	private final ArrayList<FindCoachModel> list;
	
	private Context context;
	public ImageLoader imageLoader; 
	
	 
	
	
	public FindCoachProfileAdapter(Context context, int textViewResourceId, ArrayList<FindCoachModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());
		   
	}

	class ViewHolder{
		TextView tvSportCat;
		ImageView ivProfile;
		
		public ViewHolder(View v) {
			tvSportCat = (TextView) v.findViewById(R.id.tv_custom_find_coaches_gridview);
			ivProfile = (ImageView) v.findViewById(R.id.iv_custom_find_coaches_gridview);
			
		}
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_find_coaches_gridview, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		
		if(list.get(position).getSportName()!=null){
			holder.tvSportCat.setText(toTitleCase(list.get(position).getSportName())); 
		}
		if(list.get(position).getLogo()!=null){
			imageLoader.DisplayImage(list.get(position).getLogo(), holder.ivProfile,"sports");
			holder.ivProfile.setBackground(new ColorDrawable(context.getResources().getColor(R.color.mySport_green)));
		}
		
//		ivProfile.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//				// TODO Auto-generated method stub
//				dtInterface.findCoachProfileAdapterInterface(list.get(position).getFee(), list.get(position).getClosedDay());
//			}
//		});
		
		
		return v;
	}

	public static String toTitleCase(String givenString) {
	    String[] arr = givenString.split(" ");
	    StringBuffer sb = new StringBuffer();

	    for (int i = 0; i < arr.length; i++) {
	        sb.append(Character.toUpperCase(arr[i].charAt(0)))
	            .append(arr[i].substring(1)).append(" ");
	    }          
	    return sb.toString().trim();
	}
	
	
}