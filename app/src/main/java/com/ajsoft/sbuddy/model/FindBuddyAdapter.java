package com.ajsoft.sbuddy.model;

import java.util.ArrayList;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.util.AnimationUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class FindBuddyAdapter extends ArrayAdapter<FindBuddyModel> {
	private final ArrayList<FindBuddyModel> list;
	// LinearLayout lvMain;
	// ImageView online;

	private Context context;
	public ImageLoader imageLoader;
	int previousPosition = -5;

	public FindBuddyAdapter(Context context, int textViewResourceId, ArrayList<FindBuddyModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());

	}

	public class ViewHolder {
		TextView tvName, tvDistance, tvTotalCat, tvLocation;
		ImageView ivProfile, ivCat1, ivCat2, ivCat3, ivDistance;
		public View itemView;

		public ViewHolder(View v) {
			this.itemView = v;
			tvName = (TextView) v.findViewById(R.id.tv_custom_slot_find_sbuddyz_name);
			// tvAge = (TextView) v.findViewById(R.id.
			// tv_custom_slot_find_sbuddyz_age);
			tvDistance = (TextView) v.findViewById(R.id.tv_custom_slot_find_sbuddyz_distance);
			ivDistance = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_distance);
			tvTotalCat = (TextView) v.findViewById(R.id.tv_custom_slot_find_sbuddyz_totalSports);
			tvLocation = (TextView) v.findViewById(R.id.tv_custom_slot_find_sbuddyz_city);
			ivProfile = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_profile);
			ivCat1 = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_sCat1);
			ivCat2 = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_sCat2);
			ivCat3 = (ImageView) v.findViewById(R.id.iv_custom_slot_find_sbuddyz_sCat3);

		}
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder = null;
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_slot_find_sbuddyz, parent, false);
			holder = new ViewHolder(v);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (list.get(position).getFullName() != null) {
			// tvName.setText(toTitleCase(list.get(position).getFullName()));
			if (list.get(position).getAge() != null) {
				if (list.get(position).getGender() != null) {
					String gender = list.get(position).getGender().substring(0, 1);
					holder.tvName.setText(list.get(position).getFullName() + " | " + list.get(position).getAge() + " | "
							+ toTitleCase(gender));
				} else {
					holder.tvName.setText(list.get(position).getFullName());
				}
			}
		}

		holder.tvDistance.setText(String.valueOf(list.get(position).getKm()) + " km");

		if (list.get(position).getLocation() != null) {
			holder.tvLocation.setText(list.get(position).getLocation());
		}
		int count = Integer.parseInt(list.get(position).getsLogoCount());
		holder.tvTotalCat.setVisibility(View.INVISIBLE);
		if (count > 3) {
			holder.tvTotalCat.setText(" + " + String.valueOf(count - 3) + " more");
			holder.tvTotalCat.setVisibility(View.VISIBLE);
		}
		if (list.get(position).getImageUrl() != null) {
			imageLoader.DisplayImage(list.get(position).getImageUrl(), holder.ivProfile, "profile",false);
		}
		if (list.get(position).getImageUrl() != null) {
			imageLoader.DisplayImage(list.get(position).getsLogo1(), holder.ivCat1, "blank");
			holder.ivCat1.setColorFilter(context.getResources().getColor(R.color.mySport_green));
		}
		if (list.get(position).getsLogo2() != null) {
			imageLoader.DisplayImage(list.get(position).getsLogo2(), holder.ivCat2, "blank");
			holder.ivCat2.setColorFilter(context.getResources().getColor(R.color.mySport_grey));
			holder.ivCat2.setVisibility(View.VISIBLE);
		} else {
			holder.ivCat2.setVisibility(View.GONE);
		}
		if (list.get(position).getsLogo3() != null) {
			imageLoader.DisplayImage(list.get(position).getsLogo3(), holder.ivCat3, "blank");
			holder.ivCat3.setColorFilter(context.getResources().getColor(R.color.mySport_grey));
			holder.ivCat3.setVisibility(View.VISIBLE);
		} else {
			holder.ivCat3.setVisibility(View.GONE);
		}
		// if(position>previousPosition){
		//
		// AnimationUtils.animate(holder,true);
		// }else{
		//
		// AnimationUtils.animate(holder,false);
		// }
		// previousPosition=position;

		return v;
	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

}