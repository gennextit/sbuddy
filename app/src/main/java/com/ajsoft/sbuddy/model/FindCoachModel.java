package com.ajsoft.sbuddy.model;

import java.util.Comparator;

public class FindCoachModel {

	private String favouriteCoach;
	private String coachId;
	private String fullName;
	private String age;
	private String gender;
	private String imageUrl;
	private String about;
	private String city;
	private String rating;
	private Float km; 
	private String sportId;
	private String sportName;
	private String sLogoCount;
	private String sLogo1;
	private String sLogo2;
	private String sLogo3;  
	private String localityAvailable;
	private String tab;
	private String locality;
	private String logo;
	private String message;
	private String date;
	private String time; 
	private String status; 
	private String review; 
	
//	public static Comparator<FindCoachModel> COMPARE_BY_NAME = new Comparator<FindCoachModel>() {
//        public int compare(FindCoachModel one, FindCoachModel other) {
//            return one.km.compareTo(other.km);
//        }
//    };
	
	public String getFavouriteCoach() {
		return favouriteCoach;
	}
	public void setFavouriteCoach(String favouriteCoach) {
		this.favouriteCoach = favouriteCoach;
	}
	
	public static Comparator<FindCoachModel> COMPARE_BY_KILOMETER = new Comparator<FindCoachModel>() {
        public int compare(FindCoachModel one, FindCoachModel other) {
            return Float.valueOf(one.getKm()).compareTo(other.getKm());
        }
    };
    public String getSportId() {
		return sportId;
	}
	public void setSportId(String sportId) {
		this.sportId = sportId;
	}
	
    public Float getKm() {
		return km;
	}
	public void setKm(Float km) {
		this.km = km;
	}
	
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getFee() {
		return Fee;
	}
	public void setFee(String fee) {
		Fee = fee;
	}
	public String getClosedDay() {
		return closedDay;
	}
	public void setClosedDay(String closedDay) {
		this.closedDay = closedDay;
	}
	private String Fee;
	private String closedDay;
	
	public String getLocality() {
		return locality;
	}
	public void setLocality(String locality) {
		this.locality = locality;
	}
	 
	public String getCoachId() {
		return coachId;
	}
	public void setCoachId(String coachId) {
		this.coachId = coachId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getAbout() {
		return about;
	}
	public void setAbout(String about) {
		this.about = about;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getSportName() {
		return sportName;
	}
	public void setSportName(String sportName) {
		this.sportName = sportName;
	}
	public String getsLogoCount() {
		return sLogoCount;
	}
	public void setsLogoCount(String sLogoCount) {
		this.sLogoCount = sLogoCount;
	}
	public String getsLogo1() {
		return sLogo1;
	}
	public void setsLogo1(String sLogo1) {
		this.sLogo1 = sLogo1;
	}
	public String getsLogo2() {
		return sLogo2;
	}
	public void setsLogo2(String sLogo2) {
		this.sLogo2 = sLogo2;
	}
	public String getsLogo3() {
		return sLogo3;
	}
	public void setsLogo3(String sLogo3) {
		this.sLogo3 = sLogo3;
	}
	public String getLocalityAvailable() {
		return localityAvailable;
	}
	public void setLocalityAvailable(String localityAvailable) {
		this.localityAvailable = localityAvailable;
	}
	public String getTab() {
		return tab;
	}
	public void setTab(String tab) {
		this.tab = tab;
	}
	
}
