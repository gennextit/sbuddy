package com.ajsoft.sbuddy.model;

import android.content.Context;
import android.widget.ArrayAdapter;

public class SpinnerSportsAdapter extends ArrayAdapter<String> {

	public SpinnerSportsAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub

	}

	@Override
	public int getCount() {

		// TODO Auto-generated method stub
		int count = super.getCount();
		//L.m("Sport List Count :: "+String.valueOf(count));
		return count > 0 ? count - 1 : count;

	}

}