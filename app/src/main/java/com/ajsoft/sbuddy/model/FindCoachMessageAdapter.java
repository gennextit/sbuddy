package com.ajsoft.sbuddy.model;

import java.util.ArrayList;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.image_cache.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FindCoachMessageAdapter extends ArrayAdapter<FindCoachModel> {
	private final ArrayList<FindCoachModel> list;
	TextView tvName, tvMsg, tvDate, tvTime;

	private Context context;
	public ImageLoader imageLoader;

	public FindCoachMessageAdapter(Context context, int textViewResourceId, ArrayList<FindCoachModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());

	}

	@Override
	public int getViewTypeCount() {
		return 3;
	}

	@Override
	public int getItemViewType(int position) {
		if (list.get(position).getStatus().equalsIgnoreCase("r")) {
			return 0;
		} else if (list.get(position).getStatus().equalsIgnoreCase("s")) {
			return 1;
		}
		return 2;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		int type = getItemViewType(position);

		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			if (type == 0) {
				// Inflate the layout with image
				v = inflater.inflate(R.layout.custom_slot_find_coach_message_left, parent, false);
			} else if (type == 1) {
				v = inflater.inflate(R.layout.custom_slot_find_coach_message_right, parent, false);
			}
		}
		if (type == 0) {
			tvName = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_message_name);
			tvMsg = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_message_msgDetail);
			tvDate = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_message_date);
			tvTime = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_message_time);

			if (list.get(position).getFullName() != null) {
				tvName.setText(toTitleCase(list.get(position).getFullName()));
			}
			if (list.get(position).getMessage() != null) {
				tvMsg.setText(list.get(position).getMessage());
			}
			if (list.get(position).getDate() != null) {
				tvDate.setText(convertFormateDate(list.get(position).getDate(), 2, "dd-MM-yyyy"));
			}
			if (list.get(position).getTime() != null) {
				tvTime.setText(convertTime(list.get(position).getTime()));
			}
		} else if (type == 1) {
			tvName = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_message_name);
			tvMsg = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_message_msgDetail);
			tvDate = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_message_date);
			tvTime = (TextView) v.findViewById(R.id.tv_custom_slot_find_coach_message_time);

			if (list.get(position).getFullName() != null) {
				tvName.setText(toTitleCase(list.get(position).getFullName()));
			}
			if (list.get(position).getMessage() != null) {
				tvMsg.setText(list.get(position).getMessage());
			}
			if (list.get(position).getDate() != null) {
				tvDate.setText(convertFormateDate(list.get(position).getDate(), 2, "dd-MM-yyyy"));
			}
			if (list.get(position).getTime() != null) {
				tvTime.setText(convertTime(list.get(position).getTime()));
			}
		}

		return v;
	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

	// enter fix Date eg.type 1- dd-MM-yyyy ,type 2- yyyy-MM-dd
	// enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
	public String convertFormateDate(String Date, int type, String dateFormat) {
		String Day, middle, Month, Year;
		String finalDate = Date;
		if (type == 1) {
			Day = Date.substring(0, 2);
			middle = Date.substring(2, 3);
			Month = Date.substring(3, 5);
			Year = Date.substring(6, 10);

		} else {
			Year = Date.substring(0, 4);
			middle = Date.substring(4, 5);
			Month = Date.substring(5, 7);
			Day = Date.substring(8, 10);
		}

		switch (dateFormat) {
		case "dd-MM-yyyy":
			finalDate = Day + middle + Month + middle + Year;
			break;
		case "yyyy-MM-dd":
			finalDate = Year + middle + Month + middle + Day;
			break;
		case "MM-dd-yyyy":
			finalDate = Month + middle + Day + middle + Year;
			break;
		default:
			finalDate = "Date Format Incorrest";
		}
		return finalDate;
	}
	public String convertTime(String time){
		//String s = "12:18:00"; 
		String[] res=time.split(":");
		int hr=Integer.parseInt(res[0]);
		String min=res[1];
		
		if(hr==12){ 
			return (12+":"+min+" "+((hr>=12)?"PM":"AM"));
		}
		
//	  String  finaltime = "" ;
//
//	     SimpleDateFormat f1 = new SimpleDateFormat("kk:mm");
//	     Date d = null;
//	        try {
//	             d = f1.parse(time);
//	             SimpleDateFormat f2 = new SimpleDateFormat("hh:mm aa");
//	             finaltime = f2.format(d).toUpperCase(); // "12:18am"
//
//	    } catch (ParseException e) {
//
//	        // TODO Auto-generated catch block
//	            e.printStackTrace();
//	        }
	    
	    
	    return (hr%12+":"+min+" "+((hr>=12)?"PM":"AM"));
	}
}