package com.ajsoft.sbuddy.model;
 

import java.util.ArrayList;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.image_cache.ImageLoader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class FindCoachAdapter extends ArrayAdapter<FindCoachModel> {
	private final ArrayList<FindCoachModel> list;
	
	private Context context;
	public ImageLoader imageLoader; 
	
	public FindCoachAdapter(Context context, int textViewResourceId, ArrayList<FindCoachModel> list) {
		super(context, textViewResourceId, list);
		this.context = context;
		this.list = list;
		imageLoader = new ImageLoader(context.getApplicationContext());
	       
	}

	class ViewHolder{
		TextView tvName,tvDistance,tvCity;
		ImageView ivProfile,ivDistance;
		RatingBar ratingBar;
		
		public ViewHolder(View v) {
			tvName = (TextView) v.findViewById(R.id. tv_custom_slot_find_coaches_name);
			//tvAge = (TextView) v.findViewById(R.id. tv_custom_slot_find_coaches_age);
			tvDistance = (TextView) v.findViewById(R.id. tv_custom_slot_find_coaches_distance);
			ivDistance= (ImageView) v.findViewById(R.id. iv_custom_slot_find_coaches_distance);
			tvCity = (TextView) v.findViewById(R.id. tv_custom_slot_find_coaches_city);
			ivProfile = (ImageView) v.findViewById(R.id. iv_custom_slot_find_coaches_profile);
			ratingBar= (RatingBar) v.findViewById(R.id. ratingBar1);
			
		}
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder=null;
		if (v == null) {
			// Inflate the layout according to the view type
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			v = inflater.inflate(R.layout.custom_slot_find_coaches, parent, false);
			holder=new ViewHolder(v);
			v.setTag(holder);
		}else{
			holder=(ViewHolder) v.getTag();
		}
		
		if(list.get(position).getFullName()!=null){
			if(list.get(position).getAge()!=null){
				if(list.get(position).getGender()!=null){
					String gender=list.get(position).getGender().substring(0,1);
					holder.tvName.setText(list.get(position).getFullName()+" | "+list.get(position).getAge()+" | "+toTitleCase(gender));
				}else{
					holder.tvName.setText(list.get(position).getFullName());
				}
			}
		}
		
		if(list.get(position).getKm()!=null){
			holder.tvDistance.setText(String.valueOf(list.get(position).getKm())+" km");
		}else{
			holder.tvDistance.setVisibility(View.INVISIBLE);
			holder.ivDistance.setVisibility(View.INVISIBLE);
		}
		if(list.get(position).getCity()!=null){
			holder.tvCity.setText(list.get(position).getCity());
		}
		if(list.get(position).getRating()!=null){
			if(!list.get(position).getRating().equals("false")){
				holder.ratingBar.setRating(Float.parseFloat(list.get(position).getRating()));
			}
		}else{
			holder.ratingBar.setRating(1);
		}
		if(list.get(position).getImageUrl()!=null){
			imageLoader.DisplayImage(list.get(position).getImageUrl(), holder.ivProfile,"profile",false);
		}
		
		return v;
	}

	public static String toTitleCase(String givenString) {
	    String[] arr = givenString.split(" ");
	    StringBuffer sb = new StringBuffer();

	    for (int i = 0; i < arr.length; i++) {
	        sb.append(Character.toUpperCase(arr[i].charAt(0)))
	            .append(arr[i].substring(1)).append(" ");
	    }          
	    return sb.toString().trim();
	}
	
	
}