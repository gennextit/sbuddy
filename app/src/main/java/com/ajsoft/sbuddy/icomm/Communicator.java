package com.ajsoft.sbuddy.icomm;

public interface Communicator {
	public void respond(int position);
}
