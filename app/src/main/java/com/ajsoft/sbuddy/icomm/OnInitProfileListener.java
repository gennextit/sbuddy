package com.ajsoft.sbuddy.icomm;

public interface OnInitProfileListener {
	public void onInitProfileListener(int screenPosition,String imageUrl, String name, String age, String gender, String city,
			String location, String clubOrApartment, String about);
}
