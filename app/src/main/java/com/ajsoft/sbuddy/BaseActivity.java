package com.ajsoft.sbuddy;

/**
 * Created by Abhijit on 3/3/2016.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ajsoft.sbuddy.coach.CoachProfile;
import com.ajsoft.sbuddy.coach.CoachRequestCoin;
import com.ajsoft.sbuddy.fragments.UserGuide;
import com.ajsoft.sbuddy.fragments.event.MyCreatedEvents;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.SideMenu;
import com.ajsoft.sbuddy.model.SideMenuAdapter;
import com.ajsoft.sbuddy.util.Coach;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Venue;
import com.ajsoft.sbuddy.venue.VenueProfile;
import com.ajsoft.sbuddy.venue.VenueRequestCoin;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class BaseActivity extends AppCompatActivity {
	boolean conn = false;
	Builder alertDialog;
	ProgressDialog progressDialog;
	AlertDialog dialog = null;
	ImageView ActionBack;
	TextView ActionBarHeading;

	DrawerLayout dLayout;
	ListView dList;
	List<SideMenu> sideMenuList;
	SideMenuAdapter slideMenuAdapter;

	static int COACH = 1, VENUE = 2;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	protected void setHeading(String title, final Activity act) {

		// getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar_bg));
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setCustomView(R.layout.action_bar);
		TextView appName = (TextView) findViewById(R.id.tv_action_appName);
		ImageView backPressed = (ImageView) findViewById(R.id.iv_action_back);
		backPressed.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				act.finish();
				showToast("Action Back");
			}
		});
		appName.setText(setFont(title));

	}
	
	public SpannableString setActionBarTitle(String title) {
		return setBoldFont(R.color.white,Typeface.BOLD,title);
	}

	public SpannableString setBoldFont(int colorId,int typeFaceStyle, String title) {
		SpannableString s = new SpannableString(title);
		Typeface externalFont=Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/segoeui.ttf");
		s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(getResources().getColor(colorId)), 0, s.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new StyleSpan(typeFaceStyle), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		return s;
	}

	public void setActionBarOption() {
		ActionBack = (ImageView) findViewById(R.id.actionbar_back);
		ActionBarHeading = (TextView) findViewById(R.id.actionbar_title);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	public void showPDialog(Context context, String msg) {
		progressDialog = new ProgressDialog(context);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(msg);
		progressDialog.setIndeterminate(false);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	public void hideActionBar() {
		//getSupportActionBar().hide();
	}

	public SpannableString setFont(String title) {
		SpannableString s = new SpannableString(title);
//		s.setSpan(new TypefaceSpan("CircularStd-Book.otf"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), 0, s.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		s.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), 0);
		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		return s;
	}

	public void hideKeybord(Activity act) {
		InputMethodManager inputManager = (InputMethodManager) getSystemService(act.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

	}

	// public void showToast(String txt,Boolean status){
	// // Inflate the Layout
	// LayoutInflater lInflater = (LayoutInflater)getSystemService(
	// Activity.LAYOUT_INFLATER_SERVICE);
	// //LayoutInflater inflater = context.getLayoutInflater();
	//
	// //View layout = lInflater.inflate(R.layout.custom_toast,(ViewGroup)
	// findViewById(R.id.custom_toast_layout_id));
	// View layout=lInflater.inflate(R.layout.custom_toast, null);
	// TextView a=(TextView)layout.findViewById(R.id.tv_toast_msg);
	// ImageView b=(ImageView)layout.findViewById(R.id.imageView1);
	// //layout.setBackgroundResource((status) ? R.drawable.toast_bg :
	// R.drawable.toast_bg_red);
	// b.setImageResource((status) ? R.drawable.success : R.drawable.fail);
	// a.setText(txt);
	// //a.setTextColor((status) ? getResources().getColor(R.color.icon_green) :
	// getResources().getColor(R.color.icon_red));
	// // Create Custom Toast
	// Toast toast = new Toast(BaseActivity.this);
	// toast.setGravity(Gravity.CENTER, 0, 0);
	// toast.setDuration(Toast.LENGTH_LONG);
	// toast.setView(layout);
	// toast.show();
	// }
	//
	// public void ExitAlertBox(Activity Act) {
	//
	// final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
	//
	// // ...Irrelevant code for customizing the buttons and title
	// LayoutInflater inflater = Act.getLayoutInflater();
	//
	// View v=inflater.inflate(R.layout.custom_dialog, null);
	// dialogBuilder.setView(v);
	// Button quitButton = (Button) v.findViewById(R.id.button1);
	// Button ResultButton = (Button) v.findViewById(R.id.button2);
	// Button cancelButton = (Button) v.findViewById(R.id.button3);
	// // if decline button is clicked, close the custom dialog
	// quitButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// // Close dialog
	// dialog.dismiss();
	// finish();
	// }
	// });
	// ResultButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// // Close dialog
	// dialog.dismiss();
	// }
	// });
	// cancelButton.setOnClickListener(new OnClickListener() {
	// @Override
	// public void onClick(View v) {
	// // Close dialog
	// dialog.dismiss();
	// }
	// });
	// dialog = dialogBuilder.create();
	// dialog.show();
	//
	// }

	public SpannableStringBuilder subScr(int text) {

		SpannableStringBuilder cs = new SpannableStringBuilder(String.valueOf(text));
		cs.setSpan(new SubscriptSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		cs.setSpan(new RelativeSizeSpan(0.75f), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

		return cs;
	}

	// public boolean isConnected() {
	//
	// if (isOnline() == false) {
	// showAlertInternet(getSt(R.string.internet_error_tag),
	// getSt(R.string.internet_error_msg), false);
	// return false;
	// } else {
	// return true;
	// }
	// }

	// ProgressDialog progressDialog; I have declared earlier.
	public void showPDialog(String msg) {
		progressDialog = new ProgressDialog(BaseActivity.this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		// progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.dialog_animation));
		progressDialog.setMessage(msg);
		progressDialog.setIndeterminate(false);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	public void dismissPDialog() {
		if (progressDialog != null)
			progressDialog.dismiss();
	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

	public boolean isConnectionAvailable() {

		if (isOnline() == false) {
			return false;
		} else {
			return true;
		}
	}

	public String LoadPref(String key) {
		return LoadPref(BaseActivity.this, key);
	}

	public String LoadPref(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String data = sharedPreferences.getString(key, "");
		return data;
	}

	public void SavePref(String key, String value) {
		SavePref(BaseActivity.this, key, value);
	}

	public void SavePref(Context context, String key, String value) {

		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();

	}

	public String convert(String res) {
		String UrlEncoding = null;
		try {
			UrlEncoding = URLEncoder.encode(res, "UTF-8");

		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		return UrlEncoding;
	}

	public boolean isOnline() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(BaseActivity.this.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		if (haveConnectedWifi == true || haveConnectedMobile == true) {
			L.m("Log-Wifi"+ String.valueOf(haveConnectedWifi));
			L.m("Log-Mobile"+ String.valueOf(haveConnectedMobile)); 
			conn = true;
		} else {
			L.m("Log-Wifi"+ String.valueOf(haveConnectedWifi));
			L.m("Log-Mobile"+ String.valueOf(haveConnectedMobile)); 
			conn = false;
		}

		return conn;
	}

	// public void showAlertInternet(String title, String message, Boolean
	// status) {
	// alertDialog = new AlertDialog.Builder(BaseActivity.this);
	//
	// // Setting Dialog Title
	// alertDialog.setTitle(title);
	//
	// // Setting Dialog Message
	// alertDialog.setMessage(message);
	//
	// if (status != null)
	// // Setting alert dialog icon
	// alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
	//
	// // On pressing Settings button
	// alertDialog.setPositiveButton("SETTING", new
	// DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int which) {
	// EnableMobileIntent();
	// }
	// });
	// // Showing Alert Message
	// alertDialog.show();
	// }

	public String getSt(int id) {

		return getResources().getString(id);
	}

	private void EnableMobileIntent() {
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(android.provider.Settings.ACTION_SETTINGS);
		startActivity(intent);

	}

	public String viewTime() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df.format(c.getTime());

	}

	public String getDate() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	public String getTime12() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df.format(c.getTime());

	}

	public String getTime24() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df1 = new SimpleDateFormat("H:mm:ss");
		df1.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df1.format(c.getTime());
	}

	public String getTime() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("H:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df.format(c.getTime());
	}

	// give format like dd-MMM-yyyy,dd/MMM/yyyy
	public String viewDate(String format) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(format);
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	public String vDate() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	// Enter Format eg.dd-MMM-yyyy,dd/MM/yyyy
	public String viewFormatDate(String format) {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(format);
		String formattedDate = df.format(c.getTime());
		return formattedDate;

	}

	// missing 0 issue resolve
	// enter Date eg. dd-MM-yyyy
	// enter dateFormat eg. dd-MM-yyyy
	public String currectFormateDate(String Date, String dateFormat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date cDate = null;
		try {
			cDate = sdf.parse(Date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String formattedDate = sdf.format(cDate);
		return formattedDate;
	}

	public void showToast(String txt) {
		// Inflate the Layout
		Toast toast = Toast.makeText(BaseActivity.this, txt, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER | Gravity.CENTER, 0, 0);
		toast.show();
	}

	// enter fix Date eg.type 1- dd-MM-yyyy ,type 2- yyyy-MM-dd
	// enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
	public static String convertFormateDate(String Date, int type, String dateFormat) {
		String Day, middle, Month, Year;
		String finalDate = Date;
		if (type == 1) {
			Day = Date.substring(0, 2);
			middle = Date.substring(2, 3);
			Month = Date.substring(3, 5);
			Year = Date.substring(6, 10);

		} else {
			Day = Date.substring(0, 4);
			middle = Date.substring(4, 5);
			Month = Date.substring(5, 7);
			Year = Date.substring(8, 10);
		}

		switch (dateFormat) {
		case "dd-MM-yyyy":
			finalDate = Day + middle + Month + middle + Year;
			break;
		case "yyyy-MM-dd":
			finalDate = Year + middle + Month + middle + Day;
			break;
		case "MM-dd-yyyy":
			finalDate = Month + middle + Day + middle + Year;
			break;
		default:
			finalDate = "Date Format Incorrest";
		}
		return finalDate;
	}

	// SpannableStringBuilder cs = new SpannableStringBuilder("X3 + X2");
	// cs.setSpan(new SubscriptSpan(), 1, 3,
	// Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	// cs.setSpan(new RelativeSizeSpan(0.75f), 1, 2,
	// Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	// cs.setSpan(new SubscriptSpan(), 6, 7,
	// Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	// cs.setSpan(new RelativeSizeSpan(0.75f), 6, 7,
	// Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

	protected void SetDrawer(final Activity act, LinearLayout iv, ImageLoader imageLoader, final int SWITCH) {
		// TODO Auto-generated method stub
		iv.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean drawerOpen = dLayout.isDrawerOpen(dList);
				if (!drawerOpen) {
					dLayout.openDrawer(dList);
				} else {
					dLayout.closeDrawer(dList);
				}

			}
		});

		dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		dList = (ListView) findViewById(R.id.left_drawer);
		LayoutInflater inflater = getLayoutInflater();
		View listHeaderView = inflater.inflate(R.layout.side_menu_header_coach_venue, null, false);
		TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
		ImageView ivProfile = (ImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
		if (SWITCH == COACH) {
			Coach.setCoachProfileImage(act, imageLoader, ivProfile);
			// tvName.setText(LoadPref(Coach.CoachId)+" :
			// "+LoadPref(Coach.Name));
			tvName.setText(LoadPref(Coach.Name));
		} else {
			Venue.setVenueProfileImage(act, imageLoader, ivProfile);
			tvName.setText(LoadPref(Venue.Name));
		}

		dList.addHeaderView(listHeaderView);

		SideMenu s1 = new SideMenu("Profile & Reviews", R.drawable.menu1);
		SideMenu s2 = new SideMenu("Request Coin", R.drawable.menu2);
		SideMenu s3 = new SideMenu("My Events", R.drawable.menu4);
		SideMenu s4 = new SideMenu("Invite Friends", R.drawable.menu5);
		SideMenu s5 = new SideMenu("User guide/T&C", R.drawable.user_guide);
		// SideMenu s7 = new SideMenu("Logout", R.drawable.menu8);

		sideMenuList = new ArrayList<SideMenu>();
		sideMenuList.add(s1);
		sideMenuList.add(s2);
		sideMenuList.add(s3);
		sideMenuList.add(s4);
		sideMenuList.add(s5);
		// sideMenuList.add(s6);
		// sideMenuList.add(s7);

		slideMenuAdapter = new SideMenuAdapter(act, R.layout.side_menu_list_slot, sideMenuList);
		// adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);

		dList.setAdapter(slideMenuAdapter);
		// dList.setSelector(android.R.color.holo_blue_dark);

		dList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

				dLayout.closeDrawers();

				// FragmentManager mannager = getFragmentManager();
				// FragmentTransaction transaction;

				Intent intent;
				FragmentManager mannager;
				FragmentTransaction transaction;

				if (SWITCH == COACH) {
					switch (position) {

					case 1:
						CoachProfile coachProfile = new CoachProfile();
						mannager = getFragmentManager();
						transaction = mannager.beginTransaction();
						transaction.replace(android.R.id.content, coachProfile, "coachProfile");
						transaction.addToBackStack("coachProfile");
						transaction.commit();
						break;
					case 2:
						CoachRequestCoin coachRequestCoin = new CoachRequestCoin();
						mannager = getFragmentManager();
						transaction = mannager.beginTransaction();
						transaction.replace(android.R.id.content, coachRequestCoin, "coachRequestCoin");
						transaction.addToBackStack("coachRequestCoin");
						transaction.commit();
						break;
					case 3:
						MyCreatedEvents myCreatedEvents = new MyCreatedEvents();
						MyCreatedEvents.SWITCH=MyCreatedEvents.COACH;
						mannager = getFragmentManager();
						transaction = mannager.beginTransaction();
						transaction.replace(android.R.id.content, myCreatedEvents, "myCreatedEvents");
						transaction.addToBackStack("myCreatedEvents");
						transaction.commit();
						break;
					case 4:

						try {
							Intent i = new Intent(Intent.ACTION_SEND);
							i.setType("text/plain");
							i.putExtra(Intent.EXTRA_SUBJECT, "sbuddy");
							i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_friends_message));
							startActivity(Intent.createChooser(i, "choose one"));
						} catch (Exception e) { // e.toString();
						}
						break;
					case 5:
						UserGuide userGuide = new UserGuide();
						mannager = getFragmentManager();
						transaction = mannager.beginTransaction();
						transaction.replace(android.R.id.content, userGuide, "userGuide");
						transaction.addToBackStack("userGuide");
						transaction.commit();
						
						break;

					}

				} else if (SWITCH == VENUE) {
					switch (position) {

					case 1:
						VenueProfile venueProfile = new VenueProfile();
						mannager = getFragmentManager();
						transaction = mannager.beginTransaction();
						transaction.replace(android.R.id.content, venueProfile, "venueProfile");
						transaction.addToBackStack("venueProfile");
						transaction.commit();
						break;
					case 2:
						VenueRequestCoin venueRequestCoin = new VenueRequestCoin();
						mannager = getFragmentManager();
						transaction = mannager.beginTransaction();
						transaction.replace(android.R.id.content, venueRequestCoin, "venueRequestCoin");
						transaction.addToBackStack("venueRequestCoin");
						transaction.commit();
						break;
					case 3:
						MyCreatedEvents myCreatedEvents = new MyCreatedEvents();
						MyCreatedEvents.SWITCH=MyCreatedEvents.VENUE;
						mannager = getFragmentManager();
						transaction = mannager.beginTransaction();
						transaction.replace(android.R.id.content, myCreatedEvents, "myCreatedEvents");
						transaction.addToBackStack("myCreatedEvents");
						transaction.commit();
						break;
					case 4:

						try {
							Intent i = new Intent(Intent.ACTION_SEND);
							i.setType("text/plain");
							i.putExtra(Intent.EXTRA_SUBJECT, "sbuddy");
							i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_friends_message));
							startActivity(Intent.createChooser(i, "choose one"));
						} catch (Exception e) { // e.toString();
						}
						break;
					case 5:
						UserGuide userGuide = new UserGuide();
						mannager = getFragmentManager();
						transaction = mannager.beginTransaction();
						transaction.replace(android.R.id.content, userGuide, "userGuide");
						transaction.addToBackStack("userGuide");
						transaction.commit();
						
						break;

					}
				}

			}

		});
	}

}