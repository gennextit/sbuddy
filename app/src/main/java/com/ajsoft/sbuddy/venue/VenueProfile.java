package com.ajsoft.sbuddy.venue;
 

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.FindVenueModel; 
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Venue;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class VenueProfile extends CompactFragment {

	ImageView ivProfile;
	RatingBar rbRating;
	TextView tvReview, tvAbout, tvCoachingLocation, tvCoachingFee;
	LinearLayout llEditProfile,llvenueSportcontainer;
	ImageLoader imageLoader;
	ShowProfileTask showProfileTask;
	ArrayList<FindVenueModel> cList;
	String review, about, rating,openHours;
	StringBuilder sbLocation;
	ScrollView mainLayout;
	Animation animMove;
	FragmentManager mannager;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (showProfileTask != null) {
			showProfileTask.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (showProfileTask != null) {
			showProfileTask.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.venue_profile, container, false);
		imageLoader = new ImageLoader(getActivity());
		mannager = getFragmentManager();
		setActionBarOption(v);
		
		initAnim();
		ivProfile = (ImageView) v.findViewById(R.id.iv_coach_profile_pic);
		tvReview = (TextView) v.findViewById(R.id.tv_coach_profile_review);
		tvAbout = (TextView) v.findViewById(R.id.tv_coach_profile_about);
		tvCoachingLocation = (TextView) v.findViewById(R.id.tv_coach_profile_location);
		tvCoachingFee = (TextView) v.findViewById(R.id.tv_coach_profile_fee);
		rbRating = (RatingBar) v.findViewById(R.id.rb_coach_profile_rating);
		llEditProfile = (LinearLayout) v.findViewById(R.id.ll_coach_profile_editrofile);
		mainLayout = (ScrollView) v.findViewById(R.id.scrollView1);
		llvenueSportcontainer = (LinearLayout) v.findViewById(R.id.venue_sport_container);

		llEditProfile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), "Please contact admin", Toast.LENGTH_SHORT).show();
			}
		});
		Venue.setVenueProfileImage(getActivity(), imageLoader, ivProfile);
		
		
		showProfileTask = new ShowProfileTask(getActivity());
		showProfileTask.execute(LoadPref(Venue.VenueJsonData));

		return v;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
		TextView tvTitle=(TextView)view.findViewById(R.id.actionbar_title);
		tvTitle.setText(LoadPref(Venue.Name));
		ActionBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("venueProfile", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) { 
				mannager.popBackStack("venueDashboard", 0);
			}
		});
	}
	private void initAnim() {
		animMove = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down); 
	}

	private class ShowProfileTask extends AsyncTask<String, Void, ArrayList<FindVenueModel>> {

		Activity activity;

		public ShowProfileTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected ArrayList<FindVenueModel> doInBackground(String... params) {
			// TODO Auto-generated method stub
			String response, locality = null, fee = null;

			cList = new ArrayList<FindVenueModel>(); 
			sbLocation = new StringBuilder();
			response = params[0];
			// L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray main = new JSONArray(response);
					for (int k = 0; k < main.length(); k++) {
						JSONObject sub = main.getJSONObject(k);
						review = sub.optString("review");
						rating = sub.optString("rating");
						about = sub.optString("about");
						openHours = sub.optString("openHours");
						sbLocation.append(sub.optString("locality"));
						JSONArray sportPlay = sub.getJSONArray("sportPlay");
						L.m(sportPlay.toString());
						for (int i = 0; i < sportPlay.length(); i++) {
							JSONObject sPData = sportPlay.getJSONObject(i);
							if (!sPData.optString("sportName").equals("")) {
								FindVenueModel model = new FindVenueModel();
								model.setSportId(sPData.optString("sportId"));
								model.setSportName(sPData.optString("sportName"));
								model.setLogo(sPData.optString("logo"));
								
								model.setmemFee(sPData.optString("memFee"));
								model.setPnpFee(sPData.optString("memFee"));
								cList.add(model);
							}
						} 

					}
				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return cList;
		}

		@Override
		protected void onPostExecute(ArrayList<FindVenueModel> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (activity != null) {
				if (result != null) {
					
					if (review != null) {
						tvReview.setText(review);
					}
					if (rating != null) {
						rbRating.setRating(Float.parseFloat(rating));
					}
					if (about != null) {
						tvAbout.setText(about);
					}
					tvCoachingLocation.setText(sbLocation.toString());
					tvCoachingFee.setText(openHours);

					allocateSport(result);
					
					
					mainLayout.setVisibility(View.VISIBLE);
					mainLayout.startAnimation(animMove);
				}

			}
		}

		private void allocateSport(ArrayList<FindVenueModel> result) {
			for (int i = 0; i < result.size(); i++) {
				addImageDynamacally(i, result.get(i).getSportName(), result.get(i).getLogo(), 1);
			}
		}

		private void addImageDynamacally(final int position, String sportName, String sportImage, int status) {
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.MATCH_PARENT);
			params.setMargins(0, 0, 20, 0);
			LinearLayout layout = new LinearLayout(getActivity());
			layout.setOrientation(LinearLayout.VERTICAL);
			layout.setGravity(Gravity.CENTER);
			layout.setLayoutParams(params);

			LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			
			LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
					getResources().getDimensionPixelSize(R.dimen.slt_sport_image_size),
					getResources().getDimensionPixelSize(R.dimen.slt_sport_image_size));
			
			ImageView imageView = new ImageView(getActivity());
			imageView.setLayoutParams(params2);
			imageView.setTag(position);
			imageLoader.DisplayImage(sportImage, imageView, "blank");

			if (status == 0) {
				imageView.setBackgroundResource(R.drawable.rounded_grey);
				imageView.setColorFilter(getResources().getColor(R.color.mySport_grey));
			} else {
				imageView.setBackgroundResource(R.drawable.rounded_green);
				imageView.setColorFilter(getResources().getColor(R.color.mySport_green));
			}

			layout.addView(imageView);

			TextView textView = new TextView(getActivity());
			textView.setLayoutParams(params1);
			textView.setText(sportName);
			textView.setGravity(Gravity.CENTER);
			layout.addView(textView);

//			imageView.setOnClickListener(new View.OnClickListener() {
//
//				@Override
//				public void onClick(View v) {
//
//					int but_id = Integer.parseInt(v.getTag().toString().trim());
//					int spPosition = sportPosition;
//					if (but_id == position) {
//						// Toast.makeText(getActivity(), String.valueOf(but_id),
//						// Toast.LENGTH_SHORT).show();
//						// sltLogic(but_id);
//
//						View view = llvenueSportcontainer.getRootView();
//						ImageView iv2 = (ImageView) view.findViewWithTag(spPosition);
//						iv2.setBackgroundResource(R.drawable.rounded_grey);
//						iv2.setColorFilter(getResources().getColor(R.color.mySport_grey));
//
//						ImageView iv = (ImageView) view.findViewWithTag(but_id);
//						iv.setBackgroundResource(R.drawable.rounded_green);
//						iv.setColorFilter(getResources().getColor(R.color.mySport_green));
//
//						sportPosition = but_id;
//						setSelectedSportOnUi(but_id);
//					}
//				}
//
//			});

			llvenueSportcontainer.addView(layout);

		}
	}

}

