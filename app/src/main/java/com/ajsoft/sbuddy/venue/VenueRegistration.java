package com.ajsoft.sbuddy.venue;
 

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Validation;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

public class VenueRegistration extends CompactFragment implements View.OnTouchListener {
	EditText etName, etEmailTelephone,etFacilityAcailable,etWorkingDays,etVenueTiming,etVenueAddress,etContactPersonName,etPhone,etEmail,etOtherInfo; 
	LinearLayout llSubmit;
	CheckBox cbPnP,cbMemOnly;
	RegisterRequist registerRequist;
	ProgressBar progressBar; 
	FragmentManager mannager; 
	Boolean isSoftKeyboardDisplayed = false;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (registerRequist != null) {
			registerRequist.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (registerRequist != null) {
			registerRequist.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.venue_registration, container, false);
		mannager = getFragmentManager();
		setActionBarOption(v);
		isSoftKeyboardDisplayed = false;

		etName = (EditText) v.findViewById(R.id.et_register_name);
		etEmailTelephone = (EditText) v.findViewById(R.id.et_register_emailTelephone);
		etFacilityAcailable = (EditText) v.findViewById(R.id.et_register_facilitiesAavailable);
		etWorkingDays = (EditText) v.findViewById(R.id.et_register_workingDays);
		etVenueTiming = (EditText) v.findViewById(R.id.et_register_Timings);
		etVenueAddress = (EditText) v.findViewById(R.id.et_register_Address);
		etContactPersonName = (EditText) v.findViewById(R.id.et_register_ContactPersonName);
		etPhone = (EditText) v.findViewById(R.id.et_register_Phone);
		etEmail = (EditText) v.findViewById(R.id.et_register_Email);
		etOtherInfo = (EditText) v.findViewById(R.id.et_register_otherInfo);
		llSubmit = (LinearLayout) v.findViewById(R.id.ll_register_submit);
		cbPnP = (CheckBox) v.findViewById(R.id.cb_pnp);
		cbMemOnly = (CheckBox) v.findViewById(R.id.cb_memOnly);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		
		setTypsFace(etName, etEmailTelephone, etFacilityAcailable, etWorkingDays, etVenueTiming);
		setTypsFace(etVenueAddress, etContactPersonName, etPhone, etEmail, etOtherInfo);
		setTypsFace(cbPnP);
		setTypsFace(cbPnP);
		
		etName.setOnTouchListener(this);
		etOtherInfo.setOnTouchListener(this);
		
		llSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkValidation()) {
					if (isSoftKeyboardDisplayed) {
						hideKeybord();
					}

					registerRequist = new RegisterRequist(getActivity());
					registerRequist.execute(AppSettings.setVenueRegistrationRequest);
				}
			}
		});

		

		return v;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText("REFER COACH");
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack();
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack();
			}
		});
	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isName(etName, true)) {
			ret = false;
		} else if (!Validation.isEmpty(etEmailTelephone,true)) {
			ret = false;
		} else if (!cbPnP.isChecked()&&!cbMemOnly.isChecked()) {
			Toast.makeText(getActivity(), "Please select one Pay n Play or Members Only", Toast.LENGTH_SHORT).show();
			ret = false;
		} else if (!Validation.isEmpty(etFacilityAcailable,true)) {
			ret = false;
		} else if (!Validation.isEmpty(etWorkingDays,true)) {
			ret = false;
		} else if (!Validation.isEmpty(etVenueTiming,true)) {
			ret = false;
		} else if (!Validation.isEmpty(etVenueAddress,true)) {
			ret = false;
		} else if (!Validation.isEmpty(etContactPersonName,true)) {
			ret = false;
		} else if (!Validation.isEmpty(etPhone,true)) {
			ret = false;
		} else if (!Validation.isEmpty(etEmail,true)) {
			ret = false;
		} 

		return ret;
	}

	
	private class RegisterRequist extends AsyncTask<String, Void, String> {

		private Activity activity;

		public RegisterRequist(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			llSubmit.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response;
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
				// sportId,choosePlan, chooseDate, chooseSlot, playerId, venueId
				params.add(new BasicNameValuePair("venueName", etName.getText().toString()));
				params.add(new BasicNameValuePair("venueEmailTel", etEmailTelephone.getText().toString()));
				if(cbMemOnly.isChecked()){
					params.add(new BasicNameValuePair("venueMemberFlag", "selected"));
				}else{
					params.add(new BasicNameValuePair("venueMemberFlag", "not selected"));
				}
				if(cbPnP.isChecked()){
					params.add(new BasicNameValuePair("venuePnPFlag", "selected"));
				}else{
					params.add(new BasicNameValuePair("venuePnPFlag", "not selected"));
				}
				params.add(new BasicNameValuePair("facilities", etFacilityAcailable.getText().toString()));
				params.add(new BasicNameValuePair("workingDays", etWorkingDays.getText().toString()));
				params.add(new BasicNameValuePair("timings", etVenueTiming.getText().toString()));
				params.add(new BasicNameValuePair("address", etVenueAddress.getText().toString()));
				params.add(new BasicNameValuePair("contactName", etContactPersonName.getText().toString()));
				params.add(new BasicNameValuePair("contactPhone", etPhone.getText().toString()));
				params.add(new BasicNameValuePair("contactEmail", etEmail.getText().toString()));
				params.add(new BasicNameValuePair("otherInfo", etOtherInfo.getText().toString()));
				
			

			HttpReq ob = new HttpReq();

			return response = ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				progressBar.setVisibility(View.GONE);
				llSubmit.setVisibility(View.VISIBLE);
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						showReviewAlertSuccess(getActivity(), "informCoach",0);
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	
	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		// TODO Auto-generated method stub
		isSoftKeyboardDisplayed = true;
		return false;
	}

}


