package com.ajsoft.sbuddy.venue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.GCMIntentService;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.model.CoachDashboardAdapter;
import com.ajsoft.sbuddy.model.CoachDashboardModel;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.DBManager;
import com.ajsoft.sbuddy.util.DateTimeUtility;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Venue;
import com.ajsoft.sbuddy.util.WakeLocker;
import com.google.android.gcm.GCMRegistrar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class VenueDashboard extends CompactFragment {

	// ListView lvMain;
	String jsonData, sportId;
	ArrayList<CoachDashboardModel> myMsgList;
	LoadMessageList loadMessageList;
	// ProgressBar progressBar;
	String JsonData;
	CoachDashboardAdapter ExpAdapter;
	OnVenueDashboardListener comm;
	ListView lv;
	LinearLayout llProgress,llTapToRefresh;
	DBManager dbManager;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (loadMessageList != null) {
			loadMessageList.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (loadMessageList != null) {
			loadMessageList.onDetach();
		}
	}

	public void setCommunicator(OnVenueDashboardListener comm) {
		this.comm = comm;
	}

	public interface OnVenueDashboardListener {
		public void onVenueDashboardListener(String JsonData, String playerId, String playerImage);

		public void onUpdateCoin(String availableCoin);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.venue_dashboard, container, false);
		dbManager = new DBManager(getActivity());

		// lvMain = (ListView) v.findViewById(R.id.lv_dashboard);
		// progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		lv = (ListView) v.findViewById(R.id.lv_venue_dashboard);
		llTapToRefresh = (LinearLayout) v.findViewById(R.id.ll_tapToRefresh);
		llProgress = (LinearLayout) v.findViewById(R.id.ll_progressBar);
		// progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		// Set a listener to be invoked when the list should be refreshed.
//		lv.setOnRefreshListener(new OnRefreshListener() {
//			@Override
//			public void onRefresh() {
//				// Do work to refresh the list here.
//
//				loadMessageList = new LoadMessageList(getActivity(), null);
//				loadMessageList.execute(AppSettings.receiveVenueMessages);
//			}
//		});
		llTapToRefresh.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				loadMessageList = new LoadMessageList(getActivity(), "loader");
				loadMessageList.execute(AppSettings.receiveVenueMessages);
			}
		});
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
				if (myMsgList != null) {
					comm.onVenueDashboardListener(JsonData, myMsgList.get(position).getPlayerId(),
							myMsgList.get(position).getImageUrl());
				}
			}
		});

		GCMIntentService.notificationStatus = true;
		getActivity().registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));

		
		loadMessageList = new LoadMessageList(getActivity(), "loader");
		loadMessageList.execute(AppSettings.receiveVenueMessages);
		return v;
	}

	private void showOrHideLoader(Boolean status) {
		if (status) {
			llProgress.setVisibility(View.VISIBLE);
			llTapToRefresh.setVisibility(View.GONE);
		} else {
			llProgress.setVisibility(View.GONE);
			llTapToRefresh.setVisibility(View.VISIBLE);
		}
	}

	
	private class LoadMessageList extends AsyncTask<String, Void, String> {

		private Activity activity;
		private String loader = "loader";

		public LoadMessageList(Activity activity, String loader) {
			this.activity = activity;
			this.loader = loader;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (loader != null) {
				//llProgress.setVisibility(View.VISIBLE);
				showOrHideLoader(true);
			}
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error", response2 = "error";
			ErrorMessage = "No record available";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("venueId", LoadPref(Venue.VenueId)));
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 1, params);
			response2 = ob.makeConnection(AppSettings.getVenueAvailableCoins, HttpReq.POST, params);

			if (response2.contains("[")) {
				try {
					JSONArray arr = new JSONArray(response2);
					JSONObject obj = arr.getJSONObject(0);
					if (obj.optString("status").equalsIgnoreCase("success")) {
						if (activity != null) {
							SavePref(Venue.noOfCoins, obj.optString("message"));
						}

					} else if (obj.optString("status").equalsIgnoreCase("failure")) {
						ErrorMessage = obj.optString("message");
						//return null;
					}

				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response2;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response2);
				ErrorMessage = response;
				return null;
			}
			myMsgList = new ArrayList<>();
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					JSONObject coachData = message.getJSONObject(0);
					if (coachData.optString("status").equalsIgnoreCase("success")) {
						JSONArray mainArr = coachData.getJSONArray("message");
						JsonData = mainArr.toString();
						for (int k = 0; k < mainArr.length(); k++) {
							JSONObject sPData = mainArr.getJSONObject(k);
							if (!sPData.optString("playerId").equals("")) {
								response = "success";
								CoachDashboardModel model = new CoachDashboardModel();
								model.setPlayerId(sPData.optString("playerId"));
								model.setFullName(sPData.optString("fullName"));
								model.setGender(sPData.optString("gender"));
								model.setAge(sPData.optString("age"));
								model.setImageUrl(sPData.optString("imageUrl"));
								model.setMobileNumber(sPData.optString("mobileNumber"));
								model.setEmailId(sPData.optString("emailId"));
								JSONArray messages = sPData.getJSONArray("messages");
								JSONObject messagesData =messages.getJSONObject(messages.length()-1);
								if(activity!=null){
									model.setCounter(matchIdAndReturnCounter(sPData.optString("playerId"), messages.length()));
									model.setLastAccess(messagesData.optString("dateOfMsg")+" "+messagesData.optString("timeOfMsg"));
									model.setLastAccessDateTime(convertFormateDate(messagesData.optString("dateOfMsg"), 2, "dd-MM-yyyy") + " " + DateTimeUtility.convertTime(messagesData.optString("timeOfMsg")));
								}
								myMsgList.add(model);

							}

						}
					} else if (coachData.optString("status").equalsIgnoreCase("failure")) {
						response = "failure";
						ErrorMessage = coachData.optString("message");
					}

				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}
			if (response.equals("success")) {
				// Sort by km.
				Collections.sort(myMsgList, CoachDashboardModel.COMPARE_BY_Date);
			}
			return response;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {

				if (loader != null) {
					//llProgress.setVisibility(View.GONE);
					showOrHideLoader(false);
				}
				//lv.onRefreshComplete();
				if (result != null) {
					if (result.equalsIgnoreCase("success")) {
						ExpAdapter = new CoachDashboardAdapter(activity, R.layout.coach_dashboard_custom_lead_slot,
								myMsgList,CoachDashboardAdapter.VENUE);
						lv.setAdapter(ExpAdapter);
						comm.onUpdateCoin("success");
						//startIntroFirstTime();
						
					} else if (result.equalsIgnoreCase("failure")) {
						ArrayList<String> errorList = new ArrayList<String>();
						errorList.add("No message Available");
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
								R.layout.custom_error_slot, R.id.tv_error_text, errorList);
						lv.setAdapter(adapter);
						comm.onUpdateCoin("success");
						myMsgList=null;
					} else {
						showServerErrorAlertBox(ErrorMessage);
					}

				} else {

				}
			}
		}


		public String convertFormateDate(String Date, int type, String dateFormat) {
			String Day, middle, Month, Year;
			String finalDate = Date;
			if(Date!=null){
				if(Date.length()==10){
					if (type == 1) {
						Day = Date.substring(0, 2);
						middle = Date.substring(2, 3);
						Month = Date.substring(3, 5);
						Year = Date.substring(6, 10);

					} else {
						Year = Date.substring(0, 4);
						middle = Date.substring(4, 5);
						Month = Date.substring(5, 7);
						Day = Date.substring(8, 10);
					}

					switch (dateFormat) {
						case "dd-MM-yyyy":
							finalDate = Day + middle + Month + middle + Year;
							break;
						case "yyyy-MM-dd":
							finalDate = Year + middle + Month + middle + Day;
							break;
						case "MM-dd-yyyy":
							finalDate = Month + middle + Day + middle + Year;
							break;
						default:
							finalDate = "Date Format Incorrest";
					}
				}

			}

			return finalDate;
		}

//		private void startIntroFirstTime() {
//			if(LoadPref(Venue.venueIntro).equals("")){
//				CoachIntro coachIntro = new CoachIntro();
//				FragmentManager mannager = getFragmentManager();
//				FragmentTransaction transaction = mannager.beginTransaction();
//				transaction.replace(android.R.id.content, coachIntro, "coachIntro");
//				transaction.addToBackStack("coachIntro");
//				transaction.commit();
//				SavePref(Venue.venueIntro, "success");
//			}
//				
//		}
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				if(dialog!=null)dialog.dismiss();
				if (isOnline()) {
					loadMessageList = new LoadMessageList(getActivity(), "loader");
					loadMessageList.execute(AppSettings.receiveCoachMessages);
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	public int matchIdAndReturnCounter(String playerId, int currentCounter) {
		String cCounter = String.valueOf(currentCounter);

		Cursor c = dbManager.ViewVenueCounter(playerId);
		// Checking if records found
		if (c != null) {
			while (c.moveToNext()) {
				if (c.getInt(1) <= currentCounter) {
					// dbManager.UpdateCounterData(playerId, cCounter);
					return (currentCounter - c.getInt(1));
				} else {
					dbManager.UpdateVenueCounter(playerId, cCounter);
					return 0;
				}
			}
		} else {
			dbManager.InsertVenueCounter(playerId, 0);
			return currentCounter;
		}
		return 0;
	}

	
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getActivity().getApplicationContext());

			
			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 */
			loadMessageList = new LoadMessageList(getActivity(), "loader");
			loadMessageList.execute(AppSettings.receiveVenueMessages);
			
			
//			Toast.makeText(getActivity(), newMessage, Toast.LENGTH_SHORT).show();
			WakeLocker.release();
		}
	};

	
	@Override
	public void onDestroy() {

		super.onDestroy();
		dbManager.CloseDB();
		
		try {
			getActivity().unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(getActivity());
			GCMIntentService.notificationStatus = true;
		} catch (Exception e) {
			L.m("UnRegister Receiver Error"+ "> " + e.getMessage());
		}
	}
}
