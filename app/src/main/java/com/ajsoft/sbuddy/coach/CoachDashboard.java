package com.ajsoft.sbuddy.coach;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.ajsoft.sbuddy.GCMIntentService;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.model.CoachDashboardAdapter;
import com.ajsoft.sbuddy.model.CoachDashboardModel;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Coach;
import com.ajsoft.sbuddy.util.DBManager;
import com.ajsoft.sbuddy.util.DateTimeUtility;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.WakeLocker;
import com.google.android.gcm.GCMRegistrar;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CoachDashboard extends CompactFragment {

	ArrayList<CoachDashboardModel> myMsgList;
	LoadMessageList loadMessageList;
	LinearLayout llProgress;
	String JsonData;
	CoachDashboardAdapter ExpAdapter;
	OnCoachDashboardListener comm;
	ListView lv;
	DBManager dbManager;
	LinearLayout llTapToRefresh;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (loadMessageList != null) {
			loadMessageList.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (loadMessageList != null) {
			loadMessageList.onDetach();
		}
	}

	public void setCommunicator(OnCoachDashboardListener comm) {
		this.comm = comm;
	}

	public interface OnCoachDashboardListener {
		public void onCoachDashboardListener(String JsonData, String playerId, String playerImage);

		public void onUpdateCoin(String availableCoin);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.coach_dashboard, container, false);
		dbManager = new DBManager(getActivity());
		lv = (ListView) v.findViewById(R.id.lv_coach_dashboard);
		llTapToRefresh = (LinearLayout) v.findViewById(R.id.ll_tapToRefresh);
		llProgress = (LinearLayout) v.findViewById(R.id.ll_progressBar);


		llTapToRefresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				loadMessageList = new LoadMessageList(getActivity(), "loader");
				loadMessageList.execute(AppSettings.receiveCoachMessages);
			}
		});

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
				if (myMsgList != null) {
					comm.onCoachDashboardListener(JsonData, myMsgList.get(position).getPlayerId(),
							myMsgList.get(position).getImageUrl());
				}
			}
		});

		GCMIntentService.notificationStatus = true;
		getActivity().registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));

		loadMessageList = new LoadMessageList(getActivity(), "loader");
		loadMessageList.execute(AppSettings.receiveCoachMessages);
		return v;
	}

	private void showOrHideLoader(Boolean status) {
		if (status) {
			llProgress.setVisibility(View.VISIBLE);
			llTapToRefresh.setVisibility(View.GONE);
		} else {
			llProgress.setVisibility(View.GONE);
			llTapToRefresh.setVisibility(View.VISIBLE);
		}
	}

	private class LoadMessageList extends AsyncTask<String, Void, String> {

		private Activity activity;
		private String loader = "loader";

		public LoadMessageList(Activity activity, String loader) {
			this.activity = activity;
			this.loader = loader;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (loader != null) {
				showOrHideLoader(true);
			}
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error", response2 = "error";
			ErrorMessage = "No record available";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("coachId", LoadPref(Coach.CoachId)));
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], HttpReq.GET, params);
			response2 = ob.makeConnection(AppSettings.getAvailableCoins, HttpReq.POST, params);

			if (response2.contains("[")) {
				try {
					JSONArray arr = new JSONArray(response2);
					JSONObject obj = arr.getJSONObject(0);
					if (obj.optString("status").equalsIgnoreCase("success")) {
						if (activity != null) {
							SavePref(Coach.noOfCoins, obj.optString("message"));
						}
					} else if (obj.optString("status").equalsIgnoreCase("failure")) {
						ErrorMessage = obj.optString("message");
						//return null;
					}

				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response2;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response2);
				ErrorMessage = response;
				return null;
			}

			myMsgList = new ArrayList<>();
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					JSONObject coachData = message.getJSONObject(0);
					if (coachData.optString("status").equalsIgnoreCase("success")) {
						JSONArray mainArr = coachData.getJSONArray("message");
						JsonData = mainArr.toString();
						for (int k = 0; k < mainArr.length(); k++) {
							JSONObject sPData = mainArr.getJSONObject(k);
							if (!sPData.optString("playerId").equals("")) {
								response = "success";
								CoachDashboardModel model = new CoachDashboardModel();
								model.setPlayerId(sPData.optString("playerId"));
								model.setFullName(sPData.optString("fullName"));
								model.setGender(sPData.optString("gender"));
								model.setAge(sPData.optString("age"));
								model.setImageUrl(sPData.optString("imageUrl"));
								model.setMobileNumber(sPData.optString("mobileNumber"));
								model.setEmailId(sPData.optString("emailId"));
								JSONArray messages = sPData.getJSONArray("messages");
								JSONObject messagesData = messages.getJSONObject(messages.length() - 1);
								if(activity!=null){
									model.setCounter(matchIdAndReturnCounter(sPData.optString("playerId"), messages.length()));
									model.setLastAccess(messagesData.optString("dateOfMsg") + " "
											+ messagesData.optString("timeOfMsg"));
									model.setLastAccessDateTime(convertFormateDate(messagesData.optString("dateOfMsg"), 2, "dd-MM-yyyy") + " " + DateTimeUtility.convertTime(messagesData.optString("timeOfMsg")));
								}
								myMsgList.add(model);

							}

						}
					} else if (coachData.optString("status").equalsIgnoreCase("failure")) {
						response = "failure";
						ErrorMessage = coachData.optString("message");
					}

				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			if (response.equals("success")) {
				Collections.sort(myMsgList, CoachDashboardModel.COMPARE_BY_Date);

			}

			return response;

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {

				if (loader != null) {
					showOrHideLoader(false);

				}
				if (result != null) {
					if (result.equalsIgnoreCase("success")) {
						ExpAdapter = new CoachDashboardAdapter(activity, R.layout.coach_dashboard_custom_lead_slot,
								myMsgList,CoachDashboardAdapter.COACH);
						lv.setAdapter(ExpAdapter);
						comm.onUpdateCoin("success");

					} else if (result.equalsIgnoreCase("failure")) {
						ArrayList<String> errorList = new ArrayList<String>();
						errorList.add("No message Available");
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
								R.layout.custom_error_slot, R.id.tv_error_text, errorList);
						lv.setAdapter(adapter);
						comm.onUpdateCoin("success");
						myMsgList=null;
					} else {
//						Button retry = showBaseServerErrorAlertBox(LoadPref(Coach.CoachId)+"::"+result);
//						retry.setOnClickListener(new OnClickListener() {
//							@Override
//							public void onClick(View v) {
//								// Close dialog
//								if(dialog!=null)dialog.dismiss();
//								if (isOnline()) {
//									loadMessageList = new LoadMessageList(getActivity(), "loader");
//									loadMessageList.execute(AppSettings.receiveCoachMessages);
//								} else {
//									showInternetAlertBox(getActivity());
//								}
//							}
//						});
						Toast.makeText(getActivity(),result,Toast.LENGTH_LONG).show();
					}

				} else {

					Button retry = showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// Close dialog
							if(dialog!=null)dialog.dismiss();
							if (isOnline()) {
								loadMessageList = new LoadMessageList(getActivity(), "loader");
								loadMessageList.execute(AppSettings.receiveCoachMessages);
							} else {
								showInternetAlertBox(getActivity());
							}
						}
					});
				}

			}
		}

	}
	
	
	
	public String convertFormateDate(String Date, int type, String dateFormat) {
		String Day, middle, Month, Year;
		String finalDate = Date;
		if(Date!=null){
			 if(Date.length()==10){
				 if (type == 1) {
					 Day = Date.substring(0, 2);
					 middle = Date.substring(2, 3);
					 Month = Date.substring(3, 5);
					 Year = Date.substring(6, 10);

				 } else {
					 Year = Date.substring(0, 4);
					 middle = Date.substring(4, 5);
					 Month = Date.substring(5, 7);
					 Day = Date.substring(8, 10);
				 }

				 switch (dateFormat) {
					 case "dd-MM-yyyy":
						 finalDate = Day + middle + Month + middle + Year;
						 break;
					 case "yyyy-MM-dd":
						 finalDate = Year + middle + Month + middle + Day;
						 break;
					 case "MM-dd-yyyy":
						 finalDate = Month + middle + Day + middle + Year;
						 break;
					 default:
						 finalDate = "Date Format Incorrest";
				 }
			 }

		}

		return finalDate;
	}

	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getActivity().getApplicationContext());

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 */
			loadMessageList = new LoadMessageList(getActivity(), "loader");
			loadMessageList.execute(AppSettings.receiveCoachMessages);

			WakeLocker.release();
		}
	};

	public int matchIdAndReturnCounter(String playerId, int currentCounter) {
		String cCounter = String.valueOf(currentCounter);

		Cursor c = dbManager.ViewCoachCounter(playerId);
		// Checking if records found
		if (c != null) {
			while (c.moveToNext()) {
				if (c.getInt(1) <= currentCounter) {
					return (currentCounter - c.getInt(1));
				} else {
					dbManager.UpdateCoachCounter(playerId, cCounter);
					return 0;
				}
			}
		} else {
			dbManager.InsertCoachCounter(playerId, 0);
			return currentCounter;
		}
		return 0;
	}

	@Override
	public void onDestroy() {

		dbManager.CloseDB();
		try {
			getActivity().unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(getActivity());
			GCMIntentService.notificationStatus = true;
		} catch (Exception e) {
			L.m("UnRegister Receiver Error"+ "> " + e.getMessage());
		}
		super.onDestroy();
	}

}
