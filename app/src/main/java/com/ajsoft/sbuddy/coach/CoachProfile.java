package com.ajsoft.sbuddy.coach;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.FindCoachModel;
import com.ajsoft.sbuddy.util.Coach;
import com.ajsoft.sbuddy.util.L;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class CoachProfile extends CompactFragment {

	ImageView ivProfile;
	RatingBar rbRating;
	TextView tvReview, tvAbout, tvCoachingLocation, tvCoachingFee;
	LinearLayout llEditProfile;
	ImageView ivCat1, ivCat2, ivCat3, ivCat4;
	TextView tvCat1, tvCat2, tvCat3, tvCat4;
	ImageLoader imageLoader;
	ShowProfileTask showProfileTask;
	ArrayList<FindCoachModel> cList;
	String review, about, rating;
	StringBuilder sbLocation, sbFees;
	ScrollView mainLayout;
	Animation animMove;
	FragmentManager mannager;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (showProfileTask != null) {
			showProfileTask.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (showProfileTask != null) {
			showProfileTask.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.coach_profile, container, false);
		imageLoader = new ImageLoader(getActivity());
		mannager = getFragmentManager();
		setActionBarOption(v);
		
		initAnim();
		ivProfile = (ImageView) v.findViewById(R.id.iv_coach_profile_pic);
		tvReview = (TextView) v.findViewById(R.id.tv_coach_profile_review);
		tvAbout = (TextView) v.findViewById(R.id.tv_coach_profile_about);
		tvCoachingLocation = (TextView) v.findViewById(R.id.tv_coach_profile_location);
		tvCoachingFee = (TextView) v.findViewById(R.id.tv_coach_profile_fee);
		rbRating = (RatingBar) v.findViewById(R.id.rb_coach_profile_rating);
		llEditProfile = (LinearLayout) v.findViewById(R.id.ll_coach_profile_editrofile);
		mainLayout = (ScrollView) v.findViewById(R.id.scrollView1);

		ivCat1 = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat1);
		ivCat2 = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat2);
		ivCat3 = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat3);
		ivCat4 = (ImageView) v.findViewById(R.id.iv_main_find_coaches_detail_profile_sportCat4);
		tvCat1 = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_sportName1);
		tvCat2 = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_sportName2);
		tvCat3 = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_sportName3);
		tvCat4 = (TextView) v.findViewById(R.id.tv_main_find_coaches_detail_profile_sportName4);
		llEditProfile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), "Please contact admin", Toast.LENGTH_SHORT).show();
			}
		});
		Coach.setCoachProfileImage(getActivity(), imageLoader, ivProfile);
		
		
		showProfileTask = new ShowProfileTask(getActivity());
		showProfileTask.execute(LoadPref(Coach.CoachJsonData));

		return v;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
		TextView tvTitle=(TextView)view.findViewById(R.id.actionbar_title);
		tvTitle.setText(LoadPref(Coach.Name));
		ActionBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("coachProfile", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) { 
				mannager.popBackStack("coachDashboard", 0);
			}
		});
	}
	private void initAnim() {
		animMove = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down); 
	}

	private class ShowProfileTask extends AsyncTask<String, Void, ArrayList<FindCoachModel>> {

		Activity activity;

		public ShowProfileTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected ArrayList<FindCoachModel> doInBackground(String... params) {
			// TODO Auto-generated method stub
			String response, locality = null, fee = null;

			cList = new ArrayList<FindCoachModel>();
			sbFees = new StringBuilder();
			sbLocation = new StringBuilder();
			response = params[0];
			// L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray main = new JSONArray(response);
					for (int k = 0; k < main.length(); k++) {
						JSONObject sub = main.getJSONObject(k);
						review = sub.optString("review");
						rating = sub.optString("rating");
						about = sub.optString("about");
						JSONArray locArr = sub.getJSONArray("location");
						for (int l = 0; l < locArr.length(); l++) {
							JSONObject locObj = locArr.getJSONObject(l);
							if (!locObj.optString("locality").equals("")) {
								if (locality == null) {
									locality = locObj.optString("locality") + " \n\n";
								} else {
									locality += locObj.optString("locality") + " \n\n";
								}
							}
						}
						sbLocation.append(locality);
						JSONArray sportPlay = sub.getJSONArray("sportPlay");
						L.m(sportPlay.toString());
						for (int i = 0; i < sportPlay.length(); i++) {
							JSONObject sPData = sportPlay.getJSONObject(i);
							if (!sPData.optString("sportName").equals("")) {
								FindCoachModel model = new FindCoachModel();
								model.setSportId(sPData.optString("sportId"));
								model.setSportName(sPData.optString("sportName"));
								model.setLogo(sPData.optString("logo"));
								if (!sPData.optString("Fee").equals("")) {
									if (fee == null) {
										fee = sPData.optString("sportName") + " - " + sPData.optString("Fee") + " \n\n";
									} else {
										fee += sPData.optString("sportName") + " - " + sPData.optString("Fee")
												+ " \n\n";
									}
								}
								model.setFee(sPData.optString("Fee"));
								model.setClosedDay(sPData.optString("closedDay"));
								cList.add(model);
							}
						}
						sbFees.append(fee);

					}
				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return cList;
		}

		@Override
		protected void onPostExecute(ArrayList<FindCoachModel> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (activity != null) {
				if (result != null) {
					
					if (review != null) {
						tvReview.setText(review);
					}
					if (rating != null) {
						rbRating.setRating(Float.parseFloat(rating));
					}
					if (about != null) {
						tvAbout.setText(about);
					}
					tvCoachingLocation.setText(sbLocation.toString());
					tvCoachingFee.setText(sbFees.toString());

					int counter = 1;
					for (FindCoachModel ob : result) {
						setSport(counter, ob.getLogo(), ob.getSportName());
						counter++;
					}

					
					mainLayout.setVisibility(View.VISIBLE);
					mainLayout.startAnimation(animMove);
				}

			}
		}

		private void setSport(int pos, String logo, String sportName) {
			switch (pos) {
			case 1:
				ivCat1.setVisibility(View.VISIBLE);
				tvCat1.setVisibility(View.VISIBLE);
				imageLoader.DisplayImage(logo, ivCat1, "blank");
				tvCat1.setText(sportName);

				break;
			case 2:
				ivCat2.setVisibility(View.VISIBLE);
				tvCat2.setVisibility(View.VISIBLE);
				imageLoader.DisplayImage(logo, ivCat2, "blank");
				tvCat2.setText(sportName);

				break;
			case 3:
				ivCat3.setVisibility(View.VISIBLE);
				tvCat3.setVisibility(View.VISIBLE);
				imageLoader.DisplayImage(logo, ivCat3, "blank");
				tvCat3.setText(sportName);

				break;
			case 4:
				ivCat4.setVisibility(View.VISIBLE);
				tvCat4.setVisibility(View.VISIBLE);
				imageLoader.DisplayImage(logo, ivCat4, "blank");
				tvCat4.setText(sportName);

				break;

			}
		}
	}

}
