package com.ajsoft.sbuddy.coach;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Validation;

import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class CoachRegistration extends CompactFragment implements View.OnTouchListener {
    EditText etName, etGenderAge, etEmailTelephone, etSportCoachingProvided, etFeeDetail, etAchievementAbout,
            etCoachingLocation, etAgeGroup, etWeeklyOff, etOtherInfo;
    LinearLayout llSubmit;
    RegisterRequist registerRequist;
    ProgressBar progressBar;
    FragmentManager mannager;
    Boolean isSoftKeyboardDisplayed = false;

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (registerRequist != null) {
            registerRequist.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (registerRequist != null) {
            registerRequist.onDetach();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.coach_registration, container, false);
        mannager = getFragmentManager();
        setActionBarOption(v);
        isSoftKeyboardDisplayed = false;

        etName = (EditText) v.findViewById(R.id.et_register_name);
        etGenderAge = (EditText) v.findViewById(R.id.et_register_genderAge);
        etEmailTelephone = (EditText) v.findViewById(R.id.et_register_emailTelephone);
        etSportCoachingProvided = (EditText) v.findViewById(R.id.et_register_coachingProvided);
        etFeeDetail = (EditText) v.findViewById(R.id.et_register_feeDetail);
        etAchievementAbout = (EditText) v.findViewById(R.id.et_register_achievementsAbout);
        etCoachingLocation = (EditText) v.findViewById(R.id.et_register_coachingLocation);
        etAgeGroup = (EditText) v.findViewById(R.id.et_register_ageGroup);
        etWeeklyOff = (EditText) v.findViewById(R.id.et_register_weeklyOff);
        etOtherInfo = (EditText) v.findViewById(R.id.et_register_otherInfo);
        llSubmit = (LinearLayout) v.findViewById(R.id.ll_register_submit);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

        setTypsFace(etName, etGenderAge, etEmailTelephone, etSportCoachingProvided, etFeeDetail);
        setTypsFace(etAchievementAbout, etCoachingLocation, etAgeGroup, etWeeklyOff, etOtherInfo);

        etName.setOnTouchListener(this);
        etGenderAge.setOnTouchListener(this);
        etOtherInfo.setOnTouchListener(this);

        llSubmit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    if (isSoftKeyboardDisplayed) {
                        hideKeybord();
                    }

                    registerRequist = new RegisterRequist(getActivity(), etName.getText().toString()
                            , etGenderAge.getText().toString(), etEmailTelephone.getText().toString()
                            , etSportCoachingProvided.getText().toString(), etFeeDetail.getText().toString()
                            , etAchievementAbout.getText().toString(), etCoachingLocation.getText().toString()
                            , etAgeGroup.getText().toString(), etWeeklyOff.getText().toString()
                            , etOtherInfo.getText().toString());
                    registerRequist.execute(AppSettings.setCoachRegistrationRequest);
                }
            }
        });

        return v;
    }

    public void setActionBarOption(View view) {
        LinearLayout ActionBack, ActionHome;
        ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
        ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
        ActionBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mannager.popBackStack();
            }
        });
        ActionHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mannager.popBackStack();
            }
        });
    }

    private boolean checkValidation() {
        boolean ret = true;

        if (!Validation.isName(etName, true)) {
            ret = false;
        } else if (!Validation.isEmpty(etGenderAge, true)) {
            ret = false;
        } else if (!Validation.isEmpty(etEmailTelephone, true)) {
            ret = false;
        } else if (!Validation.isEmpty(etSportCoachingProvided, true)) {
            ret = false;
        } else if (!Validation.isEmpty(etFeeDetail, true)) {
            ret = false;
        } else if (!Validation.isEmpty(etCoachingLocation, true)) {
            ret = false;
        }

        return ret;
    }

    private class RegisterRequist extends AsyncTask<String, Void, String> {


        private Activity activity;
        String coachName, coachGenderAge, coachEmailTel,
                sportCoaching, coachingFee, achievements, locations,
                ageGroup, weeklyOff, otherInfo;

        public RegisterRequist(Activity activity, String coachName, String coachGenderAge, String coachEmailTel,
                               String sportCoaching, String coachingFee, String achievements, String locations,
                               String ageGroup, String weeklyOff, String otherInfo) {
            onAttach(activity);
            this.coachName = coachName;
            this.coachGenderAge = coachGenderAge;
            this.coachEmailTel = coachEmailTel;
            this.sportCoaching = sportCoaching;
            this.coachingFee = coachingFee;
            this.achievements = achievements;
            this.locations = locations;
            this.ageGroup = ageGroup;
            this.weeklyOff = weeklyOff;
            this.otherInfo = otherInfo;

        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
            llSubmit.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... urls) {
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("coachName", coachName));
            params.add(new BasicNameValuePair("coachGenderAge", coachGenderAge));
            params.add(new BasicNameValuePair("coachEmailTel", coachEmailTel));
            params.add(new BasicNameValuePair("sportCoaching", sportCoaching));
            params.add(new BasicNameValuePair("coachingFee", coachingFee));
            params.add(new BasicNameValuePair("achievements", achievements));
            params.add(new BasicNameValuePair("locations", locations));
            params.add(new BasicNameValuePair("ageGroup", ageGroup));
            params.add(new BasicNameValuePair("weeklyOff", weeklyOff));
            params.add(new BasicNameValuePair("otherInfo", otherInfo));

            HttpReq ob = new HttpReq();

            return ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);

        }

        @Override
        protected void onPostExecute(String result) {

            if (activity != null) {
                progressBar.setVisibility(View.GONE);
                llSubmit.setVisibility(View.VISIBLE);
                if (result != null) {
                    L.m(result);
                    if (result.equalsIgnoreCase("success")) {
                        showReviewAlertSuccess(getActivity(), "informCoach", 0);
                    } else {
                        Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public boolean onTouch(View arg0, MotionEvent arg1) {
        // TODO Auto-generated method stub
        isSoftKeyboardDisplayed = true;
        return false;
    }

}
