package com.ajsoft.sbuddy.coach;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.ajsoft.sbuddy.GCMIntentService;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.model.CoachDashboardModel;
import com.ajsoft.sbuddy.model.CoachVenueInboxAdapter;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Coach;
import com.ajsoft.sbuddy.util.DBManager;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.WakeLocker;
import com.google.android.gcm.GCMRegistrar;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CoachInbox extends CompactFragment {

	String jsonData, sltPlayerId, sltPlayerImage;
	ArrayList<CoachDashboardModel> myMsgList;
	LoadMessageList loadMessageList;
	CoachVenueInboxAdapter ExpAdapter;
	LinearLayout ll_reply, llTapToRefresh;
	SendMessage sendMessage;
	OnCoachInboxListener comm;
	ListView lv;
	LinearLayout llProgress;
	UpdateMessageList updateMessageList;
	DBManager dbManager;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (loadMessageList != null) {
			loadMessageList.onAttach(activity);
		}
		if (sendMessage != null) {
			sendMessage.onAttach(activity);
		}
		if (updateMessageList != null) {
			updateMessageList.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (loadMessageList != null) {
			loadMessageList.onDetach();
		}
		if (sendMessage != null) {
			sendMessage.onDetach();
		}
		if (updateMessageList != null) {
			updateMessageList.onDetach();
		}
	}

	public void setData(String jsonData, String playerId, String playerImage) {
		this.jsonData = jsonData;
		this.sltPlayerId = playerId;
		this.sltPlayerImage = playerImage;

	}

	public void setConnecter(OnCoachInboxListener comm) {
		this.comm = comm;
	}

	public interface OnCoachInboxListener {
		public void onCoachInboxListener(String availableCoins);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.coach_inbox, container, false);
		dbManager = new DBManager(getActivity());
		// lvMain = (ListView) v.findViewById(R.id.lv_dashboard);
		ll_reply = (LinearLayout) v.findViewById(R.id.ll_coach_inbox_reply);
		lv = (ListView) v.findViewById(R.id.lv_coach_inbox);
		llTapToRefresh = (LinearLayout) v.findViewById(R.id.ll_tapToRefresh);
		llProgress = (LinearLayout) v.findViewById(R.id.ll_progressBar);

		GCMIntentService.notificationStatus = true;
		getActivity().registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));


		llTapToRefresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				updateMessageList=new UpdateMessageList(getActivity());
				 updateMessageList.execute(AppSettings.receivePlayerCoachMessages);
			}
		});

		ll_reply.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showMessageAlert(getActivity());
			}
		});

		if (jsonData != null) {
			loadMessageList = new LoadMessageList(getActivity());
			loadMessageList.execute(jsonData);
		}

		return v;
	}
	private void showOrHideLoader(Boolean status) {
		if(status){
			llProgress.setVisibility(View.VISIBLE);
			llTapToRefresh.setVisibility(View.GONE);
		}else{
			llProgress.setVisibility(View.GONE);
			llTapToRefresh.setVisibility(View.VISIBLE);
		}
	}

	private class LoadMessageList extends AsyncTask<String, Void, ArrayList<CoachDashboardModel>> {

		private Activity activity;

		public LoadMessageList(Activity activity) {
			this.activity = activity;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showOrHideLoader(true);
		}

		@Override
		protected ArrayList<CoachDashboardModel> doInBackground(String... urls) {
			String response = "error";
			ErrorMessage = "No record available";
			myMsgList = new ArrayList<>();

			response = urls[0];
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					for (int i = 0; i < message.length(); i++) {
						JSONObject coachData = message.getJSONObject(i);
						if (coachData.optString("playerId").equalsIgnoreCase(sltPlayerId)) {
							JSONArray mainArr = coachData.getJSONArray("messages");
							if (activity != null) {
								dbManager.UpdateCoachCounter(sltPlayerId, String.valueOf(mainArr.length()));
							}
							for (int k = 0; k < mainArr.length(); k++) {
								JSONObject sPData = mainArr.getJSONObject(k);
								if (!sPData.optString("message").equals("")) {
									CoachDashboardModel model = new CoachDashboardModel();
									model.setMessageId(sPData.optString("id"));
									model.setMessage(sPData.optString("message"));
									model.setDateOfMsg(sPData.optString("dateOfMsg"));
									model.setTimeOfMsg(sPData.optString("timeOfMsg"));
									if (sPData.optString("status").equalsIgnoreCase("s")) {
										model.setStatus("r");
										model.setImageUrl(sltPlayerImage);
									} else {
										model.setStatus("s");
									}
									myMsgList.add(model);

								}

							}
						} else if (coachData.optString("status").equalsIgnoreCase("failure")) {
							ErrorMessage = coachData.optString("message");
							return null;
						}
					}

				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			return myMsgList;

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(ArrayList<CoachDashboardModel> result) {
			if (activity != null) {

				showOrHideLoader(false);
				if (result != null) {
					ExpAdapter = new CoachVenueInboxAdapter(getActivity(), CoachVenueInboxAdapter.COACH,
							R.layout.coach_dashboard_custom_lead_slot, myMsgList);
					lv.setAdapter(ExpAdapter);
				} else {
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add(ErrorMessage);
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lv.setAdapter(adapter);
				}
			}
		}
	}

	private class UpdateMessageList extends AsyncTask<String, Void, ArrayList<CoachDashboardModel>> {

		private Activity activity;

		public UpdateMessageList(Activity activity) {
			this.activity = activity;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showOrHideLoader(true);
		}

		@Override
		protected ArrayList<CoachDashboardModel> doInBackground(String... urls) {
			String response = "error";

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("coachId", LoadPref(Coach.CoachId)));
				params.add(new BasicNameValuePair("playerId", sltPlayerId));
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 1, params);

			myMsgList = new ArrayList<>();
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					JSONObject coachData = message.getJSONObject(0);

					JSONArray messageArr = coachData.getJSONArray("message");
					JSONObject messageObj = messageArr.getJSONObject(0);
					String CoachName = messageObj.optString("coachName");

					JSONArray mainArr = messageObj.getJSONArray("message");
					if(activity!=null){
						dbManager.UpdateCoachCounter(sltPlayerId, String.valueOf(mainArr.length()));
					}
					for (int k = 0; k < mainArr.length(); k++) {
						JSONObject sPData = mainArr.getJSONObject(k);

						CoachDashboardModel model = new CoachDashboardModel();
						model.setMessageId(sPData.optString("id"));
						model.setMessage(sPData.optString("message"));
						model.setDateOfMsg(sPData.optString("dateOfMsg"));
						model.setTimeOfMsg(sPData.optString("timeOfMsg"));
						if (sPData.optString("status").equalsIgnoreCase("s")) {
							model.setStatus("r");
							model.setImageUrl(sltPlayerImage);
						} else {
							model.setStatus("s");
						}
						myMsgList.add(model);


					}
				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return myMsgList;

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(ArrayList<CoachDashboardModel> result) {
			if (activity != null) {
				showOrHideLoader(false);
				if (result != null) {
					ExpAdapter = new CoachVenueInboxAdapter(getActivity(), CoachVenueInboxAdapter.COACH,
							R.layout.coach_dashboard_custom_lead_slot, myMsgList);
					lv.setAdapter(ExpAdapter);
				} else {
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lv.setAdapter(adapter);
				}
			}
		}
	}

	public void addNewReviewInList(String message, String dateOfMsg, String timeOfMsg) {

		CoachDashboardModel model = new CoachDashboardModel();
		model.setMessage(message);
		model.setDateOfMsg(dateOfMsg);
		model.setTimeOfMsg(timeOfMsg);
		model.setStatus("s");
		myMsgList.add(model);
		if (ExpAdapter != null) {
			ExpAdapter.notifyDataSetChanged();
		}
		dbManager.UpdateCoachCounter(sltPlayerId, String.valueOf(myMsgList.size()));

	}

	public void showMessageAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custorm_find_coach_sendmessage, null);
		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendmessage_send);
		final EditText etReview = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendmessage);

		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				sendMessage = new SendMessage(getActivity(), etReview.getText().toString());
				sendMessage.execute(AppSettings.sendC2PMessage);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	private class SendMessage extends AsyncTask<String, Void, String> {

		private Activity activity;
		String message;

		public SendMessage(Activity activity, String message) {
			this.message = message;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("coachId", LoadPref(Coach.CoachId)));
				params.add(new BasicNameValuePair("playerId", sltPlayerId));
				params.add(new BasicNameValuePair("message", message));

			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], HttpReq.POST, params);

			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray arr = new JSONArray(response);
					JSONObject obj = arr.getJSONObject(0);
					if (obj.optString("status").equals("success")) {
						response = "success";
						JSONObject message = obj.getJSONObject("message");
						if (message.optString("lastConnectStatus").equalsIgnoreCase("N")) {
							SavePref(Coach.noOfCoins, message.optString("availableCoins"));
						}
					} else if (obj.optString("status").equals("failure")) {
						response = "failure";
						ErrorMessage = obj.optString("message");
					}
				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = response;
				return null;
			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						addNewReviewInList(message, getDateMDY(), viewTime());
						comm.onCoachInboxListener("success");
					} else {
						addNewReviewInList(message, getDateMDY(), viewTime());
						showDefaultAlertBox(getActivity(), "Alert!!!", ErrorMessage);
					}
				} else {
					showDefaultAlertBox(getActivity(), "Your device is offline.", ErrorMessage);
				}
			}
		}
	}

	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getActivity().getApplicationContext());

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 */
			updateMessageList = new UpdateMessageList(getActivity());
			updateMessageList.execute(AppSettings.receivePlayerCoachMessages);
			WakeLocker.release();
		}
	};

	@Override
	public void onDestroy() {

		super.onDestroy();
		dbManager.CloseDB();
		try {
			getActivity().unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(getActivity());
			GCMIntentService.notificationStatus = true;
		} catch (Exception e) {
			L.m("UnRegister Receiver Error"+ "> " + e.getMessage());
		}
	}
}
