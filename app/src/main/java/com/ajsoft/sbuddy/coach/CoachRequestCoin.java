package com.ajsoft.sbuddy.coach;
 


import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.Coach;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Validation;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

public class CoachRequestCoin extends CompactFragment {
	EditText etRequestCoin;
	Button btnSubmit;
	RequestCoinTask requestCoinTask;
	ProgressBar progressBar;
	FragmentManager mannager; 
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (requestCoinTask != null) {
			requestCoinTask.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (requestCoinTask != null) {
			requestCoinTask.onDetach();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.coach_venue_request_coin, container, false);
		mannager = getFragmentManager();
		setActionBarOption(v);
		
		etRequestCoin = (EditText) v.findViewById(R.id.et_coach_venue_request_coin);
		btnSubmit = (Button) v.findViewById(R.id.btn_coach_venue_request_submit);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		
		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(checkValidation()){
					requestCoinTask = new RequestCoinTask(getActivity());
					requestCoinTask.execute(AppSettings.requestCoachCoins);
				}
			}
		});
		
		
		
		return v;
	}
	
	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
		ActionBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("coachRequestCoin", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		}); 
		ActionHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainRSSFeed", 0);
			}
		}); 
	}
	
	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isEmpty(etRequestCoin, true)) {
			ret = false;
		}
		return ret;
	}

	

	
	private class RequestCoinTask extends AsyncTask<String, Void, String> {

		private Activity activity;

		public RequestCoinTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			btnSubmit.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response;
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				//sportId,choosePlan, chooseDate, chooseSlot, playerId, venueId
				params.add(new BasicNameValuePair("coachId", LoadPref(Coach.CoachId)));
				params.add(new BasicNameValuePair("noOfCoins", etRequestCoin.getText().toString()));
				
			}

			HttpReq ob = new HttpReq();
			

			return response = ob.makeConnection(urls[0], HttpReq.POST, params,HttpReq.EXECUTE_TASK);
			
		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				progressBar.setVisibility(View.GONE);
				btnSubmit.setVisibility(View.VISIBLE);
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						showReviewAlertSuccess(getActivity(),"requestCoachVenueCoin",4); 
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_error_msg),
							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}
	
}


