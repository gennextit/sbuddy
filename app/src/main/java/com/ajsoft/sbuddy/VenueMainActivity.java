package com.ajsoft.sbuddy;

import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Coach;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Venue;
import com.ajsoft.sbuddy.util.WakeLocker;
import com.ajsoft.sbuddy.venue.VenueDashboard;
import com.ajsoft.sbuddy.venue.VenueDashboard.OnVenueDashboardListener;
import com.ajsoft.sbuddy.venue.VenueInbox;
import com.google.android.gcm.GCMRegistrar;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class VenueMainActivity extends BaseActivity implements OnVenueDashboardListener{
	LinearLayout llMenu, llShadow, llDashbord, llInbox,llNavDrawer;
	LinearLayout llDashbordLine, llInboxLine;
	ImageView ivDashbord, ivInbox;
	TextView tvDashbord, tvInbox,ActionBarTitle,tvCoinCounter;
	FragmentManager mannager;
	ImageLoader imageLoader;
	int TAB_DASHBOARD=1,TAB_INDEX=2,SWITCH;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_venue);
		hideActionBar();
		imageLoader=new ImageLoader(this);
		// setActionBarOption();
		mannager = getFragmentManager();
		GCMIntentService.notificationStatus = true;
		registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));

		initUi();
		SetDrawer(this,llNavDrawer,imageLoader,VENUE);
		
		tvCoinCounter.setText(LoadPref(Venue.noOfCoins));
		
	}

	private void initUi() {
		llMenu = (LinearLayout) findViewById(R.id.ll_main_menu_bottom);
		llShadow = (LinearLayout) findViewById(R.id.ll_main_shadow);
		llDashbord = (LinearLayout) findViewById(R.id.ll_main_venue_dashbord);
		llDashbordLine = (LinearLayout) findViewById(R.id.ll_main_venue_dashbord_line);
		llInbox = (LinearLayout) findViewById(R.id.ll_main_venue_inbox);
		llInboxLine = (LinearLayout) findViewById(R.id.ll_main_venue_inbox_line);
		llNavDrawer = (LinearLayout) findViewById(R.id.ll_nav_drawer);
		ActionBarTitle = (TextView) findViewById(R.id.actionbar_title);

		ivDashbord = (ImageView) findViewById(R.id.iv_main_venue_dashbord);
		ivInbox = (ImageView) findViewById(R.id.iv_main_venue_inbox);
		tvDashbord = (TextView) findViewById(R.id.tv_main_venue_dashbord);
		tvInbox = (TextView) findViewById(R.id.tv_main_venue_inbox);
		tvCoinCounter = (TextView) findViewById(R.id.actionbar_coin_counter);

		llDashbord.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(TAB_DASHBOARD);
				setScreenDynamically(TAB_DASHBOARD);
			}
		});
//		llInbox.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				mannager.popBackStack("venueDashboard", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//				switchTab(TAB_INDEX);
//				setScreenDynamically(TAB_INDEX);
//			}
//		});
		switchTab(TAB_DASHBOARD);
		setScreenDynamically(TAB_DASHBOARD);

	}

	private void switchTab(int key) {

		switch (key) {
		case 1:
			ActionBarTitle.setText("DASHBOARD");
			llDashbordLine.setVisibility(View.VISIBLE);
			llInboxLine.setVisibility(View.INVISIBLE);
			ivDashbord.setColorFilter(Color.WHITE);
			tvDashbord.setTextColor(Color.WHITE);
			ivInbox.setColorFilter(getResources().getColor(R.color.c_tab_icon));
			tvInbox.setTextColor(getResources().getColor(R.color.c_tab_icon));
			break;
		case 2:
			ActionBarTitle.setText("INBOX");
			llDashbordLine.setVisibility(View.INVISIBLE);
			llInboxLine.setVisibility(View.VISIBLE);
			ivDashbord.setColorFilter(getResources().getColor(R.color.c_tab_icon));
			tvDashbord.setTextColor(getResources().getColor(R.color.c_tab_icon));
			ivInbox.setColorFilter(Color.WHITE);
			tvInbox.setTextColor(Color.WHITE);
			break;

		}

	}

	private void setScreenDynamically(int position) {
		setScreenDynamically(position, null, null, null);
	}

	private void setScreenDynamically(int position, String sltPlayerId, String sltPlayerImage, String jsonData) {
		FragmentTransaction transaction;
		switch (position) {
		case 1:
			VenueDashboard venueDashboard = new VenueDashboard();
			venueDashboard.setCommunicator(VenueMainActivity.this);
			transaction = mannager.beginTransaction();
			transaction.add(R.id.group_main_venue, venueDashboard, "venueDashboard");
			//transaction.addToBackStack("venueDashboard");
			transaction.commit();

			break;
		case 2:
			VenueInbox venueInbox = new VenueInbox();
			//coachInbox.setCommunicator(CoachMainActivity.this);
			venueInbox.setData(jsonData, sltPlayerId, sltPlayerImage);
			transaction = mannager.beginTransaction();
			transaction.add(R.id.group_main_venue, venueInbox, "venueInbox");
			//transaction.addToBackStack("coachInbox");
			transaction.commit();

			break;
		
		}
	}

	@Override
	public void onBackPressed() {
		if (mannager.getBackStackEntryCount() > 1) {
			int count = mannager.getBackStackEntryCount();
			mannager.popBackStack();
			if (count == 2) {
				// ActionBarHeading.setText("HOME");
				llMenu.setVisibility(View.VISIBLE);
				llShadow.setVisibility(View.VISIBLE);
				switchTab(TAB_DASHBOARD);
			}
		} else {
			L.m("MainActivity" + "nothing on backstack, calling super");
			super.onBackPressed();
		}
	}

	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getApplicationContext());

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 */
			WakeLocker.release();
		}
	};

	@Override
	public void onDestroy() {

		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
			GCMIntentService.notificationStatus = true;
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	}

	@Override
	public void onVenueDashboardListener(String JsonData, String playerId, String playerImage) {
		switchTab(TAB_INDEX);
		setScreenDynamically(TAB_INDEX,playerId,playerImage,JsonData);
	}

	@Override
	public void onUpdateCoin(String availableCoin) {
		tvCoinCounter.setText(LoadPref(Venue.noOfCoins));
	}

}
