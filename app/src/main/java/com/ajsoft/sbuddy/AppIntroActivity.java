package com.ajsoft.sbuddy;

import com.ajsoft.sbuddy.fragments.intro.AppIntro1;
import com.ajsoft.sbuddy.fragments.intro.AppIntro2;
import com.ajsoft.sbuddy.fragments.intro.AppIntro3;
import com.ajsoft.sbuddy.fragments.intro.AppIntro4;
import com.ajsoft.sbuddy.icomm.Communicator;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Base;
import com.ajsoft.sbuddy.util.L;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AppIntroActivity extends Base implements Communicator{
	FragmentManager mannager;
	private int position=1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.app_intro_activity);
		mannager=getFragmentManager();
		setScreenDynamically(position);
		
	}

	private void setScreenDynamically(int position){
		FragmentTransaction transaction;
		switch(position){
		
		case 1:
			AppIntro1 appIntro1=new AppIntro1();
			appIntro1.setCommunicator(AppIntroActivity.this);
			transaction=mannager.beginTransaction();
			transaction.add(R.id.group_app_intro,appIntro1 ,"appIntro1");
			transaction.addToBackStack("appIntro1"); 
			transaction.commit();
			break;
		case 2:
			AppIntro2 appIntro2=new AppIntro2();
			appIntro2.setCommunicator(AppIntroActivity.this);
			transaction=mannager.beginTransaction();
			transaction.add(R.id.group_app_intro,appIntro2 ,"appIntro2");
			transaction.addToBackStack("appIntro2");
			transaction.commit();
			break;
		case 3:
			AppIntro3 appIntro3=new AppIntro3();
			appIntro3.setCommunicator(AppIntroActivity.this);
			transaction=mannager.beginTransaction();
			transaction.add(R.id.group_app_intro,appIntro3 ,"appIntro3");
			transaction.addToBackStack("appIntro3");
			transaction.commit();
			
			break;
		case 4:
			
			AppIntro4 appIntro4=new AppIntro4();
			appIntro4.setCommunicator(AppIntroActivity.this);
			transaction=mannager.beginTransaction();
			transaction.add(R.id.group_app_intro,appIntro4 ,"appIntro4");
			transaction.addToBackStack("appIntro4");
			transaction.commit();
			
			break;
		case 5:
			SavePref(AppTokens.SessionIntro, "success");
			Intent ob=new Intent(AppIntroActivity.this,SignUp.class);
			startActivity(ob);
			finish();
			break;
		}
		
	}

	@Override
	public void respond(int position) {
		
		setScreenDynamically(position);
	}
	
	@Override
	public void onBackPressed() {
		L.m(String.valueOf(mannager.getBackStackEntryCount()));
		if (mannager.getBackStackEntryCount() >1) {
			mannager.popBackStack();
			 
		} else {
			L.m("MainActivity" + "nothing on backstack, calling super");
			super.onBackPressed();
			finish();
		}
	}
}
