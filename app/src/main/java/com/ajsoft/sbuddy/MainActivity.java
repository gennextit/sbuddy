package com.ajsoft.sbuddy;

import com.ajsoft.sbuddy.fragments.MainNotification;
import com.ajsoft.sbuddy.fragments.MainNotification.OnNotificationCounterChangeListener;
import com.ajsoft.sbuddy.fragments.MainRSSFeed;
import com.ajsoft.sbuddy.fragments.MainMessage.OnMessageCounterChangeListener;
import com.ajsoft.sbuddy.fragments.coaches.MainFindCoaches;
import com.ajsoft.sbuddy.fragments.coaches.MainFindCoaches.OnFindCoachesListener;
import com.ajsoft.sbuddy.fragments.coaches.MainFindCoachesDetail;
import com.ajsoft.sbuddy.fragments.coaches.MainFindCoachesDetailReviews.OnCoachReviewCount;
import com.ajsoft.sbuddy.fragments.coaches.MainFindCoachesView;
import com.ajsoft.sbuddy.fragments.sbuddy.MainFindSbuddyz;
import com.ajsoft.sbuddy.fragments.sbuddy.MainFindSbuddyz.OnFindSbuddyzListener;
import com.ajsoft.sbuddy.fragments.sbuddy.MainFindSbuddyzView;
import com.ajsoft.sbuddy.fragments.venu.MainFindVenuDetail;
import com.ajsoft.sbuddy.fragments.venu.MainFindVenuDetailReviews.OnVenueReviewCount;
import com.ajsoft.sbuddy.fragments.venu.MainFindVenue;
import com.ajsoft.sbuddy.fragments.venu.MainFindVenue.OnFindVenueListener;
import com.ajsoft.sbuddy.fragments.venu.MainFindVenueView;
import com.ajsoft.sbuddy.service.GetNotificationServices;
import com.ajsoft.sbuddy.service.SmsVerifyService;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.WakeLocker;
import com.google.android.gcm.GCMRegistrar;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends BaseActivity implements OnFindSbuddyzListener, OnFindCoachesListener, OnFindVenueListener, OnCoachReviewCount, OnVenueReviewCount, OnNotificationCounterChangeListener, OnMessageCounterChangeListener {
    LinearLayout llMenu, llShadow, llFsbuddyz, llFCoaches, llFVenues, llCEvent;
    LinearLayout llFsbuddyzLine, llFCoachesLine, llFVenuesLine, llCEventLine;
    ImageView ivFsbuddyz, ivFCoaches, ivFVenues, ivCEvent;
    FragmentManager mannager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideActionBar();
        //setActionBarOption();
        mannager = getFragmentManager();
        GCMIntentService.notificationStatus = true;
        registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));


        initUi();
    }

    private void initUi() {
        llMenu = (LinearLayout) findViewById(R.id.ll_main_menu_bottom);
        llShadow = (LinearLayout) findViewById(R.id.ll_main_shadow);
        llFsbuddyz = (LinearLayout) findViewById(R.id.ll_main_fsbuddyz);
        llFsbuddyzLine = (LinearLayout) findViewById(R.id.ll_main_fsbuddyz_line);
        llFCoaches = (LinearLayout) findViewById(R.id.ll_main_fCoaches);
        llFCoachesLine = (LinearLayout) findViewById(R.id.ll_main_fCoaches_line);
        llFVenues = (LinearLayout) findViewById(R.id.ll_main_fVenues);
        llFVenuesLine = (LinearLayout) findViewById(R.id.ll_main_fVenues_line);
        llCEvent = (LinearLayout) findViewById(R.id.ll_main_cEvents);
        llCEventLine = (LinearLayout) findViewById(R.id.ll_main_cEvents_line);

        ivFsbuddyz = (ImageView) findViewById(R.id.iv_main_findSbuddy);
        ivFCoaches = (ImageView) findViewById(R.id.iv_main_searchCoach);
        ivFVenues = (ImageView) findViewById(R.id.iv_main_searchVenue);
        ivCEvent = (ImageView) findViewById(R.id.iv_main_createEvent);

        llFsbuddyz.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mannager.popBackStack("mainRSSFeed", 0);
                switchTab(1);
                setScreenDynamically(1);
            }
        });
        llFCoaches.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mannager.popBackStack("mainRSSFeed", 0);
                switchTab(2);
                setScreenDynamically(3);
            }
        });
        llFVenues.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mannager.popBackStack("mainRSSFeed", 0);
                switchTab(3);
                setScreenDynamically(5);
            }
        });
        llCEvent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //mannager.popBackStack("mainRSSFeed", 0);
                mannager.popBackStack("mainRSSFeed", 0);
                switchTab(4);
                setScreenDynamically(7);
            }
        });
        setScreenDynamically(0);

//		if(GCMIntentService.notifyMsgOrNotification.equals("msg")){
//			
//		}else if(GCMIntentService.notifyMsgOrNotification.equals("noti")){
//			setScreenDynamically(9);
//		}
    }


    public void SwitchTab() {
        switchTab(0);
    }


    private void switchTab(int key) {

        switch (key) {
            case 0:
                llFsbuddyzLine.setVisibility(View.INVISIBLE);
                llFCoachesLine.setVisibility(View.INVISIBLE);
                llFVenuesLine.setVisibility(View.INVISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));
                break;
            case 1:
                llFsbuddyzLine.setVisibility(View.VISIBLE);
                llFCoachesLine.setVisibility(View.INVISIBLE);
                llFVenuesLine.setVisibility(View.INVISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_act));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));

                break;
            case 2:
                llFsbuddyzLine.setVisibility(View.INVISIBLE);
                llFCoachesLine.setVisibility(View.VISIBLE);
                llFVenuesLine.setVisibility(View.INVISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_active));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));
                break;
            case 3:
                llFsbuddyzLine.setVisibility(View.INVISIBLE);
                llFCoachesLine.setVisibility(View.INVISIBLE);
                llFVenuesLine.setVisibility(View.VISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_activate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));
                break;
            case 4:
                llFsbuddyzLine.setVisibility(View.INVISIBLE);
                llFCoachesLine.setVisibility(View.INVISIBLE);
                llFVenuesLine.setVisibility(View.INVISIBLE);
                llCEventLine.setVisibility(View.INVISIBLE);
                ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
                ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
                ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
                ivCEvent.setImageDrawable(getResources().getDrawable(R.drawable.create_venue_inactive));
                break;

        }

    }

    private void setScreenDynamically(int position) {
        setScreenDynamically(position, null, null, null, null);
    }

    private void setScreenDynamically(int position, String sltSportId) {
        setScreenDynamically(position, sltSportId, null, null, null);
    }

    private void setScreenDynamically(int position, String sltSportId, String searchData) {
        setScreenDynamically(position, sltSportId, null, null, searchData);
    }

    private void setScreenDynamically(int position, String sltSportId, String sltSportName, String sltSportBaseImage, String searchData) {
        FragmentTransaction transaction;
        switch (position) {
            case 0:
                //ActionBarHeading.setText("HOME");
                // llMenu.setVisibility(View.GONE);
                // llShadow.setVisibility(View.GONE);
                //MainRSSFeed mainRSSFeed1 = (MainRSSFeed) mannager.findFragmentByTag("mainRSSFeed");
                transaction = mannager.beginTransaction();
//			if (mainRSSFeed1 == null) {
                MainRSSFeed mainRSSFeed = new MainRSSFeed();
                //mainRSSFeed.setCommunicator(MainActivity.this);
                transaction.add(R.id.group_main, mainRSSFeed, "mainRSSFeed");
                transaction.addToBackStack("mainRSSFeed");
                transaction.commit();
//			} else {
//				if (mainRSSFeed1.isVisible()) {
////					//llFsbuddyzLine.setVisibility(View.INVISIBLE);
////					//ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
////					//mainRSSFeed1.setCommunicator(MainActivity.this);
////					tr0.detach(mainRSSFeed1);
////					tr0.addToBackStack("detachRSSFeed");
////					tr0.commit();
//
//				} else {
//					//llFsbuddyzLine.setVisibility(View.VISIBLE);
//					//ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_act));
//					//mainRSSFeed1.setCommunicator(MainActivity.this);
//					transaction.attach(mainRSSFeed1);
//					transaction.addToBackStack("attachRSSFeed");
//					transaction.commit();
//				}
//			}
                //hideFragmentVisible();

                break;
            case 1:
                //ActionBarHeading.setText("SEARCH SBUDDYZ");
                // llMenu.setVisibility(View.GONE);
                // llShadow.setVisibility(View.GONE);
                //MainFindSbuddyz mainFindSbuddyz1 = (MainFindSbuddyz) mannager.findFragmentByTag("mainFindSbuddyz");
                transaction = mannager.beginTransaction();
//			if (mainFindSbuddyz1 == null) {
                MainFindSbuddyz mainFindSbuddyz = new MainFindSbuddyz();
                mainFindSbuddyz.setCommunicator(MainActivity.this);
                transaction.add(R.id.group_main, mainFindSbuddyz, "mainFindSbuddyz");
                transaction.addToBackStack("mainFindSbuddyz");
                transaction.commit();
//			} else {
//				if (mainFindSbuddyz1.isVisible()) {
//					llFsbuddyzLine.setVisibility(View.INVISIBLE);
//					ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
//					FragmentTransaction trFsbuddyz = mannager.beginTransaction();
//					mainFindSbuddyz1.setCommunicator(MainActivity.this);
//					trFsbuddyz.detach(mainFindSbuddyz1);
//					trFsbuddyz.addToBackStack("detachFindSbuddyz");
//					trFsbuddyz.commit();
//
//				}
////				else {
//					llFsbuddyzLine.setVisibility(View.VISIBLE);
//					ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_act));
//					FragmentTransaction trFsbuddyz1 = mannager.beginTransaction();
//					mainFindSbuddyz1.setCommunicator(MainActivity.this);
//					trFsbuddyz1.attach(mainFindSbuddyz1);
//					trFsbuddyz1.addToBackStack("attachFindSbuddyz");
//					trFsbuddyz1.commit();
////				}
//			}
                //hideFragmentVisible();

                break;
            case 2:
                //ActionBarHeading.setText("SBUDDYZ");
                // llMenu.setVisibility(View.VISIBLE);
                // llShadow.setVisibility(View.VISIBLE);

                MainFindSbuddyzView mainFindSbuddyzView = new MainFindSbuddyzView();
                // mainSearchSbuddyz.setCommunicator(AppIntroActivity.this);
                mainFindSbuddyzView.setSelectSport(sltSportId, sltSportName);
                mainFindSbuddyzView.setSearchBuddyData(searchData);
                mainFindSbuddyzView.setSportBaseImage(sltSportBaseImage);
                transaction = mannager.beginTransaction();
                transaction.add(R.id.group_main, mainFindSbuddyzView, "mainSearchSbuddyzView");
                transaction.addToBackStack("mainFindSbuddyzView");
                transaction.commit();
                //switchTab(0);
                break;
            case 3:
                //ActionBarHeading.setText("SEARCH COACHES");
                // llMenu.setVisibility(View.GONE);
                // llShadow.setVisibility(View.GONE);

                MainFindCoaches mainFindCoaches1 = (MainFindCoaches) mannager.findFragmentByTag("mainFindCoaches");
                transaction = mannager.beginTransaction();
//			if (mainFindCoaches1 == null) {
                MainFindCoaches mainFindCoaches = new MainFindCoaches();
                mainFindCoaches.setCommunicator(MainActivity.this);
                transaction.add(R.id.group_main, mainFindCoaches, "mainFindCoaches");
                transaction.addToBackStack("mainFindCoaches");
                transaction.commit();
//			} else {
//				if (mainFindCoaches1.isVisible()) {
//					llFCoachesLine.setVisibility(View.INVISIBLE);
//					ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
//					mainFindCoaches1.setCommunicator(MainActivity.this);
//					tr3.detach(mainFindCoaches1);
//					tr3.addToBackStack("detachFindCoaches");
//					tr3.commit();
//				}
////				else {
//					llFCoachesLine.setVisibility(View.VISIBLE);
//					ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_active));
//					FragmentTransaction trFCoaches = mannager.beginTransaction();
//					mainFindCoaches1.setCommunicator(MainActivity.this);
//					trFCoaches.attach(mainFindCoaches1);
//					trFCoaches.addToBackStack("attachFindCoaches");
//					trFCoaches.commit();
////				}
//			}

                //hideFragmentVisible();

                break;

            case 4:
                //ActionBarHeading.setText("SBUDDYZ");
                // llMenu.setVisibility(View.VISIBLE);
                // llShadow.setVisibility(View.VISIBLE);

                MainFindCoachesView mainFindCoachesView = new MainFindCoachesView();
                mainFindCoachesView.setSelectSport(sltSportId, sltSportName);
                mainFindCoachesView.setSearchCoachData(searchData);
                mainFindCoachesView.setSportBaseImage(sltSportBaseImage);
                transaction = mannager.beginTransaction();
                transaction.add(R.id.group_main, mainFindCoachesView, "mainFindCoachesView");
                transaction.addToBackStack("mainFindSbuddyzView");
                transaction.commit();
                //switchTab(0);
                break;
            case 5:
                //ActionBarHeading.setText("SEARCH VENUES");
                // llMenu.setVisibility(View.GONE);
                // llShadow.setVisibility(View.GONE);

                MainFindVenue MainFindVenue1 = (MainFindVenue) mannager.findFragmentByTag("mainFindVenue");
                transaction = mannager.beginTransaction();
//			if (MainFindVenue1 == null) {
                MainFindVenue mainFindVenue = new MainFindVenue();
                mainFindVenue.setCommunicator(MainActivity.this);
                transaction.add(R.id.group_main, mainFindVenue, "mainFindVenue");
                transaction.addToBackStack("mainFindVenue");
                transaction.commit();
//			} else {
//				if (MainFindVenue1.isVisible()) {
//					llFVenuesLine.setVisibility(View.INVISIBLE);
//					ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
//					MainFindVenue1.setCommunicator(MainActivity.this);
//					tr5.detach(MainFindVenue1);
//					tr5.addToBackStack("detachFindVenue");
//					tr5.commit();
//				} 
////				else {
//					llFVenuesLine.setVisibility(View.VISIBLE);
//					ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_activate));
//					FragmentTransaction trFVenues = mannager.beginTransaction();
//					MainFindVenue1.setCommunicator(MainActivity.this);
//					trFVenues.attach(MainFindVenue1);
//					trFVenues.addToBackStack("attachFindVenue");
//					trFVenues.commit();
////				}
//			}

                //hideFragmentVisible();

                break;

            case 6:
                //ActionBarHeading.setText("SBUDDYZ");
                // llMenu.setVisibility(View.VISIBLE);
                // llShadow.setVisibility(View.VISIBLE);

                MainFindVenueView mainFindVenueView = new MainFindVenueView();
                // mainSearchSbuddyz.setCommunicator(AppIntroActivity.this);
                mainFindVenueView.setSelectSport(sltSportId, sltSportName);
                mainFindVenueView.setSearchVenueData(searchData);
                mainFindVenueView.setSportBaseImage(sltSportBaseImage);
                transaction = mannager.beginTransaction();
                transaction.add(R.id.group_main, mainFindVenueView, "mainFindVenueView");
                transaction.addToBackStack("mainFindVenueView");
                transaction.commit();
                //switchTab(0);
                break;
            case 7:

//			llMenu.setVisibility(View.GONE);
//			llShadow.setVisibility(View.GONE);

                Intent ob = new Intent(MainActivity.this, MainEvent.class);
                startActivity(ob);

//			transaction = mannager.beginTransaction(); 
//			MainEvent createEvent = new MainEvent();
//			//createEvent.setCommunicator(MainActivity.this);
//			transaction.replace(R.id.group_main, createEvent, "mainEvent");
//			transaction.addToBackStack("mainEvent");
//			transaction.commit();

                break;

            case 9:
                MainNotification mainNotification = new MainNotification();
                // mainSearchSbuddyz.setCommunicator(AppIntroActivity.this);
//			mainNotification.setSelectSport(sltSportId,sltSportName);
//			mainNotification.setSearchVenueData(searchData);
//			mainNotification.setSportBaseImage(sltSportBaseImage);
                transaction = mannager.beginTransaction();
                transaction.add(R.id.group_main, mainNotification, "mainNotification");
                //transaction.addToBackStack("mainNotification");
                transaction.commit();

                break;
        }
    }

    private Boolean isFragmentVisible(String FragmentTag) {
        if (getFragmentManager().findFragmentByTag(FragmentTag) != null
                && getFragmentManager().findFragmentByTag(FragmentTag).isVisible()) {
            return true;
        }
        return false;
    }

//	private void hideFragmentVisible() {
//		FragmentTransaction tr = mannager.beginTransaction();
//		if (getFragmentManager().findFragmentByTag("mainFindSbuddyz") != null
//				&& getFragmentManager().findFragmentByTag("mainFindSbuddyz").isVisible()) {
//			tr.detach(getFragmentManager().findFragmentByTag("mainFindSbuddyz"));
//			tr.commit();
//			llFsbuddyzLine.setVisibility(View.INVISIBLE);
//			ivFsbuddyz.setImageDrawable(getResources().getDrawable(R.drawable.search_sbuddy_inact));
//			
//		}
//		if (getFragmentManager().findFragmentByTag("mainFindCoaches") != null
//				&& getFragmentManager().findFragmentByTag("mainFindCoaches").isVisible()) {
//			tr.detach(getFragmentManager().findFragmentByTag("mainFindCoaches"));
//			tr.commit();
//			llFCoachesLine.setVisibility(View.INVISIBLE);
//			ivFCoaches.setImageDrawable(getResources().getDrawable(R.drawable.search_coach_inact));
//			
//		}
//		if (getFragmentManager().findFragmentByTag("mainFindVenue") != null
//				&& getFragmentManager().findFragmentByTag("mainFindVenue").isVisible()) {
//			tr.detach(getFragmentManager().findFragmentByTag("mainFindVenue"));
//			tr.commit();
//			llFVenuesLine.setVisibility(View.INVISIBLE);
//			ivFVenues.setImageDrawable(getResources().getDrawable(R.drawable.service_venue_inactivate));
//			
//		}
//		if (getFragmentManager().findFragmentByTag(FragmentTag) != null
//				&& getFragmentManager().findFragmentByTag(FragmentTag).isVisible()) {
//			tr.detach(getFragmentManager().findFragmentByTag(FragmentTag));
//			tr.commit();
//		}

//	}

    @Override
    public void onFindSbuddyzListener(Boolean status, String sltSportId, String sltSportName, String sltSportBaseImage, String searchBuddyData) {
        setScreenDynamically(2, sltSportId, sltSportName, sltSportBaseImage, searchBuddyData);
    }

    @Override
    public void onFindCoachesListener(Boolean status, String sltSportId, String sltSportName, String sltSportBaseImage, String searchCoachData) {
        setScreenDynamically(4, sltSportId, sltSportName, sltSportBaseImage, searchCoachData);
    }

    @Override
    public void onFindVenueListener(Boolean status, String sltSportId, String sltSportName, String sltSportBaseImage, String searchVenueData) {
        setScreenDynamically(6, sltSportId, sltSportName, sltSportBaseImage, searchVenueData);
    }


    @Override
    public void onBackPressed() {
        if (mannager.getBackStackEntryCount() > 1) {
            //L.m("MainActivity" + "popping backstack: "+mannager.getBackStackEntryCount());
            int count = mannager.getBackStackEntryCount();
            //mannager.popBackStack("mainRSSFeed", 0);
            mannager.popBackStack();
            if (count == 2) {
                //ActionBarHeading.setText("HOME");
                llMenu.setVisibility(View.VISIBLE);
                llShadow.setVisibility(View.VISIBLE);
                switchTab(0);
            }
//			for(int i=count-1;i>=0;i--){
//				FragmentManager.BackStackEntry entry=mannager.getBackStackEntryAt(i);
//				L.m("BackButton pressed "+entry.getName());
//			}
        } else {
            L.m("MainActivity" + "nothing on backstack, calling super");
            super.onBackPressed();
            finish();
        }
    }

    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            /**
             * Take appropriate action on this message
             * depending upon your app requirement
             * For now i am just displaying it on the screen
             * */

            // Showing received message
            //Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
            // Releasing wake lock
            WakeLocker.release();
        }
    };

    public void getConnectRequest() {
        Intent grapprIntent = new Intent(MainActivity.this, GetNotificationServices.class);
        grapprIntent.putExtra(GetNotificationServices.URL, AppSettings.getConnectRequest);
        grapprIntent.putExtra(GetNotificationServices.PLAYER_ID, LoadPref(Buddy.PlayerId));
        startService(grapprIntent);

    }

    @Override
    public void onResume() {
        super.onResume();
        L.m("The onResume() event");
        GetNotificationServices.shouldContinue = true;

        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(GetNotificationServices.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(testReceiver, filter);

        getConnectRequest();


    }

    @Override
    public void onPause() {
        super.onPause();
        L.m("The onPause() event");
        GetNotificationServices.shouldContinue = false;

        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(testReceiver);
        // or `unregisterReceiver(testReceiver)` for a normal broadcast
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver testReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int resultCode = intent.getIntExtra(GetNotificationServices.NotiResultCode, RESULT_CANCELED);
            if (resultCode == RESULT_OK) {
                int NotiCounter = intent.getIntExtra(GetNotificationServices.NotiCounter, 0);
                setNotificationCounter();
            }
        }
    };

    public void setNotificationCounter(){
        MainRSSFeed mainRSSFeed = (MainRSSFeed) mannager.findFragmentByTag("mainRSSFeed");
        if (mainRSSFeed != null) {

            mainRSSFeed.setNotificationCounter(AppTokens.newNotificationPush.size()+AppTokens.newNotificationPull.size()+AppTokens.newChat.size());

        }
    }

    @Override
    public void onDestroy() {

        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(this);
            GCMIntentService.notificationStatus = true;
        } catch (Exception e) {
            L.m("UnRegister Receiver Error" + "> " + e.getMessage());
        }
        super.onDestroy();
    }

    @Override
    public void onCoachReviewCount(String review, String rating) {
        MainFindCoachesDetail mainFindCoachesDetail = (MainFindCoachesDetail) mannager.findFragmentByTag("mainFindCoachesDetail");
        if (mainFindCoachesDetail != null) {
            mainFindCoachesDetail.setRatingAndReview(rating, review);
        }
    }

    @Override
    public void onVenueReviewCount(String review, String rating) {
        MainFindVenuDetail mainFindVenuDetail = (MainFindVenuDetail) mannager.findFragmentByTag("mainFindVenuDetail");
        if (mainFindVenuDetail != null) {
            mainFindVenuDetail.setRatingAndReview(rating, review);
        }
    }

    @Override
    public void onNotificationCounterChangeListener(int NotiCounter) {
        MainRSSFeed mainRSSFeed = (MainRSSFeed) mannager.findFragmentByTag("mainRSSFeed");
        if (mainRSSFeed != null) {
            mainRSSFeed.setNotificationCounter(AppTokens.newNotificationPush.size()+AppTokens.newNotificationPull.size()+AppTokens.newChat.size());
        }
    }

    @Override
    public void onMessageCounterChangeListener(int MsgCounter) {
        MainRSSFeed mainRSSFeed = (MainRSSFeed) mannager.findFragmentByTag("mainRSSFeed");
        if (mainRSSFeed != null) {
            mainRSSFeed.setMessageCounter(MsgCounter);
        }
    }

//	@Override
//	public void onRSSFeedListener(Boolean status) {
//		llMenu.setVisibility(View.VISIBLE);
//		llShadow.setVisibility(View.VISIBLE);
//	}


}
