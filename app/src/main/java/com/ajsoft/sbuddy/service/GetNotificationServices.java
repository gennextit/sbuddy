package com.ajsoft.sbuddy.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.ajsoft.sbuddy.GCMIntentService;
import com.ajsoft.sbuddy.model.NotificationModel;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.HttpReqService;
import com.ajsoft.sbuddy.util.L;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Abhijit on 03/08/16.
 */
public class GetNotificationServices extends IntentService {

    public static final String ACTION = "com.ajsoft.sbuddy.service.GetNotificationServices";
    private String  MSG = "not Available";

    public static final String NotiResultCode="NotiResultCode",NotiCounter="NotiCounter",URL="url",PLAYER_ID="playerId";

    private static String TAG = GetNotificationServices.class.getSimpleName();

    public static volatile boolean shouldContinue = true;

    public GetNotificationServices() {
        super(GetNotificationServices.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String url = intent.getStringExtra(URL);
            String playerId = intent.getStringExtra(PLAYER_ID);
            if(shouldContinue){
                GetConnectRequest(url,playerId);
            }
        }
    }

    public void GetConnectRequest(String url,String playerId){
        new AsyncRequest(playerId).execute(url);
    }



    /**
     * Posting the OTP to server and activating the user
     *
     * param otp otp received in the SMS
     */
    public class AsyncRequest extends AsyncTask<String, Void, String> {
        String playerId;
        public AsyncRequest(String playerId) {
            this.playerId=playerId;

            /*Intent in = new Intent(ACTION);
            in.putExtra("CSResultCode", Activity.RESULT_OK);
            in.putExtra("otp", otp);
            in.putExtra("status", "otp");
            LocalBroadcastManager.getInstance(GetNotificationServices.this).sendBroadcast(in);*/

        }
        @Override
        protected String doInBackground(String... urls) {
            L.m(TAG + " doInBackground");
            String response=null;
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            params.add(new BasicNameValuePair("playerId", playerId));

            String result = "";

            HttpReqService ob = new HttpReqService();
            response = ob.makeConnection(urls[0], HttpReq.GET, params);
            String output = "not available";
            L.m(response);
            if (response.contains("[")) {
                try {
                    JSONArray json = new JSONArray(response);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        if (!obj.optString("senderId").equals("")) {
                            output = "success";
                            NotificationModel model = new NotificationModel();
                            model.setNotification("A buddy request has been sent to you by " + obj.optString("senderName"));
                            model.setRequestId(obj.optString("senderId"));
                            model.setRequestedName(obj.optString("senderName"));
                            model.setRequestedStatue("request");
                            int flagCount=0;
                            for (int j = 0; j <  AppTokens.newNotificationPull.size(); j++) {
                                if (AppTokens.newNotificationPull.get(j).getRequestId().equalsIgnoreCase(obj.optString("senderId"))) {
                                    if(AppTokens.newNotificationPull.get(j).getRequestedStatue().equals("request")){
                                        flagCount=1;
                                    }
                                }
                            }
                            if(flagCount==0){
                                AppTokens.newNotificationPull.add(model);
                                GCMIntentService.notificationCounter++;
                            }
                        } else if (!obj.optString("status").equals("")) {
                            output = "failure";
                            //ErrorMessage = obj.optString("message");
                        }
                    }


                } catch (JSONException e) {
                    L.m("Json Error :" + e.toString());
                    MSG = e.toString() + "\n" + response;
                    return null;
                }
            } else {
                L.m("Server Error : " + response);
                MSG = response;
                return null;
            }

            return output;


        }

        @Override
        protected void onPostExecute(String result) {


            if (result!=null) {
                L.m(TAG + result);
                if(result.equals("success")){
                    GetNotificationServices.shouldContinue = false;
                    Intent in = new Intent(ACTION);
                    in.putExtra(NotiResultCode, Activity.RESULT_OK);
                    in.putExtra(NotiCounter, GCMIntentService.notificationCounter++);
                    LocalBroadcastManager.getInstance(GetNotificationServices.this).sendBroadcast(in);
                }
//                else{
//                    Toast.makeText(GetNotificationServices.this,MSG,Toast.LENGTH_SHORT).show();
//                }

            } else {
                L.m(MSG);
            }

        }

    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        L.m(TAG+" onDestroy");

    }
}
