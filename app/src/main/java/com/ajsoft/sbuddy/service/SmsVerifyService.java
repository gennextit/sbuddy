package com.ajsoft.sbuddy.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;

import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
 

/**
 * Created by Abhijit on 04/04/15.
 */
public class SmsVerifyService extends IntentService {
	
	public static final String ACTION = "com.ajsoft.sbuddy.service.SmsVerifyService";
	private String  MSG = "not Available";
	
    private static String TAG = SmsVerifyService.class.getSimpleName();
    
    public static volatile boolean shouldContinue = true;
    
    public SmsVerifyService() {
        super(SmsVerifyService.class.getSimpleName());
    }

    @Override
     protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String otp = intent.getStringExtra("otp");
			String mobile = intent.getStringExtra("mobile");
			String url = intent.getStringExtra("url");
			String playerId = intent.getStringExtra("playerId");
			if(shouldContinue){
				verifyMobile(otp,mobile,url,playerId);
            }
        }
    }

	public void verifyMobile(String otp,String mobile,String url,String playerId){
		new AsyncRequest(otp,mobile,playerId).execute(url);
	}



    /**
     * Posting the OTP to server and activating the user
     *
     * param otp otp received in the SMS
     */
    public class AsyncRequest extends AsyncTask<String, Void, String> {
    	String otp,mobile,playerId;
    	public AsyncRequest(String otp,String mobile,String playerId) {
			this.otp=otp;
			this.mobile=mobile;
			this.playerId=playerId;
			
			Intent in = new Intent(ACTION);
			in.putExtra("CSResultCode", Activity.RESULT_OK);
			in.putExtra("otp", otp); 
			in.putExtra("status", "otp"); 
			LocalBroadcastManager.getInstance(SmsVerifyService.this).sendBroadcast(in);

		}
		@Override
		protected String doInBackground(String... urls) {
			L.m(TAG + " doInBackground");
			String output=null;
			int counterFlag = 0;
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("mobileNumber",mobile));
			params.add(new BasicNameValuePair("otp", otp));
			params.add(new BasicNameValuePair("playerId", playerId));

			String result = "";

			HttpReq ob = new HttpReq();
			result = ob.makeConnection(urls[0], HttpReq.POST, params);
			L.m(result);
			if (result.contains("[")) {

				try {
					JSONArray data = new JSONArray(result);
					JSONObject key = data.getJSONObject(0);
					if (!key.optString("status").equals("")&&key.optString("status").equals("success")) { 
						Utility.SavePref(getApplicationContext(),Buddy.PlayerId, key.optString("message"));
						output=key.optString("status");
					}else if(key.optString("status").equals("failure")){
						output=key.optString("message");
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					output=e.toString();
					L.m(e.toString());
				}

			} else {
				output=result;
				L.m(result);
			} 
				
			return output;
			

		}
 
		@Override
		protected void onPostExecute(String result) {

			L.m(TAG + result);

			if (result!=null) {
				Intent in = new Intent(ACTION);
				in.putExtra("CSResultCode", Activity.RESULT_OK);
				in.putExtra("otp", otp); 
				in.putExtra("status", result); 
				LocalBroadcastManager.getInstance(SmsVerifyService.this).sendBroadcast(in);

			} else {
				L.m(MSG);
			}

		}

	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		L.m(TAG+" onDestroy"); 
		
	}
}
