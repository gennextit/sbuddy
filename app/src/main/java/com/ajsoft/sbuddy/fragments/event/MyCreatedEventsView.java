package com.ajsoft.sbuddy.fragments.event;

import java.util.ArrayList;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.MyEventModel;
import com.ajsoft.sbuddy.model.MyEventViewSlider;
import com.ajsoft.sbuddy.util.AutoScrollViewPager;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyCreatedEventsView extends CompactFragment {
	ImageView ivSportSelect;
	TextView tvEventName;
	LinearLayout tabAbout, tabLocation, tabContact, llJoin, llInvite;
	FragmentManager mannager;
	ImageLoader ivLoader;
	TextView ActionBarHeading;
	ImageView ActionBack, ActionHome;
	// ConnectRequest connectRequest;
	String eventType,eventStartDate, eventEndDate, eventStartTime, eventEndTime, discription, percentageAge, regFee,
			lastDateOfReg;
	String EventAddress, EventLocation, Direction;
	// String Website,ContactNo,Email;
	String Website, fbUrl, regUrl, pPerson, pContactNo, pEmail, sPerson, spContactNo, sEmail;

	String EventName, EventImage, SportImage;
	AutoScrollViewPager viewPager;
	ArrayList<MyEventModel> imgList;

	public void setEventData(String EventName, String EventImage, String SportImage, ArrayList<MyEventModel> imgList) {
		this.EventName = EventName;
		this.EventImage = EventImage;
		this.SportImage = SportImage;
		this.imgList = imgList;
	}

	public void setAboutData(String eventType,String eventStartDate, String eventEndDate, String eventStartTime, String eventEndTime,
			String discription, String percentageAge, String regFee, String lastDateOfReg) {
		this.eventType=eventType;
		this.eventStartDate = eventStartDate;
		this.eventEndDate = eventEndDate;
		this.eventStartTime = eventStartTime;
		this.eventEndTime = eventEndTime;
		this.discription = discription;
		this.percentageAge = percentageAge;
		this.regFee = regFee;
		this.lastDateOfReg = lastDateOfReg;
	}

	public void setLocationData(String EventAddress, String EventLocation, String Direction) {
		this.EventAddress = EventAddress;
		this.EventLocation = EventLocation;
		this.Direction = Direction;
	}

	// public void setContactData(String Website,String ContactNo,String Email){
	// this.Website=Website;
	// this.ContactNo=ContactNo;
	// this.Email=Email;
	// }
	public void setContactData(String Website, String fbUrl, String regFee, String regUrl, String pPerson,
			String pContactNo, String pEmail, String sPerson, String spContactNo, String sEmail) {
		this.Website = Website;
		this.fbUrl = fbUrl;
		this.regFee = regFee;
		this.regUrl = regUrl;
		this.pPerson = pPerson;
		this.pContactNo = pContactNo;
		this.pEmail = pEmail;
		this.sPerson = sPerson;
		this.spContactNo = spContactNo;
		this.sEmail = sEmail;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.my_created_events_view, container, false);
		mannager = getFragmentManager();
		setActionBarOption(v);
//		ivEventImage = (ImageView) v.findViewById(R.id.iv_event_view_eventImages);
		ivSportSelect = (ImageView) v.findViewById(R.id.iv_event_view_eventSportSelect);
		tvEventName = (TextView) v.findViewById(R.id.tv_event_view_eventName);
//		tvEventLocation = (TextView) v.findViewById(R.id.tv_event_view_eventLocation);
//		tvEventDate = (TextView) v.findViewById(R.id.tv_event_view_eventCreatedDate);
		tabAbout = (LinearLayout) v.findViewById(R.id.ll_event_view_eventAboutTab);
		tabLocation = (LinearLayout) v.findViewById(R.id.ll_event_view_eventLocationTab);
		tabContact = (LinearLayout) v.findViewById(R.id.ll_event_view_eventContactTab);
		llJoin = (LinearLayout) v.findViewById(R.id.ll_event_view_eventJoinButton);
		// llBreak = (LinearLayout) v.findViewById(R.id.ll_footer_break);
		llInvite = (LinearLayout) v.findViewById(R.id.ll_event_view_eventInviteButton);
		viewPager = (AutoScrollViewPager) v.findViewById(R.id.myviewpager);

		ivLoader = new ImageLoader(getActivity());

		if (EventName != null) {
			tvEventName.setText(EventName);
		}
//		if (EventLocation != null) {
//			tvEventLocation.setText(EventLocation);
//		}
//		if (eventStartDate != null) {
//			tvEventDate.setText(eventStartDate);
//		}
		if (imgList != null) {
			// ivLoader.DisplayImage(EventImage, ivEventImage, "profile");
			MyEventViewSlider myPagerAdapter = new MyEventViewSlider(getActivity(), imgList);
			viewPager.setAdapter(myPagerAdapter);
			viewPager.setInterval(5000);
			viewPager.startAutoScroll();
		}
		if (SportImage != null) {
			ivLoader.DisplayImage(SportImage, ivSportSelect, "blank");
			// ivCat1.setBackgroundResource(R.drawable.rounded_green);
			// ivCat1.setBackgroundResource(R.drawable.rounded_green);
		}

		tabAbout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(1);
				setScreenDynamically(1);
			}
		});
		tabLocation.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switchTab(2);
				setScreenDynamically(2);
			}
		});
		tabContact.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(3);
				setScreenDynamically(3);
			}
		});

		switchTab(1);
		setScreenDynamically(1);

		return v;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		ActionBarHeading = (TextView) view.findViewById(R.id.actionbar_title);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("myCreatedEventsView", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switch (MyCreatedEvents.SWITCH) {
				case 1:
					mannager.popBackStack("mainRSSFeed", 0);
					break;
				case 2:
					mannager.popBackStack("myCreatedEvents", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					break;
				case 3:
					mannager.popBackStack("myCreatedEvents", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					break;

				}
			}
		});
	}

	private void switchTab(int key) {

		switch (key) {
		case 0:
			tabAbout.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabContact.setBackgroundColor(getResources().getColor(R.color.main_grey));

			break;
		case 1:
			tabAbout.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabContact.setBackgroundColor(getResources().getColor(R.color.main_grey));

			break;
		case 2:
			tabAbout.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			tabContact.setBackgroundColor(getResources().getColor(R.color.main_grey));
			break;
		case 3:
			tabAbout.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabContact.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			break;
		}
	}

	private void setScreenDynamically(int position) {
		setScreenDynamically(position, null, null, null);
	}

	private void setScreenDynamically(int position, String JsonData, String playerId, String searchData) {
		FragmentTransaction transaction;
		switch (position) {
		case 1:

			ActionBarHeading.setText("ABOUT"); 
			
			MyCreatedEventsViewAbout myCreatedEventsViewAbout = new MyCreatedEventsViewAbout();
			myCreatedEventsViewAbout.setData(eventType,eventStartDate, eventEndDate, eventStartTime, eventEndTime, discription,
					percentageAge, lastDateOfReg);
			transaction = mannager.beginTransaction();
			transaction.replace(R.id.group_create_event_view, myCreatedEventsViewAbout, "myCreatedEventsViewAbout");
			transaction.commit();

			break;
		case 2:
			ActionBarHeading.setText("LOCATION");
			MyCreatedEventsViewLocation myCreatedEventsViewLocation = new MyCreatedEventsViewLocation();
			myCreatedEventsViewLocation.setData(EventAddress, EventLocation, Direction);
			transaction = mannager.beginTransaction();
			transaction.replace(R.id.group_create_event_view, myCreatedEventsViewLocation,
					"myCreatedEventsViewLocation");
			transaction.commit();

			break;
		case 3:
			ActionBarHeading.setText("CONTACT");
			MyCreatedEventsViewContact myCreatedEventsViewContact = new MyCreatedEventsViewContact();
			myCreatedEventsViewContact.setData(Website, fbUrl, regFee, regUrl, pPerson, pContactNo, pEmail, sPerson,
					spContactNo, sEmail);
			transaction = mannager.beginTransaction();
			transaction.replace(R.id.group_create_event_view, myCreatedEventsViewContact, "myCreatedEventsViewContact");
			transaction.commit();

			break;
		}
	}

	// private class ConnectRequest extends AsyncTask<String, Void, String> {
	//
	// private Activity activity;
	//
	// public ConnectRequest(Activity activity) {
	// onAttach(activity);
	// }
	//
	// public void onAttach(Activity activity) {
	// this.activity = activity;
	// }
	//
	// public void onDetach() {
	// this.activity = null;
	// }
	//
	// @Override
	// protected void onPreExecute() {
	// // TODO Auto-generated method stub
	// super.onPreExecute();
	// showPDialog(getActivity(), "Processing please wait");
	// }
	//
	// @Override
	// protected String doInBackground(String... urls) {
	// String response = "error";
	// List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
	// if (activity != null) {
	// // L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
	// params.add(new BasicNameValuePair("senderId", LoadPref(Buddy.PlayerId)));
	// params.add(new BasicNameValuePair("receiverId", playerId));
	// }
	//
	// HttpReq ob = new HttpReq();
	// response = ob.makeConnection(urls[0], 3, params);
	// String output = null;
	// L.m(response);
	// if (response.contains("[")) {
	// try {
	// JSONArray json = new JSONArray(response);
	// for (int i = 0; i < json.length(); i++) {
	// JSONObject obj = json.getJSONObject(i);
	// if (obj.optString("status").equals("success")) {
	// output = "success";
	//
	// } else if (obj.optString("status").equals("failure")) {
	// output = obj.optString("message");
	// }
	// }
	//
	// } catch (JSONException e) {
	// L.m("Json Error :" + e.toString());
	// return null;
	// // return e.toString();
	// }
	// } else {
	// L.m("Invalid JSON found : " + response);
	// // return response;
	// return null;
	// }
	//
	// return output;
	// // return "success";
	//
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	//
	// if (activity != null) {
	// dismissPDialog();
	// if (result != null) {
	// L.m(result);
	// if (result.equalsIgnoreCase("success")) {
	// showReviewAlertSuccess(getActivity(), "connect", playerName);
	// } else {
	// switch (result) {
	// case "A":
	// showDefaultAlertBox(getActivity(), "Alert!!!", "You are already connected
	// with this buddy");
	// //Toast.makeText(getActivity(), "You have already connected with this
	// buddy", Toast.LENGTH_SHORT).show();
	// break;
	// case "B":
	// showDefaultAlertBox(getActivity(), "Alert!!!", "You have been blocked by
	// this buddy");
	// //Toast.makeText(getActivity(), "You are blocked by this buddy",
	// Toast.LENGTH_SHORT).show();
	// break;
	// case "X":
	// showDefaultAlertBox(getActivity(), "Alert!!!", "Your request has been
	// rejected by this buddy");
	// //Toast.makeText(getActivity(), "Your request rejected by this buddy",
	// Toast.LENGTH_SHORT).show();
	// break;
	// case "R":
	// showDefaultAlertBox(getActivity(), "Alert!!!", "Your request has already
	// been sent to this buddy");
	// //Toast.makeText(getActivity(), "Your request rejected by this buddy",
	// Toast.LENGTH_SHORT).show();
	// break;
	// default:
	// Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
	// }
	// }
	// } else {
	// Toast.makeText(getActivity(),
	// getActivity().getResources().getString(R.string.internet_error_msg),
	// Toast.LENGTH_SHORT).show();
	// }
	// }
	// }
	// }

}
