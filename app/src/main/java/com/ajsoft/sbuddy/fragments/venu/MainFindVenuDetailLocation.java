package com.ajsoft.sbuddy.fragments.venu;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.util.L;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainFindVenuDetailLocation extends CompactFragment{
	
	String venuDetail="Cannnot Place",venueAddrs; 
	//String lat ,lng;
	String lat;
	String lng;
	// Google Map
	MapView mMapView;
	private GoogleMap googleMap;
	TextView tvAddrs;

    
	public void setData(String venuName,String venueAddrs,String lat,String lng){
		this.venuDetail=venuName;
		this.lat=lat;
		this.lng=lng;
		this.venueAddrs=venueAddrs;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v =inflater.inflate(R.layout.main_find_venu_detail_location, container,false);
		//L.m(venueAddrs);
		mMapView = (MapView) v.findViewById(R.id.mapView);
		tvAddrs = (TextView) v.findViewById(R.id.tv_main_find_venu_detail_location_address);
		if(venueAddrs!=null){
			if(!venueAddrs.equals("")){
				tvAddrs.setText(venueAddrs);
			}else{
				tvAddrs.setText("Not available");
			}
		}
	    mMapView.onCreate(savedInstanceState);

//	    mMapView.setSatellite(true); // Satellite View
//	    mMapView.setStreetView(true); // Street View
//	    mMapView.setTraffic(true);
	    mMapView.onResume();// needed to get the map to display immediately

	    try {
	        MapsInitializer.initialize(getActivity().getApplicationContext());
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    googleMap = mMapView.getMap(); 
//	    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//	    googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
//	    googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//	    googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
	    
	    //You can also enable or disable the zoom gestures in the map by calling the setZoomControlsEnabled(boolean) method
//	    googleMap.getUiSettings().setZoomGesturesEnabled(true);
	    if(lat!=null&&!lat.equals("")&&lng!=null&&!lng.equals("")){
	    	// Getting status
			int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

			// Showing status
			if (status == ConnectionResult.SUCCESS) {
			    showLocationOnMap();
			} else {
				Toast.makeText(getActivity(),
						"This app won't run without Google Map services, which are missing from your phone.",
						Toast.LENGTH_SHORT).show(); 
			}
	    }else{
	    	Toast.makeText(getActivity(), "Location not Available", Toast.LENGTH_SHORT).show();
	    }
	  
		return v;
	}
	
	private void showLocationOnMap() { 
		
		
		
		double latitude= Double.parseDouble(lat);
		double longitude=Double.parseDouble(lng);
		  // create marker
	    MarkerOptions marker = new MarkerOptions().position(
	            new LatLng(latitude, longitude)).title(venuDetail);

//	    // Changing marker icon
//	    marker.icon(BitmapDescriptorFactory
//	            .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

	    // adding marker
	    googleMap.addMarker(marker);
	    CameraPosition cameraPosition = new CameraPosition.Builder()
	            .target(new LatLng(latitude, longitude)).zoom(14).build();
	    googleMap.animateCamera(CameraUpdateFactory
	            .newCameraPosition(cameraPosition));

	    // Perform any camera updates here
		
	}

	@Override
	public void onResume() {
	    super.onResume();
	    mMapView.onResume();
	}

	@Override
	public void onPause() {
	    super.onPause();
	    mMapView.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    mMapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
	    super.onLowMemory();
	    mMapView.onLowMemory();
	}
 
}
