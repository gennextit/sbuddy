package com.ajsoft.sbuddy.fragments;

import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.image_cache.ImageLoader;

public class MainRSSFeedDetail extends CompactFragment{
	
	String blog,pubDate,creator,image;
	StringBuilder title;
	TextView tvPubDate;//,tvBlog,tvTitle,tvDescription,tvCreator;
	ImageView ivImage;
	ImageLoader imageLoader;
	FragmentManager mannager; 
	StringBuilder description,finalBlog;
	WebView webView;

	public void setData(String blog,String pubDate,StringBuilder title,StringBuilder description,String creator,String image){
		this.blog=blog;
		this.title=title;
		this.description=description;
		this.creator=creator;
		this.image=image;
		this.pubDate=pubDate;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v =inflater.inflate(R.layout.main_rss_feed_detail, container,false);
		imageLoader=new ImageLoader(getActivity());
		mannager = getFragmentManager();
		setActionBarOption(v);
		
//		tvBlog=(TextView)v.findViewById(R.id.tv_main_rss_feed_detail_blog);
//		tvTitle=(TextView)v.findViewById(R.id.tv_main_rss_feed_detail_title);
//		tvDescription=(TextView)v.findViewById(R.id.tv_main_rss_feed_detail_description);
//		tvCreator=(TextView)v.findViewById(R.id.tv_main_rss_feed_detail_createdBy);
//		tvCreator=(TextView)v.findViewById(R.id.tv_main_rss_feed_detail_createdBy);
		tvPubDate=(TextView)v.findViewById(R.id.tv_main_rss_feed_detail_publishDate);
		ivImage=(ImageView)v.findViewById(R.id.iv_main_rss_feed_detail_image);
		
//		if(blog!=null){
//			tvBlog.setText(blog);
//		}
//		L.m(pubDate);
		if(pubDate!=null){
//			if(pubDate.length()>25){
//				pubDate=pubDate.substring(0,25);
//			}
			tvPubDate.setText(pubDate);
		}
//		if(title!=null){
//			tvTitle.setText(title);
////			tvTitle
//		}
//		if(description!=null){
//			tvDescription.setText(description);
//		}
//		if(creator!=null){
//			tvCreator.setText(creator);
//		}else{
//			tvCreator.setVisibility(View.GONE);
//		}
		if(image!=null){
			imageLoader.DisplayImage(image, ivImage, "base_medium",false);
		}else{
			ivImage.setImageResource(R.drawable.sport_bg_small);
		}

		webView = (WebView) v.findViewById(R.id.webView_browser);

		//webView.setInitialScale(100);
//		webView.setWebChromeClient(new WebChromeClient() {
//			public void onProgressChanged(WebView view, int progress) {
//				progressBar.setProgress(progress);
//				if (progress == 100) {
//					progressBar.setVisibility(View.GONE);
//				} else {
//					progressBar.setVisibility(View.VISIBLE);
//				}
//			}
//		});
		// Javascript inabled on webview
		finalBlog=new StringBuilder();
		finalBlog.append("<strong>"+title+"</strong>");
		finalBlog.append("\n\n");
		finalBlog.append(description);

		webView.loadData(finalBlog.toString(), "text/html; charset=utf-8","utf-8");
		//startWebView(description.toString());
		return v;
	}


	private void startWebView(String url) {
		webView.setWebViewClient(new WebViewClient() {

			// If you will not use this method url links are opeen in new brower
			// not in webview
			public boolean shouldOverrideUrlLoading(WebView view, String url) {

					view.loadData(url, "text/html; charset=utf-8","utf-8");


				return true;
			}

			// Show loader on url load
			public void onLoadResource(WebView view, String url) {

			}

			public void onPageFinished(WebView view, String url) {

			}

		});



		// Javascript inabled on webview
		webView.getSettings().setJavaScriptEnabled(true);

		// Other webview options
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		//webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		//webView.setScrollbarFadingEnabled(false);

			webView.loadData(url, "text/html; charset=utf-8","utf-8");


	}


	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("BLOG AND NEWS FEEDS"));
		
		ActionBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainRSSFeedDetail", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		}); 
		ActionHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainRSSFeed", 0);
			}
		}); 
	}
}
