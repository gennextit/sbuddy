package com.ajsoft.sbuddy.fragments.intro;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.icomm.Communicator;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class AppIntro3 extends CompactFragment implements View.OnClickListener{
	Communicator comm;
	RelativeLayout RL;
	TextView tv;
	
	public void setCommunicator(Communicator communicator){
		this.comm=communicator;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		View view =inflater.inflate(R.layout.app_intro3,container, false);
		RL=(RelativeLayout)view.findViewById(R.id.ai_screen3);
//		tv=(TextView)view.findViewById(R.id.textView1);
		RL.setOnClickListener(this);
//		tv.setText(setBoldFont(R.color.walkthrough3,Typeface.BOLD,getResources().getString(R.string.walkthrough3_text1)));
		return view;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		comm.respond(4);
	}

}
