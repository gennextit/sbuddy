package com.ajsoft.sbuddy.fragments.event;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.ImageCroperActivity;
import com.ajsoft.sbuddy.ImageCroperEventActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.fragments.ImageCroper;
import com.ajsoft.sbuddy.model.EventImageModel;
import com.ajsoft.sbuddy.model.MySportsModel;
import com.ajsoft.sbuddy.model.SpinnerSportsAdapter;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.CameraUtility;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;
import com.ajsoft.sbuddy.util.Validation;
import com.ajsoft.sbuddy.util.image.FileUtils;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainCreateEvent extends CompactFragment {
	TextView tvImageCounter;
	EditText etName,etAge;
	Spinner spSports; 
	ImageView ivAddImage;
	ProgressBar progressBar;
	LinearLayout llImagecontainer;
	Button btnSave;
	private ArrayList<MySportsModel> sportArrayList;
	private String sltSportId;
	String TAG = "MainCreateEvent: ";
	private static final int REQUEST_CODE = 6384; // onActivityResult request
    // code

	static Boolean instance;
	// Captur Image or pickup image tokens
	private Uri mImageCaptureUri, cropImageUri;
	private static final int CROP_FROM_FILE = 2;
	private Bitmap cropedPhoto;
	OnCreateEventListener comm;
	private ArrayList<EventImageModel> imgList;
	public static Uri mTempCropImageUri;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		L.m(TAG + "onAttach");
		imgList = new ArrayList<EventImageModel>(); 
		instance = false;
	}

	public void setCropedImageAndUri(Bitmap bmp,Uri uri){
		addImage(bmp,uri);
	}


	public void setCommunicator(OnCreateEventListener comm) {
		this.comm = comm;
	}

	public interface OnCreateEventListener {
		public void onCreateEventListener(String EventName,String sportId,String age,ArrayList<EventImageModel> list);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		L.m(TAG + "onCreateView");
		View v = inflater.inflate(R.layout.main_event_create, container, false); 
		initUi(v);
		LoadSportsData();
		if (instance) {
			L.m(TAG + "savedInstanceState not null");
			if (imgList != null)
				restoreImage();
		} else {
			L.m(TAG + "savedInstanceState null");
		}
		return v;
	}

	private void initUi(View v) {
		etName = (EditText) v.findViewById(R.id.et_main_event_create_name); 
		etAge = (EditText) v.findViewById(R.id.et_main_event_create_ageGroup); 
		tvImageCounter = (TextView) v.findViewById(R.id.tv_main_event_create_imageCounter);
		spSports = (Spinner) v.findViewById(R.id.sp_main_event_create_sportList); 
		ivAddImage = (ImageView) v.findViewById(R.id.iv_main_event_create_addMore);
		llImagecontainer = (LinearLayout) v.findViewById(R.id.ll_main_event_create_imageContainer);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		btnSave = (Button) v.findViewById(R.id.btn_events_save);
		
		setTypsFace(btnSave);
		setTypsFace(etName);
		setTypsFace(etAge);
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// GenerateImageList();
				if(checkValidation()&&sltSportId!=null){
					comm.onCreateEventListener(etName.getText().toString(),sltSportId,etAge.getText().toString(),imgList);
				}
			}

		});
		
		spSports.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				spSports.setSelection(0);
				return false;
			}
		});
		spSports.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (sportArrayList != null && spSports.getSelectedItem().toString() != "Select Sport") {

					sltSportId = sportArrayList.get(position).getSportId();

				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		ivAddImage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// addImage();
				ImagePicker();
			}
		});
	}
	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isEmpty(etName, true)) {
			return false;
		} 
		if(spSports.getSelectedItem().toString() == "Select Sport"){
			Toast.makeText(getActivity(), "Please select sport", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (!Validation.isEmpty(etAge, true)) {
			return false;
		} 
		
		return ret;
	}

	public void openImageCroper(Uri uri){
		ImageCroper imageCroper = new ImageCroper();
		ImageCroper.SWITCH=ImageCroper.EVENT;
		imageCroper.setImgUri(uri);
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.add(android.R.id.content, imageCroper, "imageCroper");
		transaction.addToBackStack("imageCroper");
		transaction.commit();
	}

	public void LoadSportsData() {
		ArrayList<String> result = LoadSports();
		if (result != null) {
			SpinnerSportsAdapter adapter = new SpinnerSportsAdapter(getActivity(), R.layout.spinner_slot);
			adapter.addAll(result);
			adapter.add("Select Sport");
			spSports.setAdapter(adapter);
			spSports.setSelection(adapter.getCount());
		}

	}

	private ArrayList<String> LoadSports() {

		ArrayList<String> sList = new ArrayList<String>();
		sportArrayList = new ArrayList<MySportsModel>();
		String response = Utility.LoadPref(getActivity(), AppTokens.SportList);

		if (response.contains("[")) {
			try {
				JSONArray sportList = new JSONArray(response);

				for (int i = 0; i < sportList.length(); i++) {

					JSONObject obj = sportList.getJSONObject(i);
					if (!obj.optString("status").equals("") && obj.optString("status").equalsIgnoreCase("success")) {
						JSONArray message = obj.getJSONArray("message");
						for (int j = 0; j < message.length(); j++) {
							JSONObject messageData = message.getJSONObject(j);
							MySportsModel model = new MySportsModel();
							model.setSportId(messageData.optString("sportId"));
							model.setSportName(messageData.optString("sportName"));
							model.setLogo(messageData.optString("logo"));
							model.setBaseImage(messageData.optString("baseImage"));
							sList.add(messageData.optString("sportName"));
							sportArrayList.add(model);

						}

					}
				}
			} catch (JSONException e) {
				L.m(e.toString());
				return null;
			}
		} else {
			L.m("Invalid JSON found : " + response);
			return null;
		}

		return sList;
	}

	public void addImage(Bitmap bm, Uri cropImgUri) {
		if (imgList == null) {
			imgList = new ArrayList<EventImageModel>();
		}
		LinearLayout innerLinearLayout = new LinearLayout(getActivity());

		ImageView imageView = new ImageView(getActivity());
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				getResources().getDimensionPixelSize(R.dimen.login_signup_icon_width),
				LinearLayout.LayoutParams.MATCH_PARENT);
		params.rightMargin = 10;
		imageView.setLayoutParams(params);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imageView.setImageURI(cropImgUri);

		innerLinearLayout.addView(imageView);
		llImagecontainer.addView(innerLinearLayout);
		EventImageModel ob = new EventImageModel();
		ob.setImagePath(cropImgUri);
		ob.setImageBitmap(null);
		imgList.add(ob);
		instance = true;
		mTempCropImageUri=null;
		
	}

	private void restoreImage() {
		for (EventImageModel ob : imgList) {
			LinearLayout innerLinearLayout = new LinearLayout(getActivity());
			ImageView imageView = new ImageView(getActivity());
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					getResources().getDimensionPixelSize(R.dimen.login_signup_icon_width),
					LinearLayout.LayoutParams.MATCH_PARENT);
			params.rightMargin = 10;
			imageView.setLayoutParams(params);
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setImageBitmap(ob.getImageBitmap());
			innerLinearLayout.addView(imageView);
			llImagecontainer.addView(innerLinearLayout);
		}
	}


	private void ImagePicker() {
		// pick from file
		Intent ob=new Intent(getActivity(), ImageCroperEventActivity.class);
		startActivity(ob);
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) { 
		L.m(TAG+"onActivityResult");
		if (resultCode != getActivity().RESULT_OK)
			return;

		switch (requestCode) {
		case REQUEST_CODE:
			// doCrop();

			 // If the file selection was successful
            if (resultCode == getActivity().RESULT_OK) {
                if (data != null) {
                    // Get the URI of the selected file
                    final Uri uri = data.getData();
                    L.m("Uri = " + uri.toString()); 
//        			
                    try {
                        // Get the file path from the URI
                        final String path = FileUtils.getPath(getActivity(), uri);
                        mImageCaptureUri =Uri.fromFile(new File(path));
            			//cropCapturedImage(mImageCaptureUri);
						openImageCroper(mImageCaptureUri);
//                        Toast.makeText(getActivity(),
//                                "File Selected: " + path, Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        L.m("FileSelectorTestActivity"+ "File select error"+ e.toString());
                    }
                }
            }
            break;

		case CROP_FROM_FILE:
			Bundle extras = data.getExtras();

			if (extras != null) {
				cropedPhoto = extras.getParcelable("data");
				// rotateImage(photo);
				cropImageUri = Uri.fromFile(CameraUtility.getOutputCropedMediaFile());
				CameraUtility.saveImageExternal(getActivity(), cropedPhoto, cropImageUri);
				addImage(cropedPhoto, cropImageUri);
				// createImageFromBitmap(cropedPhoto);
			}

			// File f = new File(mImageCaptureUri.getPath());
			//
			// if (f.exists())
			// f.delete();

			break;

		}
	}


	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		getActivity().finish();
	}

}
