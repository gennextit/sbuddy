package com.ajsoft.sbuddy.fragments;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.model.CountryCodeAdapter;
import com.ajsoft.sbuddy.model.CountryCodeModel;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpMobile extends CompactFragment {

	private EditText etMobile;
	private Button btnSignIn;
	// public static final String FRAGMENT_TAG = "fragment_2";
	private ProgressBar progressBar;
	private OnVerifyMobile comm;
	Boolean isSoftKeyboardDisplayed = false;
	HttpTask httpTask;
	ArrayList<CountryCodeModel> countryList;
	public AutoCompleteTextView acCountryCode;
	FragmentManager mannager; 
	TextView actionbarTitle;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (httpTask != null) {
			httpTask.onAttach(activity);
		}

	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (httpTask != null) {
			httpTask.onDetach();
		}
	}

	public void setCommunicator(OnVerifyMobile onVerifyMobile) {
		this.comm = onVerifyMobile;
	}

	public interface OnVerifyMobile {
		public void onVerifyMobile(String sportId);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.fragment_mobile_singup, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		etMobile = (EditText) view.findViewById(R.id.et_mobile_signup);
		btnSignIn = (Button) view.findViewById(R.id.btn_mobile_signin);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		actionbarTitle = (TextView) view.findViewById(R.id.actionbar_title);
		acCountryCode = (AutoCompleteTextView) view.findViewById(R.id.ac_profile_countryCode);
		LoadCountryCode(acCountryCode);
		mannager = getFragmentManager();
		setActionBarOption(view);
		setTypsFace(etMobile);
		setTypsFace(acCountryCode);
		setTypsFace(btnSignIn);  
		LinearLayout tearmsAndUse = (LinearLayout) view.findViewById(R.id.tearmsOfUse);
		tearmsAndUse.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				AppWebView appWebView=new AppWebView();
				appWebView.setUrl(getResources().getString(R.string.t_and_c),AppSettings.USER_GUIDE_TC, AppWebView.URL);
				mannager=getFragmentManager();
				FragmentTransaction transaction=mannager.beginTransaction();
				transaction.add(android.R.id.content, appWebView, "appWebView");
				transaction.addToBackStack("appWebView");
				transaction.commit();
			}
		});
		//actionbarTitle.setText(setBoldFont(R.color.white, Typeface.BOLD, "SIGN UP"));
		etMobile.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayed = true;
				return false;
			}
		});
		btnSignIn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
					httpTask = new HttpTask(getActivity(), btnSignIn, progressBar,acCountryCode.getText().toString(),etMobile.getText().toString());
					httpTask.execute(AppSettings.SET_PLAYER_REGISTRATION_BY_MOBILE);
					// new
					// HttpTask(btnSignIn,progressBar).execute(AppSettings.setPlayerRegistrationByMobile);
				} else {
					Toast.makeText(getActivity(), "Please enter mobile number", Toast.LENGTH_SHORT).show();
				}

			}
		});
	}
	public void setActionBarOption(View view) {
		LinearLayout ActionBack;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("signUpMobile", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		}); 
		
	}
	
	
	private void LoadCountryCode(AutoCompleteTextView sp) {
		// TODO Auto-generated method stub
		countryList = new ArrayList<CountryCodeModel>();
		String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
		for (int i = 0; i < rl.length; i++) {
			String[] g = rl[i].split(",");
			CountryCodeModel ob = new CountryCodeModel();
			ob.setPhoneCode(g[0]);
			countryList.add(ob);
		}
		CountryCodeAdapter adapter = new CountryCodeAdapter(getActivity(), R.layout.custom_slot, R.id.lbl_name,
				countryList);
		sp.setAdapter(adapter);
		
		setDefaultCode();
	}

	private void setDefaultCode() {
		String CountryID="";
	    String CountryZipCode=null;

	    TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
	    //getNetworkCountryIso
	    CountryID= manager.getSimCountryIso().toUpperCase();
	    String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
	    for(int i=0;i<rl.length;i++){
	        String[] g=rl[i].split(",");
	        if(g[1].trim().equals(CountryID.trim())){
	            CountryZipCode=g[0];
	            break;  
	        }
	    }
	    if(CountryZipCode!=null){
	    	acCountryCode.setText(CountryZipCode);
	    }
	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		Button btn;
		ProgressBar pBar;
		Activity activity;
		String mobile,cCode;

		public void onAttach(Activity activity) {
			// TODO Auto-generated method stub
			this.activity = activity;

		}

		public void onDetach() {
			// TODO Auto-generated method stub
			this.activity = null;

		}

		public HttpTask(Activity activity, Button btn, ProgressBar pBar,String cCode,String mobile) {
			this.activity = activity;
			this.btn = btn;
			this.cCode=cCode;
			this.mobile=mobile;
			this.pBar = pBar;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			btn.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			int flagSelect = 0;

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if(!acCountryCode.equals("")){
				params.add(new BasicNameValuePair("mobileNumber", cCode+mobile));
				params.add(new BasicNameValuePair("macAddr", getMACAddress(getActivity())));
			}else{
				params.add(new BasicNameValuePair("mobileNumber",mobile));
				params.add(new BasicNameValuePair("macAddr", getMACAddress(getActivity())));
			}
			
			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String MSG = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (!obj.optString("status").equals("")
								&& obj.optString("status").equalsIgnoreCase("success")) {
							MSG = "success";
						} else if (!obj.optString("status").equals("")
								&& obj.optString("status").equalsIgnoreCase("failure")) {
							MSG = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Errszwor :" + e.toString());
					ErrorMessage= e.toString()+response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage=response;
				return null;
			}

			return MSG;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			pBar.setVisibility(View.GONE);
			btn.setVisibility(View.VISIBLE);
			if (activity != null) {
				if (result != null) {
					L.m(result);
					if(result.equalsIgnoreCase("success")){
						SavePref(Buddy.CompleteMobile, acCountryCode.getText().toString()+etMobile.getText().toString());
						SavePref(Buddy.Mobile, etMobile.getText().toString());
						SavePref(Buddy.countryCode, acCountryCode.getText().toString());
						comm.onVerifyMobile(result);
					}else{
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				}else{
					showServerErrorAlertBox(ErrorMessage);
				}
			}
		}
	}
	
	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					httpTask = new HttpTask(getActivity(), btnSignIn, progressBar,acCountryCode.getText().toString(),etMobile.getText().toString());
					httpTask.execute(AppSettings.SET_PLAYER_REGISTRATION_BY_MOBILE);


				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
}
