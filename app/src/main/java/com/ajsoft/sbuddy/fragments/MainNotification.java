package com.ajsoft.sbuddy.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajsoft.sbuddy.ChatActivity;
import com.ajsoft.sbuddy.GCMIntentService;
import com.ajsoft.sbuddy.MainActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.sbuddy.MainFindSbuddyDetailNotification;
import com.ajsoft.sbuddy.model.ChatSbuddyModel;
import com.ajsoft.sbuddy.model.FindBuddyModel;
import com.ajsoft.sbuddy.model.NotificationAdapter;
import com.ajsoft.sbuddy.model.NotificationModel;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.MyNotification;
import com.ajsoft.sbuddy.util.image.FileUtils;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainNotification extends CompactFragment {
    Button test;
    EditText et;
    ListView lvMain;
    ProgressBar progressBar;
    FragmentManager mannager;
    ArrayList<NotificationModel> myNotificationList;
    NotificationAdapter adapter;
    HttpTask httpTask;
    int NOTIFICATION = 1, REQUEST = 2, GET_CONNECT_REC = 3;
    GetCoachVenueDetailRequest getCoachVenueDetailRequest;
    ArrayList<FindBuddyModel> myBuddyList;
    String setSearchBuddyData;
    OnNotificationCounterChangeListener comm;
    GetConnectRequest getConnectRequest;

    public void setComm(Activity activity) {
        this.comm = (MainActivity) activity;
    }

    public interface OnNotificationCounterChangeListener {
        public void onNotificationCounterChangeListener(int NotiCounter);
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (httpTask != null) {
            httpTask.onAttach(activity);
        }
        if (getCoachVenueDetailRequest != null) {
            getCoachVenueDetailRequest.onAttach(activity);
        }
        if (getConnectRequest != null) {
            getConnectRequest.onAttach(activity);
        }

    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (httpTask != null) {
            httpTask.onDetach();
        }
        if (getCoachVenueDetailRequest != null) {
            getCoachVenueDetailRequest.onDetach();
        }
        if (getConnectRequest != null) {
            getConnectRequest.onDetach();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.main_noitification, container, false);
        mannager = getFragmentManager();
        initUi(v);

       loadNotificationAtFirstTime();

        return v;
    }

    public void loadNotificationAtFirstTime() {
        httpTask = new HttpTask(getActivity(), progressBar);
        httpTask.execute(AppSettings.getConnectRequest);
    }

    private void initUi(View v) {
        setActionBarOption(v);

        lvMain = (ListView) v.findViewById(R.id.lv_notification_list);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

        lvMain.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

                if (myNotificationList != null) {
                    // comm.onChatBuddyzListListener(myNotificationList.get(position).getPlayerId(),myNotificationList.get(position).getFullName(),myNotificationList.get(position).getImageUrl());
                    if (myNotificationList.get(position).getRequestId() != null) {
                        if (myNotificationList.get(position).getRequestedStatue().equalsIgnoreCase("request")) {
                            OptionAlertBox(myNotificationList.get(position).getRequestId(), position);
//                            GCMIntentService.notificationCounter--;
//                            comm.onNotificationCounterChangeListener(GCMIntentService.notificationCounter);
                        }else if (myNotificationList.get(position).getRequestedStatue().equalsIgnoreCase("chat")) {
                            MyNotification.removeChatCounter(myNotificationList.get(position).getRequestId());
                            ((MainActivity)getActivity()).setNotificationCounter();
                            //AppTokens.tempPlayerId=myNotificationList.get(position).getRequestId();
                            ChatActivity.sltBuddyId = myNotificationList.get(position).getRequestId();
                            ChatActivity.sltBuddyName = myNotificationList.get(position).getRequestedName();
                            ChatActivity.sltBuddyImage = myNotificationList.get(position).getRequestedName();
                            Intent ob = new Intent(getActivity(), ChatActivity.class);
                            startActivity(ob);
                           // myNotificationList.remove(position);
//                            comm.onNotificationCounterChangeListener(GCMIntentService.notificationCounter);
                             mannager.popBackStack();
                        }else{
                            OtherOptionAlertBox(getActivity(),position,myNotificationList.get(position).getRequestId());
                        }

                    }
                }

            }

        });

        myNotificationList = new ArrayList<NotificationModel>();
        adapter = new NotificationAdapter(getActivity(), R.layout.custom_notification_slot, myNotificationList);
        lvMain.setAdapter(adapter);

    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        adapter.notifyDataSetChanged();

        clearAllNotification(getActivity());
    }

    /*public void removeChatCounter(String playerId) {
        String player = playerId;
        ArrayList<ChatSbuddyModel> tempList = new ArrayList<ChatSbuddyModel>();
        ArrayList<ChatSbuddyModel> finalList = new ArrayList<ChatSbuddyModel>();
        tempList = AppTokens.newChat;
        finalList = tempList;
        for (int i = 0; i < tempList.size(); i++) {
            if (tempList.get(i).getSenderId().equalsIgnoreCase(player)) {
                finalList.remove(i);
                GCMIntentService.notificationCounter--;

            }
        }
        AppTokens.newChat.clear();
        AppTokens.newChat = finalList;
    }*/

    // public void removeChatCounter(int position) {
    // AppTokens.newChat.remove(position);
    // }


    public void OtherOptionAlertBox(Activity Act, final int position, final String playerId) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = Act.getLayoutInflater();

        View v = inflater.inflate(R.layout.confirm_delete, null);
        dialogBuilder.setView(v);
        Button button1 = (Button) v.findViewById(R.id.button1);
        Button button2 = (Button) v.findViewById(R.id.button2);
        // if decline button is clicked, close the custom dialog
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                MyNotification.removeNotiRejAprBlockCounter(playerId);
                removeListItem(position);
                ((MainActivity)getActivity()).setNotificationCounter();

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });
        dialog = dialogBuilder.create();
        dialog.show();

    }


    public void setActionBarOption(View view) {
        LinearLayout ActionBack, ActionHome;
        ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
        ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
        // TextView
        // actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
        // actionbarTitle.setText(setActionBarTitle("NOTIFICATION"));
        ActionBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mannager.popBackStack();

            }
        });
        ActionHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mannager.popBackStack();
            }
        });
    }

    /*public void removeCounter(String playerId) {
        String player = playerId;
        ArrayList<NotificationModel> tempList = new ArrayList<NotificationModel>();
        ArrayList<NotificationModel> finalList = new ArrayList<NotificationModel>();
        tempList = AppTokens.newNotificationPull;
        finalList = tempList;
        for (int i = 0; i < tempList.size(); i++) {
            if (tempList.get(i).getRequestId().equalsIgnoreCase(player)) {
                GCMIntentService.notificationCounter--;
                finalList.remove(i);
            }
        }
        AppTokens.newNotificationPull.clear();
        AppTokens.newNotificationPull = finalList;
    }
*/
    private class HttpTask extends AsyncTask<String, Void, String> {
        ProgressBar pBar;
        Activity activity;

        public HttpTask(Activity activity, ProgressBar pBar) {
            this.pBar = pBar;
            onAttach(activity);
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "failure";

            ArrayList<ChatSbuddyModel> tempChatList = new ArrayList<ChatSbuddyModel>();
            tempChatList = AppTokens.newChat;
            if (tempChatList != null) {
                for (ChatSbuddyModel ob : tempChatList) {
                    response = "success";
                    NotificationModel data = new NotificationModel();
                    data.setRequestId(ob.getSenderId());
                    data.setRequestedName(ob.getName());
                    data.setNotification("Chat message from " + ob.getName());
                    data.setRequestedStatue("chat");
                    myNotificationList.add(data);
                }
            }

            ArrayList<NotificationModel> tempList = new ArrayList<NotificationModel>();
            tempList = AppTokens.newNotificationPush;
            if (tempList != null) {
                for (NotificationModel ob : tempList) {
                    response = "success";

                    /*NotificationModel data = new NotificationModel();
                    data.setRequestId(ob.getRequestId());
                    data.setRequestedName(ob.getRequestedName());
                    data.setNotification(ob.getNotification());
                    data.setRequestedStatue(ob.getRequestedStatue());
                    myNotificationList.add(data);*/

					if(!ob.getRequestedStatue().equals("request")){
						NotificationModel data = new NotificationModel();
						data.setRequestId(ob.getRequestId());
						data.setRequestedName(ob.getRequestedName());
						data.setNotification(ob.getNotification());
						data.setRequestedStatue(ob.getRequestedStatue());
						myNotificationList.add(data);
					}

                }
            }
            ArrayList<NotificationModel> tempReqList = new ArrayList<NotificationModel>();
            tempReqList = AppTokens.newNotificationPull;
            if (tempReqList != null) {
                for (NotificationModel ob : tempReqList) {
                    response = "success";

                    NotificationModel data = new NotificationModel();
                    data.setRequestId(ob.getRequestId());
                    data.setRequestedName(ob.getRequestedName());
                    data.setNotification(ob.getNotification());
                    data.setRequestedStatue(ob.getRequestedStatue());
                    myNotificationList.add(data);

					/*if(!ob.getRequestedStatue().equals("request")){
						NotificationModel data = new NotificationModel();
						data.setRequestId(ob.getRequestId());
						data.setRequestedName(ob.getRequestedName());
						data.setNotification(ob.getNotification());
						data.setRequestedStatue(ob.getRequestedStatue());
						myNotificationList.add(data);
					}*/

                }
            }
            return response;
            // return "success";

        }

        @Override
        protected void onPostExecute(String result) {
            if (activity != null) {
                pBar.setVisibility(View.GONE);
                if (result != null) {

                    switch (result) {
                        case "success":
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                                /*getConnectRequest = new GetConnectRequest(getActivity(), progressBar);
                                getConnectRequest.execute(AppSettings.getConnectRequest);*/
                            }
                            break;
                        case "failure":
                            /*getConnectRequest = new GetConnectRequest(getActivity(), progressBar);
                            getConnectRequest.execute(AppSettings.getConnectRequest);*/
						Toast.makeText(getActivity(), "There is No pending Notification for you", Toast.LENGTH_SHORT)
								.show();
                            break;
                        case "not available":

                            break;
                    }
                } else {
                    showServerErrorAlertBox1(ErrorMessage, 1);
                }
            }

        }
    }

    public void showServerErrorAlertBox1(String errorDetail, int option) {
        showAlertBox1(option, getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail, null,0);
    }

    public void showServerErrorAlertBox(String errorDetail, int option, String requestedId,int position) {
        showAlertBox1(option, getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail, requestedId,position);
    }

    public void showInternetAlertBox1() {
        showAlertBox1(0, getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null, null,0);
    }

    public void showAlertBox1(final int option, String title, String Description, int noOfButtons,
                              final String errorMessage, final String requestedId, final int position) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        // ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.alert_dialog, null);
        dialogBuilder.setView(v);
        ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
        Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
        Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
        TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
        final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
        LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
        LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
        ivAbout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (errorMessage != null) {
                    tvDescription.setText(errorMessage);
                }
            }
        });

        tvTitle.setText(title);
        tvDescription.setText(Description);
        if (noOfButtons == 1) {
            button2.setVisibility(View.GONE);
            llBtn2.setVisibility(View.GONE);
        }
        button1.setText("Retry");
        button1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
                if (isOnline()) {
                    if (option == NOTIFICATION) {
                        httpTask = new HttpTask(getActivity(), progressBar);
                        httpTask.execute(AppSettings.getConnectRequest);
                    } else if (option == REQUEST) {
                        getCoachVenueDetailRequest = new GetCoachVenueDetailRequest(getActivity(), requestedId,position);
                        getCoachVenueDetailRequest.execute(AppSettings.getMyBuddyDetails);

                    } else if (option == GET_CONNECT_REC) {
                        getConnectRequest = new GetConnectRequest(getActivity(), progressBar);
                        getConnectRequest.execute(AppSettings.getConnectRequest);
                    }

                } else {
                    showInternetAlertBox(getActivity());
                }
            }
        });
        button2.setText("Cancel");
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                dialog.dismiss();
            }
        });

        dialog = dialogBuilder.create();
        dialog.show();

    }

    // public void OptionAlertBox(Activity Act, final int position) {
    //
    // final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
    //
    // // ...Irrelevant code for customizing the buttons and title
    // LayoutInflater inflater = Act.getLayoutInflater();
    //
    // View v = inflater.inflate(R.layout.custom_connect_request_option, null);
    // dialogBuilder.setView(v);
    // Button button1 = (Button) v.findViewById(R.id.button1);
    // Button button2 = (Button) v.findViewById(R.id.button2);
    // // if decline button is clicked, close the custom dialog
    // button1.setOnClickListener(new OnClickListener() {
    // @Override
    // public void onClick(View v) {
    // // Close dialog
    // dialog.dismiss();
    //
    // }
    // });
    // button2.setOnClickListener(new OnClickListener() {
    // @Override
    // public void onClick(View v) {
    // // Close dialog
    // dialog.dismiss();
    //
    //
    // }
    // });
    // dialog = dialogBuilder.create();
    // dialog.show();
    //
    // }

    private void OptionAlertBox(String requestedId, int position) {

        getCoachVenueDetailRequest = new GetCoachVenueDetailRequest(getActivity(), requestedId,position);
        getCoachVenueDetailRequest.execute(AppSettings.getMyBuddyDetails);
    }

    private void removeListItem(int position2) {
        myNotificationList.remove(position2);
        adapter.notifyDataSetChanged();
    }

    private class GetCoachVenueDetailRequest extends AsyncTask<String, Void, String> {

        private Activity activity;
        private String requestedId;
        private int position = 0;

        public GetCoachVenueDetailRequest(Activity activity, String requestedId,int position) {
            onAttach(activity);
            this.requestedId = requestedId;
            //this.position=position;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showPDialog(getActivity(), "Processing please wait");
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "error";
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            if (activity != null) {
                params.add(new BasicNameValuePair("playerId", requestedId));
            }

            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], HttpReq.GET, params);
            String output = null;
            myBuddyList = new ArrayList<FindBuddyModel>();

            L.m(response);
            setSearchBuddyData = response;

            if (response.contains("[")) {
                try {
                    JSONArray messageArray = new JSONArray(response);
                    JSONObject messageObj = messageArray.getJSONObject(0);
                    if (messageObj.optString("status").equalsIgnoreCase("success")) {
                        JSONArray message = messageObj.getJSONArray("message");
                        for (int j = 0; j < message.length(); j++) {
                            JSONObject messageData = message.getJSONObject(j);
                            if (!messageData.optString("playerId").equals("")) {
                                output = "success";
                                FindBuddyModel model = new FindBuddyModel();
                                model.setsBudddy("");
                                model.setPlayerId(messageData.optString("playerId"));
                                model.setFullName(messageData.optString("fullName"));
                                model.setAge(messageData.optString("age"));
                                model.setGender(messageData.optString("gender"));
                                model.setKm(0.0f);
                                model.setImageUrl(messageData.optString("imageUrl"));
                                model.setLocation(messageData.optString("location"));
                                model.setAbout(messageData.optString("about"));
                                model.setCity(messageData.optString("city"));
                                model.setClubOrApartment(messageData.optString("clubOrApartment"));


                                model.setsLogoCount("0");
                                myBuddyList.add(model);
                            }
                        }
                    } else if (messageObj.optString("status").equalsIgnoreCase("failure")) {
                        output = "failure";
                        ErrorMessage = messageObj.optString("message");
                    }


                } catch (JSONException e) {
                    L.m("Json Error :" + e.toString());
                    ErrorMessage = e.toString() + response;
                    return null;
                    // return e.toString();
                }
            } else {
                L.m("Invalid JSON found : " + response);
                ErrorMessage = response;
                return null;
            }

            return output;

        }

        @Override
        protected void onPostExecute(String result) {

            if (activity != null) {
                dismissPDialog();
                if (result != null) {
                    if (result.equalsIgnoreCase("success")) {

                        //removeListItem(position);

                        if (myBuddyList != null) {
                            MainFindSbuddyDetailNotification mfsdn = new MainFindSbuddyDetailNotification();
                            // mainSearchSbuddyz.setCommunicator(AppIntroActivity.this);
							/*MainFindSbuddyDetail.openFromMain=false;
							MainFindSbuddyDetail.openFromChat=false;*/
                            if (myBuddyList != null) {
                                mfsdn.setData("search", myBuddyList.get(position).getsBudddy(), myBuddyList.get(position).getAbout(),
                                        myBuddyList.get(position).getLocation(), myBuddyList.get(position).getClubOrApartment(),
                                        myBuddyList.get(position).getCity());
                                mfsdn.setPlayerProfile(myBuddyList.get(position).getPlayerId(),
                                        myBuddyList.get(position).getFullName(), myBuddyList.get(position).getImageUrl(),
                                        myBuddyList.get(position).getGender(), myBuddyList.get(position).getAge(), "online");
                                mfsdn.setJSONData(setSearchBuddyData);
                                FragmentManager mannager = getActivity().getFragmentManager();
                                FragmentTransaction transaction = mannager.beginTransaction();
                                transaction.replace(android.R.id.content, mfsdn, "mainFindSbuddyDetailNotification");
                                transaction.addToBackStack("mainFindSbuddyDetailNotification");
                                transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                                transaction.commit();
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showServerErrorAlertBox(ErrorMessage, REQUEST, requestedId,position);
                }
            }
        }

    }

    private class GetConnectRequest extends AsyncTask<String, Void, String> {
        ProgressBar pBar;
        Activity activity;

        public GetConnectRequest(Activity activity, ProgressBar pBar) {
            this.pBar = pBar;
            onAttach(activity);
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        private void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            pBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "error";

            List<BasicNameValuePair> params = new ArrayList<>();
            if (activity != null) {
                params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
            }
            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], HttpReq.GET, params);
            String output = "not available";
            L.m(response);
            if (response.contains("[")) {
                try {
                    JSONArray json = new JSONArray(response);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        if (!obj.optString("senderId").equals("")) {
                            output = "success";
                            NotificationModel model = new NotificationModel();
                            model.setNotification("A buddy request has been sent to you by " + obj.optString("senderName"));
                            model.setRequestId(obj.optString("senderId"));
                            model.setRequestedName(obj.optString("senderName"));
                            model.setRequestedStatue("request");
                            myNotificationList.add(model);
                        } else if (!obj.optString("status").equals("")) {
                            output = "failure";
                            ErrorMessage = obj.optString("message");
                        }
                    }


                } catch (JSONException e) {
                    L.m("Json Error :" + e.toString());
                    ErrorMessage = e.toString() + "\n" + response;
                    return null;
                }
            } else {
                L.m("Server Error : " + response);
                ErrorMessage = response;
                return null;
            }

            return output;
            // return "success";

        }

        @Override
        protected void onPostExecute(String result) {
            if (activity != null) {
                pBar.setVisibility(View.GONE);
                if (result != null) {

                    switch (result) {
                        case "success":
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                            break;
                        case "failure":
                            if (ErrorMessage.contains("Error207")) {
                                Toast.makeText(getActivity(), "There is No pending Notification for you", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case "not available":

                            break;
                    }
                } else {
                    showServerErrorAlertBox(ErrorMessage, GET_CONNECT_REC, null,0);
                }
            }

        }
    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        GCMIntentService.notificationCounter = 0;
        GCMIntentService.notificationStatus = true;
    }
}
