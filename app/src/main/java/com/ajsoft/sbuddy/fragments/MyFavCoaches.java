package com.ajsoft.sbuddy.fragments;
 

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.coaches.MainFindCoachesDetail;
import com.ajsoft.sbuddy.model.FindCoachAdapter;
import com.ajsoft.sbuddy.model.FindCoachModel;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MyFavCoaches extends CompactFragment{
	Button test;
	EditText et;
	ListView lvMain;
	ProgressBar progressBar;
	FragmentManager mannager;
	ArrayList<FindCoachModel> mySportList;
	FindCoachAdapter adapter; 
	String searchCoachData,searchCoachDataUpdate;
	HttpTask httpTask; 
	  
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		L.m("onAttach");
		if(httpTask!=null){
			httpTask.onAttach(activity);
		}
	}
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if(httpTask!=null){
			httpTask.onDetach();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v =inflater.inflate(R.layout.favourate_coach_venu_layout, container,false);
		mannager = getFragmentManager();
		initUi(v);
		
		httpTask=new HttpTask(getActivity(), progressBar);
		httpTask.execute(AppSettings.getFavouriteCoach);
		
		return v;
	}
	
	private void initUi(View v) { 
		setActionBarOption(v);
		
		lvMain = (ListView) v.findViewById(R.id.lv_chat_buddy_list);  
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

		lvMain.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
				MainFindCoachesDetail mainFindCoachesDetail = new MainFindCoachesDetail();
				// mainSearchSbuddyz.setCommunicator(AppIntroActivity.this);
				if (mySportList != null) {
					mainFindCoachesDetail.setData("y",mySportList.get(position).getAbout(),
							mySportList.get(position).getLocality(), "", "");

					mainFindCoachesDetail.setPlayerProfile(mySportList.get(position).getCoachId(),
							mySportList.get(position).getFullName(), mySportList.get(position).getImageUrl(),
							mySportList.get(position).getReview(), mySportList.get(position).getRating());
					mainFindCoachesDetail.setJSONData(searchCoachDataUpdate);
					FragmentManager mannager = getActivity().getFragmentManager();
					FragmentTransaction transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, mainFindCoachesDetail, "mainFindCoachesDetail");
					transaction.addToBackStack("mainFindCoachesDetail");
					transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();
				}
			}
		});
 

	}
	
	
	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		actionbarTitle.setText("MY FAVOURITE COACHES");
		
		ActionBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("myFavCoaches", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) { 
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}
	 
	private class HttpTask extends AsyncTask<String, Void, String> {
		ProgressBar pBar;
		Activity activity;

		public HttpTask(Activity activity, ProgressBar pBar) {
			this.pBar = pBar;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error"; 
			mySportList=new ArrayList<FindCoachModel>();
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if(activity!=null){
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
			}
			if(searchCoachData==null){
				HttpReq ob = new HttpReq();
				response = ob.makeConnection(urls[0], HttpReq.GET, params);
			}else{
				response=searchCoachData;
			}
			String output = "not available",locality=null;
			L.m(response);
			if (response.contains("[")) {
				searchCoachData=response;
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					if (messageObj.optString("status").equals("success")) {
					JSONArray json = messageObj.getJSONArray("message");
					for (int i = 0; i < json.length(); i++) {
						JSONObject messageData = json.getJSONObject(i);
						if(!messageData.optString("coachId").equals("")){
							output="success";
							FindCoachModel model = new FindCoachModel();
							model.setCoachId(messageData.optString("coachId"));
							model.setAbout(messageData.optString("about"));
							model.setFullName(messageData.optString("fullName"));
							model.setAge(messageData.optString("age"));
							model.setGender(messageData.optString("gender"));
							model.setImageUrl(messageData.optString("imageUrl"));
							model.setCity(messageData.optString("address"));
							model.setRating(messageData.optString("rating"));
							model.setReview(messageData.optString("review"));
							
							locality = null;
							JSONArray locArr = messageData.getJSONArray("location");
							for (int k = 0; k < locArr.length(); k++) {
								JSONObject locObj = locArr.getJSONObject(k);
								if (!locObj.optString("locality").equals("")) {
									if (locality == null) { 
										locality = locObj.optString("locality") + "  \n";
									} else {
										locality += locObj.optString("locality") + " \n";
									}
								}
							}
							model.setKm(null);
							model.setLocality(locality);
							mySportList.add(model);
							searchCoachDataUpdate=json.toString();
						}
					}
					}else if (messageObj.optString("status").equals("failure")) {
						output="failure";
						ErrorMessage=messageObj.optString("message");
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = e.toString()+"\n"+response;
					return null;
				}
			} else {
				L.m("Server Error : " + response);
				ErrorMessage = response;
				return null;
			}

			return output;
			// return "success";
			
		}

		@Override
		protected void onPostExecute(String result) {  
			if (activity != null) {
				pBar.setVisibility(View.GONE); 
				if (result != null) {
					if (result.equals("success")) {
						FindCoachAdapter ExpAdapter = new FindCoachAdapter(getActivity(), R.layout.custom_slot_find_coaches,
								mySportList);
						lvMain.setAdapter(ExpAdapter);
					} else {
						mySportList=null;
						ArrayList<String> errorList = new ArrayList<String>();
						errorList.add("No record available");
						ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
								R.id.tv_error_text, errorList);
						lvMain.setAdapter(adapter);
					}
				}else{
					showServerErrorAlertBox(ErrorMessage);
				}
			}

		}
	}
	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					httpTask=new HttpTask(getActivity(), progressBar);
					httpTask.execute(AppSettings.findMySbuddy);
					
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
	 
	 
}

