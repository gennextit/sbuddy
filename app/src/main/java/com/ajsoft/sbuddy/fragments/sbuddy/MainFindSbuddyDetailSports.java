package com.ajsoft.sbuddy.fragments.sbuddy;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.model.MySkillsAdapter;
import com.ajsoft.sbuddy.model.MySportsModel;
import com.ajsoft.sbuddy.util.L;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainFindSbuddyDetailSports extends CompactFragment{
	 
	ListView lvMain;
	String jsonData,playerId;
	ArrayList<MySportsModel> mySportList;
	LoadSportList loadSportList;
	
	public void setData(String JsonData){
		this.jsonData=JsonData;
	}
	public void setPlayerId(String playerId){
		this.playerId=playerId;
	}
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if(loadSportList!=null){
			loadSportList.onAttach(activity);
		}
	}
	
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if(loadSportList!=null){
			loadSportList.onDetach();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v =inflater.inflate(R.layout.main_find_sbuddy_detail_sports, container,false);
		
		lvMain=(ListView)v.findViewById(R.id.lv_main_find_sbuddy_detail_sports);
		//L.m(jsonData);
		loadSportList=new LoadSportList(getActivity());
		loadSportList.execute(jsonData);
		return v;
	}
	
private class LoadSportList extends AsyncTask<String, Void, ArrayList<MySportsModel>> {
		
		private Activity activity;
		
		public LoadSportList(Activity activity) {
			this.activity=activity;
		}
		
		public void onAttach(Activity activity) {
			this.activity=activity;
		}
		public void onDetach() {
			this.activity=null;
		}
		
//		@Override
//		protected void onPreExecute() {
//			// TODO Auto-generated method stub
//			super.onPreExecute();
//			btnSave.setVisibility(View.GONE);
//			progressBar.setVisibility(View.VISIBLE);
//		}
		@Override
		protected ArrayList<MySportsModel> doInBackground(String... urls) {
			String response = "error";
			
			response=urls[0];
			mySportList=new ArrayList<MySportsModel>(); 
			L.m(response);
			if(response.contains("[")){
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					JSONArray main = messageObj.getJSONArray("message");
					for(int k=0;k<main.length();k++){
						JSONObject sub=main.getJSONObject(k);
						L.m(sub.optString("playerId")+" : "+(playerId));
						if(sub.optString("playerId").equalsIgnoreCase(playerId)){
							JSONArray sportPlay=sub.getJSONArray("sportPlay");
							//L.m(sportPlay.toString());
							for(int i=0;i<sportPlay.length();i++){
								JSONObject sPData=sportPlay.getJSONObject(i);
								if(!sPData.optString("sportName").equals("")){
									MySportsModel model=new MySportsModel();
									model.setSportName(sPData.optString("sportName"));
									model.setLogo(sPData.optString("logo"));
									model.setSkillLevel(sPData.optString("skillLevel"));
									model.setSelect(1);
									mySportList.add(model);
								}
							}
						}
					}
				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			}else{
				L.m("Invalid JSON found : "+response);
				return null;
			}
			
			return mySportList;
			//return "success";
					
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(ArrayList<MySportsModel> result) {
			if(activity!=null){
				if(result!=null){
					MySkillsAdapter ExpAdapter= new MySkillsAdapter(activity,R.layout.custom_slot_skills, result,false);
					lvMain.setAdapter(ExpAdapter);
				}else {
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lvMain.setAdapter(adapter); 
				}
			}
		}
	}

	 
}
