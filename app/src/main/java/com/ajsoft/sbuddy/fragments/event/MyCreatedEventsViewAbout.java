package com.ajsoft.sbuddy.fragments.event;
 
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MyCreatedEventsViewAbout extends CompactFragment{
	
	String eventType,eventStartDate,eventEndDate,eventStartTime,eventEndTime,discription,percentageAge,lastDateOfReg;
	TextView tveventType,tvEventStartDate,tvEventEndDate,tvEventStartTime,tvEventEndTime,tvDiscription,tvPercentageAge,tvLastDayOfReg;
	LinearLayout llDiscription;
	public void setData(String eventType,String eventStartDate,String eventEndDate,String eventStartTime,String eventEndTime,String discription,String percentageAge,String lastDateOfReg){
		this.eventType=eventType;
		this.eventStartDate=eventStartDate;
		this.eventEndDate=eventEndDate;
		this.eventStartTime=eventStartTime;
		this.eventEndTime=eventEndTime;
		this.discription=discription;
		this.percentageAge=percentageAge;
		this.lastDateOfReg=lastDateOfReg;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v =inflater.inflate(R.layout.my_created_events_view_about, container,false);
		
		tveventType=(TextView)v.findViewById(R.id.tv_my_created_events_view_about_eventType);
		tvEventStartDate=(TextView)v.findViewById(R.id.tv_my_created_events_view_about_eventStartDate);
		tvEventEndDate=(TextView)v.findViewById(R.id.tv_my_created_events_view_about_eventEndDate);
		tvEventStartTime=(TextView)v.findViewById(R.id.tv_my_created_events_view_about_startTime);
		tvEventEndTime=(TextView)v.findViewById(R.id.tv_my_created_events_view_about_entTime);
		tvDiscription=(TextView)v.findViewById(R.id.tv_my_created_events_view_about_description);
		llDiscription=(LinearLayout)v.findViewById(R.id.ll_my_created_events_view_about_description);
		tvPercentageAge=(TextView)v.findViewById(R.id.tv_my_created_events_view_about_percentageAge);
		tvLastDayOfReg=(TextView)v.findViewById(R.id.tv_my_created_events_view_about_lastDateOfReg);
		
		if(eventType!=null){
			switch (eventType) {
			case "openToAll":
				tveventType.setText("Open to all");
				break;
			case "inviteOnly":
				tveventType.setText("Invite Only");
				break;
			case "paidEntry":
				tveventType.setText("Paid entry");
				break;
			}
		}
		if(eventStartDate!=null){
			tvEventStartDate.setText(eventStartDate);
		}
		if(eventEndDate!=null){
			tvEventEndDate.setText(eventEndDate);
		}
		if(eventStartTime!=null){
			tvEventStartTime.setText(eventStartTime);
		}
		if(eventEndTime!=null){
			tvEventEndTime.setText(eventEndTime);
		}
		if(discription!=null){
			if(discription.equals("")){
				llDiscription.setVisibility(View.GONE);
			}else{
				tvDiscription.setText(discription);
			}
		}
		if(percentageAge!=null){
			tvPercentageAge.setText(percentageAge);
		}
		if(lastDateOfReg!=null){
			tvLastDayOfReg.setText(lastDateOfReg);
		}
		
		return v;
	}
	
	 
}

