package com.ajsoft.sbuddy.fragments.sbuddy;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.MainActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.gps.AppLocationServices;
import com.ajsoft.sbuddy.gps.GPSCallback;
import com.ajsoft.sbuddy.gps.GPSManager;
import com.ajsoft.sbuddy.model.MySportsModel;
import com.ajsoft.sbuddy.model.SpinnerSportsAdapter; 
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;
import static com.ajsoft.sbuddy.util.AppSettings.*;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainFindSbuddyz extends CompactFragment implements GPSCallback {
	Spinner spSports, spGender;
	SeekBar sbsAge, sbeAge;
	TextView tvRadious, tvSAge, tvEAge;
	AutoCompleteTextView acCityOrApp;
	Button btnSearchBuddy;
	SeekBar sbRadious;
	private ProgressBar progressBar;
	private ArrayList<MySportsModel> sportArrayList;
	private String selectGender;
	private int sAge = 0, eAge = 0;
	private String sltSportId, sltSportName, sltSportBaseImage;

	private OnFindSbuddyzListener comm;
	private static String searchBuddyData;
	double updateLati = 0.0, updateLongi = 0.0;
	private GPSManager gpsManager;
	LocationManager manager;
	AppLocationServices appLocationService;
	String MSG = MainFindSbuddyz.class.getSimpleName();
	Builder alertDialog;
	private LocationManager locationManager;
	private LocationListener locationListener;
	Boolean isSoftKeyboardDisplayed = false;
	HttpTask httpTask;
	LinearLayout mainLayout;
	Activity activity;
	FragmentManager mannager;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.activity = activity;
		if (httpTask != null) {
			httpTask.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		this.activity = null;
		if (httpTask != null) {
			httpTask.onDetach();
		}
	}

	public void setCommunicator(OnFindSbuddyzListener onFindSbuddyzListener) {
		this.comm = onFindSbuddyzListener;
	}

	public interface OnFindSbuddyzListener {
		public void onFindSbuddyzListener(Boolean status, String sltSportId, String sltSportName,
				String sltSportBaseImage, String searchBuddyData);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.main_search_sbuddyz, container, false);
		mannager = getFragmentManager();
		setActionBarOption(view);
		tvRadious = (TextView) view.findViewById(R.id.tv_main_search_sbuddyz_searchRadious);
		tvSAge = (TextView) view.findViewById(R.id.tv_main_search_sbuddyz_sAge);
		tvEAge = (TextView) view.findViewById(R.id.tv_main_search_sbuddyz_eAge);
		spSports = (Spinner) view.findViewById(R.id.sp_main_search_sbuddyz_sports);
		spGender = (Spinner) view.findViewById(R.id.sp_main_search_sbuddyz_gender);
		tvRadious = (TextView) view.findViewById(R.id.tv_main_search_sbuddyz_searchRadious);
		acCityOrApp = (AutoCompleteTextView) view.findViewById(R.id.ac_main_search_sbuddyz_CityorAppartment);
		btnSearchBuddy = (Button) view.findViewById(R.id.btn_main_search_sbuddyz_searchSbudyz);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		sbRadious = (SeekBar) view.findViewById(R.id.sb_main_search_sbuddyz_sRadious);
		sbsAge = (SeekBar) view.findViewById(R.id.sb_main_search_sbuddyz_sAge);
		sbeAge = (SeekBar) view.findViewById(R.id.sb_main_search_sbuddyz_eAge);
		mainLayout = (LinearLayout) view.findViewById(R.id.ll_main_search_sbuddyz_mainLayout);

		setTypsFace(btnSearchBuddy);
		setTypsFace(acCityOrApp);
		
		tvSAge.setText("13");
		tvEAge.setText("26");

		spSports.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				spSports.setSelection(0);
				return false;
			}
		});

		spSports.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (sportArrayList != null && spSports.getSelectedItem().toString() != "Select Sport") {

					sltSportId = sportArrayList.get(position).getSportId();
					sltSportName = sportArrayList.get(position).getSportName();
					sltSportBaseImage = sportArrayList.get(position).getBaseImage();
					// L.m(spSports.getSelectedItem().toString()+" :
					// "+sltSportName);

				} else {
					sltSportName = spSports.getSelectedItem().toString();
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		acCityOrApp.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.autocomplete_list_item));
		acCityOrApp.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				String str = (String) adapterView.getItemAtPosition(position);
				// Toast.makeText(ProfileActivity.this, str,
				// Toast.LENGTH_SHORT).show();
			}
		});

		acCityOrApp.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayed = true;
				return false;
			}
		});
		spGender.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				String gender = spGender.getSelectedItem().toString();
				selectGender = gender;
				// L.m(gender);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		sbRadious.setMax(60);
		sbRadious.setProgress(5);
		sbRadious.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress < 5) {
					sbRadious.setProgress(5);
				} else {
					tvRadious.setText(String.valueOf(progress));
				}
			}
		});
		sbsAge.setMax(65);
		sbsAge.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress < 13) {
					sbsAge.setProgress(13);
				} else {
					tvSAge.setText(String.valueOf(progress));
				}
			}
		});
		sbeAge.setMax(65);
		sbeAge.setProgress(26);
		sbeAge.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress < 13) {
					sbeAge.setProgress(13);
				} else {
					tvEAge.setText(String.valueOf(progress));
				}
			}
		});

		btnSearchBuddy.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isOnline()) {
					if (!sltSportName.equalsIgnoreCase("Select Sport")) {
						httpTask = new HttpTask(getActivity(), btnSearchBuddy, progressBar);
						httpTask.execute(findSbuddy);
					} else {
						Toast.makeText(getActivity(), "Please Select Sports", Toast.LENGTH_SHORT).show();
					}

				} else {
					showInternetAlertBox(getActivity());
				}
				// new HttpTask(btnSearchBuddy,
				// progressBar).execute(AppSettings.findSbuddy);
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
					isSoftKeyboardDisplayed = false;
				}

			}
		});
		mainLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
					isSoftKeyboardDisplayed = false;
				}
			}
		});

		LoadSportsData();

		LoadGpsListener();

		return view;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("SEARCH SBUDDYZ"));
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				mannager.popBackStack("mainFindSbuddyz", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}

	private void LoadGpsListener() {
		// TODO Auto-generated method stub
		appLocationService = new AppLocationServices(getActivity());
		gpsManager = new GPSManager();
		gpsManager.startListening(getActivity());
		gpsManager.setGPSCallback(this);
		manager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
		OfflineLocation();
	}

	private void OfflineLocation() {
		// Acquire a reference to the system Location Manager
		// locationManager = (LocationManager)
		// getActivity().getSystemService(Context.LOCATION_SERVICE);
		//
		// // Define a listener that responds to location updates
		// locationListener = new LocationListener() {
		// public void onLocationChanged(Location location) {
		// // Called when a new location is found by the network location
		// // provider.
		// // makeUseOfNewLocation(location);
		// updateLati = location.getLatitude();
		// updateLongi = location.getLongitude();
		// }
		//
		// public void onStatusChanged(String provider, int status, Bundle
		// extras) {
		// }
		//
		// public void onProviderEnabled(String provider) {
		// }
		//
		// public void onProviderDisabled(String provider) {
		// }
		// };

		// Register the listener with the Location Manager to receive location
		// updates
		// locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
		// 0, 0, locationListener);
		// return String.valueOf(updateLati+"-"+updateLongi);
	}

	public void LoadSportsData() {
		ArrayList<String> result = LoadSports();
		if (result != null) {
			SpinnerSportsAdapter adapter = new SpinnerSportsAdapter(getActivity(), R.layout.spinner_slot);
			adapter.addAll(result);
			adapter.add("Select Sport");
			spSports.setAdapter(adapter);
			spSports.setSelection(adapter.getCount());
		}

		String[] gender = getResources().getStringArray(R.array.gender_search);
		SpinnerSportsAdapter adapter = new SpinnerSportsAdapter(getActivity(), R.layout.spinner_slot);
		adapter.addAll(gender);
		adapter.add("Male");
		spGender.setAdapter(adapter);
		spGender.setSelection(adapter.getCount());
	}

	private ArrayList<String> LoadSports() {

		ArrayList<String> sList = new ArrayList<String>();
		sportArrayList = new ArrayList<MySportsModel>();
		String response = Utility.LoadPref(getActivity(), AppTokens.SportList);

		if (response.contains("[")) {
			try {
				JSONArray sportList = new JSONArray(response);

				for (int i = 0; i < sportList.length(); i++) {

					JSONObject obj = sportList.getJSONObject(i);
					if (!obj.optString("status").equals("") && obj.optString("status").equalsIgnoreCase("success")) {
						JSONArray message = obj.getJSONArray("message");
						for (int j = 0; j < message.length(); j++) {
							JSONObject messageData = message.getJSONObject(j);
							MySportsModel model = new MySportsModel();
							model.setSportId(messageData.optString("sportId"));
							model.setSportName(messageData.optString("sportName"));
							model.setLogo(messageData.optString("logo"));
							model.setBaseImage(messageData.optString("baseImage"));
							sList.add(messageData.optString("sportName"));
							sportArrayList.add(model);

						}

					}
				}
			} catch (JSONException e) {
				L.m(e.toString());
				return null;
			}
		} else {
			L.m("Invalid JSON found : " + response);
			return null;
		}

		return sList;
	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		Button btn;
		ProgressBar pBar;
		Activity activity;

		public HttpTask(Activity activity, Button btn, ProgressBar pBar) {
			this.btn = btn;
			this.pBar = pBar;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			btn.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			int flagSelect = 0;

			// L.m(sltSportId+"\n"+selectGender+"\n"+tvRadious.getText().toString()+"\n"+tvSAge.getText().toString()+"\n"+tvEAge.getText().toString()+"\n"+acCityOrApp.getText().toString());
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if(activity!=null){
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
			}
			params.add(new BasicNameValuePair("sportId", sltSportId));
			params.add(new BasicNameValuePair("gender", selectGender));
			params.add(new BasicNameValuePair("radius", tvRadious.getText().toString()));
			params.add(new BasicNameValuePair("minAge", tvSAge.getText().toString()));
			params.add(new BasicNameValuePair("maxAge", tvEAge.getText().toString()));
			params.add(new BasicNameValuePair("clubOrApartment", acCityOrApp.getText().toString()));
			if (activity != null) {
				if (!LoadPref(AppTokens.GpsLatitude).equals("")) {
					params.add(new BasicNameValuePair("latitude", LoadPref(AppTokens.GpsLatitude)));
				} else {
					params.add(new BasicNameValuePair("latitude", AppTokens.lat));
				}
				if (!LoadPref(AppTokens.GpsLongitude).equals("")) {
					params.add(new BasicNameValuePair("longitude", LoadPref(AppTokens.GpsLongitude)));
				} else {
					params.add(new BasicNameValuePair("longitude", AppTokens.lng));
				}
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					for (int i = 0; i < message.length(); i++) {
						JSONObject obj = message.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";
							// SavePref(AppTokens.SearchBuddyData,
							// json.toString());
							searchBuddyData = message.toString();
						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = e.toString()+response;
					return "server";
				}
			} else {
				L.m("Server Error : " + response);
				ErrorMessage = response;
				return "server";
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				pBar.setVisibility(View.GONE);
				btn.setVisibility(View.VISIBLE);
				if (result != null) {
					// L.m(result);
					// if (result.equalsIgnoreCase("success")) {
					// comm.onFindSbuddyzListener(true,
					// sltSportName,sltSportBaseImage, searchBuddyData);
					// } else {
					// Toast.makeText(getActivity(), result,
					// Toast.LENGTH_SHORT).show();
					// }
					switch (result) {
					case "success":
						if (LoadPref(AppTokens.GpsUpdateStatue).equals("")) {
							Toast.makeText(getActivity(), getSt(R.string.search_buddy_home_location_msg),
									Toast.LENGTH_SHORT).show();
						}
						if (acCityOrApp.getText().toString().length() != 0
								&& tvRadious.getText().toString().equalsIgnoreCase("5")) {
							MainFindSbuddyzView.SWITCH = 2;
						} else {
							MainFindSbuddyzView.SWITCH = 1;
						}

						comm.onFindSbuddyzListener(true, sltSportId, sltSportName, sltSportBaseImage, searchBuddyData);

						break;
					case "server":
						showServerErrorAlertBox(ErrorMessage);
						break;
					default:
						L.m(result);
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
						break;
					}
				}
			}

		}
	}

	public static ArrayList<String> autocomplete(String input) {
		ArrayList<String> resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(
					AppTokens.PLACES_API_BASE + AppTokens.TYPE_AUTOCOMPLETE + AppTokens.OUT_JSON);
			sb.append("?key=" + AppTokens.API_KEY);
			sb.append("&components=country:in");
			sb.append("&components=city:dl");
			sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			URL url = new URL(sb.toString());

			System.out.println("URL: " + url);
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e("Find Buddyz", "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e("Find Buddyz", "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {

			L.m(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				String res = predsJsonArray.getJSONObject(i).getString("description");
				res = res.substring(0, res.length() - 7);
				System.out.println(res);
				System.out.println("============================================================");
				resultList.add(res);

			}
		} catch (JSONException e) {
			Log.e("Find Buddyz", "Cannot process JSON results", e);
		}

		return resultList;
	}

	public class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
		private ArrayList<String> resultList;

		public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);
		}

		@Override
		public int getCount() {
			return resultList.size();
		}

		@Override
		public String getItem(int index) {
			return resultList.get(index);
		}

		@Override
		public Filter getFilter() {
			Filter filter = new Filter() {
				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					FilterResults filterResults = new FilterResults();
					if (constraint != null) {
						// Retrieve the autocomplete results.
						resultList = autocomplete(constraint.toString());

						// Assign the data to the FilterResults
						filterResults.values = resultList;
						filterResults.count = resultList.size();
					}
					return filterResults;
				}

				@Override
				protected void publishResults(CharSequence constraint, FilterResults results) {
					if (results != null && results.count > 0) {
						notifyDataSetChanged();
					} else {
						notifyDataSetInvalidated();
					}
				}
			};
			return filter;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		// if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
		// showGPSSettingsAlert();
		// }
	}

	public void showGPSSettingsAlert() {
		// util.startTTS("please enable gps");
		// alertFlag=1;
		alertDialog = new AlertDialog.Builder(getActivity());

		// Setting Dialog Title
		alertDialog.setTitle(getSt(R.string.GPSSetting));

		// Setting Dialog Message
		alertDialog.setMessage(getSt(R.string.GPSmessage));

		// On pressing Settings button
		alertDialog.setPositiveButton(getSt(R.string.action_settings), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				getActivity().startActivity(intent);
			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton(getSt(R.string.Cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	@Override
	public void onGPSUpdate(Location location) {
		// TODO Auto-generated method stub
		updateLati = location.getLatitude();
		updateLongi = location.getLongitude();
		if (activity != null) {
			SavePref(AppTokens.GpsUpdateStatue, "updated");
			SavePref(AppTokens.GpsLatitude, String.valueOf(location.getLatitude()));
			SavePref(AppTokens.GpsLongitude, String.valueOf(location.getLongitude()));
		}
		// Toast.makeText(getActivity(), "updateLati :" + updateLati +
		// "updateLongi :" + updateLongi, Toast.LENGTH_SHORT).show();
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					httpTask = new HttpTask(getActivity(), btnSearchBuddy, progressBar);
					httpTask.execute(findSbuddy);
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
}
