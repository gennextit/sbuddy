package com.ajsoft.sbuddy.fragments.venu;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.FindCoachReviewsAdapter;
import com.ajsoft.sbuddy.model.FindVenueModel;
import com.ajsoft.sbuddy.model.VenuGalleryAdapter;
import com.ajsoft.sbuddy.util.L;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainFindVenuDetailGallery extends CompactFragment{
	
	String JsonData,venueId,sportId;
	ArrayList<FindVenueModel> cList;
	ViewGalleryTask viewGalleryTask;
	ImageLoader imageLoader;
	ListView lvMain;
	ProgressBar progressBar;
	
	public void setData(String JsonData,String venueId,String sportId){
		this.JsonData=JsonData; 
		this.venueId=venueId; 
		this.sportId=sportId; 
		
	}
	
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (viewGalleryTask != null) {
			viewGalleryTask.onAttach(activity);
		} 
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (viewGalleryTask != null) {
			viewGalleryTask.onDetach();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v =inflater.inflate(R.layout.main_find_venu_detail_gallery, container,false);
		imageLoader = new ImageLoader(getActivity());

		lvMain = (ListView) v.findViewById(R.id.lv_main_find_venue_detail_gallery); 
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
 
		if(JsonData!=null){
			viewGalleryTask=new ViewGalleryTask(getActivity());
			viewGalleryTask.execute(JsonData);
		}
		
		return v;
	}
	
	private class ViewGalleryTask extends AsyncTask<String, Void, ArrayList<FindVenueModel>> {

		Activity activity;

		public ViewGalleryTask(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected ArrayList<FindVenueModel> doInBackground(String... params) {
			// TODO Auto-generated method stub
			String response;
			cList = new ArrayList<FindVenueModel>();
			response = params[0];
			// L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					JSONArray main = messageObj.getJSONArray("message");
					for (int k = 0; k < main.length(); k++) {
						JSONObject sub = main.getJSONObject(k);
						L.m(sub.optString("venueId") + " : " + (venueId));
						if (sub.optString("venueId").equalsIgnoreCase(venueId)) {
							JSONArray galleryImage = sub.getJSONArray("galleryImage");
							//L.m(galleryImage.toString());
							for (int i = 0; i < galleryImage.length(); i++) {
								//L.m(galleryImage.optString(i));
								FindVenueModel ob=new FindVenueModel();
								ob.setImageUrl(galleryImage.optString(i));
								
								cList.add(ob);
							}
						}
					}
				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return cList;
		}

		@Override
		protected void onPostExecute(ArrayList<FindVenueModel> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (activity != null) {
				if (result != null) {
				 VenuGalleryAdapter ExpAdapter = new VenuGalleryAdapter(activity,
							R.layout.custom_find_venu_gallery, result);
					lvMain.setAdapter(ExpAdapter);
				}else {
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lvMain.setAdapter(adapter);
				}

			}
		}
	}
}
