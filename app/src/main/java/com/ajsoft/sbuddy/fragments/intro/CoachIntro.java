package com.ajsoft.sbuddy.fragments.intro;
 

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.icomm.Communicator;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class CoachIntro extends Fragment implements View.OnClickListener{
	Communicator comm;
	LinearLayout ll;
	
	FragmentManager manager;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view =inflater.inflate(R.layout.intro_coach, container,false);
		ll=(LinearLayout)view.findViewById(R.id.ll_intro_coach);
		ll.setOnClickListener(this);
		manager=getFragmentManager();
		return view;
	}
	
	@Override
	public void onClick(View v) {
		manager.popBackStack();
	}

}

