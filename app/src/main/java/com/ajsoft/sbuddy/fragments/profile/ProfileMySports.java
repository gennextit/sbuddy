package com.ajsoft.sbuddy.fragments.profile;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.ProfileActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.MySportsAdapter;
import com.ajsoft.sbuddy.model.MySportsModel;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ProfileMySports extends CompactFragment implements View.OnClickListener{

	private OnInitSkillListener comm;
	private TextView tvName,tvCity;
	private ImageView ivProfile;
	private ListView lvSportList;
	private Button btnSave;
	private LinearLayout llEditrofile;
	public ImageLoader imageLoader;
	private ArrayList<MySportsModel> mySportList; 
	private ProgressBar progressBar;
	LoadSportTask loadSportTask; 
	public static boolean editStatus;
	TextView tvEditrofile;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		imageLoader = new ImageLoader(getActivity());
		
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity); 
		if(loadSportTask!=null){
			loadSportTask.onAttach(activity);
		}
	}
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if(loadSportTask!=null){
			loadSportTask.onDetach();
		}
	}
//	public void setData(boolean editStatus) {
//		this.editStatus=editStatus;
//	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view =inflater.inflate(R.layout.profile_my_sports, container,false);
		initUi(view,savedInstanceState);
		return view;
	}

	private void initUi(View view,Bundle savedInstanceState) {
		tvName = (TextView) view.findViewById(R.id.tv_mysports_name);
		tvCity = (TextView) view.findViewById(R.id.tv_mysports_city);
		ivProfile = (ImageView) view.findViewById(R.id.iv_mysports_profilepic);
		lvSportList = (ListView) view.findViewById(R.id.lv_mysports_sportlist);
		btnSave = (Button) view.findViewById(R.id.btn_mysports);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		btnSave.setOnClickListener(this);
		llEditrofile = (LinearLayout) view.findViewById(R.id.ll_edit_profile);
		tvEditrofile = (TextView) view.findViewById(R.id.tv_edit_profile);
		llEditrofile.setOnClickListener(this);
		
		loadSportTask=new LoadSportTask(getActivity());
		loadSportTask.execute(AppSettings.getSportListAndPlayerSelectedSport);
		
		setTypsFace(btnSave);
		//L.m(AppSettings.getSportListAndPlayerSelectedSport);
		
		setData(Utility.LoadPref(getActivity(), Buddy.Name),Utility.LoadPref(getActivity(), Buddy.City),Utility.LoadPref(getActivity(), Buddy.Image));
		if(!AppTokens.EDIT_PROFILE_FROM.equals("main")){
			llEditrofile.setVisibility(View.GONE);
			ProfileMySkills.editStatus=true;
		}else{
			ChangeButtonListener();
		}

	}
	private void ChangeButtonListener() {
		if (editStatus) {
			btnSave.setText("Save & Proceed");
			tvEditrofile.setText("View");
			ProfileMySkills.editStatus=true;
		} else {
			btnSave.setText("Skip");
			tvEditrofile.setText("Edit");

		}
	}
	public void setData(String Name,String city,String image){
		tvName.setText(Name);
		tvCity.setText(city);
		Buddy.setBuddyProfileImage(getActivity(), imageLoader, ivProfile); 
		
		//imageLoader.DisplayImage( image, ivProfile,"profile");
	}

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {
		case R.id.btn_mysports:
			comm.onInitSkillListener(3,mySportList);
			break;
		case R.id.ll_edit_profile:
			if (editStatus) {
				editStatus = false;
			} else {
				editStatus = true;
				loadSportTask=new LoadSportTask(getActivity());
				loadSportTask.execute(AppSettings.getSportListAndPlayerSelectedSport);
				
			}
			ChangeButtonListener();
			
			break;

		}
	
	}
	public void setCommunicator(OnInitSkillListener onInitSkillListener) {
		this.comm = onInitSkillListener;
	}

	public interface OnInitSkillListener {
		public void onInitSkillListener(int screenPosition,ArrayList<MySportsModel> skillList);
	}

	
	
	private class LoadSportTask extends AsyncTask<String, Void, ArrayList<MySportsModel>> {
		
		private Activity activity;
		
		public LoadSportTask(Activity activity) {
			this.activity=activity;
		}
		
		public void onAttach(Activity activity) {
			this.activity=activity;
		}
		public void onDetach() {
			this.activity=null;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			btnSave.setVisibility(View.GONE);
			progressBar.setVisibility(View.VISIBLE);
			llEditrofile.setVisibility(View.GONE);
		}
		@Override
		protected ArrayList<MySportsModel> doInBackground(String... urls) {
			String response = "error";
			int flagSelect=0;
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("playerId", Utility.LoadPref(getActivity(), Buddy.PlayerId)));
			if(editStatus){
				HttpReq ob = new HttpReq();
				response=ob.makeConnection(urls[0],HttpReq.GET,params);
			}else{
				if(activity!=null)
				response=LoadPref(AppTokens.selectSportAndSportList);
				if(response.equals("")){
					HttpReq ob = new HttpReq();
					response=ob.makeConnection(urls[0],HttpReq.GET,params);
				}
			}
			mySportList=new ArrayList<MySportsModel>(); 
			L.m(response);
			if(response.contains("[")){
				try {
					JSONArray main=new JSONArray(response);
					JSONObject sub=main.getJSONObject(0);
					
					JSONArray sportList=sub.getJSONArray("sportList");
					if(activity!=null){
						Utility.SavePref(getActivity(), AppTokens.selectSportAndSportList, response);
						Utility.SavePref(getActivity(), AppTokens.SportList, sportList.toString());
						
					}
					
					JSONArray playerSportSelected=sub.getJSONArray("playerSportSelected");
					
					for(int i=0;i<sportList.length();i++){
						
						JSONObject obj=sportList.getJSONObject(i);
						if(!obj.optString("status").equals("")&& obj.optString("status").equalsIgnoreCase("success")){
							
							JSONArray message=obj.getJSONArray("message");
							for(int j=0;j<message.length();j++){
								JSONObject messageData=message.getJSONObject(j);
								MySportsModel model=new MySportsModel();
								model.setSportId(messageData.optString("sportId"));
								model.setSportName(messageData.optString("sportName"));
								model.setLogo(messageData.optString("logo"));
								flagSelect=0;
								for(int l=0;l<playerSportSelected.length();l++){
									JSONObject obj1=playerSportSelected.getJSONObject(l);
									if(!obj1.optString("status").equals("")&& obj1.optString("status").equalsIgnoreCase("success")){
										JSONArray message1=obj1.getJSONArray("message");
										for(int k=0;k<message1.length();k++){
											JSONObject messageData1=message1.getJSONObject(k);
											if(!messageData1.optString("skillLevel").equals("-1")&&messageData1.optString("sportId").equals(messageData.optString("sportId"))){
												flagSelect=1;
												//L.m("Loop :"+j+" = "+messageData1.optString("skillLevel"));
												model.setSkillLevel(messageData1.optString("skillLevel"));
											}
										}
									}
								}
								if(flagSelect==1){
									model.setSelect(1); 
								}else{
									model.setSelect(0); 
								}
								mySportList.add(model);
								
								model.setSkillList(mySportList);
								
							}

						}else if(obj.optString("status").equalsIgnoreCase("failure")){
							response="failure";
							ErrorMessage=obj.optString("message");
						}
					}
				} catch (JSONException e) {
					L.m(e.toString());
					ErrorMessage=e.toString();
					return null;
				}
			}else{
				L.m("Invalid JSON found : "+response);
				ErrorMessage=response;
				return null;
			}
			
			return mySportList;
			//return "success";
					
		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(ArrayList<MySportsModel> result) {
			if(activity!=null){
				progressBar.setVisibility(View.GONE);
				if(AppTokens.EDIT_PROFILE_FROM.equals("main")){
					llEditrofile.setVisibility(View.VISIBLE);
				}
				if(result!=null){
					btnSave.setVisibility(View.VISIBLE);
					MySportsAdapter ExpAdapter= new MySportsAdapter(activity,R.layout.custom_slot_sports, result);
					lvSportList.setAdapter(ExpAdapter);
				}else{
					btnSave.setVisibility(View.GONE);
					showServerErrorAlertBox(ErrorMessage);
				}
			}
		}
	}
	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					loadSportTask=new LoadSportTask(getActivity());
					loadSportTask.execute(AppSettings.getSportListAndPlayerSelectedSport);
					
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}
	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString("name", tvName.getText().toString());
		outState.putString("city", tvCity.getText().toString());
		outState.putString("image", Utility.LoadPref(getActivity(), Buddy.Image));
		
	}
}
