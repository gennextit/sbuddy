package com.ajsoft.sbuddy.fragments.profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ajsoft.sbuddy.ImageCroperActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.fragments.ImageCroper;
import com.ajsoft.sbuddy.fragments.VerifyMobile;
import com.ajsoft.sbuddy.gplus.AbstractGetNameNameTask;
import com.ajsoft.sbuddy.icomm.OnInitProfileListener;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.CountryCodeAdapter;
import com.ajsoft.sbuddy.model.CountryCodeModel;
import com.ajsoft.sbuddy.model.SpinnerSportsAdapter;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.CameraUtility;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;
import com.ajsoft.sbuddy.util.Validation;
import com.ajsoft.sbuddy.util.WakeLocker;
import com.ajsoft.sbuddy.util.image.FileUtils;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ProfileMyProfile extends CompactFragment implements View.OnTouchListener {
	private OnInitProfileListener comm;
	private ImageView ivImg;
	LinearLayout llEditrofile;
	private EditText etName, etAge, etMobile, etEmail, etAbout;
	private Spinner spGender;
	public AutoCompleteTextView acCity, acLocation, acClub, acCountryCode;
	public ImageLoader imageLoader;
	TextView tvEditrofile;
	public Button btnSave;
	private static final String LOG_TAG = "sBuddy";
	AlertDialog dialog = null;
	private String selectGender;
	// Captur Image or pickup image tokens
	//private Uri mImageCaptureUri, cropImageUri;
	private Uri mCropImageUri;

	public static Uri mTempCropImageUri;


	private static final int PICK_FROM_CAMERA = 1;
	private static final int CROP_FROM_CAMERA = 2;
	private static final int PICK_FROM_FILE = 3;
	private static final int REQUEST_CODE_IMAGE_PICKER = 6384; // onActivityResult
	// request

	private Bitmap cropedPhoto;
	private ProgressBar progressBar;
	Boolean isSoftKeyboardDisplayed = false;
	HttpTask httpTask;
	static ArrayList<String> listLocationId;
	HttpAsyncTaskLogIn httpAsyncTaskLogIn;
	GetGeoLocationTask getGeoLocationTask;
	private Activity act;
	private String googlePlaceId = "";
	private String gcmRegId;
	ArrayList<CountryCodeModel> countryList;
	boolean editStatus;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		this.act = activity;
		if (httpTask != null) {
			httpTask.onAttach(getActivity());
		}
		if (httpAsyncTaskLogIn != null) {
			httpAsyncTaskLogIn.onAttach(getActivity());
		}
		if (getGeoLocationTask != null) {
			getGeoLocationTask.onAttach(getActivity());
		}

	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (httpTask != null) {
			httpTask.onDetach();
		}
		if (httpAsyncTaskLogIn != null) {
			httpAsyncTaskLogIn.onDetach();
		}
		if (getGeoLocationTask != null) {
			getGeoLocationTask.onDetach();
		}
	}

	public void setData(boolean editStatus) {
		this.editStatus = editStatus;
	}

	public void setCommunicator(OnInitProfileListener onInitProfileListener) {
		this.comm = onInitProfileListener;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		imageLoader = new ImageLoader(getActivity());

	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isName(etName, true)) {
			ret = false;
		} else if (!Validation.isAge(etAge)) {
			ret = false;
		} else if (!Validation.is10DigitMobile(etMobile)) {
			ret = false;
		} else if (!Validation.isEmpty(acCity)) {
			ret = false;
		} else if (!Validation.isEmpty(acLocation)) {
			ret = false;
		}
		return ret;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.profile_my_profile, container, false);
		initUi(view);
		getActivity().registerReceiver(mHandleMessageReceiver, new IntentFilter(AppTokens.DISPLAY_MESSAGE_ACTION));
		// Getting status
		Utility.SavePref(getActivity(), AppTokens.APP_USER, "sbuddy");

		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

		// Showing status
		if (status == ConnectionResult.SUCCESS) {
			SavePref(AppTokens.GCM_ID, gcmRegId);
			gcmRegId = getGCMRegId();
		} else {
			Toast.makeText(getActivity(),
					"This app won't run without Google Play services, which are missing from your phone.",
					Toast.LENGTH_SHORT).show();
			// SavePref(AppTokens.GCM_ID, "test");
		}
		return view;
	}

	public void setCropedImageAndUri(Bitmap bmp,Uri uri){
		//ivImg.setImageBitmap(bmp);

		if(uri!=null){
			ivImg.setImageURI(uri);
			mCropImageUri=uri;

		}
		mTempCropImageUri=null;
	}

	private void initUi(View view) {

		L.m("OnCreateView_ProfileActivity");

		etName = (EditText) view.findViewById(R.id.et_profile_name);
		etAge = (EditText) view.findViewById(R.id.et_profile_age);
		etMobile = (EditText) view.findViewById(R.id.et_profile_mobile);
		etEmail = (EditText) view.findViewById(R.id.et_profile_email);
		etAbout = (EditText) view.findViewById(R.id.et_profile_about);
		ivImg = (ImageView) view.findViewById(R.id.imageView1);
		llEditrofile = (LinearLayout) view.findViewById(R.id.ll_edit_profile);
		tvEditrofile = (TextView) view.findViewById(R.id.tv_edit_profile);
		spGender = (Spinner) view.findViewById(R.id.sp_profile_gender);
		acCity = (AutoCompleteTextView) view.findViewById(R.id.ac_profile_city);
		acLocation = (AutoCompleteTextView) view.findViewById(R.id.ac_profile_location);
		acClub = (AutoCompleteTextView) view.findViewById(R.id.ac_profile_cluborApartment);
		acCountryCode = (AutoCompleteTextView) view.findViewById(R.id.ac_profile_countryCode);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		btnSave = (Button) view.findViewById(R.id.btn_myprofile_save);

		// TextView
		// actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
		// actionbarTitle.setText(setActionBarTitle("MY PROFILE"));
		etName.setOnTouchListener(this);
		etAge.setOnTouchListener(this);
		etMobile.setOnTouchListener(this);
		etEmail.setOnTouchListener(this);
		etAbout.setOnTouchListener(this);
		acCity.setOnTouchListener(this);
		acClub.setOnTouchListener(this);
		acCountryCode.setOnTouchListener(this);

		setTypsFace(etName, etAge, etMobile, etEmail, etAbout);
		setTypsFace(acCity, acLocation, acClub, acCountryCode);
		setTypsFace(btnSave);
		LoadCountryCode(acCountryCode);

		// if (editStatus) {
		// llEditrofile.setVisibility(View.GONE);
		// } else {
		// llEditrofile.setVisibility(View.VISIBLE);
		// }

		if (AppTokens.EDIT_PROFILE_FROM.equals("main")) {
			ChangeButtonListener();
			llEditrofile.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (editStatus) {
						editStatus = false;
					} else {
						editStatus = true;
					}
					ChangeButtonListener();
				}
			});
		} else {
			llEditrofile.setVisibility(View.GONE);
			ProfileMySports.editStatus = true;
			ProfileMySkills.editStatus = true;

		}

		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				onSaveButtonClick();

			}
		});

		acCity.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.autocomplete_list_item, false));
		acCity.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				String str = (String) adapterView.getItemAtPosition(position);
				googlePlaceId = listLocationId.get(position);
				SavePref(AppTokens.GooglePlaceId, googlePlaceId);
				L.m("locationId :" + googlePlaceId);
				// Toast.makeText(ProfileActivity.this, str,
				// Toast.LENGTH_SHORT).show();
			}
		});
		acLocation
				.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.autocomplete_list_item, true));
		acLocation.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				String str = (String) adapterView.getItemAtPosition(position);
				googlePlaceId = listLocationId.get(position);
				SavePref(AppTokens.GooglePlaceId, googlePlaceId);
				L.m("locationId :" + googlePlaceId);
				// Toast.makeText(ProfileActivity.this, str,
				// Toast.LENGTH_SHORT).show();
			}
		});
		acClub.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.autocomplete_list_item, true));
		acClub.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				String str = (String) adapterView.getItemAtPosition(position);
				// Toast.makeText(ProfileActivity.this, str,
				// Toast.LENGTH_SHORT).show();
			}
		});
		// ArrayAdapter adapter=new
		// ArrayAdapter<String>(getApplicationContext(),
		// android.R.layout.simple_spinner_item,R.id, R.array.list_item);
		// spGender.setAdapter(adapter);
		String[] gender = getResources().getStringArray(R.array.gender);
		SpinnerSportsAdapter adapter = new SpinnerSportsAdapter(getActivity(), R.layout.spinner_slot);
		adapter.addAll(gender);
		adapter.add("Select Gender");
		spGender.setAdapter(adapter);
		spGender.setSelection(adapter.getCount());
		spGender.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				String gender = spGender.getSelectedItem().toString();
				selectGender = gender;
				L.m(gender);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		Intent intent = getActivity().getIntent();
		String from = intent.getStringExtra("from");
		//L.m("form" + from);
		// if (from.equalsIgnoreCase("main")) {
		// }
		if (from.equalsIgnoreCase("google")) {
			try {
				JSONObject profileData = new JSONObject(AbstractGetNameNameTask.GOOGLE_USER_DATE);
				// L.m(profileData.toString());
				if (profileData.has("picture")) {
					Utility.SavePref(getActivity(), Buddy.Image, profileData.getString("picture"));
					imageLoader.DisplayImage(profileData.getString("picture"), ivImg, "profile", false);

					// new
					// LoadImage().execute(profileData.getString("picture"));
				}
				if (profileData.has("name")) {
					Utility.SavePref(getActivity(), Buddy.Name, profileData.getString("name"));
					etName.setText(profileData.getString("name"));
					etName.setSelection(etName.getText().length());


				}

			} catch (JSONException e) {
				L.m(e.toString());
			}
		} else {

		}
		Buddy.setPlayerDetail(act, imageLoader, etName, ivImg, etAge, spGender, acCountryCode, etMobile, etEmail,
				acCity, acLocation, acClub, etAbout, googlePlaceId, selectGender);
		String pGooglePlaceId = Utility.LoadPref(getActivity(), AppTokens.GooglePlaceId);
		String pGender = Utility.LoadPref(getActivity(), Buddy.GENDER);
		if (pGooglePlaceId != null) {
			googlePlaceId = pGooglePlaceId;
		}

		if (pGender != null) {
			selectGender = pGender;
			switch (selectGender) {
				case "Male":
					spGender.setSelection(0);
					break;
				case "Female":
					spGender.setSelection(1);
					break;
				case "TransGender":
					spGender.setSelection(2);
					break;

			}
		}

		ivImg.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (editStatus) {
					/*if(isStoragePermissionGranted()){*/
					//OptionAlertBox(getActivity());
					Intent ob=new Intent(getActivity(),ImageCroperActivity.class);
					startActivity(ob);
					//CropImage.startPickImageActivity(getActivity());

				} else {
					Toast.makeText(getActivity(), "Please Click edit button first", Toast.LENGTH_LONG).show();
				}
			}
		});

		SaveImageFromUrl();
	}

	public void onSaveButtonClick() {

		if (editStatus) {
			if (checkValidation()) {
				if (isSoftKeyboardDisplayed) {
					hideKeybord();
					isSoftKeyboardDisplayed = false;
				}

				getGeoLocationTask = new GetGeoLocationTask(getActivity(), btnSave, progressBar);
				getGeoLocationTask
						.execute(AppTokens.GEOLOCATION_BASE + googlePlaceId + AppTokens.GEOLOCATION_KEY);

			}
		} else {
			comm.onInitProfileListener(2, Utility.LoadPref(getActivity(), Buddy.Image),
					etName.getText().toString(), etAge.getText().toString(), selectGender,
					acCity.getText().toString(), acLocation.getText().toString(), acClub.getText().toString(),
					etAbout.getText().toString());
		}
	}

	/*public  boolean isStoragePermissionGranted() {
		if (Build.VERSION.SDK_INT >= 23) {
			if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
					== PackageManager.PERMISSION_GRANTED) {
				L.m("Permission is granted");
				return true;
			} else {

				L.m("Permission is revoked");
				ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppTokens.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
				return false;
			}
		}
		else { //permission is automatically granted on sdk<23 upon installation
			L.m("Permission is granted");
			return true;
		}


	}*/

	/*@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static boolean checkPermission(final Context context)
	{
		int currentAPIVersion = Build.VERSION.SDK_INT;
		if(currentAPIVersion>=android.os.Build.VERSION_CODES.M)
		{
			if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
				if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.READ_EXTERNAL_STORAGE)) {
					AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
					alertBuilder.setCancelable(true);
					alertBuilder.setTitle("Permission necessary");
					alertBuilder.setMessage("External storage permission is necessary");
					alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
						@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
						public void onClick(DialogInterface dialog, int which) {
							ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, AppTokens.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
						}
					});
					AlertDialog alert = alertBuilder.create();
					alert.show();

				} else {
					ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, AppTokens.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
				}
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}*/

	/*@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case AppTokens.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					*//*if(userChoosenTask.equals("Take Photo"))
						cameraIntent();
					else if(userChoosenTask.equals("Choose from Library"))
						galleryIntent();*//*
					OptionAlertBox(getActivity());
				} else {
					//code for deny
				}
				break;
		}
	}*/

	/*public void openImageCroper(Bitmap bmp){
		ImageCroper imageCroper = new ImageCroper();
		imageCroper.setImgBmp(bmp);
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.add(android.R.id.content, imageCroper, "imageCroper");
		transaction.addToBackStack("imageCroper");
		transaction.commit();
	}*/
	public void openImageCroper(Uri uri){
		/*ImageCroper imageCroper = new ImageCroper();
		ImageCroper.SWITCH=ImageCroper.PROFILE;
		imageCroper.setImgUri(uri);
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.add(android.R.id.content, imageCroper, "imageCroper");
		transaction.addToBackStack("imageCroper");
		transaction.commit();*/
		CropImage.activity(uri)
				.setGuidelines(CropImageView.Guidelines.ON)
				.start(getActivity());
	}

	private void SaveImageFromUrl() {
		if (LoadPref(Buddy.profileImageUri).equals("")) {
			new SaveImageFromUrlTask().execute();
		}
	}

	private class SaveImageFromUrlTask extends AsyncTask<Void, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(Void... params) {
			Bitmap bitmap = imageLoader.getImageBitmap(Utility.LoadPref(getActivity(), Buddy.Image));
			SaveProfileImage(bitmap);

			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap bitmap) {
			// TODO Auto-generated method stub
			super.onPostExecute(bitmap);
			if (bitmap != null) {
				L.m("Bitmap ************** not null");
				// bmp=bitmap;
				mCropImageUri = Uri.fromFile(CameraUtility.getOutputCropedMediaFile());
				SavePref(Buddy.profileImageUri, mCropImageUri.toString());
				CameraUtility.saveImageExternal(getActivity(), bitmap, mCropImageUri);

			} else {
				L.m("Bitmap ************** null");
			}

		}
	}

	private void ChangeButtonListener() {
		if (editStatus) {
			ProfileMySports.editStatus = true;
			ProfileMySkills.editStatus = true;
			tvEditrofile.setText("View");
			btnSave.setText("Save & Proceed");
			etName.setEnabled(true);
			etAge.setEnabled(true);
			acCountryCode.setEnabled(true);
			etMobile.setEnabled(true);
			etEmail.setEnabled(true);
			acCity.setEnabled(true);
			acLocation.setEnabled(true);
			acClub.setEnabled(true);
			etAbout.setEnabled(true);
		} else {
			ProfileMySports.editStatus = false;
			ProfileMySkills.editStatus = false;
			tvEditrofile.setText("Edit");
			btnSave.setText("Skip");
			etName.setEnabled(false);
			etAge.setEnabled(false);
			acCountryCode.setEnabled(false);
			etMobile.setEnabled(false);
			etEmail.setEnabled(false);
			acCity.setEnabled(false);
			acLocation.setEnabled(false);
			acClub.setEnabled(false);
			etAbout.setEnabled(false);
		}
	}

	public void OptionAlertBox(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custom_take_camera_dialog, null);
		dialogBuilder.setView(v);
		Button button1 = (Button) v.findViewById(R.id.button1);
		Button button2 = (Button) v.findViewById(R.id.button2);
		// if decline button is clicked, close the custom dialog
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

				mCropImageUri = getOutputMediaFileUri();
				if(mCropImageUri!=null){
					intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mCropImageUri);
				}

				try {
					intent.putExtra("return-data", true);
					/*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);*/

					startActivityForResult(intent, PICK_FROM_CAMERA);
				} catch (ActivityNotFoundException e) {
					e.printStackTrace();
				}

			}
		});
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				// pick from file
				// Intent intent = new Intent();
				//
				// intent.setType("image/*");
				// intent.setAction(Intent.ACTION_GET_CONTENT);
				//
				// startActivityForResult(Intent.createChooser(intent, "Complete
				// action using"), PICK_FROM_FILE);

				Intent target = FileUtils.createGetContentIntent();
				// Create the chooser Intent
				Intent intent = Intent.createChooser(target, "Select Image");
				try {
					startActivityForResult(intent, REQUEST_CODE_IMAGE_PICKER);
				} catch (ActivityNotFoundException e) {
					// The reason for the existence of aFileChooser
				}

			}
		});
		dialog = dialogBuilder.create();
		dialog.show();

	}

	@Override
	public void onResume() {
		super.onResume();
		L.m("ProfileMyProfile onResume");
	}

	/*@Override
	@SuppressLint("NewApi")
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		L.m("ProfileMyProfile onActivityResult");
		if (resultCode != getActivity().RESULT_OK)
			return;



		boolean requirePermissions;
		// handle result of pick image chooser
		if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
			Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

			// For API >= 23 we need to check specifically that we have permissions to read external storage.
			requirePermissions = false;
			if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
				// request permissions and handle the result in onRequestPermissionsResult()
				requirePermissions = true;
				mCropImageUri = imageUri;
				requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
			} else {
				// no permissions required or already grunted, can start crop image activity
				startCropImageActivity(imageUri);
			}
		}

		// handle result of CropImageActivity
		if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
			CropImage.ActivityResult result = CropImage.getActivityResult(data);
			if (resultCode == Activity.RESULT_OK) {
				ivImg.setImageURI(result.getUri());
				Toast.makeText(getActivity(),result.getUri().getPath(), Toast.LENGTH_LONG).show();
				*//*((TextView)findViewById(R.id.tv_detail)).setText(result.getUri().getPath());
				((ImageView) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());*//*
			} else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
				Toast.makeText(getActivity(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
			}
		}

		if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
			CropImage.ActivityResult result = CropImage.getActivityResult(data);
			if (resultCode == getActivity().RESULT_OK) {
				Uri resultUri = result.getUri();
				cropCapturedImage(resultUri);
			} else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
				Exception error = result.getError();
			}
		}

		switch (requestCode) {
			case PICK_FROM_CAMERA:
				//cropCapturedImage(mImageCaptureUri);
				//openImageCroper(mCropImageUri);
				// doCrop();

				if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), mCropImageUri)) {
					// request permissions and handle the result in onRequestPermissionsResult()
					requirePermissions = true;
					mCropImageUri = mCropImageUri;
					requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
				} else {
					// no permissions required or already grunted, can start crop image activity
					startCropImageActivity(mCropImageUri);
				}
				break;

			case REQUEST_CODE_IMAGE_PICKER:

				// If the file selection was successful
				if (resultCode == getActivity().RESULT_OK) {
					if (data != null) {
						// Get the URI of the selected file
						final Uri uri = data.getData();
						L.m("Uri = " + uri.toString());
						//
						try {
							// Get the file path from the URI
							*//*final String path = FileUtils.getPath(getActivity(), uri);
							mCropImageUri = Uri.fromFile(new File(path));
							if(mCropImageUri==null){
								mCropImageUri=uri;
							}
							openImageCroper(mCropImageUri);*//*
							if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), uri)) {
								// request permissions and handle the result in onRequestPermissionsResult()
								requirePermissions = true;
								mCropImageUri = uri;
								requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
							} else {
								// no permissions required or already grunted, can start crop image activity
								startCropImageActivity(uri);
							}
							//cropCapturedImage(mImageCaptureUri);
							//L.m("File Selected: " + path);

						} catch (Exception e) {
							L.m("FileSelectorTestActivity"+ "File select error"+ e.toString());
						}
					}
				}
				break;

			*//*case CROP_FROM_CAMERA:
				if(data!=null){
						Bundle extras = data.getExtras();
						cropedPhoto = extras.getParcelable("data");

						cropImageUri = Uri.fromFile(CameraUtility.getOutputCropedMediaFile());
						CameraUtility.saveImageExternal(getActivity(), cropedPhoto, cropImageUri);
						//ivImg.setImageBitmap(cropedPhoto);
						//L.m("cropImageUri**************" +cropImageUri);
						addImage(cropedPhoto, cropImageUri);

				}


				// File f = new File(mImageCaptureUri.getPath());
				//
				// if (f.exists())
				// f.delete();

				break;*//*



		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			// required permissions granted, start crop image activity
			startCropImageActivity(mCropImageUri);
		} else {
			Toast.makeText(getActivity(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
		}
	}
*/



	/*@Override
	@SuppressLint("NewApi")
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		// handle result of pick image chooser
		if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
			Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

			// For API >= 23 we need to check specifically that we have permissions to read external storage.
			boolean requirePermissions = false;
			if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
				// request permissions and handle the result in onRequestPermissionsResult()
				requirePermissions = true;
				cropImageUri = imageUri;
				requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
			} else {
				// no permissions required or already grunted, can start crop image activity
				startCropImageActivity(imageUri);
			}
		}

		// handle result of CropImageActivity
		if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
			CropImage.ActivityResult result = CropImage.getActivityResult(data);
			if (resultCode == getActivity().RESULT_OK) {
				//((ImageView) findViewById(R.id.quick_start_cropped_image))p
				ivImg.setImageURI(result.getUri());

			} else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
				Toast.makeText(getActivity(), "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
			}
		}
	}*/

	/*@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if (cropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			// required permissions granted, start crop image activity
			startCropImageActivity(cropImageUri);
		} else {
			Toast.makeText(getActivity(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
		}
	}
*/
	/**
	 * Start crop image activity for the given image.
	 */
	private void startCropImageActivity(Uri imageUri) {
		CropImage.activity(imageUri)
				.setAspectRatio(1,1)
				.setFixAspectRatio(true)
				.setGuidelines(CropImageView.Guidelines.ON)
				.start(getActivity());
	}

	private void addImage(Bitmap cropedPhoto2, Uri cropImageUri2) {
		ivImg.setImageBitmap(cropedPhoto2);
		SaveProfileImage(cropedPhoto2);
		//createImageFromBitmap(cropedPhoto);

	}

	private void SaveProfileImage(Bitmap cropedPhoto2) {
		Buddy.SaveBuddyProfileImage(getActivity(), cropedPhoto2);
	}

	/*
	 * Creating file uri to store image/video
	 */
	public Uri getOutputMediaFileUri() {
		return Uri.fromFile(getOutputMediaFile());
	}

	/*
	 * returning image / video
	 */
	private static File getOutputMediaFile() {

		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				AppTokens.PROFILE_IMAGE_DIRECTORY_NAME);

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(AppTokens.PROFILE_IMAGE_DIRECTORY_NAME,
						"Oops! Failed create " + AppTokens.PROFILE_IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
		File mediaFile;

		mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	private static File getOutputCropedMediaFile() {

		// External sdcard location
		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"sbuddyCrop");

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d(AppTokens.PROFILE_IMAGE_DIRECTORY_NAME,
						"Oops! Failed create " + AppTokens.PROFILE_IMAGE_DIRECTORY_NAME + " directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
		File mediaFile;

		mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	/*public void createImageFromBitmap(Bitmap bmp) {
		cropImageUri = Uri.fromFile(getOutputCropedMediaFile());
		L.m(cropImageUri.getPath());
		// File outputFile = new File(cropImageUri.getPath());

		// x,y is the starting point and width,height is the distance from start
		// createBitmap(Bitmap source, int x, int y, int width, int height)
		// Bitmap bitmap = Bitmap.createBitmap(bmp, 0, 0, 20, 20);

		// new AndroidBmpUtil().save(bmp, cropImageUri.getPath());
		cropImageUri = Uri.fromFile(CameraUtility.getOutputCropedMediaFile());
		SavePref(Buddy.profileImageUri, cropImageUri.toString());
		CameraUtility.saveImageExternal(getActivity(), bmp, cropImageUri);

		// OutputStream fout = null;
		//
		// try {
		// fout = new FileOutputStream(outputFile);
		// //if format is png(lossless) then quality argument is ignored
		// //(Bitmap.CompressFormat format, int quality, OutputStream stream)
		// bmp.compress(Bitmap.CompressFormat.PNG, 100, fout);
		// fout.flush();
		// fout.close();
		//
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}*/

	public void cropCapturedImage(Uri picUri) {
		// call the standard crop action intent
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		// indicate image type and Uri of image
		cropIntent.setDataAndType(picUri, "image/*");
		// set crop properties
		cropIntent.putExtra("crop", "true");
		// indicate aspect of desired crop
		cropIntent.putExtra("aspectX", 1);
		cropIntent.putExtra("aspectY", 1);
		// indicate output X and Y
		cropIntent.putExtra("outputX", 256);
		cropIntent.putExtra("outputY", 256);
		// retrieve data on return
		cropIntent.putExtra("return-data", true);
		// start the activity - we handle returning in onActivityResult
		// Exception will be thrown if read permission isn't granted
		cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

		startActivityForResult(cropIntent, 2);
	}

	private void rotateImage(Bitmap bitmap) {
		ExifInterface exifInterface = null;
		try {
			exifInterface = new ExifInterface(mCropImageUri.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
				ExifInterface.ORIENTATION_UNDEFINED);
		Matrix matrix = new Matrix();
		switch (orientation) {

			case ExifInterface.ORIENTATION_ROTATE_90:
				matrix.setRotate(90);
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				matrix.setRotate(180);
				break;
			default:
		}
		Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
		ivImg.setImageBitmap(rotatedBitmap);
	}

	// private class LoadImage extends AsyncTask<String, String, Bitmap> {
	// @Override
	// protected void onPreExecute() {
	// super.onPreExecute();
	//
	// }
	//
	// protected Bitmap doInBackground(String... args) {
	// Bitmap bitmap = null;
	// try {
	// bitmap = BitmapFactory.decodeStream((InputStream) new
	// URL(args[0]).getContent());
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return bitmap;
	// }
	//
	// protected void onPostExecute(Bitmap bmp) {
	//
	// if (bmp != null) {
	// ivImg.setImageBitmap(bmp);
	//
	// } else {
	//
	// }
	// }
	// }

	public static ArrayList<String> autocomplete(String input, String keyFilter, Boolean Check) {
		ArrayList<String> resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(
					AppTokens.PLACES_API_BASE + AppTokens.TYPE_AUTOCOMPLETE + AppTokens.OUT_JSON);
			sb.append("?key=" + AppTokens.API_KEY);
			sb.append("&components=country:in");
			sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			URL url = new URL(sb.toString());

			System.out.println("URL: " + url);
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e(LOG_TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e(LOG_TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {

			L.m(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length());
			listLocationId = new ArrayList<String>(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				String res = predsJsonArray.getJSONObject(i).getString("description");
				String placeId = predsJsonArray.getJSONObject(i).getString("place_id");
				if (Check) {
					if (res.contains(toTitleCase(keyFilter))) {
						res = res.substring(0, res.length() - 7);
						System.out.println(res);
						System.out.println("============================================================");
						resultList.add(res);
						listLocationId.add(placeId);
					}
				} else {
					res = res.substring(0, res.length() - 7);
					System.out.println(res);
					System.out.println("============================================================");
					resultList.add(res);
					listLocationId.add(placeId);
				}

			}
		} catch (JSONException e) {
			Log.e(LOG_TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}

	public class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
		private ArrayList<String> resultList;
		private Boolean Check;

		public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId, Boolean Check) {
			super(context, textViewResourceId);
			this.Check = Check;
			if (Check == false) {
				googlePlaceId = "";
			}
		}

		@Override
		public int getCount() {
			return resultList.size();
		}

		@Override
		public String getItem(int index) {
			return resultList.get(index);
		}

		@Override
		public Filter getFilter() {
			Filter filter = new Filter() {
				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					FilterResults filterResults = new FilterResults();
					if (constraint != null) {
						// Retrieve the autocomplete results.
						resultList = autocomplete(constraint.toString(), acCity.getText().toString(), Check);

						// Assign the data to the FilterResults
						filterResults.values = resultList;
						filterResults.count = resultList.size();
					}
					return filterResults;
				}

				@Override
				protected void publishResults(CharSequence constraint, FilterResults results) {
					if (results != null && results.count > 0) {
						notifyDataSetChanged();
					} else {
						notifyDataSetInvalidated();
					}
				}
			};
			return filter;
		}
	}

	public String LoadPref(Context context, String key) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		String data = sharedPreferences.getString(key, "");
		return data;
	}

	public void SavePref(Context context, String key, String value) {

		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();

	}

	private class GetGeoLocationTask extends AsyncTask<String, Void, String> {
		Button btn;
		ProgressBar pBar;
		Activity activity;
		String lattitude, longitude;

		public GetGeoLocationTask(Activity activity, Button btn, ProgressBar pBar) {
			this.btn = btn;
			this.pBar = pBar;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			btn.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			int flagSelect = 0;
			// if (!LoadPref(AppTokens.GpsLatitude).equals("")) {
			// return "success";
			// }

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 1);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONObject jsonObj = new JSONObject(response.toString());
					JSONObject result = jsonObj.getJSONObject("result");
					JSONObject geometry = result.getJSONObject("geometry");
					JSONObject location = geometry.getJSONObject("location");
					if (!location.optString("lat").equals("")) {
						output = "success";
						lattitude = location.optString("lat");
						longitude = location.optString("lng");
						L.m("lattitude : " + lattitude);
						L.m("longitude : " + longitude);
					} else {
						L.m("JSON incorrect path");
					}
				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					// return null;
					return "json";
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
				// return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				pBar.setVisibility(View.GONE);
				btn.setVisibility(View.VISIBLE);
				if (result != null) {
					if (result.equalsIgnoreCase("success")) {
						SavePref(AppTokens.GpsLatitude, lattitude);
						SavePref(AppTokens.GpsLongitude, longitude);

						switch (LoadPref(AppTokens.LoginStatus)) {
							case "g":
								httpTask = new HttpTask(getActivity(), btnSave, progressBar, "g",etName.getText().toString());
								httpTask.execute(AppSettings.setPlayerRegistrationByGoogle);
								break;
							case "f":
								httpTask = new HttpTask(getActivity(), btnSave, progressBar, "f",etName.getText().toString());
								httpTask.execute(AppSettings.setPlayerRegistrationByFacebook);
								break;
							case "m":
								httpAsyncTaskLogIn = new HttpAsyncTaskLogIn(getActivity(),etName.getText().toString(),etAge.getText().toString(),selectGender.toLowerCase(),
										acCity.getText().toString(),acLocation.getText().toString(),acClub.getText().toString(),etAbout.getText().toString(),
										acCountryCode.getText().toString(),etMobile.getText().toString(),etEmail.getText().toString());
								httpAsyncTaskLogIn.execute(AppSettings.updatePlayerProfileByM);
								// new
								// HttpAsyncTaskLogIn().execute(AppSettings.updatePlayerProfileByM);
								break;
						}

						// new
						// HttpAsyncTaskLogIn().execute(AppSettings.updatePlayerProfileByM);
						// comm.onFindVenueListener(true,sltSportName,sltSportBaseImage,searchVenueData);
					} else if (result.equalsIgnoreCase("json")) {
						Toast.makeText(getActivity(), "Please connect to internet and select city and location again from suggested list.", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					//Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					Toast.makeText(getActivity(), "Please connect to internet and select city and location again from suggested list.", Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		Button btn;
		ProgressBar pBar;
		private String from;
		Activity activity;
		String name;

		public HttpTask(Activity activity, Button btn, ProgressBar pBar, String from,String name) {
			this.btn = btn;
			this.pBar = pBar;
			this.from = from;
			this.name=name;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
			btn.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			int flagSelect = 0;
			/*if (!LoadPref(Buddy.PlayerId).equals("")) {
				return "success";
			}
*/
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (from.equals("f")) {
				params.add(new BasicNameValuePair("facebookId", LoadPref(Buddy.fbId)));
			} else {
				params.add(new BasicNameValuePair("googleId", LoadPref(Buddy.gmainId)));
			}
			L.m("Mac Address : " + getMACAddress(getActivity()));
			params.add(new BasicNameValuePair("macAddr", getMACAddress(getActivity()).toString()));
			params.add(new BasicNameValuePair("fullName", name));
			params.add(new BasicNameValuePair("image", LoadPref(Buddy.Image)));

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";
							if (activity != null) {
								SavePref(Buddy.PlayerId, obj.optString("message"));
								L.m(LoadPref(Buddy.PlayerId));
							}
						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					// return null;
					ErrorMessage = "Json Error :" + e.toString() + response;
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				ErrorMessage = "Invalid JSON found : " + response;
				return null;
				// return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				pBar.setVisibility(View.GONE);
				btn.setVisibility(View.VISIBLE);
				if (result != null) {
					// L.m(result);
					if (result.equalsIgnoreCase("success")) {

						httpAsyncTaskLogIn = new HttpAsyncTaskLogIn(getActivity(),etName.getText().toString(),etAge.getText().toString(),selectGender.toLowerCase(),
								acCity.getText().toString(),acLocation.getText().toString(),acClub.getText().toString(),etAbout.getText().toString(),
								acCountryCode.getText().toString(),etMobile.getText().toString(),etEmail.getText().toString());
						httpAsyncTaskLogIn.execute(AppSettings.updatePlayerProfileByM);

					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}
		}
	}

	private class HttpAsyncTaskLogIn extends AsyncTask<String, Void, String> {
		Activity activity;
		String name,age,gender,city,location,club,about,countryCode,mobile,email;

		public HttpAsyncTaskLogIn(Activity activity,String name,String age,String gender,String city,String location,String club,String about,String countryCode,String mobile,String email) {
			onAttach(activity);
			this.name=name;
			this.age=age;
			this.gender=gender;
			this.city=city;
			this.location=location;
			this.club=club;
			this.about=about;
			this.countryCode=countryCode;
			this.mobile=mobile;
			this.email=email;

		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			btnSave.setVisibility(View.GONE);

			if(activity!=null){
				SavePref(Buddy.Name, etName.getText().toString());
				SavePref(Buddy.City, acCity.getText().toString());
				SavePref(Buddy.AGE, etAge.getText().toString());
				SavePref(Buddy.GENDER, selectGender);
				SavePref(Buddy.LOCATION, acLocation.getText().toString());
				SavePref(Buddy.CLUB_OR_APPARTMENT, acClub.getText().toString());
				SavePref(Buddy.ABOUT, etAbout.getText().toString());
				SavePref(Buddy.countryCode, acCountryCode.getText().toString());
				SavePref(Buddy.Mobile, etMobile.getText().toString());
				SavePref(Buddy.CompleteMobile, acCountryCode.getText().toString()+etMobile.getText().toString());
				SavePref(Buddy.gmainId, etEmail.getText().toString());
			}
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			File sourceFile = null;
			String output = null;
			if (mCropImageUri != null) {
				sourceFile = new File(mCropImageUri.getPath());
				L.m("#########################cropImageUri not null");
				L.m(sourceFile.toString());
			} else {
				L.m("#########################cropImageUri null");
				if (activity != null) {
					if(LoadPref(ImageCroper.ProfileStatus).equals("")){
						if (!LoadPref(Buddy.profileImageUri).equals("")) {
							mCropImageUri = CameraUtility.StringToUri(LoadPref(Buddy.profileImageUri));
							sourceFile = new File(mCropImageUri.getPath());
							L.m(sourceFile.toString());
						} else {
							sourceFile = null;
						}
					}
				}
			}
			// Adding file data to http body
			@SuppressWarnings("deprecation")
			MultipartEntity entity = new MultipartEntity();
			try {
				L.m("PlayerId" + LoadPref(Buddy.PlayerId));
				L.m("selectGender" + selectGender);

				entity.addPart("playerId", new StringBody(LoadPref(Buddy.PlayerId)));
				if (activity != null) {
					entity.addPart("latitudeLongitude",
							new StringBody(LoadPref(AppTokens.GpsLatitude) + "," + LoadPref(AppTokens.GpsLongitude)));
					if(LoadPref(AppTokens.ENTRY).equals("")){
						entity.addPart("entry", new StringBody("F", ContentType.TEXT_PLAIN));
						SavePref(AppTokens.ENTRY,"done");
					}else{
						entity.addPart("entry", new StringBody("S", ContentType.TEXT_PLAIN));
					}

				}

				entity.addPart("fullName", new StringBody(name,ContentType.TEXT_PLAIN));
				entity.addPart("age", new StringBody(age, ContentType.TEXT_PLAIN));
				entity.addPart("gender", new StringBody(gender, ContentType.TEXT_PLAIN));
				entity.addPart("city", new StringBody(city, ContentType.TEXT_PLAIN));
				entity.addPart("location", new StringBody(location, ContentType.TEXT_PLAIN));
				entity.addPart("clubOrApartment", new StringBody(club, ContentType.TEXT_PLAIN));
				entity.addPart("about", new StringBody(about, ContentType.TEXT_PLAIN));
				entity.addPart("macAddr", new StringBody(getMACAddress(getActivity()).toString(), ContentType.TEXT_PLAIN));
				if (!LoadPref(AppTokens.GCM_ID).equals("")) {
					entity.addPart("gcmId", new StringBody(LoadPref(AppTokens.GCM_ID), ContentType.TEXT_PLAIN));
				} else {
					entity.addPart("gcmId", new StringBody("test", ContentType.TEXT_PLAIN));
				}
				if (sourceFile != null) {
					entity.addPart("image", new FileBody(sourceFile));
				} else {
					// entity.addPart("image", new StringBody("image not
					// available"));
				}
				entity.addPart("mobileNumber",
						new StringBody(countryCode + mobile, ContentType.TEXT_PLAIN));
				entity.addPart("email", new StringBody(email, ContentType.TEXT_PLAIN));



			} catch (UnsupportedEncodingException e) {
				L.m(e.toString());
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, entity);
			L.m(response);
			if (response.contains("[")) {
				String finalResult = response.substring(response.indexOf('['), response.indexOf(']') + 1);

				try {
					JSONArray data = new JSONArray(finalResult);
					JSONObject key = data.getJSONObject(0);
					if (!key.optString("status").equals("") && key.optString("status").equals("success")) {
						output = key.optString("status");
					} else if (key.optString("status").equals("failure")) {
						output = key.optString("message");
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					L.m(e.toString());
					ErrorMessage = e.toString() + response;
					return null;
				}

			} else {
				L.m(response);
				ErrorMessage = response;
				return null;
			}
			return output;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(String result) {
			if (activity != null) {
				progressBar.setVisibility(View.GONE);
				btnSave.setVisibility(View.VISIBLE);

				if (result != null) {
					if (result.equalsIgnoreCase("success")) {
						SavePref(Buddy.Name, etName.getText().toString());
						SavePref(Buddy.City, acCity.getText().toString());
						SavePref(Buddy.AGE, etAge.getText().toString());
						SavePref(Buddy.GENDER, selectGender);
						SavePref(Buddy.LOCATION, acLocation.getText().toString());
						SavePref(Buddy.CLUB_OR_APPARTMENT, acClub.getText().toString());
						SavePref(Buddy.ABOUT, etAbout.getText().toString());
						SavePref(Buddy.countryCode, acCountryCode.getText().toString());
						SavePref(Buddy.Mobile, etMobile.getText().toString());
						SavePref(Buddy.gmainId, etEmail.getText().toString());

						Buddy.SaveBuddyProfileImage(getActivity(),mCropImageUri);

						comm.onInitProfileListener(2, Utility.LoadPref(getActivity(), Buddy.Image),
								etName.getText().toString(), etAge.getText().toString(), selectGender,
								acCity.getText().toString(), acLocation.getText().toString(),
								acClub.getText().toString(), etAbout.getText().toString());

					} else {
						if(result.contains("Info252")){
							Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
						}else if(result.contains("Info259")){
							VerifyMobile verifyMobile = new VerifyMobile();
							verifyMobile.setCommunicator(getActivity());
							FragmentTransaction transaction = getFragmentManager().beginTransaction();
							transaction.add(R.id.group_profile, verifyMobile, "verifyMobile");
							transaction.addToBackStack("verifyMobile");
							transaction.commit();
						}else{
                            Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
						}
					}

				} else {
					showServerErrorAlertBox(ErrorMessage);
				}

			}

		}
	}

	public String photoCompress() {
		// converting bitmap into byte stream
		byte[] bitmapData;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		cropedPhoto.compress(Bitmap.CompressFormat.JPEG, 50, baos);
		byte[] imageBytes = baos.toByteArray();
		String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

		return encodedImage;
	}

	public String getGCMRegId() {
		String regId;
		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(act);

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		GCMRegistrar.checkManifest(act);

		// Get GCM registration id
		regId = GCMRegistrar.getRegistrationId(act);
		Log.e("@-regId", regId);
		// Check if regid already presents
		if (regId.equals("")) {
			// Registration is not present, register now with GCM
			GCMRegistrar.register(act, AppTokens.SENDER_ID);
			regId = GCMRegistrar.getRegistrationId(act);
			return regId;
		} else {
			SavePref(AppTokens.GCM_ID, regId);
		}
		return regId;
	}

	/**
	 * Receiving push messages
	 */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(AppTokens.EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getActivity().getApplicationContext());

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 */

			// Showing received message
			Toast.makeText(getActivity().getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG)
					.show();

			// Releasing wake lock
			WakeLocker.release();
		}
	};

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					getGeoLocationTask = new GetGeoLocationTask(getActivity(), btnSave, progressBar);
					getGeoLocationTask.execute(AppTokens.GEOLOCATION_BASE + googlePlaceId + AppTokens.GEOLOCATION_KEY);
				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	private void LoadCountryCode(AutoCompleteTextView sp) {
		// TODO Auto-generated method stub
		countryList = new ArrayList<CountryCodeModel>();
		String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
		for (int i = 0; i < rl.length; i++) {
			String[] g = rl[i].split(",");
			CountryCodeModel ob = new CountryCodeModel();
			ob.setPhoneCode(g[0]);
			countryList.add(ob);
		}
		CountryCodeAdapter adapter = new CountryCodeAdapter(getActivity(), R.layout.custom_slot, R.id.lbl_name,
				countryList);
		sp.setAdapter(adapter);
		if (sp.getText().toString().length() == 0) {
			setDefaultCode();
		}
	}

	private void setDefaultCode() {
		String CountryID = "";
		String CountryZipCode = null;

		TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
		// getNetworkCountryIso
		CountryID = manager.getSimCountryIso().toUpperCase();
		String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
		for (int i = 0; i < rl.length; i++) {
			String[] g = rl[i].split(",");
			if (g[1].trim().equals(CountryID.trim())) {
				CountryZipCode = g[0];
				break;
			}
		}
		if (CountryZipCode != null) {
			acCountryCode.setText(CountryZipCode);
		}
	}

	@Override
	public void onDestroy() {
		try {
			getActivity().unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(getActivity());
		} catch (Exception e) {
			L.m("UnRegister Receiver Error"+ "> " + e.getMessage());
		}
		super.onDestroy();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub

		isSoftKeyboardDisplayed = true;
		return false;
	}
}
