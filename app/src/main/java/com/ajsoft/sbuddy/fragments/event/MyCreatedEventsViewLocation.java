package com.ajsoft.sbuddy.fragments.event;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.util.Geocoding;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MyCreatedEventsViewLocation extends CompactFragment {

	String EventAddress, EventLocation, Direction;
	TextView tvEventAddress, tvEventLocation, tvDirection;
	LinearLayout llDirection, llAddress;
	// Google Map
	MapView mMapView;
	private GoogleMap googleMap;
	TextView tvAddrs;
	LoadAddress loadAddress;

	public void setData(String EventAddress, String EventLocation, String Direction) {
		this.EventAddress = EventAddress;
		this.EventLocation = EventLocation;
		this.Direction = Direction;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (loadAddress != null) {
			loadAddress.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (loadAddress != null) {
			loadAddress.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.my_created_events_view_location, container, false);

		tvEventAddress = (TextView) v.findViewById(R.id.tv_my_created_events_view_location_eventAddress);
		tvEventLocation = (TextView) v.findViewById(R.id.tv_my_created_events_view_location_eventLocation);
		tvDirection = (TextView) v.findViewById(R.id.tv_my_created_events_view_location_direction);
		llDirection = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_location_direction);
		llAddress = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_location_eventAddress);
		mMapView = (MapView) v.findViewById(R.id.mapView);

		if (EventAddress != null) {
			if (EventAddress.equals("")) {
				llAddress.setVisibility(View.GONE);
			} else {
				tvEventAddress.setText(EventAddress);
			}
		}
		if (EventLocation != null) {
			tvEventLocation.setText(EventLocation);
		}
		// if (Direction != null) {
		// if (Direction.equals("N/A")) {
		// llDirection.setVisibility(View.GONE);
		// } else {
		// tvDirection.setText(Direction);
		// }
		// }

		mMapView.onCreate(savedInstanceState);

		// mMapView.setSatellite(true); // Satellite View
		// mMapView.setStreetView(true); // Street View
		// mMapView.setTraffic(true);
		mMapView.onResume();// needed to get the map to display immediately

		try {
			MapsInitializer.initialize(getActivity().getApplicationContext());
		} catch (Exception e) {
			e.printStackTrace();
		}

		googleMap = mMapView.getMap();
		// googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		// googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		// googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		// googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);

		// You can also enable or disable the zoom gestures in the map by
		// calling the setZoomControlsEnabled(boolean) method
		// googleMap.getUiSettings().setZoomGesturesEnabled(true);
		if (EventLocation != null && !EventLocation.equals("")) {
			// Getting status
			int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

			// Showing status
			if (status == ConnectionResult.SUCCESS) {
				loadAddress = new LoadAddress(getActivity());
				loadAddress.execute();
			} else {
				Toast.makeText(getActivity(),
						"This app won't run without Google Map services, which are missing from your phone.",
						Toast.LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(getActivity(), "Location not Available", Toast.LENGTH_SHORT).show();
		}

		return v;
	}

	private class LoadAddress extends AsyncTask<Void, Void, LatLng> {

		Activity activity;

		public LoadAddress(Activity activity) {
			this.activity = activity;
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected LatLng doInBackground(Void... arg0) {

			return Geocoding.reverseGeocoding(getActivity(), EventLocation);
		}

		@Override
		protected void onPostExecute(LatLng latLang) {
			// TODO Auto-generated method stub
			super.onPostExecute(latLang);
			if (activity != null) {
				if (latLang != null) {
					showLocationOnMap(latLang);
				} else {
					Toast.makeText(getActivity(), "No location found", Toast.LENGTH_SHORT).show();
				}
			}
		}

	}

	private void showLocationOnMap(LatLng latLang) {

		// create marker
		MarkerOptions marker = new MarkerOptions().position(latLang).title(EventLocation);

		// // Changing marker icon
		// marker.icon(BitmapDescriptorFactory
		// .defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

		// adding marker
		googleMap.addMarker(marker);
		CameraPosition cameraPosition = new CameraPosition.Builder().target(latLang).zoom(14).build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

		// Perform any camera updates here

	}

	@Override
	public void onResume() {
		super.onResume();
		mMapView.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		mMapView.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mMapView.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		mMapView.onLowMemory();
	}

}
