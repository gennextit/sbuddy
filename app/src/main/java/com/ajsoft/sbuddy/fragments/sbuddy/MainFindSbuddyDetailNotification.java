package com.ajsoft.sbuddy.fragments.sbuddy;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ajsoft.sbuddy.MainActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.MyNotification;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainFindSbuddyDetailNotification extends CompactFragment {
    ImageView ivProfile;
    TextView tvName, tvGender, tvAge;
    LinearLayout tabProfile, tabSports, tabPosts, llBreak, llNotiConnect, llNotiReject;
    FragmentManager mannager;
    String chatStatus, sBuddy, about, location, clubNApartment, city;
    String JsonData, playerId, playerName, playerImage, playerGender, playerAge, playerStatus;
    ImageLoader ivLoader;
    TextView ActionBarHeading;
    ImageView ActionBack, ActionHome;
    ConnectRequest connectRequest;
    //	OnChatBuddyzListListener comm;
    public static boolean openFromMain;
    public static boolean openFromChat;
    ApprovedRequest approvedRequest;
    //int NOTIFICATION = 1, APPROVED = 2, REJECTED = 3;

    public void setData(String chatStatus, String sBuddy, String about, String location, String clubNApartment,
                        String city) {
        this.chatStatus = chatStatus;
        this.sBuddy = sBuddy;
        this.about = about;
        this.location = location;
        this.clubNApartment = clubNApartment;
        this.city = city;
    }

    public void setJSONData(String JSONData) {
        this.JsonData = JSONData;
    }

//	public void setCommunicator(OnChatBuddyzListListener onChatBuddyzListListener) {
//		this.comm = onChatBuddyzListListener;
//	}
//
//	public interface OnChatBuddyzListListener {
//		public void onChatBuddyzListListener(String sltBuddyId, String sltBuddyName, String sltBuddyImage);
//	}

    public void setPlayerProfile(String playerId, String playerName, String playerImage, String playerGender,
                                 String playerAge, String playerStatus) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.playerImage = playerImage;
        this.playerGender = playerGender;
        this.playerAge = playerAge;
        this.playerStatus = playerStatus;
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        if (connectRequest != null) {
            connectRequest.onAttach(activity);
        }
        if (approvedRequest != null) {
            approvedRequest.onAttach(activity);
        }
    }

    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
        if (connectRequest != null) {
            connectRequest.onDetach();
        }
        if (approvedRequest != null) {
            approvedRequest.onDetach();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.main_find_sbuddy_detail_notification, container, false);
        mannager = getFragmentManager();
        setActionBarOption(v);
        ivProfile = (ImageView) v.findViewById(R.id.iv_main_find_sbuddy_detail_profile);
        tvName = (TextView) v.findViewById(R.id.tv_main_find_sbuddy_detail_pName);
        tvGender = (TextView) v.findViewById(R.id.tv_main_find_sbuddy_detail_pGender);
        tvAge = (TextView) v.findViewById(R.id.tv_main_find_sbuddy_detail_pAge);
        tabProfile = (LinearLayout) v.findViewById(R.id.ll_main_find_sbuddy_detail_profileTab);
        tabSports = (LinearLayout) v.findViewById(R.id.ll_main_find_sbuddy_detail_sportsTab);
        tabPosts = (LinearLayout) v.findViewById(R.id.ll_main_find_sbuddy_detail_postsTab);
        llNotiConnect = (LinearLayout) v.findViewById(R.id.ll_main_find_sbuddy_detail_viewProfile_connect);
        llBreak = (LinearLayout) v.findViewById(R.id.ll_footer_break);
        llNotiReject = (LinearLayout) v.findViewById(R.id.ll_main_find_sbuddy_detail_viewProfile_reject);
        ivLoader = new ImageLoader(getActivity());

        if (playerName != null) {
            tvName.setText(playerName);
        }
        if (playerGender != null) {
            tvGender.setText(playerGender);
        }
        if (playerAge != null) {
            tvAge.setText(playerAge);
        }
        if (playerImage != null) {
            ivLoader.DisplayImage(playerImage, ivProfile, "profile", false);
        }

        tabProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                switchTab(1);
                setScreenDynamically(1);
            }
        });
        tabSports.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (JsonData != null) {
                    switchTab(2);
                    setScreenDynamically(2, JsonData, playerId);

                }
            }
        });
        tabPosts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                switchTab(3);
                setScreenDynamically(3);
            }
        });
        // if(connectStatus){
        // llConnect.setVisibility(View.GONE);
        // llBreak.setVisibility(View.GONE);
        // }
       /* if (sBuddy != null) {
            if (sBuddy.equalsIgnoreCase("y")) {
                llConnect.setBackground(getResources().getDrawable(R.drawable.link_button_bg_grey));
                llConnect.setEnabled(false);
                llConnect.setClickable(false);
            } else {
                llConnect.setEnabled(true);
                llConnect.setClickable(true);
            }
        }*/

        /*llConnect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                connectRequest = new ConnectRequest(getActivity());
                connectRequest.execute(AppSettings.setConnectRequest);
            }
        });
        llChat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (chatStatus.equalsIgnoreCase("fav")) {
                    //comm.onChatBuddyzListListener(playerId, playerName, playerImage);
                    ChatActivity.position = 2;
                    ChatActivity.sltBuddyId = playerId;
                    ChatActivity.sltBuddyName = playerName;
                    ChatActivity.sltBuddyImage = playerImage;
                    Intent ob = new Intent(getActivity(), ChatActivity.class);
                    startActivity(ob);
                } else {
                    ChatActivity.position = 2;
                    ChatActivity.sltBuddyId = playerId;
                    ChatActivity.sltBuddyName = playerName;
                    ChatActivity.sltBuddyImage = playerImage;
                    Intent ob = new Intent(getActivity(), ChatActivity.class);
                    startActivity(ob);
                }
            }
        });
*/
        llNotiConnect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                approvedRequest = new ApprovedRequest(getActivity(), 1,
                        playerId,
                        playerName);
                approvedRequest.execute(AppSettings.setConnectApprove);
            }
        });
        llNotiReject.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                approvedRequest = new ApprovedRequest(getActivity(), 2,
                        playerId,
                        playerName);
                approvedRequest.execute(AppSettings.setConnectReject);
            }
        });

        switchTab(1);
        setScreenDynamically(1);

       /* profileButtonSwitch(openFromMain);*/

        return v;
    }

    /*private void profileButtonSwitch(boolean openFromMain2) {
        if (openFromMain2) {
            llSwitchMain.setVisibility(View.VISIBLE);
            llSwitchNotification.setVisibility(View.GONE);
        } else {
            llSwitchMain.setVisibility(View.GONE);
            llSwitchNotification.setVisibility(View.VISIBLE);
        }
    }*/

    public void setActionBarOption(View view) {
        L.m("On Home button click");
        LinearLayout ActionBack, ActionHome;
        ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
        ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
        ActionBarHeading = (TextView) view.findViewById(R.id.actionbar_title);
        ActionBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                mannager.popBackStack();
            }
        });
        ActionHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                ((MainActivity)getActivity()).SwitchTab();
                mannager.popBackStack("mainRSSFeed", 0);
//				if(openFromChat){
////					getActivity().finish();
//					mannager.popBackStack("mainRSSFeed", 0);
//				}else{
//					mannager.popBackStack("mainRSSFeed", 0);
//				}
            }
        });
    }

    private void switchTab(int key) {

        switch (key) {
            case 0:
                tabProfile.setBackgroundColor(getResources().getColor(R.color.main_grey));
                tabSports.setBackgroundColor(getResources().getColor(R.color.main_grey));
                tabPosts.setBackgroundColor(getResources().getColor(R.color.main_grey));

                break;
            case 1:
                tabProfile.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
                tabSports.setBackgroundColor(getResources().getColor(R.color.main_grey));
                tabPosts.setBackgroundColor(getResources().getColor(R.color.main_grey));

                break;
            case 2:
                tabProfile.setBackgroundColor(getResources().getColor(R.color.main_grey));
                tabSports.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
                tabPosts.setBackgroundColor(getResources().getColor(R.color.main_grey));
                break;
            case 3:
                tabProfile.setBackgroundColor(getResources().getColor(R.color.main_grey));
                tabSports.setBackgroundColor(getResources().getColor(R.color.main_grey));
                tabPosts.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
                break;
        }
    }

    private void setScreenDynamically(int position) {
        setScreenDynamically(position, null, null, null);
    }

    private void setScreenDynamically(int position, String JsonData) {
        setScreenDynamically(position, JsonData, null, null);
    }

    private void setScreenDynamically(int position, String JsonData, String playerId) {
        setScreenDynamically(position, JsonData, playerId, null);
    }

    private void setScreenDynamically(int position, String JsonData, String playerId, String searchData) {
        FragmentTransaction transaction;
        switch (position) {
            case 1:

                ActionBarHeading.setText("PROFILE");
                MainFindSbuddyDetailProfile mainFindSbuddyDetailProfile = new MainFindSbuddyDetailProfile();
                mainFindSbuddyDetailProfile.setData(about, location, clubNApartment, city);
                transaction = mannager.beginTransaction();
                transaction.replace(R.id.group_sbuddy_main, mainFindSbuddyDetailProfile, "mainFindSbuddyDetailProfile");
                transaction.commit();

                break;
            case 2:
                ActionBarHeading.setText("SPORTS");
                MainFindSbuddyDetailSports mainFindSbuddyDetailSports = new MainFindSbuddyDetailSports();
                mainFindSbuddyDetailSports.setData(JsonData);
                mainFindSbuddyDetailSports.setPlayerId(playerId);
                transaction = mannager.beginTransaction();
                transaction.replace(R.id.group_sbuddy_main, mainFindSbuddyDetailSports, "mainFindSbuddyDetailSports");
                transaction.commit();

                break;
            case 3:
                ActionBarHeading.setText("POSTS");
                MainFindSbuddyDetailPosts mainFindSbuddyDetailPosts = new MainFindSbuddyDetailPosts();
                // mainFindSbuddyDetailPosts.setData(about, location,
                // clubNApartment, city);
                transaction = mannager.beginTransaction();
                transaction.replace(R.id.group_sbuddy_main, mainFindSbuddyDetailPosts, "mainFindSbuddyDetailPosts");
                transaction.commit();

                break;
        }
    }

    private class ConnectRequest extends AsyncTask<String, Void, String> {

        private Activity activity;

        public ConnectRequest(Activity activity) {
            onAttach(activity);
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showPDialog(getActivity(), "Processing please wait");
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "error";
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            if (activity != null) {
                // L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
                params.add(new BasicNameValuePair("senderId", LoadPref(Buddy.PlayerId)));
                params.add(new BasicNameValuePair("receiverId", playerId));
            }

            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], 3, params);
            String output = null;
            L.m(response);
            if (response.contains("[")) {
                try {
                    JSONArray json = new JSONArray(response);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        if (obj.optString("status").equals("success")) {
                            output = "success";

                        } else if (obj.optString("status").equals("failure")) {
                            output = obj.optString("message");
                        }
                    }

                } catch (JSONException e) {
                    L.m("Json Error :" + e.toString());
                    return null;
                    // return e.toString();
                }
            } else {
                L.m("Invalid JSON found : " + response);
                // return response;
                return null;
            }

            return output;
            // return "success";

        }

        @Override
        protected void onPostExecute(String result) {

            if (activity != null) {
                dismissPDialog();
                if (result != null) {
                    L.m(result);
                    if (result.equalsIgnoreCase("success")) {
                        MyNotification.removeNotiReqCounter(playerId);
                        showReviewAlertSuccess(getActivity(), "connect", playerName);
                    } else {
                        switch (result) {
                            case "A":
                                showDefaultAlertBox(getActivity(), "Alert!!!", "You are already connected with this buddy");
                                // Toast.makeText(getActivity(), "You have already
                                // connected with this buddy",
                                // Toast.LENGTH_SHORT).show();
                                break;
                            case "B":
                                showDefaultAlertBox(getActivity(), "Alert!!!", "You have been blocked by this buddy");
                                // Toast.makeText(getActivity(), "You are blocked by
                                // this buddy", Toast.LENGTH_SHORT).show();
                                break;
                            case "X":
                                showDefaultAlertBox(getActivity(), "Alert!!!",
                                        "Your request has been rejected by this buddy");
                                // Toast.makeText(getActivity(), "Your request
                                // rejected by this buddy",
                                // Toast.LENGTH_SHORT).show();
                                break;
                            case "R":
                                showDefaultAlertBox(getActivity(), "Alert!!!",
                                        "Your request has already been sent to this buddy");
                                // Toast.makeText(getActivity(), "Your request
                                // rejected by this buddy",
                                // Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_error_msg),
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    }



    private class ApprovedRequest extends AsyncTask<String, Void, String> {

        private Activity activity;
        private String requestedId;
        private String requestedName;
        private int option, APPROVED = 1, REJECTED = 2;

        public ApprovedRequest(Activity activity, int option, String requestedId, String requestedName) {
            onAttach(activity);
            this.requestedId = requestedId;
            this.requestedName = requestedName;
            this.option = option;
        }

        public void onAttach(Activity activity) {
            this.activity = activity;
        }

        public void onDetach() {
            this.activity = null;
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            showPDialog(getActivity(), "Processing please wait");
        }

        @Override
        protected String doInBackground(String... urls) {
            String response = "error";
            List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
            if (activity != null) {
                // L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
                params.add(new BasicNameValuePair("senderId", LoadPref(Buddy.PlayerId)));
                params.add(new BasicNameValuePair("receiverId", requestedId));
            }

            HttpReq ob = new HttpReq();
            response = ob.makeConnection(urls[0], 3, params);
            String output = null;
            L.m(response);
            if (response.contains("[")) {
                try {
                    JSONArray json = new JSONArray(response);
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject obj = json.getJSONObject(i);
                        if (obj.optString("status").equals("success")) {
                            output = "success";

                        } else if (obj.optString("status").equals("failure")) {
                            output = obj.optString("message");
                        }
                    }

                } catch (JSONException e) {
                    L.m("Json Error :" + e.toString());
                    ErrorMessage = e.toString() + response;
                    return null;
                    // return e.toString();
                }
            } else {
                L.m("Invalid JSON found : " + response);
                ErrorMessage = response;
                return null;
            }

            return output;
            // return "success";

        }

        @Override
        protected void onPostExecute(String result) {

            if (activity != null) {
                dismissPDialog();
                if (result != null) {
                    L.m(result);
                    if (result.equalsIgnoreCase("success")) {
                        if (option == APPROVED) {

                            MyNotification.removeNotiReqCounter(playerId);
                            ((MainActivity)getActivity()).setNotificationCounter();
                            showReviewAlertSuccess(getActivity(), "connectApproved", requestedName,0);
//							removeCounter(requestedId);
//							removeListItem(position);
                        } else if (option == REJECTED) {

                            MyNotification.removeNotiReqCounter(playerId);
                            ((MainActivity)getActivity()).setNotificationCounter();
                            showReviewAlertSuccess(getActivity(), "connectRejected", requestedName,0);
//							removeCounter(requestedId);
//							removeListItem(position);
                        }
                    } else {
                        Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //showServerErrorAlertBox(ErrorMessage);

                }
            }
        }


    }

}

