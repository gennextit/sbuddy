package com.ajsoft.sbuddy.fragments.event;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.DateTimeUtility;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Validation;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainEventDetails extends CompactFragment {

	private static final String LOG_TAG = "sBuddy";
	AutoCompleteTextView acLocation;
	EditText etAddress, etMoreDetails;
	TextView tvSDate, tvEDate, tvSTime, tvETime;
	LinearLayout llSDate, llEDate, llSTime, llETime;
	Button btnSave;
	OnEventDetailsListener comm;
	ProgressBar progressBar;
	private static int START_DATE = 1, END_DATE = 2, status;
	private static int START_TIME = 1, END_TIME = 2;
	static String startDay, endDay, sTime, eTime;
	int startDateStamp, endDateStamp,currentDate;
	long startTimeStamp, endTimeStamp;
	int sday, smonth, syear, shours, smin;
	int eday, emonth, eyear, ehours, emin;
	
	public void setCommunicator(OnEventDetailsListener comm) {
		this.comm = comm;
	}

	public interface OnEventDetailsListener {
		public void onEventDetailsListener(String location, String address, String startDate, String endDate,
				String startTime, String endTime, String addMoreDetail);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		startDay = null;
		endDay = null;
		sTime = null;
		eTime = null;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_event_details, container, false);
		initUi(v);

		return v;
	}

	private void initUi(View v) {
		acLocation = (AutoCompleteTextView) v.findViewById(R.id.ac_main_event_detail_location);
		etAddress = (EditText) v.findViewById(R.id.et_main_event_detail_event_address);
		etMoreDetails = (EditText) v.findViewById(R.id.et_main_event_detail_moreDetail);
		tvSDate = (TextView) v.findViewById(R.id.tv_main_event_detail_sdate);
		tvEDate = (TextView) v.findViewById(R.id.tv_main_event_detail_edate);
		tvSTime = (TextView) v.findViewById(R.id.tv_main_event_detail_stime);
		tvETime = (TextView) v.findViewById(R.id.tv_main_event_detail_etime);

		llSDate = (LinearLayout) v.findViewById(R.id.ll_main_event_detail_sdate);
		llEDate = (LinearLayout) v.findViewById(R.id.ll_main_event_detail_edate);
		llSTime = (LinearLayout) v.findViewById(R.id.ll_main_event_detail_sTime);
		llETime = (LinearLayout) v.findViewById(R.id.ll_main_event_detail_eTime);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		btnSave = (Button) v.findViewById(R.id.btn_events_save);

		setTypsFace(btnSave);
		setTypsFace(acLocation);
		setTypsFace(etAddress);
		setTypsFace(etMoreDetails);
		
		acLocation.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.autocomplete_list_item));
		acLocation.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				String str = (String) adapterView.getItemAtPosition(position);

			}
		});
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (checkValidation()) {
					comm.onEventDetailsListener(acLocation.getText().toString(), etAddress.getText().toString(),
							startDay, endDay, sTime, eTime, etMoreDetails.getText().toString());
				}
			}
		});
		llSDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status = START_DATE;
				DialogFragment newFragment = new SelectDateFragment();
				newFragment.show(getFragmentManager(), "DatePicker");
			}
		});
		llEDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status = END_DATE;
				DialogFragment newFragment = new SelectDateFragment();
				newFragment.show(getFragmentManager(), "DatePicker");
			}
		});
		llSTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status = START_TIME;
				DialogFragment newFragment = new SelectTimeFragment();
				newFragment.show(getFragmentManager(), "TimePicker");
			}
		});
		llETime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status = END_TIME;
				DialogFragment newFragment = new SelectTimeFragment();
				newFragment.show(getFragmentManager(), "TimePicker");
			}
		});
		getDateTime();
	}

	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isEmpty(acLocation, true)) {
			return false;
		}
		L.m(startDateStamp+">"+endDateStamp);
		if (startDateStamp < currentDate) {
			Toast.makeText(getActivity(), "Please select a future start date", Toast.LENGTH_SHORT)
					.show();
			return false;

		}
		if (endDateStamp < currentDate) {
			Toast.makeText(getActivity(), "Please select a future end date", Toast.LENGTH_SHORT)
					.show();
			return false;

		}
		if (startDateStamp > endDateStamp) {
			Toast.makeText(getActivity(), "Event start date must be less than event end date", Toast.LENGTH_SHORT)
					.show();
			return false;

		}
		if (endDateStamp < startDateStamp) {
			Toast.makeText(getActivity(), "Event end date must be grater than event start date", Toast.LENGTH_SHORT)
					.show();

			return false;
		}
		if (sTime == null) {
			Toast.makeText(getActivity(), "Please select start time", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (eTime == null) {
			Toast.makeText(getActivity(), "Please select end time", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		if (startTimeStamp > endTimeStamp) {
			Toast.makeText(getActivity(), "Event start time must be less than event end time", Toast.LENGTH_SHORT)
					.show();
			return false;

		}
		if (endTimeStamp < startTimeStamp) {
			Toast.makeText(getActivity(), "Event end time must be grater than event start time", Toast.LENGTH_SHORT)
					.show();

			return false;
		}
		
		return ret;
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar calendar = Calendar.getInstance();
			DatePickerDialog dpd ;
			if (status == START_DATE) {
				dpd = new DatePickerDialog(getActivity(), this, syear, smonth, sday);
				
			} else {
				dpd = new DatePickerDialog(getActivity(), this, eyear, emonth, eday);
				
			}
			// Set the DatePicker minimum date selection to current date
			dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());// get the current day
			// dp.setMinDate(System.currentTimeMillis() - 1000);// Alternate way
			// to get the current day

			// //Add 6 days with current date
			// calendar.add(Calendar.DAY_OF_MONTH,6);
			//
			// //Set the maximum date to select from DatePickerDialog
			// dp.setMaxDate(calendar.getTimeInMillis());

			return dpd;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			populateSetDate(yy, mm, dd);
//			storeTimeStamp(yy,mm,dd);
			

		}

		
		public void populateSetDate(int year, int month, int day) {
			if (status == START_DATE) {
				startDay = DateTimeUtility.convertDate(day, month, year);
				tvSDate.setText(startDay);
				startDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year,month,day));
				syear = year;
				smonth = month;
				sday = day;
				startTimeStamp=Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear,smonth,sday,ehours,emin));
				
			} else {
				endDay = DateTimeUtility.convertDate(day, month, year);
				tvEDate.setText(endDay);
				endDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(year,month,day));
				MainEventContactDetails.endDateTimeStamp=endDateStamp;
				eyear = year;
				emonth = month;
				eday = day;
				endTimeStamp=Long.parseLong(DateTimeUtility.convertDateTimeStamp(eyear,emonth,eday,ehours,emin));
				
			}
		}

	}
//	private int storeTimeStamp(int yy, int mm, int dd) {
//		String year=String.valueOf(yy);
//		String month=String.valueOf(mm);
//		String date=String.valueOf(dd);
//		try {
//			return Integer.parseInt(year + month + date); 
//		} catch (NumberFormatException e) {
//			L.m(e.toString());
//		}
//		return 0;
//	}

	public void getDateTime() {
		String date, time;
		final Calendar cal = Calendar.getInstance();
		syear = cal.get(Calendar.YEAR);
		smonth = cal.get(Calendar.MONTH);
		sday = cal.get(Calendar.DAY_OF_MONTH);
		eyear = cal.get(Calendar.YEAR);
		emonth = cal.get(Calendar.MONTH);
		eday = cal.get(Calendar.DAY_OF_MONTH);

		shours = cal.get(Calendar.HOUR_OF_DAY);
		smin = cal.get(Calendar.MINUTE);
		ehours = cal.get(Calendar.HOUR_OF_DAY);
		emin = cal.get(Calendar.MINUTE);
		
		date = DateTimeUtility.convertDate(sday, smonth, syear);
		currentDate=Integer.parseInt(DateTimeUtility.convertDateStamp(syear, smonth, sday));
		startDay = date;
		endDay = date;
		startDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(syear,smonth,sday));
		endDateStamp = Integer.parseInt(DateTimeUtility.convertDateStamp(syear,smonth,sday));
		MainEventContactDetails.endDateTimeStamp=endDateStamp;
		
		startTimeStamp=Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear,smonth,sday,shours,smin));
		endTimeStamp=Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear,smonth,sday,shours,smin));
		tvSDate.setText(date);
		tvEDate.setText(date);

		time = new SimpleDateFormat("h:mm a").format(cal.getTime());

		tvSTime.setText(time);
		tvETime.setText(time);

	}

	@SuppressLint("ValidFragment")
	public class SelectTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			//final Calendar calendar = Calendar.getInstance();
			TimePickerDialog tp;
			if (status == START_TIME) {
				tp=new TimePickerDialog(getActivity(), this, shours, smin, true);
			} else {
				tp=new TimePickerDialog(getActivity(), this, ehours, emin, true);
			}
			return tp;
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, hourOfDay);
			c.set(Calendar.MINUTE, minute);

			String format = new SimpleDateFormat("h:mm a").format(c.getTime());

			if (status == START_TIME) {
				sTime = format;
				tvSTime.setText(format);
				shours = hourOfDay;
				smin = minute;
				startTimeStamp=Long.parseLong(DateTimeUtility.convertDateTimeStamp(syear,smonth,sday,shours,smin));
				
			} else {
				eTime = format;
				tvETime.setText(format);
				ehours = hourOfDay;
				emin = minute;
				endTimeStamp=Long.parseLong(DateTimeUtility.convertDateTimeStamp(eyear,emonth,eday,ehours,emin));
				
			}
		}
	}

	public static ArrayList<String> autocomplete(String input, String keyFilter) {
		ArrayList<String> resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(
					AppTokens.PLACES_API_BASE + AppTokens.TYPE_AUTOCOMPLETE + AppTokens.OUT_JSON);
			sb.append("?key=" + AppTokens.API_KEY);
			sb.append("&components=country:in");
			sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			URL url = new URL(sb.toString());

			System.out.println("URL: " + url);
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e(LOG_TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e(LOG_TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {

			L.m(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				String res = predsJsonArray.getJSONObject(i).getString("description");
				String placeId = predsJsonArray.getJSONObject(i).getString("place_id");
				if (res.contains(toTitleCase(keyFilter))) {
					res = res.substring(0, res.length() - 7);
					System.out.println(res);
					System.out.println("============================================================");
					resultList.add(res);
				}

			}
		} catch (JSONException e) {
			// Log.e(LOG_TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}

	public class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
		private ArrayList<String> resultList;

		public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);

		}

		@Override
		public int getCount() {
			return resultList.size();
		}

		@Override
		public String getItem(int index) {
			return resultList.get(index);
		}

		@Override
		public Filter getFilter() {
			Filter filter = new Filter() {
				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					FilterResults filterResults = new FilterResults();
					if (constraint != null) {
						// Retrieve the autocomplete results.
						resultList = autocomplete(constraint.toString(), acLocation.getText().toString());

						// Assign the data to the FilterResults
						filterResults.values = resultList;
						filterResults.count = resultList.size();
					}
					return filterResults;
				}

				@Override
				protected void publishResults(CharSequence constraint, FilterResults results) {
					if (results != null && results.count > 0) {
						notifyDataSetChanged();
					} else {
						notifyDataSetInvalidated();
					}
				}
			};
			return filter;
		}
	}

}
