package com.ajsoft.sbuddy.fragments.chat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.fragments.sbuddy.MainFindSbuddyDetailMain;
import com.ajsoft.sbuddy.model.ChatSbuddyListAdapter;
import com.ajsoft.sbuddy.model.FindBuddyModel;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChatBuddyzList extends CompactFragment {
	ListView lvMain;
	TextView tvCounter;
	ProgressBar progressBar;
	FragmentManager mannager;
	ArrayList<FindBuddyModel> myChatList;
	ChatSbuddyListAdapter adapter;
	String myBuddyJsonData;
	HttpTask httpTask;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (httpTask != null) {
			httpTask.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (httpTask != null) {
			httpTask.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.chat_buddy_list, container, false);
		mannager = getFragmentManager();
		initUi(v);


		httpTask = new HttpTask(getActivity(), progressBar);
		httpTask.execute(AppSettings.findMySbuddy);

		return v;
	}

	private void initUi(View v) {
		setActionBarOption(v);

		lvMain = (ListView) v.findViewById(R.id.lv_chat_buddy_list);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		tvCounter = (TextView) v.findViewById(R.id.tv_mybuddy_counter);

		lvMain.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
				MainFindSbuddyDetailMain mainFindSbuddyDetailMain = new MainFindSbuddyDetailMain();
				if (myChatList != null) {
					mainFindSbuddyDetailMain.setData("fav", "y", myChatList.get(position).getAbout(),
							myChatList.get(position).getLocation(), myChatList.get(position).getClubOrApartment(),
							myChatList.get(position).getCity());
					mainFindSbuddyDetailMain.setPlayerProfile(myChatList.get(position).getPlayerId(),
							myChatList.get(position).getFullName(), myChatList.get(position).getImageUrl(),
							myChatList.get(position).getGender(), myChatList.get(position).getAge(), "online");
					mainFindSbuddyDetailMain.setJSONData(myBuddyJsonData);
					FragmentManager mannager = getActivity().getFragmentManager();
					FragmentTransaction transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, mainFindSbuddyDetailMain, "mainFindSbuddyDetailMain");
					transaction.addToBackStack("mainFindSbuddyDetailMain");
					transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();
				}
			}
		});

		myChatList = new ArrayList<>();
		adapter = new ChatSbuddyListAdapter(getActivity(), R.layout.custom_slot_find_coach_message_left, myChatList);
		lvMain.setAdapter(adapter);

	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				 mannager.popBackStack();
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}

	private class HttpTask extends AsyncTask<String, Void, String> {
		ProgressBar pBar;
		Activity activity;

		public HttpTask(Activity activity, ProgressBar pBar) {
			this.pBar = pBar;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";

			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
			}
			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], HttpReq.POST, params);
			String output = "not available";
			L.m(response);
			if (response.contains("[")) {
				myBuddyJsonData = response;
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					if (messageObj.optString("status").equals("success")) {
						JSONArray json = messageObj.getJSONArray("message");
						for (int i = 0; i < json.length(); i++) {
							JSONObject obj = json.getJSONObject(i);
							if (!obj.optString("playerId").equals("")) {
								output = "success";
								FindBuddyModel model = new FindBuddyModel();
								model.setPlayerId(obj.optString("playerId"));
								model.setFullName(obj.optString("fullName"));
								model.setAge(obj.optString("age"));
								model.setAbout(obj.optString("about"));
								model.setClubOrApartment(obj.optString("clubOrApartment"));
								model.setGender(obj.optString("gender"));
								model.setImageUrl(obj.optString("imageUrl"));
								model.setLocation(obj.optString("location"));
								model.setCity(obj.optString("city"));
//								model.setCounter(setCounter(obj.optString("playerId"),tempList));
								myChatList.add(model);
							}
						}
					} else if (messageObj.optString("status").equals("failure")) {
						output = "failure";
						ErrorMessage = messageObj.optString("message");
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage = e.toString() + "\n" + response;
					return null;
				}
			} else {
				L.m("Server Error : " + response);
				ErrorMessage = response;
				return null;
			}

			return output;

		}


		@Override
		protected void onPostExecute(String result) {
			ArrayList<String> errorList;
			ArrayAdapter<String> errorAdapter;
			if (activity != null) {
				pBar.setVisibility(View.GONE);
				if (result != null) {

					switch (result) {
					case "success":
						if (adapter != null) {
							tvCounter.setText(String.valueOf(myChatList.size()));
							adapter.notifyDataSetChanged();
						}
						break;
					case "failure":
						errorList = new ArrayList<String>();
						errorList.add("No Buddy Connected");
						errorAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
								R.id.tv_error_text, errorList);
						lvMain.setAdapter(errorAdapter);
						myChatList=null;
						L.m(result);
						Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
						break;
					case "not available":
						errorList = new ArrayList<String>();
						errorList.add("No Buddy Connected");
						errorAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
								R.id.tv_error_text, errorList);
						lvMain.setAdapter(errorAdapter);
						myChatList=null;
						break;
					}
				} else {
					showServerErrorAlertBox(ErrorMessage);
				}
			}

		}
	}

	public void showServerErrorAlertBox(String errorDetail) {
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public void showInternetAlertBox() {
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {
					httpTask = new HttpTask(getActivity(), progressBar);
					httpTask.execute(AppSettings.findMySbuddy);

				} else {
					showInternetAlertBox(getActivity());
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

}
