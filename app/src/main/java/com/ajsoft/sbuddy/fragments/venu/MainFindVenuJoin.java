package com.ajsoft.sbuddy.fragments.venu;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.model.FindVenueModel;
import com.ajsoft.sbuddy.model.SpinnerSportsAdapter;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.DateTimeUtility;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainFindVenuJoin extends CompactFragment {

	Button btnjoinRequest;
	JoinRequest joinRequest;
	ProgressBar progressBar;
	Spinner spSports, spPlan;
	LinearLayout llDate, llSTime, llETime;
	TextView tvDate, tvSTime, tvETime; 
	private String sltSportId, sltSportName, sltSportBaseImage;
	private String sltPlan;
	private static int status;
	private static int START_TIME = 1, END_TIME = 2;
	static String sltDate, sTime, eTime;
	FragmentManager mannager;
	String sltVenueId;
	EditText etRemarks;
	ArrayList<FindVenueModel> sportList;
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (joinRequest != null) {
			joinRequest.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (joinRequest != null) {
			joinRequest.onDetach();
		}
	}
	
	public void setData(String sltVenueId,ArrayList<FindVenueModel> sportList) { 
		this.sltVenueId=sltVenueId;
		this.sportList=sportList;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.join_alert_box, container, false);
		mannager = getFragmentManager();
		setActionBarOption(v);
		
		spSports = (Spinner) v.findViewById(R.id.sp_join_alert_box_sport);
		spPlan = (Spinner) v.findViewById(R.id.sp_join_alert_box_plan);
		etRemarks = (EditText) v.findViewById(R.id.et_join_alert_box_remarks);
		llDate = (LinearLayout) v.findViewById(R.id.ll_join_alert_box_date);
		llSTime = (LinearLayout) v.findViewById(R.id.ll_join_alert_box_sTime);
		llETime = (LinearLayout) v.findViewById(R.id.ll_join_alert_box_eTime);
		tvDate = (TextView) v.findViewById(R.id.tv_join_alert_box_date);
		tvSTime = (TextView) v.findViewById(R.id.tv_join_alert_box_sTime);
		tvETime = (TextView) v.findViewById(R.id.tv_join_alert_box_eTime);
		btnjoinRequest = (Button) v.findViewById(R.id.btn_join_alert_box_join);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);

		setTypsFace(btnjoinRequest);
		
		
		btnjoinRequest.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(checkValidation()){
					joinRequest = new JoinRequest(getActivity());
					joinRequest.execute(AppSettings.setJoinMessage);
				}
			}
		});
		String[] gender = getResources().getStringArray(R.array.choose_plan);
		SpinnerSportsAdapter adapter = new SpinnerSportsAdapter(getActivity(), R.layout.spinner_slot);
		adapter.addAll(gender);
		adapter.add("Select Plan");
		spPlan.setAdapter(adapter);
		spPlan.setSelection(adapter.getCount());
		spPlan.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				if(spSports!=null){
					if (spSports.getSelectedItem().toString() != "Select Plan") {
						String plan = spPlan.getSelectedItem().toString();
						sltPlan = plan;
					}
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		LoadSportsData();
		spSports.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				spSports.setSelection(0);
				return false;
			}
		});
		spSports.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (sportList != null && spSports.getSelectedItem().toString() != "Select Sport") {

					sltSportId = sportList.get(position).getSportId();
					sltSportName = sportList.get(position).getSportName();
					sltSportBaseImage = sportList.get(position).getLogo();
					// L.m(spSports.getSelectedItem().toString()+" :
					// "+sltSportName);

				} else {
					sltSportName = spSports.getSelectedItem().toString();
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		llDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				DialogFragment newFragment = new SelectDateFragment();
				newFragment.show(getFragmentManager(), "DatePicker");
			}
		});

		llSTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status = START_TIME;
				DialogFragment newFragment = new SelectTimeFragment();
				newFragment.show(getFragmentManager(), "TimePicker");
			}
		});
		llETime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status = END_TIME;
				DialogFragment newFragment = new SelectTimeFragment();
				newFragment.show(getFragmentManager(), "TimePicker");
			}
		});
		return v;
	}


	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar calendar = Calendar.getInstance();
			int yy = calendar.get(Calendar.YEAR);
			int mm = calendar.get(Calendar.MONTH);
			int dd = calendar.get(Calendar.DAY_OF_MONTH);
			DatePickerDialog dpd =new DatePickerDialog(getActivity(), this, yy, mm, dd);
			DatePicker dp = dpd.getDatePicker();
			//Set the DatePicker minimum date selection to current date
			dp.setMinDate(calendar.getTimeInMillis());//get the current day
			//dp.setMinDate(System.currentTimeMillis() - 1000);// Alternate way to get the current day

//		    //Add 6 days with current date
//		    calendar.add(Calendar.DAY_OF_MONTH,6);
//
//		    //Set the maximum date to select from DatePickerDialog
//		    dp.setMaxDate(calendar.getTimeInMillis());

			return dpd;
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			populateSetDate(yy, mm, dd);
		}

		public void populateSetDate(int year, int month, int day) {
			sltDate = DateTimeUtility.convertDate(day, month, year);
			tvDate.setText(DateTimeUtility.convertDate(day, month, year));

		}

	}

	@SuppressLint("ValidFragment")
	public class SelectTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar calendar = Calendar.getInstance();
			int hh = calendar.get(Calendar.HOUR_OF_DAY);
			int mm = calendar.get(Calendar.MINUTE);
			return new TimePickerDialog(getActivity(), this, hh, mm, true);
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			Calendar c = Calendar.getInstance();
			c.set(Calendar.HOUR_OF_DAY, hourOfDay);
			c.set(Calendar.MINUTE, minute);

			String format = new SimpleDateFormat("h:mm a").format(c.getTime());

			if (status == START_TIME) {
				sTime = format;
				tvSTime.setText(format);
			} else {
				eTime = format;
				tvETime.setText(format);
			}
		}
	}

	
	public void setActionBarOption(View view) {
		LinearLayout ActionBack;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("CHECK SLOT"));
		ActionBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainFindVenuJoin", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
//		ActionHome.setOnClickListener(new View.OnClickListener() {
//			
//			@Override
//			public void onClick(View arg0) {
//				mannager.popBackStack("mainRSSFeed", 0);
//			}
//		});
	}
	
	private boolean checkValidation() {
		boolean ret = true;

		if(spSports.getSelectedItem().toString() == "Select Sport"){
			Toast.makeText(getActivity(), "Please select sport", Toast.LENGTH_SHORT).show();
			return false;
		}
		if(spPlan.getSelectedItem().toString() == "Select Plan"){
			Toast.makeText(getActivity(), "Please select plan", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (sltDate == null) {
			Toast.makeText(getActivity(), "Please select date", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (sTime == null) {
			Toast.makeText(getActivity(), "Please select start time", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (eTime == null) {
			Toast.makeText(getActivity(), "Please select end time", Toast.LENGTH_SHORT).show();
			return false;
		}
		
		return ret;
	}

	public void LoadSportsData() {
		ArrayList<String> result = LoadSports();
		if (result != null) {
			SpinnerSportsAdapter adapter = new SpinnerSportsAdapter(getActivity(), R.layout.spinner_slot);
			adapter.addAll(result);
			adapter.add("Select Sport");
			spSports.setAdapter(adapter);
			spSports.setSelection(adapter.getCount());
		}

	}

	private ArrayList<String> LoadSports() {

		ArrayList<String> sList = new ArrayList<String>();
		//sportArrayList = new ArrayList<MySportsModel>();
		if (sportList != null) {

			for (FindVenueModel ob : sportList) {
				sList.add(ob.getSportName()); 
			}
		}

		return sList;
	}

	private class JoinRequest extends AsyncTask<String, Void, String> {

		private Activity activity;

		public JoinRequest(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			btnjoinRequest.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				//sportId,choosePlan, chooseDate, chooseSlot, playerId, venueId
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("sportId", sltSportId)); 
				params.add(new BasicNameValuePair("choosePlan", sltPlan));
				params.add(new BasicNameValuePair("chooseDate", sltDate));
				params.add(new BasicNameValuePair("chooseSlot", sTime+" to "+eTime));
				params.add(new BasicNameValuePair("venueId", sltVenueId));
				params.add(new BasicNameValuePair("remarks", etRemarks.getText().toString()));
				
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";

						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage=e.toString()+response;
					return null;
					// return e.toString();
				}
			} else {
				ErrorMessage=response;
				L.m("Invalid JSON found : " + response);
				// return response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				progressBar.setVisibility(View.GONE);
				btnjoinRequest.setVisibility(View.VISIBLE);
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						Toast.makeText(getActivity(), "Join request sent.", Toast.LENGTH_SHORT).show();
						mannager.popBackStack("mainFindVenuJoin", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {

					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							joinRequest = new JoinRequest(getActivity());
							joinRequest.execute(AppSettings.setJoinMessage);
						}
					});
//					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_error_msg),
//							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}


}
