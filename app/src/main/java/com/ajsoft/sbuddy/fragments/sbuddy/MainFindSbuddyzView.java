package com.ajsoft.sbuddy.fragments.sbuddy;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.MainActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.FindBuddyAdapter;
import com.ajsoft.sbuddy.model.FindBuddyModel;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.L;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainFindSbuddyzView extends CompactFragment {
	LinearLayout tabLeft, tabRight;
	ListView lvMain;
	ImageView ivSportBaseImage;
	ArrayList<FindBuddyModel> mySportList;
	private String selectSportId, selectSportName;
	private ProgressBar progressBar;
	private static String setSearchBuddyData;
	private String sportBaseImage;
	public ImageLoader imageLoader;

	LeftTabLoadData leftTabLoadData;
	RightTabLoadData rightTabLoadData;
	TextView tvSportCount;
	int SportCounter = 0;
	FragmentManager mannager;
	String PlayerId;
	public static int TAB_RADIUS=1,TAB_LOCALITY=2,SWITCH=1;

	public void setSelectSport(String sltSport, String selectSportName) {
		this.selectSportId = sltSport;
		this.selectSportName = selectSportName;
	}

	public void setSearchBuddyData(String setSearchBuddyData) {
		this.setSearchBuddyData = setSearchBuddyData;
	}

	public void setSportBaseImage(String sportBaseImage) {
		this.sportBaseImage = sportBaseImage;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (leftTabLoadData != null) {
			leftTabLoadData.onAttach(getActivity());
		}
		if (rightTabLoadData != null) {
			rightTabLoadData.onAttach(getActivity());
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.main_search_sbuddyz_view, container, false);
		mannager = getFragmentManager();
		setActionBarOption(view);
		PlayerId = LoadPref(Buddy.PlayerId);
		tabLeft = (LinearLayout) view.findViewById(R.id.ll_find_tabLeft);
		tabRight = (LinearLayout) view.findViewById(R.id.ll_find_tabRight);
		lvMain = (ListView) view.findViewById(R.id.lv_search_sbuddyz_view);
		ivSportBaseImage = (ImageView) view.findViewById(R.id.iv_search_sbuddyz_view_baseSport);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		tvSportCount = (TextView) view.findViewById(R.id.tv_search_sbuddyz_view_SportCount);

		
		lvMain.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
				MainFindSbuddyDetailMain mainFindSbuddyDetailMain = new MainFindSbuddyDetailMain();
				// mainSearchSbuddyz.setCommunicator(AppIntroActivity.this);
				/*MainFindSbuddyDetail.openFromMain=true;
				MainFindSbuddyDetail.openFromChat=false;*/
				if (mySportList != null) {
					mainFindSbuddyDetailMain.setData("search",mySportList.get(position).getsBudddy(),mySportList.get(position).getAbout(),
							mySportList.get(position).getLocation(), mySportList.get(position).getClubOrApartment(),
							mySportList.get(position).getCity());
					mainFindSbuddyDetailMain.setPlayerProfile(mySportList.get(position).getPlayerId(),
							mySportList.get(position).getFullName(), mySportList.get(position).getImageUrl(),
							mySportList.get(position).getGender(), mySportList.get(position).getAge(), "online");
					mainFindSbuddyDetailMain.setJSONData(setSearchBuddyData);
					FragmentManager mannager = getActivity().getFragmentManager();
					FragmentTransaction transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, mainFindSbuddyDetailMain, "mainFindSbuddyDetail");
					transaction.addToBackStack("mainFindSbuddyDetail");
					transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();
				}

			}
		});

		tabLeft.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				switchTab(TAB_RADIUS);
			}
		});
		tabRight.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				switchTab(TAB_LOCALITY);
			}
		});
		switchTab(SWITCH);
		
		imageLoader = new ImageLoader(getActivity());
		if (sportBaseImage != null) {
			imageLoader.DisplayImage(sportBaseImage, ivSportBaseImage, "base_medium", false);
		}

		return view;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("SBUDDYZ"));
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainFindSbuddyzView", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}

	@SuppressLint("NewApi")
	private void switchTab(int key) {

		switch (key) {
		case 1:
			tabLeft.setBackground(getResources().getDrawable(R.drawable.tab_left_green));
			tabRight.setBackground(getResources().getDrawable(R.drawable.tab_right_grey));
			leftTabLoadData = new LeftTabLoadData(getActivity());
			leftTabLoadData.execute();
			break;

		case 2:
			tabLeft.setBackground(getResources().getDrawable(R.drawable.tab_left_grey));
			tabRight.setBackground(getResources().getDrawable(R.drawable.tab_right_green));
			rightTabLoadData = new RightTabLoadData(getActivity());
			rightTabLoadData.execute();
			break;
		}
	}

	private class LeftTabLoadData extends AsyncTask<Void, Void, Boolean> {
		Activity activity;

		public LeftTabLoadData(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			// progressBar.setVisibility(View.VISIBLE);
		}

		@Override
		protected Boolean doInBackground(Void... urls) {
			// String response = LoadPref(AppTokens.SearchBuddyData);
			String response = setSearchBuddyData;
			if(response==null){
				return null;
			}
			SportCounter = 0;
			Boolean output = false;
			int sLogoCount = 0, logoOne = 0, logoTwo = 0, logoThree = 0;
			mySportList = new ArrayList<FindBuddyModel>();
			if (response.contains("[")) {
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					JSONArray message = messageObj.getJSONArray("message");
					for (int j = 0; j < message.length(); j++) {
						JSONObject messageData = message.getJSONObject(j);
						if (messageData.optString("radius").equalsIgnoreCase("y")
								&& !PlayerId.equalsIgnoreCase(messageData.optString("playerId"))) {
							SportCounter++;
							output = true;
							sLogoCount = 0;
							logoOne = 0;
							logoTwo = 0;
							logoThree = 0;
							FindBuddyModel model = new FindBuddyModel();
							model.setsBudddy(messageData.optString("sBudddy"));
							model.setPlayerId(messageData.optString("playerId"));
							model.setFullName(messageData.optString("fullName"));
							model.setAge(messageData.optString("age"));
							model.setGender(messageData.optString("gender"));
							model.setKm(Float.parseFloat(messageData.optString("km")));
							model.setImageUrl(messageData.optString("imageUrl"));
							model.setLocation(messageData.optString("location"));
							model.setAbout(messageData.optString("about"));
							model.setCity(messageData.optString("city"));
							model.setClubOrApartment(messageData.optString("clubOrApartment"));

							JSONArray sportPlay = messageData.getJSONArray("sportPlay");
							for (int k = 0; k < sportPlay.length(); k++) {
								sLogoCount++;
								JSONObject sPdata = sportPlay.getJSONObject(k);
								if (sPdata.optString("sportName").trim().equalsIgnoreCase(selectSportName.trim())) {
									model.setsLogo1(sPdata.optString("logo"));
									logoOne = 1;
								} else if (logoTwo == 0) {
									model.setsLogo2(sPdata.optString("logo"));
									logoTwo = 1;
								} else if (logoThree == 0) {
									model.setsLogo3(sPdata.optString("logo"));
									logoThree = 1;
								}

							}
							model.setsLogoCount(String.valueOf(sLogoCount));
							mySportList.add(model);

						}

					}

				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}
			if (output) {
				// Sort by km.
				Collections.sort(mySportList, FindBuddyModel.COMPARE_BY_KILOMETER);
			}
			return output;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(Boolean result) {

			// progressBar.setVisibility(View.GONE);
			if (activity != null && result != null) {
				if (result) {
					FindBuddyAdapter ExpAdapter = new FindBuddyAdapter(getActivity(), R.layout.custom_slot_find_sbuddyz,
							mySportList);
					lvMain.setAdapter(ExpAdapter);
					tvSportCount.setVisibility(View.VISIBLE);
					tvSportCount.setText(String.valueOf(SportCounter) + " Players");
				}else {
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lvMain.setAdapter(adapter);
					mySportList = null;
					tvSportCount.setVisibility(View.GONE);
				}
			}
		}
	}

	private class RightTabLoadData extends AsyncTask<Void, Void, Boolean> {

		Activity activity;

		public RightTabLoadData(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected Boolean doInBackground(Void... urls) {
			// String response = LoadPref(AppTokens.SearchBuddyData);
			String response = setSearchBuddyData;
			if(response==null){
				return null;
			}
			SportCounter = 0;
			Boolean output = false;
			int sLogoCount = 0, logoOne = 0, logoTwo = 0, logoThree = 0;
			mySportList = new ArrayList<FindBuddyModel>();
			if (response.contains("[")) {
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					JSONArray message = messageObj.getJSONArray("message");
					for (int j = 0; j < message.length(); j++) {
						JSONObject messageData = message.getJSONObject(j);
						if (messageData.optString("clubOrApartmentAvailable").equalsIgnoreCase("Y")
								&& !PlayerId.equalsIgnoreCase(messageData.optString("playerId"))) {
							output = true;
							SportCounter++;
							sLogoCount = 0;
							logoOne = 0;
							logoTwo = 0;
							logoThree = 0;
							FindBuddyModel model = new FindBuddyModel();
							model.setsBudddy(messageData.optString("sBudddy"));
							model.setPlayerId(messageData.optString("playerId"));
							model.setFullName(messageData.optString("fullName"));
							model.setAge(messageData.optString("age"));
							model.setGender(messageData.optString("gender"));
							model.setKm(0.0f);
							model.setImageUrl(messageData.optString("imageUrl"));
							model.setClubOrApartmentAvailable(messageData.optString("clubOrApartmentAvailable"));
							model.setClubOrApartment(messageData.optString("clubOrApartment"));
							model.setAbout(messageData.optString("about"));
							model.setCity(messageData.optString("city"));
							model.setLocation(messageData.optString("location"));

							JSONArray sportPlay = messageData.getJSONArray("sportPlay");
							for (int k = 0; k < sportPlay.length(); k++) {
								sLogoCount++;
								JSONObject sPdata = sportPlay.getJSONObject(k);
								if (sPdata.optString("sportName").trim().equalsIgnoreCase(selectSportName.trim())) {
									model.setsLogo1(sPdata.optString("logo"));
									logoOne = 1;
								} else if (logoTwo == 0) {
									model.setsLogo2(sPdata.optString("logo"));
									logoTwo = 1;
								} else if (logoThree == 0) {
									model.setsLogo3(sPdata.optString("logo"));
									logoThree = 1;
								}

							}
							model.setsLogoCount(String.valueOf(sLogoCount));
							mySportList.add(model);

						}

					}

				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return output;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(Boolean result) {

			// progressBar.setVisibility(View.GONE);
			if (activity != null && result != null) {
				if (result) {
					FindBuddyAdapter ExpAdapter = new FindBuddyAdapter(getActivity(), R.layout.custom_slot_find_sbuddyz,
							mySportList);
					lvMain.setAdapter(ExpAdapter);
					tvSportCount.setVisibility(View.VISIBLE);
					tvSportCount.setText(String.valueOf(SportCounter) + " Players");
				} else {
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lvMain.setAdapter(adapter);
					mySportList = null;
					tvSportCount.setVisibility(View.GONE);
				}
			}
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (leftTabLoadData != null) {
			leftTabLoadData.onDetach();
		}
		if (rightTabLoadData != null) {
			rightTabLoadData.onDetach();
		}
	}
}