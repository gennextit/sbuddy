package com.ajsoft.sbuddy.fragments.coaches;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ajsoft.sbuddy.MainActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.FindCoachAdapter;
import com.ajsoft.sbuddy.model.FindCoachModel;
import com.ajsoft.sbuddy.util.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class MainFindCoachesView extends CompactFragment {
	LinearLayout tabLeft, tabRight;
	ListView lvMain;
	ImageView ivSportBaseImage;
	ArrayList<FindCoachModel> mySportList;
	private String selectSportId, selectSportName;
	private ProgressBar progressBar;
	private String searchCoachData;
	private String sportBaseImage;
	LeftTabLoadData leftTabLoadData;
	RightTabLoadData rightTabLoadData;
	public ImageLoader imageLoader;
	TextView tvSportCount;
	int SportCounter = 0;
	public static int TAB_RADIUS=1,TAB_LOCALITY=2,SWITCH=1;

	FragmentManager mannager;

	public void setSelectSport(String sltSportId, String selectSportName) {
		this.selectSportId = sltSportId;
		this.selectSportName = selectSportName;

	}

	public void setSearchCoachData(String searchCoachData) {
		this.searchCoachData = searchCoachData;
	}

	public void setSportBaseImage(String sportBaseImage) {
		this.sportBaseImage = sportBaseImage;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (leftTabLoadData != null) {
			leftTabLoadData.onAttach(getActivity());
		}
		if (rightTabLoadData != null) {
			rightTabLoadData.onAttach(getActivity());
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.main_find_coaches_view, container, false);
		mannager = getFragmentManager();
		setActionBarOption(view);

		tabLeft = (LinearLayout) view.findViewById(R.id.ll_find_tabLeft);
		tabRight = (LinearLayout) view.findViewById(R.id.ll_find_tabRight);
		lvMain = (ListView) view.findViewById(R.id.lv_find_coaches_view);
		ivSportBaseImage = (ImageView) view.findViewById(R.id.iv_find_coaches_view_baseSports);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		tvSportCount = (TextView) view.findViewById(R.id.tv_find_coaches_view_SportCount);

		
		lvMain.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> ad, View view, int position, long arg3) {
				MainFindCoachesDetail mainFindCoachesDetail = new MainFindCoachesDetail();
				if (mySportList != null) {
					mainFindCoachesDetail.setData(mySportList.get(position).getFavouriteCoach(),mySportList.get(position).getAbout(),
							mySportList.get(position).getLocality(), selectSportId, selectSportName);

					mainFindCoachesDetail.setPlayerProfile(mySportList.get(position).getCoachId(),
							mySportList.get(position).getFullName(), mySportList.get(position).getImageUrl(),
							mySportList.get(position).getReview(), mySportList.get(position).getRating());
					mainFindCoachesDetail.setJSONData(searchCoachData);
					FragmentManager mannager = getActivity().getFragmentManager();
					FragmentTransaction transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, mainFindCoachesDetail, "mainFindCoachesDetail");
					transaction.addToBackStack("mainFindCoachesDetail");
					transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					transaction.commit();
				}

			}
		});

		tabLeft.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				switchTab(TAB_RADIUS);
			}
		});
		tabRight.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				switchTab(TAB_LOCALITY);
			}
		});
		switchTab(SWITCH);
		imageLoader = new ImageLoader(getActivity());
		if (sportBaseImage != null) {
			imageLoader.DisplayImage(sportBaseImage, ivSportBaseImage, "base_medium", false);
		}
		return view;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainFindSbuddyzView", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}

	@SuppressLint("NewApi")
	private void switchTab(int key) {

		switch (key) {
		case 1:
			tabLeft.setBackground(getResources().getDrawable(R.drawable.tab_left_green));
			tabRight.setBackground(getResources().getDrawable(R.drawable.tab_right_grey));
			leftTabLoadData = new LeftTabLoadData(getActivity());
			leftTabLoadData.execute();
			break;

		case 2:
			tabLeft.setBackground(getResources().getDrawable(R.drawable.tab_left_grey));
			tabRight.setBackground(getResources().getDrawable(R.drawable.tab_right_green));
			rightTabLoadData = new RightTabLoadData(getActivity());
			rightTabLoadData.execute();
			break;
		}
	}

	private class LeftTabLoadData extends AsyncTask<Void, Void, Boolean> {

		Activity activity;

		public LeftTabLoadData(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected Boolean doInBackground(Void... urls) {
			SportCounter = 0;
			String response = searchCoachData;
			if(response==null){
				return null;
			}
			Boolean output = false;
			String km, locality,city;
			mySportList = new ArrayList<FindCoachModel>();
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					for (int j = 0; j < message.length(); j++) {
						km = null;
						city=null;
						locality = null;
						JSONObject messageData = message.getJSONObject(j);
						JSONArray locArr = messageData.getJSONArray("location");
						for (int k = 0; k < locArr.length(); k++) {
							JSONObject locObj = locArr.getJSONObject(k);
							if (locObj.optString("radius").equalsIgnoreCase("y")) {
								if (km == null) {
									km = locObj.optString("km");
									city=locObj.optString("locality");
									locality = locObj.optString("locality") + "  \n\n";
								} else {
									if (Float.valueOf(km) > Float.valueOf(locObj.optString("km"))) {
										km = locObj.optString("km");
										city = locObj.optString("locality");
									}

									locality += locObj.optString("locality") + " \n\n";
								}
							}
						}
						if (km != null) {
							SportCounter++;
							output = true;
							FindCoachModel model = new FindCoachModel();
							model.setFavouriteCoach(messageData.optString("favouriteCoach"));
							model.setCoachId(messageData.optString("coachId"));
							model.setAbout(messageData.optString("about"));
							model.setLocality(locality);
							model.setFullName(messageData.optString("fullName"));
							model.setAge(messageData.optString("age"));
							model.setGender(messageData.optString("gender"));
							model.setKm(Float.parseFloat(km));
							model.setImageUrl(messageData.optString("imageUrl"));
							model.setCity(city);
							model.setRating(messageData.optString("rating"));
							model.setReview(messageData.optString("review"));

							mySportList.add(model);
						}
					}

				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}
			if (output) {
				// Sort by address.
				Collections.sort(mySportList, FindCoachModel.COMPARE_BY_KILOMETER);
			}
			return output;

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(Boolean result) {

			if (activity != null && result != null) {
				if (result) {
					FindCoachAdapter ExpAdapter = new FindCoachAdapter(getActivity(), R.layout.custom_slot_find_coaches,
							mySportList);
					lvMain.setAdapter(ExpAdapter);
					tvSportCount.setVisibility(View.VISIBLE);
					tvSportCount.setText(String.valueOf(SportCounter) + " Coaches");
				} else {
					mySportList=null;
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lvMain.setAdapter(adapter);
					tvSportCount.setVisibility(View.GONE);
				}
			}
		}
	}

	private class RightTabLoadData extends AsyncTask<Void, Void, Boolean> {

		Activity activity;

		public RightTabLoadData(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected Boolean doInBackground(Void... urls) {
			String response = searchCoachData;
			SportCounter = 0;
			if(response==null){
				return null;
			}
			String km, locality,city;
			Boolean output = false;
			mySportList = new ArrayList<FindCoachModel>();
			if (response.contains("[")) {
				try {
					JSONArray message = new JSONArray(response);
					for (int j = 0; j < message.length(); j++) {
						km = null;
						locality = null;
						city=null;
						JSONObject messageData = message.getJSONObject(j);
						JSONArray locArr = messageData.getJSONArray("location");
						for (int k = 0; k < locArr.length(); k++) {
							JSONObject locObj = locArr.getJSONObject(k);
							if (locObj.optString("localityAvailable").equalsIgnoreCase("y")) {
								if (km == null) {
									km = locObj.optString("km");
									city=locObj.optString("locality");
									locality = locObj.optString("locality") + " \n\n";
								} else {
									if (Float.valueOf(km) > Float.valueOf(locObj.optString("km"))) {
										km = locObj.optString("km");
										city = locObj.optString("locality");
									}
									locality += locObj.optString("locality") + " \n\n";
								}
							}
						}
						if (km != null) {
							SportCounter++;
							output = true;
							FindCoachModel model = new FindCoachModel();

							model.setFavouriteCoach(messageData.optString("favouriteCoach"));
							model.setCoachId(messageData.optString("coachId"));
							model.setAbout(messageData.optString("about"));
							model.setLocality(locality);
							model.setFullName(messageData.optString("fullName"));
							model.setAge(messageData.optString("age"));
							model.setGender(messageData.optString("gender"));
							model.setKm(Float.parseFloat(km));
							model.setImageUrl(messageData.optString("imageUrl"));
							model.setCity(city);
							model.setRating(messageData.optString("rating"));
							model.setReview(messageData.optString("review"));
							mySportList.add(model);
						}
					}

				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return output;

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(Boolean result) {

			if (activity != null && result != null) {
				if (result) {
					FindCoachAdapter ExpAdapter = new FindCoachAdapter(getActivity(), R.layout.custom_slot_find_coaches,
							mySportList);
					lvMain.setAdapter(ExpAdapter);
					tvSportCount.setVisibility(View.VISIBLE);
					tvSportCount.setText(String.valueOf(SportCounter) + " Coaches");
				} else {
					mySportList=null;
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lvMain.setAdapter(adapter);
					tvSportCount.setVisibility(View.GONE);
				}
			}
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (leftTabLoadData != null) {
			leftTabLoadData.onDetach();
		}
		if (rightTabLoadData != null) {
			rightTabLoadData.onDetach();
		}
	}
}