package com.ajsoft.sbuddy.fragments.venu;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.MainActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainFindVenuDetail extends CompactFragment {
	ImageView ivProfile, ivShare;
	TextView tvName, tvReviews;
	RatingBar rbRating;
	LinearLayout tabDetail, tabLocation, tabGallery, tabReviews, tabMessage, llMessage, llReview;
	FragmentManager mannager;
	String favouriteVenue, about, highlightDetails, openHours, memberShip;
	static String sportId, sportName;
	String JsonVenueData, venueId, venueName, venueImage, venueReviews, venueRating, venueLat, venueLng;
	ImageLoader ivLoader;
//	private AlertDialog dialog = null;
	SendReview sendReview;
	SendMessage sendMessage;
	TextView ActionBarHeading;
	ImageView ivFav;
	ProgressBar pbFav;
	String venueAddrs;
	Boolean isSoftKeyboardDisplayed = false;
	
	SendFavRequest sendFavRequest;
	public static int SWITCH = 1;
	String tempReview="",tempMessage="";
	float tempRating;

	public void setJSONData(String JSONData) {
		this.JsonVenueData = JSONData;
	}

	public void setData(String favouriteVenue, String about, String highlightDetails, String openHours,
			String memberShip, String sportId, String sportName) {
		this.favouriteVenue = favouriteVenue;
		this.about = about;
		this.highlightDetails = highlightDetails;
		this.openHours = openHours;
		this.memberShip = memberShip;
		this.sportId = sportId;
		this.sportName = sportName;
	}

	public void setVenueProfile(String venueId, String venueName, String venueImage, String venueReviews,
			String venueRating, String venueLat, String venueLng) {
		this.venueId = venueId;
		this.venueName = venueName;
		this.venueImage = venueImage;
		this.venueReviews = venueReviews;
		this.venueRating = venueRating;
		this.venueLat = venueLat;
		this.venueLng = venueLng;
	}

	public void setVenueAddress(String venueAddrs) {
		this.venueAddrs = venueAddrs;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (sendReview != null) {
			sendReview.onAttach(activity);
		}
		if (sendMessage != null) {
			sendMessage.onAttach(activity);
		}
		if (sendFavRequest != null) {
			sendFavRequest.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (sendReview != null) {
			sendReview.onDetach();
		}
		if (sendMessage != null) {
			sendMessage.onDetach();
		}
		if (sendFavRequest != null) {
			sendFavRequest.onDetach();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.main_find_venu_detail, container, false);
		mannager = getFragmentManager();
		setActionBarOption(v);
		ivProfile = (ImageView) v.findViewById(R.id.iv_main_find_venue_detail_profile);
		ivShare = (ImageView) v.findViewById(R.id.iv_venue_share);
		tvName = (TextView) v.findViewById(R.id.tv_main_find_venue_detail_pName);
		tvReviews = (TextView) v.findViewById(R.id.tv_main_find_venue_detail_reviews);
		rbRating = (RatingBar) v.findViewById(R.id.rb_main_find_venue_detail);

		tabDetail = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_detailTab);
		tabLocation = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_locTab);
		tabGallery = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_galleryTab);
		tabReviews = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_reviewTab);
		tabMessage = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_messageTab);

		llMessage = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_message);
		llReview = (LinearLayout) v.findViewById(R.id.ll_main_find_venue_detail_review);
		ivFav = (ImageView) v.findViewById(R.id.iv_main_find_venue_detail_fav);
		pbFav = (ProgressBar) v.findViewById(R.id.pb_main_find_venue_detail_fav);
		ivLoader = new ImageLoader(getActivity());

		if (favouriteVenue.equalsIgnoreCase("y")) {
			ivFav.setImageResource(R.drawable.fav_48);
		}
		ivFav.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sendFavRequest = new SendFavRequest(getActivity());
				sendFavRequest.execute(AppSettings.setFavouriteVenue);

			}
		});

		ivShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				try {
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setType("text/plain");
					i.putExtra(Intent.EXTRA_TITLE, venueName);
					// String sAux = "\nLet me recommend you to join venue\n\n";
					// sAux = sAux + venueImage+"\n\n"+sportName+"\n"+venueName
					// + "\n\n";
					String sAux = getResources().getString(R.string.share_venue_profile_start);
					String eAux = getResources().getString(R.string.share_venue_profile_end);
					sAux = sAux + " " + sportName + " venue " + venueName + " at GURGAON" + eAux;

					i.putExtra(Intent.EXTRA_TEXT, sAux);
					startActivity(Intent.createChooser(i, "choose one"));
				} catch (Exception e) { // e.toString();
				}
			}
		});
		llMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showMessageAlert(getActivity());
			}
		});
		llReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showReviewAlert(getActivity());
			}
		});

		if (venueName != null) {
			tvName.setText(venueName);
		}
		setRatingAndReview(venueRating, venueReviews);

		if (venueImage != null) {
			ivLoader.DisplayImage(venueImage, ivProfile, "profile",false);
		}

		tabDetail.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(1);
				setScreenDynamically(1);
			}
		});
		tabLocation.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switchTab(2);
				setScreenDynamically(2);
				if (JsonVenueData != null) {
				}
			}
		});
		tabGallery.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(3);
				setScreenDynamically(3);
			}
		});
		tabReviews.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				switchTab(4);
				setScreenDynamically(4);
			}
		});
		tabMessage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				switchTab(5);
				setScreenDynamically(5);
			}
		});
		switchTab(SWITCH);
		setScreenDynamically(SWITCH);

		return v;
	}

	public void setRatingAndReview(String venueRating2, String venueReviews2) {
		if (venueReviews2 != null) {
			tvReviews.setText(venueReviews2);
		}
		if (venueRating2 != null) {
			if (!venueRating2.equals("false")) {
				rbRating.setRating(Float.parseFloat(venueRating2));
			}
		} else {
			rbRating.setRating(0);
		}

	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		ActionBarHeading = (TextView) view.findViewById(R.id.actionbar_title);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainFindVenuDetail", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}

	private void switchTab(int key) {

		switch (key) {
		case 0:
			tabDetail.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabGallery.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabMessage.setBackgroundColor(getResources().getColor(R.color.main_grey));

			break;
		case 1:
			tabDetail.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabGallery.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabMessage.setBackgroundColor(getResources().getColor(R.color.main_grey));

			break;
		case 2:
			tabDetail.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			tabGallery.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabMessage.setBackgroundColor(getResources().getColor(R.color.main_grey));
			break;
		case 3:
			tabDetail.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabGallery.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabMessage.setBackgroundColor(getResources().getColor(R.color.main_grey));
			break;
		case 4:
			tabDetail.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabGallery.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			tabMessage.setBackgroundColor(getResources().getColor(R.color.main_grey));
			break;
		case 5:
			tabDetail.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabLocation.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabGallery.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabReviews.setBackgroundColor(getResources().getColor(R.color.main_grey));
			tabMessage.setBackgroundColor(getResources().getColor(R.color.profile_ul_profile));
			break;
		}
	}

	private void setScreenDynamically(int position) {
		setScreenDynamically(position, JsonVenueData, venueId, sportId);
	}

	private void setScreenDynamically(int position, String JsonData) {
		setScreenDynamically(position, JsonData, null, null);
	}

	private void setScreenDynamically(int position, String JsonData, String venueId) {
		setScreenDynamically(position, JsonData, venueId, null);
	}

	private void setScreenDynamically(int position, String JsonData, String venueId, String sportId) {
		FragmentTransaction transaction;
		switch (position) {
		case 1:

			ActionBarHeading.setText("DETAILS");
			MainFindVenuDetailAbout mainFindVenuDetailAbout = new MainFindVenuDetailAbout();
			mainFindVenuDetailAbout.setData(venueId, sportId, sportName, about, highlightDetails, openHours, memberShip,
					JsonVenueData);
			transaction = mannager.beginTransaction();
			transaction.replace(R.id.group_venue, mainFindVenuDetailAbout, "mainFindVenuDetailAbout");
			transaction.commit();

			break;
		case 2:
			ActionBarHeading.setText("LOCATION");
			MainFindVenuDetailLocation mainFindVenuDetailLocation = new MainFindVenuDetailLocation();
			mainFindVenuDetailLocation.setData(venueName, venueAddrs, venueLat, venueLng);
			transaction = mannager.beginTransaction();
			transaction.replace(R.id.group_venue, mainFindVenuDetailLocation, "mainFindVenuDetailLocation");
			transaction.commit();

			break;
		case 3:
			ActionBarHeading.setText("GALLERY");
			MainFindVenuDetailGallery mainFindVenuDetailGallery = new MainFindVenuDetailGallery();
			mainFindVenuDetailGallery.setData(JsonData, venueId, sportId);
			transaction = mannager.beginTransaction();
			transaction.replace(R.id.group_venue, mainFindVenuDetailGallery, "mainFindVenuDetailGallery");
			transaction.commit();

			break;
		case 4:
			ActionBarHeading.setText("REVIEWS");
			MainFindVenuDetailReviews mainFindVenuDetailReviews = new MainFindVenuDetailReviews();
			mainFindVenuDetailReviews.setCommunicator(getActivity());
			mainFindVenuDetailReviews.setData(JsonData, venueId, sportId);
			transaction = mannager.beginTransaction();
			transaction.replace(R.id.group_venue, mainFindVenuDetailReviews, "mainFindVenuDetailReviews");
			transaction.commit();

			break;
		case 5:
			ActionBarHeading.setText("MESSAGE");
			MainFindVenuDetailMessage mainFindVenuDetailMessage = new MainFindVenuDetailMessage();
			mainFindVenuDetailMessage.setData(JsonData, venueId, sportId);
			transaction = mannager.beginTransaction();
			transaction.replace(R.id.group_venue, mainFindVenuDetailMessage, "mainFindVenuDetailMessage");
			transaction.commit();

			break;
		}
	}

	private class SendReview extends AsyncTask<String, Void, String> {

		private Activity activity;
		String rating, review;

		public SendReview(Activity activity, String rating, String review) {
			this.rating = rating;
			this.review = review;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("venueId", venueId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("sportId", sportId));
				params.add(new BasicNameValuePair("rating", rating));
				params.add(new BasicNameValuePair("review", review));
			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";

						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage=e.toString()+response;
					return null;
					// return e.toString();
				}
			} else {
				ErrorMessage=response;
				L.m("Invalid JSON found : " + response);
				// return response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						// Toast.makeText(getActivity(), "Review Sent
						// successfull", Toast.LENGTH_SHORT).show();
						showReviewAlertSuccess(getActivity(), "reviewVenue");
						switchTab(4);
						setScreenDynamically(4);
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							sendReview = new SendReview(getActivity(), String.valueOf(tempRating),
									tempReview);
							sendReview.execute(AppSettings.setVenueRating);
						}
					});
//					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_error_msg),
//							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	public void showReviewAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custorm_find_coach_sendreview, null);
		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendreview_send);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_custorm_find_coach_sendreview_title);
		final EditText etReview = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendreview_review);
		final RatingBar rbRating = (RatingBar) v.findViewById(R.id.rb_custorm_find_coach_sendreview_rating);
		etReview.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayed=true;
				return false;
			}
		});
		tvTitle.setText("Rate Us");
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
//				if(isSoftKeyboardDisplayed){
//					hideKeybord();
//				}
				hideKeyboard(getActivity());
				tempRating=rbRating.getRating();
				tempReview=etReview.getText().toString();
				sendReview = new SendReview(getActivity(), String.valueOf(tempRating),
						tempReview);
				sendReview.execute(AppSettings.setVenueRating);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	private class SendMessage extends AsyncTask<String, Void, String> {

		private Activity activity;
		String message;

		public SendMessage(Activity activity, String message) {
			this.message = message;
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showPDialog(getActivity(), "Processing please wait");
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("venueId", venueId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("message", message));

			}

			HttpReq ob = new HttpReq();
			response = ob.makeConnection(urls[0], 3, params);
			String output = null;
			L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray json = new JSONArray(response);
					for (int i = 0; i < json.length(); i++) {
						JSONObject obj = json.getJSONObject(i);
						if (obj.optString("status").equals("success")) {
							output = "success";

						} else if (obj.optString("status").equals("failure")) {
							output = obj.optString("message");
						}
					}

				} catch (JSONException e) {
					L.m("Json Error :" + e.toString());
					ErrorMessage=e.toString()+response;
					return null;
					// return e.toString();
				}
			} else {
				ErrorMessage=response;
				L.m("Invalid JSON found : " + response);
				// return response;
				return null;
			}

			return output;
			// return "success";

		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				dismissPDialog();
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						Toast.makeText(getActivity(), "Message Sent successfull", Toast.LENGTH_SHORT).show();

					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							sendMessage = new SendMessage(getActivity(), tempReview);
							sendMessage.execute(AppSettings.sendP2VMessage);
						}
					});
//					Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.internet_error_msg),
//							Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	public void showMessageAlert(Activity Act) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custorm_find_coach_sendmessage, null);
		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custorm_find_coach_sendmessage_send);
		final EditText etMessage = (EditText) v.findViewById(R.id.et_custorm_find_coach_sendmessage);
		etMessage.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				isSoftKeyboardDisplayed=true;
				return false;
			}
		});
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
//				if(isSoftKeyboardDisplayed){
//					hideKeybord();
//				}
				hideKeyboard(getActivity());
				tempReview=etMessage.getText().toString();
				sendMessage = new SendMessage(getActivity(), tempReview);
				sendMessage.execute(AppSettings.sendP2VMessage);
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	private class SendFavRequest extends AsyncTask<String, Void, String> {

		private Activity activity;

		public SendFavRequest(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			ivFav.setVisibility(View.GONE);
			pbFav.setVisibility(View.VISIBLE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response = "error";
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				// L.m("Player Id : "+LoadPref(AppTokens.PlayerId));
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("venueId", venueId));

			}
			HttpReq ob = new HttpReq();
			return response = ob.makeConnection(urls[0], HttpReq.POST, params, HttpReq.EXECUTE_TASK);
		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				ivFav.setVisibility(View.VISIBLE);
				pbFav.setVisibility(View.GONE);
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						// Toast.makeText(getActivity(), "Review Sent
						// successfull", Toast.LENGTH_SHORT).show();
						if (favouriteVenue.equalsIgnoreCase("y")) {
							favouriteVenue = "n";
							ivFav.setImageDrawable(getResources().getDrawable(R.drawable.fav_red_48));
						} else {
							favouriteVenue = "y";
							ivFav.setImageDrawable(getResources().getDrawable(R.drawable.fav_48));
						}
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							if(dialog!=null)dialog.dismiss();
							sendFavRequest = new SendFavRequest(getActivity());
							sendFavRequest.execute(AppSettings.setFavouriteVenue);
						}
					});
					//Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
				}
			}
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		SWITCH = 1;
	}
}
