package com.ajsoft.sbuddy.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {
	
	public BaseFragment() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	
	public String LoadPref(String key) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		String data = sharedPreferences.getString(key, "");
		return data;
	}
	
	public void SavePref(String key, String value) {
		
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();

	}
	
	public String getMACAddress(Activity activity) {
		// TODO Auto-generated method stub
		String address;
		WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);

		if(wifiManager.isWifiEnabled()) {
		    // WIFI ALREADY ENABLED. GRAB THE MAC ADDRESS HERE
		    WifiInfo info = wifiManager.getConnectionInfo();
		    address = info.getMacAddress();
		    //Toast.makeText(getBaseContext(),address, Toast.LENGTH_SHORT).show();
	        
		} else {
		    // ENABLE THE WIFI FIRST
		    wifiManager.setWifiEnabled(true);

		    // WIFI IS NOW ENABLED. GRAB THE MAC ADDRESS HERE
		    WifiInfo info = wifiManager.getConnectionInfo();
		    address = info.getMacAddress();
		    //Toast.makeText(getBaseContext(),address, Toast.LENGTH_SHORT).show();
		    
		    // DISABLE THE WIFI
		    wifiManager.setWifiEnabled(false);

		}
		return address;
	}
	
}
