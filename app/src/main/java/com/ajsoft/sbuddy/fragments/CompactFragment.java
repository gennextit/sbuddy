package com.ajsoft.sbuddy.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.ajsoft.sbuddy.ProfileActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.chat.ChatBuddyzList;
import com.ajsoft.sbuddy.fragments.event.MyCreatedEvents;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.SideMenu;
import com.ajsoft.sbuddy.model.SideMenuAdapter;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.L;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class CompactFragment extends Fragment {
	ProgressDialog progressDialog;
	boolean conn = false;
	public AlertDialog dialog = null;
	public static String ErrorMessage = "not available";
	public static int OPEN_TO_ALL = 1, INVITE_ONLY = 2, PAID_ENTRY = 3;

	DrawerLayout dLayout;
	ListView dList;
	List<SideMenu> sideMenuList;
	SideMenuAdapter slideMenuAdapter;

	public CompactFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}


	public void clearAllNotification(Activity activity) {
		NotificationManager notificationManager = (NotificationManager) activity
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancelAll();
	}
	public void clearNotification(Activity activity,int NOTIFICATION_ID) {
		NotificationManager notificationManager = (NotificationManager) activity
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(NOTIFICATION_ID);
	}

	public static void hideKeyboard( Activity activity ) {
	    InputMethodManager imm = (InputMethodManager)activity.getSystemService( Context.INPUT_METHOD_SERVICE );
	    View f = activity.getCurrentFocus();
	    if( null != f && null != f.getWindowToken() && EditText.class.isAssignableFrom( f.getClass() ) )
	        imm.hideSoftInputFromWindow( f.getWindowToken(), 0 );
	    else 
	        activity.getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
	}

	public void hideBaseServerErrorAlertBox() {
		if(dialog!=null)
			dialog.dismiss();
	}


	public Button showBaseServerErrorAlertBox(String errorDetail) {
		return showBaseAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);
	}

	public Button showBaseServerErrorAlertBox() {
		return showBaseAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public Button showBaseAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getActivity().getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Retry");
		
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();
		return button1;
	}

	public void hideKeybord() {
		InputMethodManager inputManager = (InputMethodManager) getActivity()
				.getSystemService(getActivity().INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}

	public String viewTime() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("h:mm a");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return df.format(c.getTime());

	}

	public void showPDialog(Context context, String msg) {
		progressDialog = new ProgressDialog(context);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(msg);
		progressDialog.setIndeterminate(false);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	public void dismissPDialog() {
		if (progressDialog != null)
			progressDialog.dismiss();
	}

//	public String setBoldFont(String title) {
//		return setBoldFont(R.color.gray,Typeface.NORMAL,title);
//	}
//	public String setActionBarTitle(String title) {
//		return setBoldFont(R.color.white,Typeface.BOLD,title);
//	}
//
//	public String setBoldFont(int colorId,int typeFaceStyle, String title) {
//		SpannableString s = new SpannableString(title);
//		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
//		s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		s.setSpan(new ForegroundColorSpan(getResources().getColor(colorId)), 0, s.length(),
//				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		s.setSpan(new StyleSpan(typeFaceStyle), 0, s.length(), 0);
//		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//		return title;
//	}
//	public static String setBoldFont(Context context,int colorId,int typeface, String title) {
//		SpannableString s = new SpannableString(title);
//		Typeface externalFont=Typeface.createFromAsset(context.getAssets(), "fonts/segoeui.ttf");
//		s.setSpan(externalFont, 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		s.setSpan(new ForegroundColorSpan(context.getResources().getColor(colorId)), 0, s.length(),
//				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		s.setSpan(new StyleSpan(typeface), 0, s.length(), 0);
//		s.setSpan(new RelativeSizeSpan(1.1f), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//
//		return title;
//	}
	public void setTypsFace(EditText et,EditText et1,EditText et2,EditText et3,EditText et4) {
		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
		et.setTypeface(externalFont);
		et1.setTypeface(externalFont);
		et2.setTypeface(externalFont);
		et3.setTypeface(externalFont);
		et4.setTypeface(externalFont);
	}
	public Typeface getBoldTypsFace() {
		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeuib.ttf");
		return externalFont;
	}
	public void setTypsFace(AutoCompleteTextView et1,AutoCompleteTextView et2,AutoCompleteTextView et3,AutoCompleteTextView et4) {
		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
		et1.setTypeface(externalFont);
		et2.setTypeface(externalFont);
		et3.setTypeface(externalFont);
		et4.setTypeface(externalFont);
	}
	public void setTypsFace(EditText et) {
		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
		et.setTypeface(externalFont);
	}
	public void setTypsFace(CheckBox et) {
		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
		et.setTypeface(externalFont);
	}
	public void setTypsFace(TextView et) {
		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
		et.setTypeface(externalFont);
	}
	public void setTypsFace(AutoCompleteTextView et) {
		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
		et.setTypeface(externalFont);
	}
	public void setTypsFace(Button et) {
		Typeface externalFont=Typeface.createFromAsset(getActivity().getAssets(), "fonts/segoeui.ttf");
		et.setTypeface(externalFont);
	}

	public String LoadPref(String key) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		String data = sharedPreferences.getString(key, "");
		return data;
	}

	public void SavePref(String key, String value) {

		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();

	}

	public String getDate() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	public String getDateMDY() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	public static String toTitleCase(String givenString) {
		String[] arr = givenString.split(" ");
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < arr.length; i++) {
			sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
		}
		return sb.toString().trim();
	}

	public String getSt(int id) {

		return getResources().getString(id);
	}

	public boolean isOnline() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getActivity()
				.getSystemService(getActivity().CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		if (haveConnectedWifi == true || haveConnectedMobile == true) {
			L.m("Log-Wifi"+ String.valueOf(haveConnectedWifi));
			L.m("Log-Mobile"+ String.valueOf(haveConnectedMobile)); 
			conn = true;
		} else {
			L.m("Log-Wifi"+ String.valueOf(haveConnectedWifi));
			L.m("Log-Mobile"+ String.valueOf(haveConnectedMobile)); 
			conn = false;
		}

		return conn;
	}

	public String getMACAddress(Activity activity) {
		// TODO Auto-generated method stub
		String address;
		WifiManager wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);

		if (wifiManager.isWifiEnabled()) {
			// WIFI ALREADY ENABLED. GRAB THE MAC ADDRESS HERE
			WifiInfo info = wifiManager.getConnectionInfo();
			address = info.getMacAddress();
			// Toast.makeText(getBaseContext(),address,
			// Toast.LENGTH_SHORT).show();

		} else {
			// ENABLE THE WIFI FIRST
			wifiManager.setWifiEnabled(true);

			// WIFI IS NOW ENABLED. GRAB THE MAC ADDRESS HERE
			WifiInfo info = wifiManager.getConnectionInfo();
			address = info.getMacAddress();
			// Toast.makeText(getBaseContext(),address,
			// Toast.LENGTH_SHORT).show();

			// DISABLE THE WIFI
			wifiManager.setWifiEnabled(false);

		}
		return address;
	}

	public void showReviewAlertSuccess(Activity Act, String cat) {
		showReviewAlertSuccess(Act, cat, null, -1);
	}

	public void showReviewAlertSuccess(Activity Act, String cat, String sltSbuddy) {
		showReviewAlertSuccess(Act, cat, sltSbuddy, -1);
	}

	public void showReviewAlertSuccess(Activity Act, String cat, int btnTask) {
		showReviewAlertSuccess(Act, cat, null, btnTask);
	}

	public void showReviewAlertSuccess(final Activity Act, String cat, String sltSbuddy, final int btnTask) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Act);
		final FragmentManager manager = getActivity().getFragmentManager();
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = Act.getLayoutInflater();

		View v = inflater.inflate(R.layout.custom_main_search_coaches_review_rate_submit, null);
		TextView baseTitle = (TextView) v.findViewById(R.id.custom_dialog_baseTitle);
		TextView baseSubTitle = (TextView) v.findViewById(R.id.custom_dialog_baseSubTitle);
		TextView baseText = (TextView) v.findViewById(R.id.custom_dialog_baseText);
		switch (cat) {
		case "reviewCoach":
			baseTitle.setText("Thanks!!");
			baseText.setText("For your reviews ! Please share My profile");
			break;
		case "reviewVenue":
			baseTitle.setText("Thanks!!");
			baseText.setText("For your reviews ! Please share Our profile");
			break;
		case "connect":
			baseTitle.setText("Awesome!!");
			if (sltSbuddy != null)
				baseText.setText("Your link request has been sent to " + sltSbuddy
						+ ". You will be notified once your request is accepted");
			break;
		case "connectApproved":
			baseTitle.setText("Awesome!!");
			if (sltSbuddy != null)
				baseText.setText("Now you are connected to " + sltSbuddy + ".");
			break;
		case "connectRejected":
			baseTitle.setText("Done!!");
			if (sltSbuddy != null)
				baseText.setText("You have rejected connect request from " + sltSbuddy + ".");
			break;
		case "createEvent":
			baseTitle.setText("Hi!!");
			baseSubTitle.setVisibility(View.VISIBLE);
			baseSubTitle.setText("Thanks for registering your event with sbuddy");
			baseText.setText(
					"we will contact you shortly for further verification and authentication \n Kindly check your mail for more details!!");
			break;
		case "createEventInviteOnly":
			baseTitle.setText("Hi!!");
			baseSubTitle.setVisibility(View.VISIBLE);
			baseSubTitle.setText(
					"Thanks for registering your event with sbuddy. This event will be visible to your connected sbuddyz listed in \" My sbuddyz\"");
			baseText.setText(
					"If you wish to publish this event on sbuddy App as open or create event, select suitable option & submit for admin review");
			break;

		case "iaminterested":
			baseTitle.setText("Thanks!!");
			baseText.setText("For your interest in Club Membership. \n The Club shall contact you soon");
			break;
		case "informCoach":
			baseTitle.setText("Thanks!!");
			baseText.setText("Your information has been sent, team sbuddy shall do the needful.");
			break;
		case "informVenue":
			baseTitle.setText("Thanks!!");
			baseText.setText("Your information has been sent, team sbuddy shall do the needful.");
			break;
		case "requestCoachVenueCoin":
			baseTitle.setText("Thanks!!");
			baseText.setText("Yor request for credit has been submitted to admin. We will revert back to you shortly.");
			break;
		case "feedBack":
			baseTitle.setText("Thanks!!");
			baseText.setText("Your feedback sent successfully.");
			break;
		case "forgotPassword":
			baseTitle.setText("Done!!");
			baseText.setText("An sms is being sent to your registered mobile number containing userId and passwrod.");
			break;
		}

		dialogBuilder.setView(v);
		Button sendButton = (Button) v.findViewById(R.id.btn_custom_main_search_coaches_review_rate_goBack);
		// TextView tvTitle = (TextView)
		// v.findViewById(R.id.tv_custorm_find_coach_sendreview_title);
		// final EditText etReview = (EditText)
		// v.findViewById(R.id.et_custorm_find_coach_sendreview_review);
		// final RatingBar rbRating = (RatingBar)
		// v.findViewById(R.id.rb_custorm_find_coach_sendreview_rating);

		// tvTitle.setText("Rate Us");
		switch (btnTask) {
		case 1:
			sendButton.setText("Ok");
			break;
		case 2:
			sendButton.setText("Ok");
			break;
		case 3:
			sendButton.setText("Ok");
			break;
		case 4:
			sendButton.setText("Ok");
			break;
		case 5:
			sendButton.setText("Ok");
			break;
		case 6:
			sendButton.setText("Ok");
			break;

		}
		sendButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				switch (btnTask) {
				case 0:
					manager.popBackStack();
					break;
				case 1:
					Act.finish();
					break;
				case 2:
					manager.popBackStack("informCoach", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					break;
				case 3:
					manager.popBackStack("informVenue", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					break;
				case 4:
					manager.popBackStack("coachRequestCoin", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					break;
				case 5:
					manager.popBackStack("venueRequestCoin", FragmentManager.POP_BACK_STACK_INCLUSIVE);
					break;
				case 6:
					manager.popBackStack();
					break;

				}

			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	// public void showReview_rate_submitToast(Context context){
	// // Inflate the Layout
	// LayoutInflater lInflater = (LayoutInflater)context.getSystemService(
	// Activity.LAYOUT_INFLATER_SERVICE);
	// //LayoutInflater inflater = context.getLayoutInflater();
	//
	// //View layout = lInflater.inflate(R.layout.custom_toast,(ViewGroup)
	// findViewById(R.id.custom_toast_layout_id));
	// View
	// layout=lInflater.inflate(R.layout.custom_main_search_coaches_review_rate_submit,
	// null);
	//// TextView
	// a=(TextView)layout.findViewById(R.id.tv_custom_toast_ViewMessage);
	//// ImageView b=(ImageView)layout.findViewById(R.id.iv_custom_toast);
	//// layout.setBackgroundResource((status) ? R.drawable.toast_bg :
	// R.drawable.toast_bg_red);
	//// b.setImageResource((status) ? R.drawable.success : R.drawable.fail);
	//// a.setText(txt);
	//// a.setTextColor((status) ? getResources().getColor(R.color.icon_green) :
	// getResources().getColor(R.color.icon_red));
	// // Create Custom Toast
	// Toast toast = new Toast(context);
	// toast.setGravity(Gravity.CENTER, 0, 0);
	// toast.setDuration(Toast.LENGTH_SHORT);
	// toast.setView(layout);
	// toast.show();
	// }
	//

	// enter fix Date eg.type 1- dd-MM-yyyy ,type 2- yyyy-MM-dd
	// enter dateFormat eg. dd-MM-yyyy,yyyy-MM-dd,
	public String convertFormateDate(String Date, int type, String dateFormat) {
		String Day, middle, Month, Year;
		String finalDate = Date;
		if (type == 1) {
			Day = Date.substring(0, 2);
			middle = Date.substring(2, 3);
			Month = Date.substring(3, 5);
			Year = Date.substring(6, 10);

		} else {
			Day = Date.substring(0, 4);
			middle = Date.substring(4, 5);
			Month = Date.substring(5, 7);
			Year = Date.substring(8, 10);
		}

		switch (dateFormat) {
		case "dd-MM-yyyy":
			finalDate = Day + middle + Month + middle + Year;
			break;
		case "yyyy-MM-dd":
			finalDate = Year + middle + Month + middle + Day;
			break;
		case "MM-dd-yyyy":
			finalDate = Month + middle + Day + middle + Year;
			break;
		default:
			finalDate = "Date Format Incorrest";
		}
		return finalDate;
	}

	public String convertTime(String time) {
		// String s = "12:18:00";

		String finaltime = "";

		SimpleDateFormat f1 = new SimpleDateFormat("kk:mm");
		Date d = null;
		try {
			d = f1.parse(time);
			SimpleDateFormat f2 = new SimpleDateFormat("h:mm a");
			finaltime = f2.format(d).toUpperCase(); // "12:18am"

		} catch (ParseException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return finaltime;
	}

	public void showInternetAlertBox(Activity activity) {
		showDefaultAlertBox(activity, getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2);
	}

	public void showDefaultAlertBox(Activity activity, String title, String Description) {
		showDefaultAlertBox(activity, title, Description, 1);
	}

	public void showDefaultAlertBox(Activity activity, String title, String Description, int noOfButtons) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = activity.getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);

		button2.setText("Cancel");
		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button1.setVisibility(View.GONE);
			llBtn1.setVisibility(View.GONE);
			button2.setText("Done");
		}
		button1.setText("Setting");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				EnableMobileIntent();
			}
		});
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	public void EnableMobileIntent() {
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(android.provider.Settings.ACTION_SETTINGS);
		startActivity(intent);

	}

	protected void SetDrawer(LinearLayout iv, View view, final ImageLoader imageLoader) {
		// TODO Auto-generated method stub
			dLayout = (DrawerLayout) view.findViewById(R.id.drawer_layout);
		dList = (ListView) view.findViewById(R.id.left_drawer);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View listHeaderView = inflater.inflate(R.layout.side_menu_header, null, false);
		TextView tvName = (TextView) listHeaderView.findViewById(R.id.tv_slide_menu_header_name);
		final ImageView ivProfile = (ImageView) listHeaderView.findViewById(R.id.iv_slide_menu_header_profile);
		tvName.setText(LoadPref(Buddy.Name));
		//tvName.setText(LoadPref(Buddy.Name)+""+ LoadPref(Buddy.PlayerId));
		LinearLayout llSbuddyHeader = (LinearLayout) listHeaderView.findViewById(R.id.ll_slide_menu_header_sbuddy);
		LinearLayout llCoachHeader = (LinearLayout) listHeaderView.findViewById(R.id.ll_slide_menu_header_coach);
		LinearLayout llVenueHeader = (LinearLayout) listHeaderView.findViewById(R.id.ll_slide_menu_header_venue);
		
		iv.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				boolean drawerOpen = dLayout.isDrawerOpen(dList);
				if (!drawerOpen) {
					dLayout.openDrawer(dList);
					Buddy.setBuddyProfileImage(getActivity(), imageLoader, ivProfile);
					
				} else {
					dLayout.closeDrawer(dList);
				}

			}
		});

	
		llSbuddyHeader.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dLayout.closeDrawers();
//				Intent intent = new Intent(getActivity(), ChatActivity.class);
//				startActivity(intent);
				ChatBuddyzList chatBuddyzList = new ChatBuddyzList();
//				chatBuddyzList.setCommunicator(ChatActivity.this);
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.replace(android.R.id.content, chatBuddyzList, "chatBuddyzList");
				transaction.addToBackStack("chatBuddyzList");
				transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.commit();
			}
		});
		llCoachHeader.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dLayout.closeDrawers();
				MyFavCoaches myFavCoaches = new MyFavCoaches();
				FragmentManager mannager = getActivity().getFragmentManager();
				FragmentTransaction transaction = mannager.beginTransaction();
				transaction.replace(android.R.id.content, myFavCoaches, "myFavCoaches");
				transaction.addToBackStack("myFavCoaches");
				transaction.commit();
			}
		});
		llVenueHeader.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				dLayout.closeDrawers();
				MyFavVenue myFavVenue = new MyFavVenue();
				FragmentManager mannager = getActivity().getFragmentManager();
				FragmentTransaction transaction = mannager.beginTransaction();
				transaction.replace(android.R.id.content, myFavVenue, "myFavVenue");
				transaction.addToBackStack("myFavVenue");
				transaction.commit();
			}
		});

		dList.addHeaderView(listHeaderView);

		SideMenu s1 = new SideMenu("My Profile", R.drawable.menu1);
		SideMenu s2 = new SideMenu("My Events", R.drawable.menu2);
		SideMenu s3 = new SideMenu("Refer Coach", R.drawable.menu3);
		SideMenu s4 = new SideMenu("Refer Venue", R.drawable.menu4);
		SideMenu s5 = new SideMenu("Invite Friends", R.drawable.menu5);
		SideMenu s6 = new SideMenu("Rate Us", R.drawable.menu6);
		SideMenu s7 = new SideMenu("User guide/T&C", R.drawable.user_guide);
		SideMenu s8 = new SideMenu("Feedback", R.drawable.home_feedback);

		sideMenuList = new ArrayList<SideMenu>();
		sideMenuList.add(s1);
		sideMenuList.add(s2);
		sideMenuList.add(s3);
		sideMenuList.add(s4);
		sideMenuList.add(s5);
		sideMenuList.add(s6);
		sideMenuList.add(s7);
		sideMenuList.add(s8);

		slideMenuAdapter = new SideMenuAdapter(getActivity(), R.layout.side_menu_list_slot, sideMenuList);
		// adapter = new ArrayAdapter<String>(this, R.layout.listlayout, menu);

		dList.setAdapter(slideMenuAdapter);
		// dList.setSelector(android.R.color.holo_blue_dark);

		dList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {

				dLayout.closeDrawers();

				// FragmentManager mannager = getFragmentManager();
				// FragmentTransaction transaction;

				Intent intent;
				FragmentManager mannager;
				FragmentTransaction transaction;
				switch (position) {

				case 1:
					AppTokens.EDIT_PROFILE_FROM="main";
					ProfileActivity.EDIT_PROFILE = false;
					intent = new Intent(getActivity(), ProfileActivity.class);
					intent.putExtra("from", "main");
					startActivity(intent);
					break;
				case 2:
					MyCreatedEvents myCreatedEvents = new MyCreatedEvents();
					MyCreatedEvents.SWITCH=MyCreatedEvents.SBUDDY;
					mannager = getActivity().getFragmentManager();
					transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, myCreatedEvents, "myCreatedEvents");
					transaction.addToBackStack("myCreatedEvents");
					transaction.commit();
					break;
				case 3:
					InformCoach informCoach = new InformCoach();
					mannager = getActivity().getFragmentManager();
					transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, informCoach, "informCoach");
					transaction.addToBackStack("informCoach");
					transaction.commit();
					break;
				case 4:
					InformVenue informVenue = new InformVenue();
					mannager = getActivity().getFragmentManager();
					transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, informVenue, "informVenue");
					transaction.addToBackStack("informVenue");
					transaction.commit();
					break;
				case 5:
					try {
						Intent i = new Intent(Intent.ACTION_SEND);
						i.setType("text/plain");
						i.putExtra(Intent.EXTRA_SUBJECT, "sbuddy");
						i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_friends_message));
						startActivity(Intent.createChooser(i, "choose one"));
					} catch (Exception e) { // e.toString();
					}
					break;
				case 6:
					try {
						Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
						Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
						startActivity(goToMarket);
					} catch (ActivityNotFoundException e) {
						startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
								"http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
					}
					break;

				case 7:
					UserGuide userGuide = new UserGuide();
					mannager = getActivity().getFragmentManager();
					transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, userGuide, "userGuide");
					transaction.addToBackStack("userGuide");
					transaction.commit();
					
					break;
				case 8:

					MainFeedBack mainFeedBack = new MainFeedBack();
					mannager = getActivity().getFragmentManager();
					transaction = mannager.beginTransaction();
					transaction.replace(android.R.id.content, mainFeedBack, "mainFeedBack");
					transaction.addToBackStack("mainFeedBack");
					transaction.commit();
					break;

				}

			}

		});
	}

}
