package com.ajsoft.sbuddy.fragments;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.util.AppSettings;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class UserGuide extends CompactFragment implements View.OnClickListener {
	FragmentManager mannager;
	LinearLayout llUserGuide, llTandC, llPolicy;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.home_user_guide, container, false);
		mannager = getFragmentManager();
		initUi(v);

		return v;
	}

	private void initUi(View v) {
		setActionBarOption(v);

		llUserGuide = (LinearLayout) v.findViewById(R.id.ll_user_guide_ug);
		llTandC = (LinearLayout) v.findViewById(R.id.ll_user_guide_tc);
		llPolicy = (LinearLayout) v.findViewById(R.id.ll_user_guide_pd);

		llUserGuide.setOnClickListener(this);
		llTandC.setOnClickListener(this);
		llPolicy.setOnClickListener(this);

	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack, ActionHome;
		ActionBack = (LinearLayout) view.findViewById(R.id.ll_actionbar_back);
		ActionHome = (LinearLayout) view.findViewById(R.id.ll_actionbar_home);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack();
			}
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack();
			}
		});
	}

	@Override
	public void onClick(View v) {
		FragmentTransaction transaction;
		AppWebView appWebView;
		switch (v.getId()) {
		case R.id.ll_user_guide_ug:
			appWebView=new AppWebView();
			appWebView.setUrl("User Guide",AppSettings.USER_GUIDE, AppWebView.PDF);
			mannager=getFragmentManager();
			transaction=mannager.beginTransaction();
			transaction.add(android.R.id.content, appWebView, "appWebView");
			transaction.addToBackStack("appWebView");
			transaction.commit();
			break;

		case R.id.ll_user_guide_tc:
			appWebView=new AppWebView();
			appWebView.setUrl(getResources().getString(R.string.t_and_c),AppSettings.USER_GUIDE_TC, AppWebView.URL);
			mannager=getFragmentManager();
			transaction=mannager.beginTransaction();
			transaction.add(android.R.id.content, appWebView, "appWebView");
			transaction.addToBackStack("appWebView");
			transaction.commit();
			break;
		case R.id.ll_user_guide_pd:
			appWebView=new AppWebView();
			appWebView.setUrl("Privacy Policy",AppSettings.USER_GUIDE_POLICY, AppWebView.URL);
			mannager=getFragmentManager();
			transaction=mannager.beginTransaction();
			transaction.add(android.R.id.content, appWebView, "appWebView");
			transaction.addToBackStack("appWebView");
			transaction.commit();
			break;

		}
	}

}
