package com.ajsoft.sbuddy.fragments.venu;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ajsoft.sbuddy.MainActivity;
import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.image_cache.ImageLoader;
import com.ajsoft.sbuddy.model.FindVenueAdapter;
import com.ajsoft.sbuddy.model.FindVenueModel;
import com.ajsoft.sbuddy.util.L;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainFindVenueView extends CompactFragment {
	LinearLayout tabLeft, tabRight;
	ListView lvMain;
	ImageView ivSportBaseImage;

	ArrayList<FindVenueModel> mySportList;
	private String sltSportId, selectSportName;
	private ProgressBar progressBar;
	private static String searchVenueJsonData;
	private String sportBaseImage;

	LeftTabLoadData leftTabLoadData;
	RightTabLoadData rightTabLoadData;
	public ImageLoader imageLoader;
	TextView tvSportCount;
	int SportCounter = 0;
	FragmentManager mannager;
	public static int TAB_RADIUS = 1, TAB_LOCALITY = 2, SWITCH = 1;

	public void setSelectSport(String sltSportId, String selectSportName) {
		this.sltSportId = sltSportId;
		this.selectSportName = selectSportName;

	}

	public void setSearchVenueData(String searchVenueJsonData) {
		this.searchVenueJsonData = searchVenueJsonData;
	}

	public void setSportBaseImage(String sportBaseImage) {
		this.sportBaseImage = sportBaseImage;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (leftTabLoadData != null) {
			leftTabLoadData.onAttach(getActivity());
		}
		if (rightTabLoadData != null) {
			rightTabLoadData.onAttach(getActivity());
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.main_find_venu_view, container, false);
		mannager = getFragmentManager();
		setActionBarOption(view);

		tabLeft = (LinearLayout) view.findViewById(R.id.ll_find_tabLeft);
		tabRight = (LinearLayout) view.findViewById(R.id.ll_find_tabRight);
		lvMain = (ListView) view.findViewById(R.id.lv_find_venu_view);
		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		ivSportBaseImage = (ImageView) view.findViewById(R.id.iv_find_venu_view_baseSport);

		tvSportCount = (TextView) view.findViewById(R.id.tv_find_venu_view_SportCount);
		tabLeft.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				switchTab(TAB_RADIUS);
			}
		});

		lvMain.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
				MainFindVenuDetail mainFindVenuDetail = new MainFindVenuDetail();
				// mainSearchSbuddyz.setCommunicator(AppIntroActivity.this);
				if (mySportList != null) {
					L.m("Latti : " + mySportList.get(position).getLatitude());
					mainFindVenuDetail.setJSONData(searchVenueJsonData);
					mainFindVenuDetail.setData(mySportList.get(position).getFavouriteVenue(),
							mySportList.get(position).getAbout(), mySportList.get(position).getHighlightOrDetails(),
							mySportList.get(position).getOpenHours(), mySportList.get(position).getMembershipDetails(),
							sltSportId, selectSportName);

					mainFindVenuDetail.setVenueProfile(mySportList.get(position).getVenueId(),
							mySportList.get(position).getVenueName(), mySportList.get(position).getImageUrl(),
							mySportList.get(position).getReview(), mySportList.get(position).getRating(),
							mySportList.get(position).getLatitude(), mySportList.get(position).getLongitude());
					mainFindVenuDetail.setVenueAddress(mySportList.get(position).getAddress());

					FragmentManager mannager = getActivity().getFragmentManager();
					FragmentTransaction tr2 = mannager.beginTransaction();
					tr2.replace(android.R.id.content, mainFindVenuDetail, "mainFindVenuDetail");
					tr2.addToBackStack("mainFindVenuDetail");
					tr2.commit();
				}

			}
		});

		tabRight.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				switchTab(TAB_LOCALITY);
			}
		});

		switchTab(SWITCH);
		// leftTabLoadData = new LeftTabLoadData(getActivity());
		// leftTabLoadData.execute();
		// L.m(sportBaseImage);

		imageLoader = new ImageLoader(getActivity());
		if (sportBaseImage != null) {
			imageLoader.DisplayImage(sportBaseImage, ivSportBaseImage, "base_medium", false);
		}
		return view;
	}

	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("VENUES"));
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainFindVenueView", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
			
		});
		ActionHome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				((MainActivity)getActivity()).SwitchTab();
				mannager.popBackStack("mainRSSFeed", 0);
			}
		});
	}

	@SuppressLint("NewApi")
	private void switchTab(int key) {

		switch (key) {
		case 1:
			tabLeft.setBackground(getResources().getDrawable(R.drawable.tab_left_green));
			tabRight.setBackground(getResources().getDrawable(R.drawable.tab_right_grey));
			leftTabLoadData = new LeftTabLoadData(getActivity());
			leftTabLoadData.execute();
			break;

		case 2:
			tabLeft.setBackground(getResources().getDrawable(R.drawable.tab_left_grey));
			tabRight.setBackground(getResources().getDrawable(R.drawable.tab_right_green));
			rightTabLoadData = new RightTabLoadData(getActivity());
			rightTabLoadData.execute();
			break;
		}
	}

	private class LeftTabLoadData extends AsyncTask<Void, Void, Boolean> {

		Activity activity;

		public LeftTabLoadData(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected Boolean doInBackground(Void... urls) {
			// String response = LoadPref(AppTokens.SearchVenueData);
			SportCounter = 0;
			String response = searchVenueJsonData;
			if(response==null){
				return null;
			}
			// L.m(searchVenueJsonData);
			Boolean output = false;
			mySportList = new ArrayList<FindVenueModel>();
			// L.m(response);
			if (response.contains("[")) {
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					JSONArray message = messageObj.getJSONArray("message");
					
					for (int j = 0; j < message.length(); j++) {
						JSONObject messageData = message.getJSONObject(j);
						L.m(messageData.toString());
						if (messageData.optString("radius").equalsIgnoreCase("y")) {
							SportCounter++;
							output = true;
							FindVenueModel model = new FindVenueModel();
							model.setFavouriteVenue(messageData.optString("favouriteVenue"));
							model.setVenueId(messageData.optString("venueId"));
							model.setVenueName(messageData.optString("venueName"));
							model.setImageUrl(messageData.optString("imageUrl"));
							model.setAbout(messageData.optString("about"));
							model.setHighlightOrDetails(messageData.optString("highlightOrDetails"));
							model.setOpenHours(messageData.optString("openHours"));
							model.setMembershipDetails(messageData.optString("membershipDetails"));
							model.setLocality(messageData.optString("locality"));
							model.setKm(Float.parseFloat(messageData.optString("km")));
							model.setReview(messageData.optString("review"));
							model.setRating(messageData.optString("rating"));
							model.setLatitude(messageData.optString("latitude"));
							model.setLongitude(messageData.optString("longitude"));
							model.setAddress(messageData.optString("address"));
							mySportList.add(model);

						}

					}

				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			if (output) {
				// Sort by address.
				Collections.sort(mySportList, FindVenueModel.COMPARE_BY_KM);
			}
			return output;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(Boolean result) {

			// progressBar.setVisibility(View.GONE);
			if (activity != null && result != null) {
				if (result) {
					FindVenueAdapter ExpAdapter = new FindVenueAdapter(getActivity(), R.layout.custom_slot_find_venu,
							mySportList);
					lvMain.setAdapter(ExpAdapter);
					tvSportCount.setVisibility(View.VISIBLE);
					tvSportCount.setText(String.valueOf(SportCounter) + " Venues");
				} else {
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lvMain.setAdapter(adapter);
					mySportList = null;
					tvSportCount.setVisibility(View.GONE);
				}
			}
		}
	}

	private class RightTabLoadData extends AsyncTask<Void, Void, Boolean> {

		Activity activity;

		public RightTabLoadData(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		private void onDetach() {
			this.activity = null;
		}

		@Override
		protected Boolean doInBackground(Void... urls) {
			// String response = LoadPref(AppTokens.SearchVenueData);
			String response = searchVenueJsonData;
			if(response==null){
				return null;
			}
			SportCounter = 0;
			Boolean output = false;
			mySportList = new ArrayList<FindVenueModel>();
			if (response.contains("[")) {
				try {
					JSONArray messageArray = new JSONArray(response);
					JSONObject messageObj = messageArray.getJSONObject(0);
					JSONArray message = messageObj.getJSONArray("message");
					
					for (int j = 0; j < message.length(); j++) {
						JSONObject messageData = message.getJSONObject(j);
						if (messageData.optString("localityAvailable").equalsIgnoreCase("Y")) {
							SportCounter++;
							output = true;
							FindVenueModel model = new FindVenueModel();
							model.setFavouriteVenue(messageData.optString("favouriteVenue"));
							model.setVenueId(messageData.optString("venueId"));
							model.setVenueName(messageData.optString("venueName"));
							model.setImageUrl(messageData.optString("imageUrl"));
							model.setAbout(messageData.optString("about"));
							model.setHighlightOrDetails(messageData.optString("highlightOrDetails"));
							model.setOpenHours(messageData.optString("openHours"));
							model.setMembershipDetails(messageData.optString("membershipDetails"));
							model.setLocality(messageData.optString("locality"));
							model.setKm(null);
							model.setReview(messageData.optString("review"));
							model.setRating(messageData.optString("rating"));
							model.setLatitude(messageData.optString("latitude"));
							model.setLongitude(messageData.optString("longitude"));
							model.setAddress(messageData.optString("address"));
							mySportList.add(model);

						}

					}

				} catch (JSONException e) {
					L.m(e.toString());
					return null;
				}
			} else {
				L.m("Invalid JSON found : " + response);
				return null;
			}

			return output;
			// return "success";

		}

		// onPostExecute displays the results of the AsyncTask.
		@Override
		protected void onPostExecute(Boolean result) {

			// progressBar.setVisibility(View.GONE);
			if (activity != null && result != null) {
				if (result) {
					FindVenueAdapter ExpAdapter = new FindVenueAdapter(getActivity(), R.layout.custom_slot_find_venu,
							mySportList);
					lvMain.setAdapter(ExpAdapter);
					tvSportCount.setVisibility(View.VISIBLE);
					tvSportCount.setText(String.valueOf(SportCounter) + " Venues");
				} else {
					ArrayList<String> errorList = new ArrayList<String>();
					errorList.add("No record available");
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_error_slot,
							R.id.tv_error_text, errorList);
					lvMain.setAdapter(adapter);
					mySportList = null;
					tvSportCount.setVisibility(View.GONE);
				}
			}
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (leftTabLoadData != null) {
			leftTabLoadData.onDetach();
		}
		if (rightTabLoadData != null) {
			rightTabLoadData.onDetach();
		}
	}
}