package com.ajsoft.sbuddy.fragments.event;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.fragments.CompactFragment;
import com.ajsoft.sbuddy.util.L;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MyCreatedEventsViewContact extends CompactFragment implements View.OnClickListener {

	String Website, fbUrl, regFee, regUrl, pPerson, pContactNo, pEmail, sPerson, spContactNo, sEmail;
	TextView tvWebsite, tvfbUrl, tvregFee, tvregUrl, tvpPerson, tvpContactNo, tvpEmail, tvsPerson, tvsContactNo,
			tvsEmail;
	LinearLayout llWebsite, llFbUrl, llRegFee, llRegUrl, llPDetail, llSDetail;

	public void setData(String Website, String fbUrl, String regFee, String regUrl, String pPerson, String pContactNo,
			String pEmail, String sPerson, String spContactNo, String sEmail) {
		this.Website = Website;
		this.fbUrl = fbUrl;
		this.regFee = regFee;
		this.regUrl = regUrl;
		this.pPerson = pPerson;
		this.pContactNo = pContactNo;
		this.pEmail = pEmail;
		this.sPerson = sPerson;
		this.spContactNo = spContactNo;
		this.sEmail = sEmail;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.my_created_events_view_contact, container, false);

		tvWebsite = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_website);
		llWebsite = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_contact_website);
		tvfbUrl = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_fbUrl);
		llFbUrl = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_contact_fbUrl);
		tvregFee = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_regFee);
		llRegFee = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_contact_regFee);
		tvregUrl = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_regUrl);
		llRegUrl = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_contact_regUrl);

		llPDetail = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_contact_primaryDetails);
		llSDetail = (LinearLayout) v.findViewById(R.id.ll_my_created_events_view_contact_secondaryDetails);

		tvpPerson = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_pPerson);
		tvpContactNo = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_pContact);
		tvpEmail = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_pEmail);
		tvsPerson = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_sPerson);
		tvsContactNo = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_sContact);
		tvsEmail = (TextView) v.findViewById(R.id.tv_my_created_events_view_contact_sEmail);

		tvWebsite.setOnClickListener(this);
		tvfbUrl.setOnClickListener(this);
		tvregUrl.setOnClickListener(this);

		if (Website != null) {
			if (Website.equals("")) {
				llWebsite.setVisibility(View.GONE);
			} else {
				tvWebsite.setText(Website);
			}
		}
		if (fbUrl != null) {
			if (fbUrl.equals("")) {
				llFbUrl.setVisibility(View.GONE);
			} else {
				tvfbUrl.setText(fbUrl);
			}
		}
		if (regFee != null) {
			if (regFee.equals("")) {
				llRegFee.setVisibility(View.GONE);
			} else {
				tvregFee.setText(regFee);
			}
		}
		if (regUrl != null) {
			if (regUrl.equals("")) {
				llRegUrl.setVisibility(View.GONE);
			} else {
				tvregUrl.setText(regUrl);
			}
		}
		if (pPerson != null) {
			if (pPerson.equals("")) {
				llPDetail.setVisibility(View.GONE);
			} else {
				tvpPerson.setText(pPerson);
				if (pContactNo != null) {
					tvpContactNo.setText(pContactNo);
				}
				if (pEmail != null) {
					tvpEmail.setText(pEmail);
				}
			}
		}

		if (sPerson != null) {
			if (sPerson.equals("")) {
				llSDetail.setVisibility(View.GONE);
			} else {
				tvsPerson.setText(sPerson);
				if (spContactNo != null) {
					tvsContactNo.setText(spContactNo);
				} else {
					tvsContactNo.setVisibility(View.INVISIBLE);
				}
				if (sEmail != null) {
					tvsEmail.setText(sEmail);
				} else {
					tvsEmail.setVisibility(View.INVISIBLE);
				}
			}
		}

		return v;
	}

	// 09988023172 Anil
	@Override
	public void onClick(View v) {
		Intent i;
		switch (v.getId()) {
		case R.id.tv_my_created_events_view_contact_website:
			i = new Intent(Intent.ACTION_VIEW);
			// URLUtil.isValidUrl(url)
			if (Patterns.WEB_URL.matcher(Website.toLowerCase().trim()).matches()) {
				if (!Website.contains("http://") && !Website.contains("https://")) {
					i.setData(Uri.parse("http://" + Website));
					startActivity(i);
				} else {
					L.m(Website);
					i.setData(Uri.parse(tvWebsite.getText().toString()));
					L.m(String.valueOf(Uri.parse(tvWebsite.getText().toString())));

					startActivity(i);
				}
			} else {
				Toast.makeText(getActivity(), "Invalid url", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.tv_my_created_events_view_contact_fbUrl:
			i = new Intent(Intent.ACTION_VIEW);
			if (Patterns.WEB_URL.matcher(fbUrl.toLowerCase().trim()).matches()) {
				if (!fbUrl.contains("http://") && !fbUrl.contains("https://")) {
					i.setData(Uri.parse("http://" + fbUrl));
					startActivity(i);
				} else {
					i.setData(Uri.parse(tvfbUrl.getText().toString()));
					startActivity(i);
				}
			} else {
				Toast.makeText(getActivity(), "Invalid url", Toast.LENGTH_SHORT).show();
			}

			break;
		case R.id.tv_my_created_events_view_contact_regUrl:
			i = new Intent(Intent.ACTION_VIEW);
			if (Patterns.WEB_URL.matcher(regUrl.toLowerCase().trim()).matches()) {
				if (!regUrl.contains("http://") && !regUrl.contains("https://")) {
					i.setData(Uri.parse("http://" + regUrl));
					startActivity(i);
				} else {
					i.setData(Uri.parse(tvregUrl.getText().toString()));
					startActivity(i);
				}
			} else {
				Toast.makeText(getActivity(), "Invalid url", Toast.LENGTH_SHORT).show();
			}

			break;

		}

	}

}
