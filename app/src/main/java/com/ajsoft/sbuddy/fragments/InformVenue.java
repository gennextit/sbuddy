package com.ajsoft.sbuddy.fragments;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.ajsoft.sbuddy.R;
import com.ajsoft.sbuddy.model.MySportsModel;
import com.ajsoft.sbuddy.model.SpinnerSportsAdapter;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.HttpReq;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;
import com.ajsoft.sbuddy.util.Validation;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class InformVenue extends CompactFragment implements View.OnTouchListener {
	private static final String LOG_TAG = "sBuddy";
	EditText etName,etNumber,etRemarks;
	AutoCompleteTextView acLocation;
	Button btnSubmit;
	InformVenueRequest informVenueRequest;
	ProgressBar progressBar;
	Spinner spSports;  
	private String sltSportId,sltSportName;   
	FragmentManager mannager; 
	private ArrayList<MySportsModel> sportArrayList;
	Boolean isSoftKeyboardDisplayed = false;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (informVenueRequest != null) {
			informVenueRequest.onAttach(activity);
		}
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (informVenueRequest != null) {
			informVenueRequest.onDetach();
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.inform_venue, container, false);
		mannager = getFragmentManager();
		setActionBarOption(v);
		
		etName = (EditText) v.findViewById(R.id.et_inform_venue_name);
		etNumber = (EditText) v.findViewById(R.id.et_inform_venue_phone);
		etRemarks = (EditText) v.findViewById(R.id.et_inform_venue_remarks);
		acLocation = (AutoCompleteTextView) v.findViewById(R.id.ac_inform_venue_location);
		spSports = (Spinner) v.findViewById(R.id.sp_inform_venue_sport);
		btnSubmit = (Button) v.findViewById(R.id.btn_inform_venue_submit);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		
		setTypsFace(etName);
		setTypsFace(etNumber);
		setTypsFace(etRemarks);
		setTypsFace(acLocation); 
		setTypsFace(btnSubmit);
		
		
		acLocation.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.autocomplete_list_item));
		acLocation.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				String str = (String) adapterView.getItemAtPosition(position);

			}
		});
		etName.setOnTouchListener(this);
		etNumber.setOnTouchListener(this);
		etRemarks.setOnTouchListener(this);

		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if(checkValidation()){
					if (isSoftKeyboardDisplayed) {
						hideKeybord();
					}
					informVenueRequest = new InformVenueRequest(getActivity());
					informVenueRequest.execute(AppSettings.informVenue);
				}
			}
		});
		LoadSportsData();
		spSports.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				spSports.setSelection(0);
				return false;
			}
		});
		spSports.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// TODO Auto-generated method stub
				if (sportArrayList != null && spSports.getSelectedItem().toString() != "Select Sport") {

					sltSportId = sportArrayList.get(position).getSportId(); 
					sltSportName = sportArrayList.get(position).getSportName(); 
					// L.m(spSports.getSelectedItem().toString()+" :
					// "+sltSportName);

				} else {
					//sltSportName = spSports.getSelectedItem().toString();
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		return v;
	}
	
	public void setActionBarOption(View view) {
		LinearLayout ActionBack,ActionHome;
		ActionBack=(LinearLayout)view.findViewById(R.id.ll_actionbar_back);
		ActionHome=(LinearLayout)view.findViewById(R.id.ll_actionbar_home); 
//		TextView actionbarTitle=(TextView)view.findViewById(R.id.actionbar_title);
//		actionbarTitle.setText(setActionBarTitle("REFER VENUE"));
		ActionBack.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("informVenue", FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		}); 
		ActionHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mannager.popBackStack("mainRSSFeed", 0);
			}
		}); 
	}
	
	private boolean checkValidation() {
		boolean ret = true;

		if (!Validation.isName(etName, true)) {
			ret = false;
		} else if (!Validation.isEmpty(etNumber,true )) {
			ret = false;
		} else if(spSports.getSelectedItem().toString() == "Select Sport"){
			Toast.makeText(getActivity(), "Please select sport", Toast.LENGTH_SHORT).show();
			return false;
		} else if (!Validation.isEmpty(acLocation, true)) {
			ret = false;
		} else if (!Validation.isEmpty(etRemarks, true)) {
			ret = false;
		} 
		
		return ret;
	}

	public void LoadSportsData() {
		ArrayList<String> result = LoadSports();
		if (result != null) {
			SpinnerSportsAdapter adapter = new SpinnerSportsAdapter(getActivity(), R.layout.spinner_slot);
			adapter.addAll(result);
			adapter.add("Select Sport");
			spSports.setAdapter(adapter);
			spSports.setSelection(adapter.getCount());
		}

	}

	private ArrayList<String> LoadSports() {

		ArrayList<String> sList = new ArrayList<String>();
		sportArrayList = new ArrayList<MySportsModel>();
		String response = Utility.LoadPref(getActivity(), AppTokens.SportList);

		if (response.contains("[")) {
			try {
				JSONArray sportList = new JSONArray(response);

				for (int i = 0; i < sportList.length(); i++) {

					JSONObject obj = sportList.getJSONObject(i);
					if (!obj.optString("status").equals("") && obj.optString("status").equalsIgnoreCase("success")) {
						JSONArray message = obj.getJSONArray("message");
						for (int j = 0; j < message.length(); j++) {
							JSONObject messageData = message.getJSONObject(j);
							MySportsModel model = new MySportsModel();
							model.setSportId(messageData.optString("sportId"));
							model.setSportName(messageData.optString("sportName"));
							model.setLogo(messageData.optString("logo"));
							model.setBaseImage(messageData.optString("baseImage"));
							sList.add(messageData.optString("sportName"));
							sportArrayList.add(model);

						}

					}
				}
			} catch (JSONException e) {
				L.m(e.toString());
				return null;
			}
		} else {
			L.m("Invalid JSON found : " + response);
			return null;
		}


		return sList;
	}

	private class InformVenueRequest extends AsyncTask<String, Void, String> {

		private Activity activity;

		public InformVenueRequest(Activity activity) {
			onAttach(activity);
		}

		public void onAttach(Activity activity) {
			this.activity = activity;
		}

		public void onDetach() {
			this.activity = null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			progressBar.setVisibility(View.VISIBLE);
			btnSubmit.setVisibility(View.GONE);
		}

		@Override
		protected String doInBackground(String... urls) {
			String response;
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			if (activity != null) {
				//sportId,choosePlan, chooseDate, chooseSlot, playerId, venueId
				params.add(new BasicNameValuePair("playerId", LoadPref(Buddy.PlayerId)));
				params.add(new BasicNameValuePair("venueName", etName.getText().toString())); 
				params.add(new BasicNameValuePair("venueNumber", etNumber.getText().toString())); 
				params.add(new BasicNameValuePair("sportName", sltSportName)); 
				params.add(new BasicNameValuePair("location", acLocation.getText().toString())); 
				params.add(new BasicNameValuePair("remarks", etRemarks.getText().toString())); 
				
			}

			HttpReq ob = new HttpReq();
			return response = ob.makeConnection(urls[0], HttpReq.POST, params,HttpReq.EXECUTE_TASK);
			
		}

		@Override
		protected void onPostExecute(String result) {

			if (activity != null) {
				progressBar.setVisibility(View.GONE);
				btnSubmit.setVisibility(View.VISIBLE);
				if (result != null) {
					L.m(result);
					if (result.equalsIgnoreCase("success")) {
						showReviewAlertSuccess(getActivity(),"informVenue",3); 
					} else {
						Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
					}
				} else {
					//Toast.makeText(getActivity(), ErrorMessage, Toast.LENGTH_SHORT).show();
					Button retry=showBaseServerErrorAlertBox(ErrorMessage);
					retry.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View view) {
							hideBaseServerErrorAlertBox();
							if (checkValidation()) {
								if (isSoftKeyboardDisplayed) {
									hideKeybord();
								}
								informVenueRequest = new InformVenueRequest(getActivity());
								informVenueRequest.execute(AppSettings.informVenue);
							}
						}
					});
				}
			}
		}
	}
	public static ArrayList<String> autocomplete(String input, String keyFilter) {
		ArrayList<String> resultList = null;

		HttpURLConnection conn = null;
		StringBuilder jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(
					AppTokens.PLACES_API_BASE + AppTokens.TYPE_AUTOCOMPLETE + AppTokens.OUT_JSON);
			sb.append("?key=" + AppTokens.API_KEY);
			sb.append("&components=country:in");
			sb.append("&input=" + URLEncoder.encode(input, "utf8"));

			URL url = new URL(sb.toString());

			System.out.println("URL: " + url);
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader(conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e(LOG_TAG, "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e(LOG_TAG, "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {

			L.m(jsonResults.toString());
			// Create a JSON object hierarchy from the results
			JSONObject jsonObj = new JSONObject(jsonResults.toString());
			JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				String res = predsJsonArray.getJSONObject(i).getString("description");
				String placeId = predsJsonArray.getJSONObject(i).getString("place_id");
				if (res.contains(toTitleCase(keyFilter))) {
					res = res.substring(0, res.length() - 7);
					System.out.println(res);
					System.out.println("============================================================");
					resultList.add(res);
				}

			}
		} catch (JSONException e) {
			// Log.e(LOG_TAG, "Cannot process JSON results", e);
		}

		return resultList;
	}

	public class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
		private ArrayList<String> resultList;

		public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
			super(context, textViewResourceId);

		}

		@Override
		public int getCount() {
			return resultList.size();
		}

		@Override
		public String getItem(int index) {
			return resultList.get(index);
		}

		@Override
		public Filter getFilter() {
			Filter filter = new Filter() {
				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					FilterResults filterResults = new FilterResults();
					if (constraint != null) {
						// Retrieve the autocomplete results.
						resultList = autocomplete(constraint.toString(), acLocation.getText().toString());

						// Assign the data to the FilterResults
						filterResults.values = resultList;
						filterResults.count = resultList.size();
					}
					return filterResults;
				}

				@Override
				protected void publishResults(CharSequence constraint, FilterResults results) {
					if (results != null && results.count > 0) {
						notifyDataSetChanged();
					} else {
						notifyDataSetInvalidated();
					}
				}
			};
			return filter;
		}
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		isSoftKeyboardDisplayed = true;
		return false;
	}
	
}

