package com.ajsoft.sbuddy;

import java.util.ArrayList;

import com.ajsoft.sbuddy.fragments.VerifyMobile;
import com.ajsoft.sbuddy.fragments.profile.ProfileMyProfile;
import com.ajsoft.sbuddy.fragments.profile.ProfileMySkills;
import com.ajsoft.sbuddy.fragments.profile.ProfileMySkills.OnInitMainListener;
import com.ajsoft.sbuddy.fragments.profile.ProfileMySports;
import com.ajsoft.sbuddy.fragments.profile.ProfileMySports.OnInitSkillListener;
import com.ajsoft.sbuddy.icomm.OnInitProfileListener;
import com.ajsoft.sbuddy.model.MySportsModel;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ProfileActivity extends BaseActivity implements OnInitProfileListener, OnInitSkillListener, OnInitMainListener ,VerifyMobile.OnVerifyMobile{
	FragmentManager mannager;
	int position = 1;
	public static boolean EDIT_PROFILE=true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		L.m("Profile Activity :: on create");
		setContentView(R.layout.activity_profile);
		mannager = getFragmentManager();
		hideActionBar();
		SavePref(AppTokens.APP_USER, "sbuddy");
		setScreenDynamically(position);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		L.m("Profile Activity :: on onSaveInstanceState");
		
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		L.m("Profile Activity :: on onRestoreInstanceState");
		
	}
	
	public void onLogOutClick(View v) {

		Utility.SavePref(ProfileActivity.this,AppTokens.LoginStatus, "Log out");
		Intent intent = new Intent(ProfileActivity.this, SignUp.class);
		startActivity(intent);
		finish();
	}

	

	@Override
	public void onInitProfileListener(int position, String imageUrl, String name, String age, String gender,
			String city, String location, String clubOrApartment, String about) {
		setScreenDynamically(position, imageUrl, name, age, gender, city, location, clubOrApartment, about,null);
		Utility.SavePref(ProfileActivity.this,Buddy.City, city);
	}

	public void setScreenDynamically(int position) {
		setScreenDynamically(position, null, null, null, null, null, null, null, null,null);
	}

	public void setScreenDynamically(int position,ArrayList<MySportsModel> skillList) {
		setScreenDynamically(position, null, null, null, null, null, null, null, null,skillList);
	}

	//

	@Override
	protected void onResume() {
		super.onResume();
		startActivityToSetImage();

	}

	private void startActivityToSetImage() {


		ProfileMyProfile profileMyFrofile = (ProfileMyProfile) getFragmentManager().findFragmentByTag("profileMyFrofile");
		if (profileMyFrofile != null) {
			profileMyFrofile.setCropedImageAndUri(null,ProfileMyProfile.mTempCropImageUri);

		}


	}

	private void setScreenDynamically(int position, String imageUrl, String name, String age, String gender,
									  String city, String location, String clubOrApartment, String about, ArrayList<MySportsModel> skillList) {

		switch (position) {

		case 1: 
			ProfileMyProfile profileMyFrofile = (ProfileMyProfile) mannager.findFragmentByTag("profileMyFrofile");
			if (profileMyFrofile == null) {
				profileMyFrofile = new ProfileMyProfile();
				profileMyFrofile.setCommunicator(ProfileActivity.this);
				profileMyFrofile.setData(EDIT_PROFILE);
				FragmentTransaction transaction = mannager.beginTransaction();
				transaction.add(R.id.group_profile, profileMyFrofile, "profileMyFrofile");
				transaction.addToBackStack("replaceProfile");
				transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				transaction.commit();
			}else{
				profileMyFrofile.setCommunicator(ProfileActivity.this);
			}
			
			break;
		case 2:
//			ProfileMyProfile profileMyFrofile1 = (ProfileMyProfile) mannager.findFragmentByTag("profileMyFrofile");
//			FragmentTransaction ft = mannager.beginTransaction();
//			if (profileMyFrofile1 != null) {
//				ft.remove(profileMyFrofile1);
//				ft.commit();
//			}

			ProfileMySports profileMySports = new ProfileMySports();
			profileMySports.setCommunicator(ProfileActivity.this);
			//profileMySports.setData(EDIT_PROFILE);
			FragmentTransaction transaction2 = mannager.beginTransaction();
			transaction2.add(R.id.group_profile, profileMySports, "profileMySports");
			transaction2.addToBackStack("replaceSports");
			transaction2.commit();

			// profileMySports=(ProfileMySports)
			// mannager.findFragmentByTag("profileMySports");
			// L.m("hello");
			// L.m(imageUrl+ name+ city);
			// if(profileMySports!=null && profileMySports.isVisible()){
			// profileMySports.setData(imageUrl, name, city);
			// }

			break;
		case 3:
//			ProfileMySports screen2 = (ProfileMySports) mannager.findFragmentByTag("profileMySports");
//			FragmentTransaction tr2 = mannager.beginTransaction();
//			if (screen2 != null) {
//				tr2.remove(screen2);
//				tr2.commit();
//			}
			ProfileMySkills profileMySkills = new ProfileMySkills();
			profileMySkills.setCommunicator(ProfileActivity.this);
			//profileMySkills.setData(EDIT_PROFILE);
			FragmentTransaction transaction3 = mannager.beginTransaction();
			transaction3.add(R.id.group_profile, profileMySkills, "profileMySkills");
			transaction3.addToBackStack("replaceSkills");
			transaction3.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			transaction3.commit();
			//profileMySkills.SetList(skillList);
			break;
		// case 4:
		// AppIntro3 FragmentC=(AppIntro3) mannager.findFragmentByTag("C");
		// FragmentTransaction ft3=mannager.beginTransaction();
		// if(FragmentC!=null){
		// ft3.remove(FragmentC);
		// ft3.commit();
		// }
		// AppIntro4 screen4=new AppIntro4();
		// screen4.setCommunicator(ProfileActivity.this);
		// FragmentTransaction transaction4=mannager.beginTransaction();
		// transaction4.add(R.id.group_app_intro,screen4 ,"D");
		// transaction4.commit();
		//
		// break;
		case 4:
			if(AppTokens.EDIT_PROFILE_FROM!=null){
				if(AppTokens.EDIT_PROFILE_FROM.equals("main")){
					finish();
				}else{
					Utility.SavePref(ProfileActivity.this,AppTokens.SessionProfile, "success");
					Intent ob = new Intent(ProfileActivity.this, MainActivity.class);
					//LoadArraytoserver 
					startActivity(ob);
					finish();
				}
			}else{
				finish();
			}
			
			
			break;
		}
	}

	@Override
	public void onInitSkillListener(int screenPosition,ArrayList<MySportsModel> skillList) {
		setScreenDynamically(screenPosition,skillList);
	}

	@Override
	public void onInitMainListener(int screenPosition, ArrayList<MySportsModel> skillList) {
		setScreenDynamically(screenPosition,skillList);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mannager.getBackStackEntryCount() > 1) {
	        L.m("ProfileActivity"+ "popping backstack");
	        mannager.popBackStack();
	    } else {
	        L.m("ProfileActivity"+ "nothing on backstack, calling super");
	        super.onBackPressed();  
	        //finish();
	    }

	}

	@Override
	public void onSuccessVerifyedMobile(Boolean status) {

	ProfileMyProfile profile=(ProfileMyProfile)getFragmentManager().findFragmentByTag("profileMyFrofile");
		if(profile!=null){
			profile.onSaveButtonClick();
		}

	}
}
