package com.ajsoft.sbuddy;

import java.util.Arrays;

import com.ajsoft.sbuddy.coach.CoachRegistration;
import com.ajsoft.sbuddy.fragments.AppWebView;
import com.ajsoft.sbuddy.fragments.LoginCoachVenue;
import com.ajsoft.sbuddy.fragments.LoginCoachVenue.OnLoginCoachVenueListener;
import com.ajsoft.sbuddy.fragments.SignUpMobile;
import com.ajsoft.sbuddy.fragments.SignUpMobile.OnVerifyMobile;
import com.ajsoft.sbuddy.fragments.SignUpMobileVerify;
import com.ajsoft.sbuddy.fragments.SignUpMobileVerify.OnSuccessVerifyedMobile;
import com.ajsoft.sbuddy.util.AppPermission;
import com.ajsoft.sbuddy.util.AppSettings;
import com.ajsoft.sbuddy.util.AppTokens;
import com.ajsoft.sbuddy.util.Buddy;
import com.ajsoft.sbuddy.util.Coach;
import com.ajsoft.sbuddy.util.L;
import com.ajsoft.sbuddy.util.Utility;
import com.ajsoft.sbuddy.util.Venue;
import com.ajsoft.sbuddy.venue.VenueRegistration;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class SignUp extends FragmentActivity
		implements View.OnClickListener, OnVerifyMobile, OnSuccessVerifyedMobile, OnLoginCoachVenueListener {
	public static final String FRAGMENT_TAG = "fragment_1";
	// private FragmentManager mFragmentManager;
	private RelativeLayout llMain;
	private ProgressDialog pdialog;
	private LinearLayout llMobile, llGoogle, llFacebook, llAlreadyMember;
	boolean conn = false;
	public AlertDialog dialog = null;

	private FragmentManager mannager;
	// Facebook integration
	//private LoginButton loginBtn;
	TextView tvHeading;
	Button btnSignUpCoach, btnSignUpVenue;
	// private TextView username;
	/*private UiLifecycleHelper uiHelper;*/
	int TAB_COACH = 1, TAB_VENUE = 2;

	CallbackManager callbackManager;
	//Button share,details;
	LoginButton login;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		FacebookSdk.sdkInitialize(getApplicationContext());
		setContentView(R.layout.activity_signup);
		// hideActionBar();
		// setActionBarOption();
		mannager = getFragmentManager();
		initUi(savedInstanceState);
		setActionBarOption();
		AppTokens.EDIT_PROFILE_FROM="signup";
		// mFragmentManager = getSupportFragmentManager();
		// // Fragment fragment =
		// mFragmentManager.findFragmentByTag(FRAGMENT_TAG);
		// FragmentTransaction transaction =
		// mFragmentManager.beginTransaction();
		// transaction.replace(R.id.container_signup, new MainFragment());
		// transaction.commit();
	}

	private void setActionBarOption() {
		LinearLayout ActionBack;
		ActionBack = (LinearLayout) findViewById(R.id.ll_actionbar_back);
		ActionBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	private void initUi(Bundle savedInstanceState) {
		llMain = (RelativeLayout) findViewById(R.id.ll_signup1);
		llMobile = (LinearLayout) findViewById(R.id.ll_signup_with_mobile);
		llGoogle = (LinearLayout) findViewById(R.id.ll_signup_with_googlePlus);
		llFacebook = (LinearLayout) findViewById(R.id.ll_signup_with_facebook);
		llAlreadyMember = (LinearLayout) findViewById(R.id.ll_signup_alreadyMember);
		tvHeading = (TextView) findViewById(R.id.actionbar_title);
		btnSignUpCoach = (Button) findViewById(R.id.btn_signup_coach);
		btnSignUpVenue = (Button) findViewById(R.id.btn_signup_venue);
		btnSignUpCoach.setText(Utility.setBoldFont(this, R.color.white, Typeface.NORMAL,
				getResources().getString(R.string.sign_up_btn_coach)));
		btnSignUpVenue.setText(Utility.setBoldFont(this, R.color.white, Typeface.NORMAL,
				getResources().getString(R.string.sign_up_btn_venue)));
		tvHeading.setText(Utility.setBoldFont(this, R.color.white, Typeface.BOLD, "SIGN UP"));

		llMobile.setOnClickListener(this);
		llGoogle.setOnClickListener(this);
		llAlreadyMember.setOnClickListener(this);
		btnSignUpCoach.setOnClickListener(this);
		btnSignUpVenue.setOnClickListener(this);

		LinearLayout tearmsAndUse = (LinearLayout) findViewById(R.id.tearmsOfUse);
		tearmsAndUse.setOnClickListener(this);

//		llMobile.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				if (isOnline()) {
//					// llMain.setVisibility(View.GONE);
//					SignUpMobile signUpMobile = new SignUpMobile();
//					signUpMobile.setCommunicator(SignUp.this);
//					FragmentTransaction transaction = mannager.beginTransaction();
//					transaction.add(android.R.id.content, signUpMobile, "signUpMobile");
//					transaction.addToBackStack("signUpMobile");
//					transaction.commit();
//				} else {
//					showInternetAlertBox(SignUp.this);
//				}
//			}
//		});
//		llAlreadyMember.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				if (isOnline()) {
//					// llMain.setVisibility(View.GONE);
//					LoginCoachVenue loginCoachVenue = new LoginCoachVenue();
//					loginCoachVenue.setCommunicator(SignUp.this);
//					FragmentTransaction transaction = mannager.beginTransaction();
//					transaction.add(android.R.id.content, loginCoachVenue, "loginCoachVenue");
//					transaction.addToBackStack("loginCoachVenue");
//					transaction.commit();
//				} else {
//					showInternetAlertBox(SignUp.this);
//				}
//			}
//		});
//
//		llGoogle.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//				if (isOnline()) {
//					Intent ob = new Intent(SignUp.this, SignUpGoogle.class);
//					startActivity(ob);
//					finish();
//				} else {
//					showInternetAlertBox(SignUp.this);
//				}
//			}
//		});

		// llFacebook.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// Intent ob = new Intent(SignUp.this, SignUpFacebook.class);
		// startActivity(ob);
		// // finish();
		// }
		// });

		/*uiHelper = new UiLifecycleHelper(this, statusCallback);
		uiHelper.onCreate(savedInstanceState);*/

		// username = (TextView) findViewById(R.id.username);
		login = (LoginButton) findViewById(R.id.fb_login_button);
		/*loginBtn.setReadPermissions(Arrays.asList("email"));
		loginBtn.setUserInfoChangedCallback(new UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser user) {
				if (user != null) {
					constructWelcomeMessage(user);
					// username.setText("You are currently logged in as " +
					// user.getId());
				} else {
					constructWelcomeMessage(user);
					// username.setText("You are not logged in.");
				}
			}
		});*/

		callbackManager = CallbackManager.Factory.create();

		login.setReadPermissions("public_profile email");




		if(AccessToken.getCurrentAccessToken() != null){
			RequestData();
			/*share.setVisibility(View.VISIBLE);
			details.setVisibility(View.VISIBLE);*/
		}
		login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(AccessToken.getCurrentAccessToken() != null) {
					/*share.setVisibility(View.INVISIBLE);
					details.setVisibility(View.INVISIBLE);*/
				}
			}
		});
		/*share.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				ShareLinkContent content = new ShareLinkContent.Builder().build();
				shareDialog.show(content);

			}
		});*/
		login.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginResult) {

				if(AccessToken.getCurrentAccessToken() != null){
					RequestData();
					/*share.setVisibility(View.VISIBLE);
					details.setVisibility(View.VISIBLE);*/
				}
			}

			@Override
			public void onCancel() {

			}

			@Override
			public void onError(FacebookException exception) {
				showServerErrorAlertBox("Network error " + exception.toString());
			}
		});
	}


	public void RequestData(){
		GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
			@Override
			public void onCompleted(JSONObject object,GraphResponse response) {

				JSONObject json = response.getJSONObject();
				try {
					if(json != null){
						constructWelcomeMessage(json.getString("id"),json.getString("name"),json.getString("email"),json.getString("link"));
						/*String text = "<b>Name :</b> "+json.getString("name")+"<br><br><b>Email :</b> "+json.getString("email")+"<br><br><b>Profile link :</b> "+json.getString("link");
						details_txt.setText(Html.fromHtml(text));
						profile.setProfileId(json.getString("id"));*/
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
		Bundle parameters = new Bundle();
		parameters.putString("fields", "id,name,link,email,picture");
		request.setParameters(parameters);
		request.executeAsync();
	}

	private void constructWelcomeMessage(String id, String name, String email, String link) {
		Utility.SavePref(getApplicationContext(), Buddy.Name, name);
		// ob.SavePref(getActivity(), AppTokens.Image,
		// profile.getProfilePictureUri(500,500).toString());
		Utility.SavePref(getApplicationContext(), Buddy.Image, link);
		Utility.SavePref(getApplicationContext(), Buddy.gmainId, email);
		Utility.SavePref(getApplicationContext(), Buddy.fbId, id);
		Utility.SavePref(getApplicationContext(), AppTokens.LoginStatus, "f");
		Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
		Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
		intent.putExtra("from", "fb");
		startActivity(intent);
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}



	/*private Session.StatusCallback statusCallback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			if (state.isOpened()) {
				Log.d("MainActivity", "Facebook session opened.");
			} else if (state.isClosed()) {
				Log.d("MainActivity", "Facebook session closed.");
				showServerErrorAlertBox("Network error");
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}*/

	/*@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}*/

	/*@Override
	public void onSaveInstanceState(Bundle savedState) {
		super.onSaveInstanceState(savedState);
		uiHelper.onSaveInstanceState(savedState);
	}

	private String constructWelcomeMessage(GraphUser profile) {
		StringBuffer stringBuffer = new StringBuffer();
		if (profile != null) {
			Utility.SavePref(getApplicationContext(), Buddy.Name, profile.getName());
			// ob.SavePref(getActivity(), AppTokens.Image,
			// profile.getProfilePictureUri(500,500).toString());
			Utility.SavePref(getApplicationContext(), Buddy.fbId, profile.getId());
			Utility.SavePref(getApplicationContext(), AppTokens.LoginStatus, "f");
			Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
			Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
			intent.putExtra("from", "fb");
			startActivity(intent);
			finish();

			// if(!profile.getId().equals("")){
			//
			// SavePref(AppTokens.SessionSignup, "success");
			// Intent intent=new Intent(getActivity(),ProfileActivity.class);
			// intent.putExtra("from", "fb");
			// getActivity().startActivity(intent);
			// getActivity().finish();
			// }else{
			// Intent intent = new Intent(getActivity(), SignUpFacebook.class);
			// getActivity().startActivity(intent);
			// getActivity().finish();
			// }

		}
		return stringBuffer.toString();
	}
*/
	@Override
	public void onVerifyMobile(String sportId) {
		// TODO Auto-generated method stub
		llMain.setVisibility(View.GONE);

		SignUpMobile signUpMobile = (SignUpMobile) mannager.findFragmentByTag("signUpMobile");
		FragmentTransaction ft = mannager.beginTransaction();
		if (signUpMobile != null) {
			ft.remove(signUpMobile);
			ft.commit();
		}

		SignUpMobileVerify signUpMobileVerify = new SignUpMobileVerify();
		signUpMobileVerify.setCommunicator(SignUp.this);
		FragmentTransaction transaction = mannager.beginTransaction();
		transaction.add(R.id.group_signup, signUpMobileVerify, "signUpMobileVerify");
		transaction.commit();
	}

	@Override
	public void onSuccessVerifyedMobile(Boolean status) {
		if (status) {
			Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
			Utility.SavePref(getApplicationContext(), AppTokens.LoginStatus, "m");
			Intent intent = new Intent(SignUp.this, ProfileActivity.class);
			intent.putExtra("from", "mobile");
			startActivity(intent);
			finish();
		}
	}

	public String getSt(int id) {

		return getResources().getString(id);
	}

	public boolean isOnline() {
		boolean haveConnectedWifi = false;
		boolean haveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					haveConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					haveConnectedMobile = true;
		}
		if (haveConnectedWifi == true || haveConnectedMobile == true) {
			Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
			Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
			conn = true;
		} else {
			Log.e("Log-Wifi", String.valueOf(haveConnectedWifi));
			Log.e("Log-Mobile", String.valueOf(haveConnectedMobile));
			conn = false;
		}

		return conn;
	}

	public void showInternetAlertBox(Activity activity) {
		showDefaultAlertBox(activity, getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2);
	}

	public void showDefaultAlertBox(Activity activity, String title, String Description, int noOfButtons) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);

		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = activity.getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Setting");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				EnableMobileIntent();
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	public void EnableMobileIntent() {
		Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setAction(android.provider.Settings.ACTION_SETTINGS);
		startActivity(intent);

	}

	@Override
	public void onLoginCoachVenueListener(String CoachJsonData, int CoachVenueStatus) {
		if (CoachJsonData != null) {
			if (CoachVenueStatus == TAB_COACH) {
				Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
				Utility.SavePref(getApplicationContext(), AppTokens.APP_USER, "coach");
				Utility.SavePref(getApplicationContext(), Coach.CoachJsonData, CoachJsonData);
				Intent i = new Intent(SignUp.this, CoachMainActivity.class);
				startActivity(i);
				finish();
			} else {
				Utility.SavePref(getApplicationContext(), AppTokens.SessionSignup, "success");
				Utility.SavePref(getApplicationContext(), AppTokens.APP_USER, "venue");
				Utility.SavePref(getApplicationContext(), Venue.VenueJsonData, CoachJsonData);
				Intent i = new Intent(SignUp.this, VenueMainActivity.class);
				startActivity(i);
				finish();
			}
		}
	}

	@Override
	public void onClick(View v) {
		FragmentTransaction transaction;
		switch (v.getId()) {
			case R.id.ll_signup_with_mobile:

				if (isOnline()) {
					// llMain.setVisibility(View.GONE);
					if(AppPermission.checkStoragePermission(SignUp.this)){
						SignUpMobile signUpMobile = new SignUpMobile();
						signUpMobile.setCommunicator(SignUp.this);
						transaction = mannager.beginTransaction();
						transaction.add(android.R.id.content, signUpMobile, "signUpMobile");
						transaction.addToBackStack("signUpMobile");
						transaction.commit();
					}
				} else {
					showInternetAlertBox(SignUp.this);
				}

				break;
			case R.id.ll_signup_with_googlePlus:
				if (isOnline()) {
					if(AppPermission.checkStoragePermission(SignUp.this)){
						if(AppPermission.checkAccountsPermission(SignUp.this)) {
							Intent ob = new Intent(SignUp.this, SignUpGoogle.class);
							startActivity(ob);
							finish();
						}
					}
				} else {
					showInternetAlertBox(SignUp.this);
				}
				break;
			case R.id.ll_signup_alreadyMember:
				if (isOnline()) {
					// llMain.setVisibility(View.GONE);
					if(AppPermission.checkStoragePermission(SignUp.this)){LoginCoachVenue loginCoachVenue = new LoginCoachVenue();
						loginCoachVenue.setCommunicator(SignUp.this);
						transaction = mannager.beginTransaction();
						transaction.add(android.R.id.content, loginCoachVenue, "loginCoachVenue");
						transaction.addToBackStack("loginCoachVenue");
						transaction.commit();
					}

				} else {
					showInternetAlertBox(SignUp.this);
				}
				break;
			case R.id.btn_signup_coach:
				if(AppPermission.checkStoragePermission(SignUp.this)){
					CoachRegistration coachRegistration = new CoachRegistration();
					transaction = mannager.beginTransaction();
					transaction.add(android.R.id.content, coachRegistration, "coachRegistration");
					transaction.addToBackStack("coachRegistration");
					transaction.commit();
				}
				break;
			case R.id.btn_signup_venue:
				if(AppPermission.checkStoragePermission(SignUp.this)){
					VenueRegistration venueRegistration = new VenueRegistration();
					transaction = mannager.beginTransaction();
					transaction.add(android.R.id.content, venueRegistration, "venueRegistration");
					transaction.addToBackStack("venueRegistration");
					transaction.commit();
				}
				break;
			case R.id.tearmsOfUse:
				if(AppPermission.checkStoragePermission(SignUp.this)){
					AppWebView appWebView=new AppWebView();
					appWebView.setUrl(getResources().getString(R.string.t_and_c),AppSettings.USER_GUIDE_TC, AppWebView.URL);
					mannager=getFragmentManager();
					transaction=mannager.beginTransaction();
					transaction.add(android.R.id.content, appWebView, "appWebView");
					transaction.addToBackStack("appWebView");
					transaction.commit();
				}

				break;
		}

	}

	public void showServerErrorAlertBox(String errorDetail) {
		if(dialog!=null){
			dialog.dismiss();
		}
		showAlertBox(getSt(R.string.server_time_out_tag), getSt(R.string.server_time_out_msg), 2, errorDetail);

	}

	public void showInternetAlertBox() {
		if(dialog!=null){
			dialog.dismiss();
		}
		showAlertBox(getSt(R.string.internet_error_tag), getSt(R.string.internet_error_msg), 2, null);
	}

	public void showAlertBox(String title, String Description, int noOfButtons, final String errorMessage) {

		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUp.this);
		// ...Irrelevant code for customizing the buttons and title
		LayoutInflater inflater = getLayoutInflater();

		View v = inflater.inflate(R.layout.alert_dialog, null);
		dialogBuilder.setView(v);
		ImageView ivAbout = (ImageView) v.findViewById(R.id.iv_alert_dialog_about);
		Button button1 = (Button) v.findViewById(R.id.btn_alert_dialog_button1);
		Button button2 = (Button) v.findViewById(R.id.btn_alert_dialog_button2);
		TextView tvTitle = (TextView) v.findViewById(R.id.tv_alert_dialog_title);
		final TextView tvDescription = (TextView) v.findViewById(R.id.tv_alert_dialog_detail);
		LinearLayout llBtn1 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button1);
		LinearLayout llBtn2 = (LinearLayout) v.findViewById(R.id.ll_alert_dialog_button2);
		ivAbout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (errorMessage != null) {
					tvDescription.setText(errorMessage);
				}
			}
		});

		tvTitle.setText(title);
		tvDescription.setText(Description);
		if (noOfButtons == 1) {
			button2.setVisibility(View.GONE);
			llBtn2.setVisibility(View.GONE);
		}
		button1.setText("Done");
		button1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
				if (isOnline()) {

				} else {
					showInternetAlertBox(SignUp.this);
				}
			}
		});
		button2.setText("Cancel");
		button2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Close dialog
				dialog.dismiss();
			}
		});

		dialog = dialogBuilder.create();
		dialog.show();

	}

	@Override
	public void onBackPressed() {
		if (mannager.getBackStackEntryCount() >0) {
			mannager.popBackStack();
		} else {
			L.m("MainActivity" + "nothing on backstack, calling super");
			super.onBackPressed();
			finish();
		}
	}
}
